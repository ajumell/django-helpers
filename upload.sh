#!/bin/sh

echo "Cleaning up previous build data"
rm -rf dist
rm MANIFEST

python2.7 setup.py sdist upload

echo "Cleaning up current build data"
rm -rf dist
rm MANIFEST
