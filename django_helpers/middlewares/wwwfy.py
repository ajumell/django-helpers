# coding=utf-8
from django.http import HttpResponsePermanentRedirect


class WWWfyMiddleware(object):
    @staticmethod
    def process_request(request):
        domain = request.META['HTTP_HOST']
        if domain.startswith("www"):
            return

        if domain.startswith("127"):
            return

        domain = "www." + domain
        if request.is_secure():
            protocol = "https://"
        else:
            protocol = "http://"

        new_path = protocol + domain + request.get_full_path()
        return HttpResponsePermanentRedirect(new_path)
