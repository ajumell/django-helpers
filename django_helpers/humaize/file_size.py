# coding=utf-8
__author__ = 'ajumell'


def formatted_file_size(num):
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0
