# coding=utf-8
from django.db import models


class BaseTransaction(models.Model):
    order_id = models.CharField(max_length=255, unique=True)
    amount = models.FloatField()

    product_info = models.TextField(blank=True)

    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)

    email = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=15, blank=True)

    address_line_1 = models.CharField(max_length=255, blank=True)
    address_line_2 = models.CharField(max_length=255, blank=True)

    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=255, blank=True)
    country = models.CharField(max_length=255, blank=True)
    post_code = models.CharField(max_length=12, blank=True)

    success_url = models.CharField(max_length=255, blank=True)
    failure_url = models.CharField(max_length=255, blank=True)

    failure_count = models.IntegerField(default=0)
    has_completed = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def add_failure(self):
        self.failure_count += 1
        self.save()

        return self.failure_url

    def complete(self):
        self.has_completed = True
        self.save()

        return self.success_url

    @classmethod
    def from_data(cls, data):
        obj = cls()

        obj.order_id = data.order_id
        obj.amount = data.amount
        obj.product_info = data.product_info

        obj.first_name = data.first_name
        obj.last_name = data.last_name

        obj.email = data.email
        obj.phone = data.phone

        obj.address_line_1 = data.address_line_1
        obj.address_line_2 = data.address_line_2

        obj.city = data.city
        obj.state = data.state
        obj.country = data.country
        obj.post_code = data.post_code

        obj.failure_url = data.failure_url
        obj.success_url = data.success_url

        obj.save()

        return obj
