# coding=utf-8
from django.shortcuts import render
from settings import KEY, URL


def sanitize_url(request, url):
    if url.startswith("http:"):
        return url

    if url.startswith("https:"):
        return url

    return request.build_absolute_uri(url)


def redirect_to_payu(request, transaction, success_url, failure_url):
    """
    :type request: django.core.HttpRequest
    :type transaction: django_helpers.pgs.payu.models.PayUTransaction
    """
    success_url = sanitize_url(request, success_url)
    failure_url = sanitize_url(request, failure_url)

    return render(request, 'payu/redirect.html', {
        'transaction': transaction,
        'HASH': transaction.generate_hash(),
        "KEY": KEY,
        'action': URL,
        "success_url": success_url,
        "failure_url": failure_url
    })
