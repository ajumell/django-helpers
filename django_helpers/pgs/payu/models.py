# coding=utf-8
from django.db import models
from hashlib import sha512
from django_helpers.pgs.payu import settings

KEYS = (
    'key',
    'txnid',
    'amount',
    'productinfo',
    'firstname',
    'email',
    'udf1',
    'udf2',
    'udf3',
    'udf4',
    'udf5',
    'udf6',
    'udf7',
    'udf8',
    'udf9',
    'udf10'
)

MAPPING = {
    'txnid': 'transaction_id',
    'productinfo': 'product_info',
    'firstname': 'first_name',
}


class PayUTransaction(models.Model):
    #
    # Mandatory Fields
    #
    transaction_id = models.CharField(max_length=255, unique=True)
    amount = models.DecimalField(max_digits=19, decimal_places=6)

    # JSON / String info of the product
    product_info = models.TextField(max_length=255)

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    email = models.CharField(max_length=255)
    phone = models.CharField(max_length=15)

    #
    # Optional Fields
    #

    #
    # Response Fields
    #
    # Payment gateway type used in transaction
    payment_gateway_type = models.CharField(max_length=20, null=True, blank=True)  # Map to PG_TYPE

    # Map to added on
    transaction_date_time = models.DateTimeField(null=True, blank=True)

    # mode  (credit card/ CD - Cheque / Net Banking)
    mode = models.CharField(max_length=10, null=True, blank=True)
    status = models.CharField(max_length=15, null=True, blank=True)

    # Unique id from PayU.in
    mihpayid = models.CharField(max_length=100, null=True, blank=True)
    bankcode = models.CharField(max_length=10, null=True, blank=True)

    # Reference number for the payment gateway (received in PG_TYPE)
    bank_ref_num = models.CharField(max_length=100, null=True, blank=True)
    discount = models.DecimalField(max_digits=19, decimal_places=6, default=0)
    additional_charges = models.DecimalField(max_digits=19, decimal_places=6, default=0)  # Charged by Payu

    # Status of transaction in PayU system
    # Map to unmapped status(initiated/ in progress /dropped / bounced / captured / auth/ failed / usercancelled/ pending)
    txn_status_on_payu = models.CharField(max_length=20, null=True, blank=True)
    hash_status = models.CharField(max_length=100, null=True, blank=True)

    def generate_hash(self):
        if not self.pk:
            self.save()

        hash_value = sha512(settings.KEY)
        keys = list(KEYS)
        for key in keys:
            key = MAPPING.get(key, key)
            if key == 'key':
                continue

            value = getattr(self, key, '')
            value = str(value)

            print key, "'{}'".format(value)
            hash_value.update("|")
            hash_value.update(value)

        hash_value.update("%s%s" % ('|', settings.SALT))
        return hash_value.hexdigest().lower()

    def generate_reverse_hash(self, data):
        # Generate hash sequence and verify it with the hash sent by PayU in the Post Response
        reversed_keys = list(reversed(KEYS))
        reversed_keys.insert(0, 'status')
        print reversed_keys
        salt = settings.SALT
        hash_value = sha512("")

        # if the additionalCharges parameter is posted in the transaction response,then hash formula is:
        # sha512(additionalCharges|SALT|status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key)
        # else
        # sha512(SALT|status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key)
        if data.get('additionalCharges'):
            value = str(data.get('additionalCharges'))
            hash_value.update(value)
            hash_value.update("|")

        hash_value.update(salt)
        hash_value.update("|")

        for key in reversed_keys:
            # key = MAPPING.get(key, key)
            if key == 'key':
                continue

            value = data.get(key, '')
            value = str(value)

            print key, "'{}'".format(value)

            hash_value.update("|")
            hash_value.update(value)

        hash_value.update("|")
        hash_value.update(settings.KEY)

        hash_value_str = hash_value.hexdigest().lower()
        return hash_value_str

    def check_hash(self, data):
        hash_value_str = self.generate_reverse_hash(data)
        has_valid_hash = hash_value_str == data.get('hash')

        print hash_value_str
        print data.get('hash')

        # Updating the transaction
        self.payment_gateway_type = data.get('PG_TYPE')
        self.transaction_date_time = data.get('addedon')
        self.mode = data.get('mode')
        self.status = data.get('status')
        self.amount = data.get('amount')
        self.mihpayid = data.get('mihpayid')
        self.bankcode = data.get('bankcode')
        self.bank_ref_num = data.get('bank_ref_num')
        self.discount = data.get('discount')
        self.additional_charges = data.get('additionalCharges', 0)
        self.txn_status_on_payu = data.get('unmappedstatus')
        self.hash_status = "Success" if has_valid_hash else "Failed"
        self.save()

        return has_valid_hash
