# coding=utf-8

from django.conf import settings

ID = getattr(settings, 'PAYU_MERCHANT_ID', None)

KEY = getattr(settings, 'PAYU_MERCHANT_KEY', None)

URL = getattr(settings, 'PAYU_MERCHANT_URL', None)

SALT = getattr(settings, 'PAYU_MERCHANT_SALT', None)
