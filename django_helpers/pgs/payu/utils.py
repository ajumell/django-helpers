# coding=utf-8

from hashlib import sha512
from django.conf import settings

KEYS = (
    'key',
    'txnid',
    'amount',
    'productinfo',
    'firstname',
    'email',
    'udf1',
    'udf2',
    'udf3',
    'udf4',
    'udf5',
    'udf6',
    'udf7',
    'udf8',
    'udf9',
    'udf10'
)


def generate_hash(data):
    hash_obj = sha512('')
    for key in KEYS:
        value = str(data.get(key, ''))
        hash_obj.update("%s%s" % (value, '|'))
    salt = settings.PAYU_INFO.get('merchant_salt')
    hash_obj.update(salt)
    return hash_obj.hexdigest().lower()


def verify_hash(data):
    keys = list(KEYS)
    keys.reverse()
    salt = settings.PAYU_INFO.get('merchant_salt')
    hash_obj = sha512(salt)
    hash_obj.update("%s%s" % ('|', str(data.get('status', ''))))
    for key in keys:
        hash_obj.update("%s%s" % ('|', str(data.get(key, ''))))

    return hash_obj.hexdigest().lower() == data.get('hash')
