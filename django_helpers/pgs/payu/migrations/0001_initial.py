# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PayUTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transaction_id', models.CharField(unique=True, max_length=255)),
                ('amount', models.DecimalField(max_digits=19, decimal_places=6)),
                ('product_info', models.TextField(max_length=255)),
                ('first_name', models.CharField(max_length=255)),
                ('last_name', models.CharField(max_length=255)),
                ('email', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=15)),
                ('payment_gateway_type', models.CharField(max_length=20, null=True, blank=True)),
                ('transaction_date_time', models.DateTimeField(null=True, blank=True)),
                ('mode', models.CharField(max_length=10, null=True, blank=True)),
                ('status', models.CharField(max_length=15, null=True, blank=True)),
                ('mihpayid', models.CharField(max_length=100, null=True, blank=True)),
                ('bankcode', models.CharField(max_length=10, null=True, blank=True)),
                ('bank_ref_num', models.CharField(max_length=100, null=True, blank=True)),
                ('discount', models.DecimalField(default=0, max_digits=19, decimal_places=6)),
                ('additional_charges', models.DecimalField(default=0, max_digits=19, decimal_places=6)),
                ('txn_status_on_payu', models.CharField(max_length=20, null=True, blank=True)),
                ('hash_status', models.CharField(max_length=100, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
