# coding=utf-8

from actions import redirect, validate
from base import PaymentData
from data import get_choices as get_payment_methods
from register import get_name
