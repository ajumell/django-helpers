# coding=utf-8
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt


# 1qitWU3lBB6oCeoCgv4Ssi8GkWvoG2ZJ2JVLAGnA9JbhQmSW9EIJRkO9Y49pYz7NrhbUfWvnmmdGiAXQhtG9CAwCpgShXQtMAG9fHOB3rrehnz7ki0vWfpEeKblUBq/K7kUUtvLK93F/mw1jWVSl7oklac3X5ZQjCriXCI4fS2N7EBqnKtxYxbhZdJISd4/wWrjwddAqJd7QojsF370dt2Pcd/Cmbwdc5uplVJfnB1X+0UO5IaLwriwHJEz7cG+L91Re6VrHVnBycyyTsqfX38kX3joPVJ0Mvv1IKqnNG3Nc2yS6GlCvcea95klqJ3VcEwCaQRoimwYFC+XodcpvvZNr0nxVB1O8le1luqdP068uZcA3X9biu1qNWCXOo4cwdiYsTYsHi3Asz4LPZLVgK5soOzsLAKO0VkJdtEP30r89t4OVTAuWCiPWahojRjAg1FtCqQmrkxPSgG7yygAC4pUpJ+3RxkqrSzh0YKl+rNrJyLeCP64vZ17NxaEcNcmpPJJVUWn+4V0u75FDr64fXWM0MPu9V8BxCPcbGwsGI6ycsx+M/4nK/TLh26elS/RaOiJFf66QsmMKRdxRpcuvPCbddAAxOCz9FqXHaCgZGr5GcJwxYCiL5LNcE/Bff1O4gdY3pClHXItRxSlN0Oqolp18Hj/ImtiHCeh9DPL4UfCLbP/qg1JCHbYuMz4nf4dcVe2DXB3vF8VZ5/bPsmyfbznbLAm3bqa0El6vZ53XZv0nCA6DNEIixpv2FGqLCyU228iP3Elt0zvAyUR6/OO1t6/d7l8FiIQydPQ/5bfgiX1l2XuAjH5OVPKDqDDxkmq8RelPE0PopW5nOt8=
@csrf_exempt
def success(request):
    from gateway import EBSPaymentGateway
    from models import Transaction
    from django.shortcuts import get_object_or_404

    get = request.GET
    dr = get.get('DR')

    gateway = EBSPaymentGateway(request)
    data = gateway.decrypt(dr)

    order_id = data.get('MerchantRefNo')[0]
    response_code = str(data.get('ResponseCode')[0])
    obj = get_object_or_404(Transaction, order_id=order_id)
    if response_code == "0":
        url = obj.complete()
    else:
        url = obj.add_failure()

    return redirect(url)


@csrf_exempt
def failure(request):
    from gateway import EBSPaymentGateway
    from models import Transaction
    from django.shortcuts import get_object_or_404

    get = request.GET
    dr = get.get('DR')

    gateway = EBSPaymentGateway(request)
    data = gateway.decrypt(dr)

    order_id = data.get('MerchantRefNo')[0]
    response_code = str(data.get('ResponseCode')[0])
    obj = get_object_or_404(Transaction, order_id=order_id)

    if response_code == "0":
        url = obj.complete()
    else:
        url = obj.add_failure()

    return redirect(url)
