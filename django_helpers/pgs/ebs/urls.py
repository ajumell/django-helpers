# coding=utf-8
from django.conf.urls import url
from views import success, failure

urlpatterns = [
    url(r'payments/ebs/complete/', success, name='ebs-payment-success'),
    url(r'payments/ebs/failed/', failure, name='ebs-payment-failure'),
]
