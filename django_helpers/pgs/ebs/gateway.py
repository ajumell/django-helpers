# coding=utf-8

from hashlib import md5

from django.conf import settings

from django_helpers.pgs.base import BasePaymentGateway


class EBSPaymentGateway(BasePaymentGateway):
    settings_prefix = "EBS_"

    id = 'ebs'

    name = getattr(settings, 'EBS_NAME', 'EBS Secure Payment Gateway')

    def get_key(self):
        return self.settings("KEY")

    def get_id(self):
        return self.settings("ACCOUNT_ID")

    def get_mode(self):
        if self.is_live():
            return 'LIVE'
        return 'TEST'

    def generate_hash(self, data):
        """
        :param data: The data to be used to get hash
        :rtype data: django_helpers.pgs.base.PaymentData
        :return:
        """

        key = self.get_key()
        acc_id = self.get_id()
        mode = self.get_mode()
        amount = self.sanitize_amount(data.amount)
        order_id = data.order_id
        success_url = self.get_success_url()

        h = md5(key)
        h.update("|")
        h.update(str(acc_id))
        h.update("|")
        h.update(str(amount))
        h.update("|")
        h.update(str(order_id))
        h.update("|")
        h.update(str(success_url))
        h.update("|")
        h.update(str(mode))

        return h.hexdigest()

    def generate_reverse_hash(self, data):
        pass

    def get_data(self, data):
        """

        :param data:
        :return:
        """
        from models import Transaction

        obj = Transaction.from_data(data)

        street = "{}, {}".format(data.address_line_1, data.address_line_2).strip()
        name = "{} {}".format(data.first_name, data.last_name)
        description = data.product_info

        address = '{}, {}, {}'.format(street, data.city, data.state)

        amount = data.amount
        amount = self.sanitize_amount(amount)
        failure_url = self.get_failure_url()
        success_url = self.get_success_url()

        return {
            'account_id': self.get_id(),

            'reference_no': data.order_id,
            'cs1': data.order_id,

            'product_name': description,

            'product_price': amount,
            'amount': amount,

            'f_name': data.first_name,
            's_name': data.last_name,

            'name': name,

            'street': street,
            'city': data.city,
            'state': data.state,
            'zip': data.post_code,
            'country': data.country,

            'phone': data.phone,
            'email': data.email,

            'ship_name': name,
            'ship_address': street,
            'ship_city': data.city,
            'ship_state': data.state,
            'ship_postal_code': data.post_code,
            'ship_country': data.country,
            'ship_phone': data.phone,

            'description': description,
            'postal_code': data.post_code,
            'address': address,

            'decline_url': failure_url,
            'return_url': success_url,
            'return': success_url,

            'secure_hash': self.generate_hash(data),
            'mode': self.get_mode()
        }

    def get_success_url(self):
        return self.reverse('ebs-payment-success') + "?DR={DR}"

    def get_failure_url(self):
        return self.reverse('ebs-payment-failure') + "?DR={DR}"

    def validate(self, data):
        pass

    #
    # URLs
    #
    def get_dev_url(self):
        return "https://secure.ebs.in/pg/ma/sale/pay/"

    def get_live_url(self):
        return "https://secure.ebs.in/pg/ma/sale/pay/"

    #
    # Response
    #
    def decrypt(self, dr):
        from urlparse import parse_qs
        from base64 import decodestring
        from django_helpers.pgs.crypt.rc4 import crypt

        dr = dr.replace(" ", "+")

        encrypted = decodestring(dr)
        decrypted = crypt(encrypted, self.get_key())
        data = parse_qs(decrypted)

        return data
