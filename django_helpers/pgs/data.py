# coding=utf-8
def has_cod():
    from django.conf import settings

    return getattr(settings, 'PG_ENABLE_COD', True)


def get_choices():
    items = getattr(get_choices, 'items', None)

    if items is not None:
        return items

    from register import NAME_REGISTER

    items = NAME_REGISTER.items()
    items = list(items)

    if has_cod():
        items.insert(0, ('cod', 'Cash On Delivery'))

    setattr(get_choices, 'items', items)
    return items
