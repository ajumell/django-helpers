# coding=utf-8
from django.core.exceptions import ValidationError

REGISTER = {}
NAME_REGISTER = {}


def register(payment_id, name, obj):
    if payment_id is not None and name is not None:
        NAME_REGISTER[payment_id] = name
        REGISTER[payment_id] = obj


def get(pg_id, request):
    if pg_id not in REGISTER:
        raise ValidationError("Payment Mode {} does not exist.".format(pg_id))

    cls = REGISTER[pg_id]

    return cls(request)


def get_name(mode):
    if mode == 'cod':
        return 'Cash On Delivery'

    if mode not in REGISTER:
        raise ValidationError("Payment Mode {} does not exist.".format(mode))

    return NAME_REGISTER[mode]
