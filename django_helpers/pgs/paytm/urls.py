# coding=utf-8
from django.conf.urls import url

from views import success

urlpatterns = [
    url(r'payments/(?P<name>.+)/complete/', success, name='paytm-payment-success'),
]
