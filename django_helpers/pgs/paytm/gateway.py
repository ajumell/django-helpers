# coding=utf-8

from django_helpers.pgs.base import BasePaymentGateway


# noinspection PyMethodMayBeStatic
class PayTMPaymentGateway(BasePaymentGateway):
    settings_prefix = "PAYTM_"

    def get_data(self, data):
        """

        :param data:
        :return:
        """
        from models import Transaction
        import checksum

        Transaction.from_data(data)

        amount = data.amount
        amount = self.sanitize_amount(amount)
        success_url = self.get_success_url()

        merchant_key = self.get_key()
        data_dict = {
            'CUST_ID': data.email,
            'EMAIL': data.email,
            'MOBILE_NO': data.phone,

            'MID': self.get_mid(),
            'ORDER_ID': data.order_id,
            'TXN_AMOUNT': amount,
            'INDUSTRY_TYPE_ID': self.get_industry_type(),
            'WEBSITE': self.get_website(),
            'CHANNEL_ID': self.get_channel_id(),
            'CALLBACK_URL': success_url,
        }

        param_dict = data_dict
        param_dict['CHECKSUMHASH'] = checksum.generate_checksum(data_dict, merchant_key)

        return param_dict

    def validate(self, data):
        import checksum

        merchant_key = self.get_key()
        checksum_hash = data.get('CHECKSUMHASH')
        verify = checksum.verify_checksum(data, merchant_key, checksum_hash)

        print verify

        if verify:
            if data['RESPCODE'] == '01':
                print "order successful"
                return True
            else:
                print "order unsuccessful because" + data['RESPMSG']
                return False
        else:
            print "order unsuccessful because" + data['RESPMSG']
            return False

    #
    # URLs
    #
    def get_dev_url(self):
        return "https://pguat.paytm.com/oltp-web/processTransaction"

    def get_live_url(self):
        return "https://secure.paytm.com/oltp-web/processTransaction"

    def get_success_url(self):
        return self.reverse('paytm-payment-success', self.id)

    #
    # Params
    #
    def get_industry_type(self):
        raise NotImplementedError()

    def get_website(self):
        raise NotImplementedError()

    def get_channel_id(self):
        raise NotImplementedError()

    def get_mid(self):
        raise NotImplementedError()

    def get_key(self):
        raise NotImplementedError()
