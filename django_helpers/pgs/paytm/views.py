# coding=utf-8
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

import checksum
import base64
import json


@csrf_exempt
def success(request, name):
    from gateway import PayTMPaymentGateway
    from models import Transaction
    from django.shortcuts import get_object_or_404

    get = request.GET
    post = request.POST

    gateway = PayTMPaymentGateway(request)
