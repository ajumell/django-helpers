# coding=utf-8
from django.core.urlresolvers import reverse
from django.utils import six

__all__ = [
    'BasePaymentGateway',
    'PaymentData'
]


class PaymentGatewayMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        from register import register

        mode = attrs.get('name', None)
        payment_id = attrs.get('id', None)

        obj = type.__new__(mcs, name, bases, attrs)
        register(payment_id, mode, obj)

        return obj


class PaymentData(object):
    order_id = None
    amount = None

    product_info = None

    first_name = None
    last_name = None

    email = None
    phone = None

    address_line_1 = None
    address_line_2 = None

    city = None
    state = None
    country = None
    post_code = None

    success_url = None
    failure_url = None

    def set_name(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def set_street(self, address_line_1, address_line_2):
        self.address_line_1 = address_line_1
        self.address_line_2 = address_line_2

    def set_address(self, city, state, country, post_code):
        self.country = country
        self.city = city
        self.post_code = post_code
        self.state = state

    def process(self):
        if not self.product_info:
            self.product_info = "Order #{}".format(self.order_id)


# noinspection PyMethodMayBeStatic
class BasePaymentGateway(six.with_metaclass(PaymentGatewayMetaClass, object)):
    settings_prefix = ""

    id = None

    name = None

    def __init__(self, request):
        self.request = request

    #
    # Utils
    #
    def sanitize_amount(self, amount):
        str_amount = str(float(amount))
        if str_amount.endswith(".0"):
            str_amount += "0"

        return str_amount

    def settings(self, name, prefix=None, default=None):
        from django.conf import settings

        if prefix is None:
            prefix = self.settings_prefix
            if prefix and not prefix.endswith("_"):
                prefix += "_"

        name = "{}{}".format(prefix, name)
        value = getattr(settings, name, None)
        if value is None:
            value = default

        return value

    def is_live(self):
        is_debug = self.settings('DEBUG', "")
        val = self.settings('PG_LIVE', "", is_debug)

        return val

    #
    # Validate
    #
    def validate(self, data):
        raise NotImplementedError()

    #
    # Data Functions
    #
    def get_data(self, data):
        """
        :param data: The data to be processed.
        :type data: PaymentData
        """
        raise NotImplementedError()

    #
    # URL Functions
    #
    def get_dev_url(self):
        raise NotImplementedError()

    def get_live_url(self):
        raise NotImplementedError()

    def get_url(self):
        return self.get_live_url()

    def get_method(self):
        return 'post'

    #
    # Reverse
    #
    def reverse(self, url_name, *args, **kwargs):
        url = reverse(url_name, args=args, kwargs=kwargs)
        return self.request.build_absolute_uri(url)
