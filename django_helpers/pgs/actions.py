# coding=utf-8
from django.http import HttpResponse, HttpResponseRedirect
from django.template import Context
from django.template import Template

from register import get
from register import get_name

tmpl_str = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Redirecting to {{ name }}</title>
</head>
<body>
<p>Redirecting to {{ name }}</p>
<form action="{{ url }}" method="{{ method | default:"post" }}" name="paymentForm">
    {% for name, value in data.items %}
        <input type="hidden" name="{{ name }}" value="{{ value }}"/>
    {% endfor %}
</form>
<script>
    (function () {
        var paymentForm = document.forms.paymentForm;
        paymentForm.submit();
    }());
</script>
</body>

</html>"""


def redirect(method, data, request):
    """
    :param method: The payment method.
    :param data: The data to be processed.
    :type data: PaymentData
    """

    if method == 'cod':
        return HttpResponseRedirect(data.success_url)

    data.process()
    obj = get(method, request)

    data = {
        'url': obj.get_url(),
        'name': get_name(method),
        'data': obj.get_data(data),
        'method': obj.get_method(),
    }

    tmpl = Template(tmpl_str)
    context = Context(data)
    html = tmpl.render(context)

    return HttpResponse(html)


def validate(method, data):
    raise NotImplementedError()
