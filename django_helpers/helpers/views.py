# coding=utf-8
from json import dumps

from django.contrib.messages import get_messages
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext, loader, Context
from django.template.loader_tags import ExtendsNode


def redirect(to, args=None, kwargs=None):
    try:
        url = reverse(to, args=args, kwargs=kwargs)
    except Exception:
        if args is None and kwargs is None:
            url = to
        else:
            raise
    return HttpResponseRedirect(url)


def change_block_names(template, change_dict):
    """
    This function will rename the blocks in the template from the
    dictionary. The keys in th change dict will be replaced with
    the corresponding values. This will rename the blocks in the
    extended templates only.
    """
    extend_nodes = template.nodelist.get_nodes_by_type(ExtendsNode)
    if len(extend_nodes) == 0:
        return
    extend_node = extend_nodes[0]
    blocks = extend_node.blocks
    for name, new_name in change_dict.items():
        if name in blocks:
            block_node = blocks[name]
            block_node.name = new_name
            blocks[new_name] = block_node
            del blocks[name]


def render_template(request, template, data=None, url_params=None, **kwargs):
    if url_params is not None:
        if data is None:
            data = dict()
        
        for url_param in url_params:
            param_value = kwargs.get(url_param, '')
            if param_value != "":
                data[url_param] = param_value
    return render(request, template, data)


def download_string(contents, file_name):
    import mimetypes
    
    content_type = mimetypes.guess_type(file_name)[0]
    response = HttpResponse(contents, content_type=content_type)
    response['Content-Length'] = len(contents)
    response['Content-Disposition'] = "attachment; filename=%s" % file_name
    return response


def ajax_preprocessor_messages(dictionary, request):
    if request is not None:
        
        if type(dictionary) is not dict:
            dictionary = {"data": dictionary}
        
        messages = get_messages(request)
        messages_arr = []
        
        for message in messages:
            messages_arr.append({
                "message": message.message,
                "tags": message.tags
            })
        
        dictionary["dj_messages"] = messages_arr


def json_redirect(dictionary, url, args=None, kwargs=None):
    try:
        url = reverse(url, args=args, kwargs=kwargs)
    except:
        pass
    
    dictionary["dj_redirect"] = url


def json_refresh_table(dictionary, name):
    key = "dj_refresh_tables"
    if key not in dictionary:
        dictionary[key] = []
    
    arr = dictionary[key]
    if name not in arr:
        arr.append(name)


def render_json(dictionary, request=None):
    if dictionary is None:
        dictionary = {}
    
    ajax_preprocessor_messages(dictionary, request)
    
    string = dumps(dictionary)
    mimetype = 'application/json'
    return HttpResponse(string, content_type=mimetype)


def render_jsonp(dictionary, request=None, callback=None):
    if callback is None:
        callback = request.GET.get("callback")
    
    ajax_preprocessor_messages(dictionary, request)
    string = dumps(dictionary)
    mimetype = 'application/json'
    string = callback + '(' + string + ');'
    return HttpResponse(string, content_type=mimetype)


def render_to_string(template, values=None, request=None):
    template_name = template
    template = loader.get_template(template)

    try:
        html = template.render(values, request)
        return html
    except Exception, e:
        print 'Error', template_name
        print e
        print '*' * 50
        return ''


def is_response(response):
    return isinstance(response, HttpResponse)


def xls_to_response(xls, file_name):
    response = HttpResponse(content_type="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=%s' % file_name
    xls.save(response)
    return response
