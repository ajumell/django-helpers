# coding=utf-8
from django.core.exceptions import ValidationError

__all__ = [
    'Validator',
    'FormValidator',
    'Rule',
    'ComparisonRule',
    'ComparisonRequireRule',
    'ModelValidator',
]


class Rule(object):
    def __init__(self, message=None):
        self.message = message
        self.validator = None

    def get_field(self):
        return None

    def message_args(self):
        return []

    def get_message(self):
        return self.message.format(*self.message_args())

    def add_error(self):
        message = self.get_message()
        name = self.get_field()

        self.validator.add_error(message, name)

    def get_value(self, name=None):
        if name is not None:
            return self.validator.get_value(name)

    def validate(self):
        raise NotImplementedError()


class ComparisonRule(Rule):
    def __init__(self, src, dest, op, message=None):
        Rule.__init__(self, message)
        self.dest = dest
        self.src = src
        self.op = op

    def message_args(self):
        src = self.src
        dest = self.dest

        src_value = self.get_value(src)
        dest_value = self.get_value(dest)

        return [src, dest, src_value, dest_value]

    def get_field(self):
        return self.dest

    def validate(self):
        op = self.op
        src_value = self.get_value(self.src)
        dest_value = self.get_value(self.dest)

        if op == "==":
            return dest_value == src_value

        if op == ">":
            return dest_value > src_value

        if op == "<":
            return dest_value < src_value

        if op == ">=":
            return dest_value >= src_value

        if op == "<=":
            return dest_value <= src_value

        if op == "!=":
            return dest_value != src_value

        return False

    def get_src_value(self):
        return self.get_value(self.src)

    def get_dest_value(self):
        return self.get_value(self.dest)


class ComparisonRequireRule(Rule):
    def __init__(self, src, op, value, dest, message=None):
        Rule.__init__(self, message)
        self.dest = dest
        self.src = src
        self.op = op
        self.value = value

    def message_args(self):
        src = self.src
        dest = self.dest

        src_value = self.get_value(src)
        dest_value = self.get_value(dest)

        return [src, dest, src_value, dest_value, self.value]

    def get_field(self):
        return self.dest

    def validate(self):
        op = self.op
        src_value = self.get_value(self.src)
        value = self.value
        dest_value = self.get_value(self.dest)

        if dest_value == "":
            dest_value = None

        result = False

        if op == "==":
            result = src_value == value

        elif op == ">":
            result = src_value > value

        elif op == "<":
            result = src_value < value

        elif op == ">=":
            result = src_value >= value

        elif op == "<=":
            result = src_value <= value

        elif op == "!=":
            result = src_value != value

        if result is False:
            return True

        if result is True:
            return dest_value is not None

        return False


class Validator(object):
    def __init__(self, instance, is_model=True):
        self.instance = instance

        self.rules = []
        self.errors = {}
        self.is_model = is_model

        self.setup()

    def setup(self):
        pass

    def add(self, rule):
        self.rules.append(rule)
        rule.validator = self

    #
    # Comparison Validator
    #
    def add_comparison_rule(self, dest, op, src, message):
        rule = ComparisonRule(src, dest, op, message)
        self.add(rule)

    def compare_equal(self, src, dest, message):
        self.add_comparison_rule(dest, "==", src, message)

    def compare_not_equal(self, src, dest, message):
        self.add_comparison_rule(dest, "!=", src, message)

    def compare_greater_than(self, src, dest, message):
        self.add_comparison_rule(dest, ">", src, message)

    def compare_less_than(self, src, dest, message):
        self.add_comparison_rule(dest, "<", src, message)

    def compare_greater_than_or_equal(self, src, dest, message):
        self.add_comparison_rule(dest, ">=", src, message)

    def compare_less_than_or_equal(self, src, dest, message):
        self.add_comparison_rule(dest, "<=", src, message)

    #
    # Comparison Required Validator
    #
    def add_comparison_required_rule(self, src, op, value, dest, message):
        rule = ComparisonRequireRule(src, op, value, dest, message)
        self.add(rule)

    def required_if_equal(self, src, value, dest, message):
        self.add_comparison_required_rule(src, "==", value, dest, message)

    def required_if_not_equal(self, src, value, dest, message):
        self.add_comparison_required_rule(src, "!=", value, dest, message)

    def required_if_greater_than(self, src, value, dest, message):
        self.add_comparison_required_rule(src, ">", value, dest, message)

    def required_if_less_than(self, src, value, dest, message):
        self.add_comparison_required_rule(src, "<", value, dest, message)

    def required_if_greater_than_or_equal(self, src, value, dest, message):
        self.add_comparison_required_rule(src, ">=", value, dest, message)

    def required_if_less_than_or_equal(self, src, value, dest, message):
        self.add_comparison_required_rule(src, "<=", value, dest, message)

    #
    # Generic Functions
    #

    def get_value(self, name):
        if self.is_model:
            self.get_value_model(name)
        else:
            self.get_value_form(name)

    def add_error(self, message, name=None):
        if name is None:
            name = "__all__"

        self.errors[name] = message

    def get_error(self, name=None):
        if name is None:
            name = "__all__"

        return self.errors.get(name)

    def raise_error(self):
        errors = self.errors
        if not self.errors:
            return

        raise ValidationError(errors)

    def validate(self):
        self.validate_common()
        if self.is_model:
            self.validate_model()

    def validate_common(self):
        rules = self.rules
        result = True
        for rule in rules:
            rule_result = rule.validate()
            result = result and rule_result

            if rule_result is False:
                rule.add_error()

    #
    # Model
    #
    def get_value_model(self, name):
        value = getattr(self.instance, name, None)
        return value

    def validate_model(self):
        self.validate_common()
        self.raise_error()

    #
    # Form
    #
    def get_value_form(self, name):
        data = self.instance.cleaned_data
        return data.get(name)
