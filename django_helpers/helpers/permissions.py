# coding=utf-8

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_migrate
from django.utils import six

REGISTER = []


def mk_permissions(appname, codename, name):
    ct, created = ContentType.objects.get_or_create(model='', app_label=appname, defaults={'name': appname})
    Permission.objects.get_or_create(codename=codename, content_type__pk=ct.id, defaults={'name': name, 'content_type': ct})
    print 'Custom permission created :', name


class CustomPermissionMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        app_label = attrs.pop("app", 'perms')
        new_attrs = {}
        for name, value in attrs.items():
            if name == '__module__':
                continue

            new_attrs[name] = app_label + "." + name
            REGISTER.append((
                app_label,
                name,
                value
            ))

        obj = type.__new__(mcs, name, bases, new_attrs)
        return obj


class CustomPermission(six.with_metaclass(CustomPermissionMetaClass)):
    pass


# noinspection PyUnusedLocal
def load_data(sender, **kwargs):
    if sender.label == "auth":
        print 'Creating custom permissions'
        for args in REGISTER:
            mk_permissions(*args)


post_migrate.connect(load_data, dispatch_uid="django_helpers.custom_permissions")
