jQuery(function ($) {

    var carousals = $("[data-owl-carousal]");

    carousals.each(function () {
        var $this = $(this);
        var single = $this.data('single') || false;
        var autoSpeed = $this.data('autoSpeed') || 3000;

        $this.owlCarousel({
            autoPlay: autoSpeed,
            stopOnHover: true,
            navigation: true,
            paginationSpeed: 1000,
            goToFirstSpeed: 2000,
            singleItem: !!single,
            autoHeight: true,
            transitionStyle: "fade"
        });
    });

});
