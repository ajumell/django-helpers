# coding=utf-8
import datetime

epoch = datetime.datetime.utcfromtimestamp(0)


def unix_time_millis(dt):
    naive = dt.replace(tzinfo=None)
    seconds = (naive - epoch).total_seconds() * 1000.0
    return long(seconds)


def unix_time_now():
    return unix_time_millis(datetime.datetime.now())


def get_max(queryset, field):
    from django.db.models import Max

    queryset = queryset.filter()
    result = queryset.aggregate(Max(field))

    return result[field + "__max"]


def get_min(queryset, field):
    from django.db.models import Min

    queryset = queryset.filter()
    result = queryset.aggregate(Min(field))

    return result[field + "__min"]


def get_unique_id():
    from django.conf import settings

    prefix = getattr(settings, 'MODEL_SYNC_DEVICE_ID', None)
    if prefix is None:
        from django.core.exceptions import ValidationError
        raise ValidationError("MODEL_SYNC_DEVICE_ID is missing.")

    prefix = str(prefix)
    now = str(unix_time_now())

    value = prefix + now

    return long(value)
