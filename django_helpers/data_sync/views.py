# coding=utf-8
from collections import OrderedDict

from django.utils import six
from rest_framework import status
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer
from rest_framework.settings import api_settings
from rest_framework.viewsets import GenericViewSet

from utils import get_max

__all__ = [
    'SyncViewSet',
    'enable_sync'
]


def find_time_field():
    pass


def find_query_and_model(attrs):
    queryset = attrs.get('queryset')
    model = attrs.get('model')

    if queryset is None and model is None:
        return None

    if queryset is not None:
        model = queryset.model
    else:
        manager = getattr(model, '_default_manager')
        queryset = manager.all()

    return queryset, model


def find_fields(attrs, model):
    fields = attrs.get('fields')
    time_field = attrs.get('time_field')

    meta = getattr(model, '_meta')
    model_fields = meta.fields

    if time_field is None:
        from fields import AutoDateIntField, AutoDateField
        for field in model_fields:
            if isinstance(field, (AutoDateIntField, AutoDateField)):
                time_field = field.name
                break

    if fields is None:
        fields = []
        for field in model_fields:
            if field.name != time_field:
                fields.append(field.name)

    return fields, time_field


def get_serializer(attrs):
    module = attrs.get('__module__')

    data = find_query_and_model(attrs)
    if data is None:
        return
    queryset, model = data

    fields, time_field = find_fields(attrs, model)

    if fields is None or time_field is None:
        return

    model = queryset.model
    model_name = model.__name__

    meta_class = type('Meta', (), {
        "model": model,
        "fields": fields,
        "__module__": module
    })

    return type(model_name + 'SyncSerializer', (ModelSerializer,), {
        "Meta": meta_class,
        "__module__": module
    }), model, queryset, time_field


class ViewMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        new_attrs = {}
        data = get_serializer(attrs)

        if data is None:
            return type.__new__(mcs, name, bases, attrs)

        serializer, model, queryset, time_field = data
        meta = getattr(model, "_meta")

        new_attrs['model'] = model
        new_attrs['serializer'] = serializer
        new_attrs['serializer_class'] = serializer

        new_attrs['queryset'] = queryset
        new_attrs['time_field'] = time_field

        cls = type.__new__(mcs, name, bases, new_attrs)

        from django.conf.urls import url
        from urls import urlpatterns

        app_label = meta.app_label.lower()
        model_name = meta.object_name.lower()

        urls = [
            url(r'^{}/{}/list/$'.format(app_label, model_name), cls.as_view({
                'get': 'list',
            }), name='{}-{}-list'.format(app_label, model_name)),

            url(r'^{}/{}/create/$'.format(app_label, model_name), cls.as_view({
                'post': 'create',
            }), name='{}-{}-create'.format(app_label, model_name)),
        ]

        urlpatterns.extend(urls)

        return cls


# noinspection PyUnusedLocal,PyMethodMayBeStatic
class SyncViewSet(six.with_metaclass(ViewMetaClass, GenericViewSet)):
    model = None
    queryset = None
    time_field = None

    def list(self, request, *args, **kwargs):
        last_time = request.GET.get("time")

        queryset = self.queryset
        if last_time == "" or last_time is None:
            return Response()

        query_params = {
            self.time_field + "__gte": last_time
        }

        queryset = queryset.filter(**query_params)
        queryset = queryset[:20]
        serializer = self.get_serializer(queryset, many=True)
        last_update_time = get_max(queryset, self.time_field)

        response = Response(OrderedDict([
            ('last_updated', last_update_time),
            ('results', serializer.data)
        ]))

        return response

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': data[api_settings.URL_FIELD_NAME]}
        except (TypeError, KeyError):
            return {}


def enable_sync(*fields):
    from django.db import models

    def actual(model):
        model_name = model.__name__
        module = model.__module__
        attrs = {"model": model, "__module__": module}

        if len(fields):
            attrs['fields'] = fields

        type(model_name + 'SyncView', (SyncViewSet,), attrs)
        return model

    if len(fields) == 1:
        _model = fields[0]
        if issubclass(_model, models.Model):
            fields = []
            return actual(_model)

    return actual
