# coding=utf-8


from django.db.models import BigIntegerField

from utils import get_unique_id
from utils import unix_time_millis
from utils import unix_time_now

__all__ = [
    'AutoDateIntField',
    'AutoDateField',
    'AutoIDField'
]


class AutoDateIntField(BigIntegerField):
    def __init__(self, date_field=None, auto_created=False, *args, **kwargs):
        if 'default' not in kwargs:
            kwargs['default'] = 0

        if 'editable' not in kwargs:
            kwargs['editable'] = False

        BigIntegerField.__init__(self, *args, **kwargs)
        
        self.auto_created = auto_created
        self.date_field = date_field

    def pre_save(self, model_instance, add):
        value = self.get_value(add, model_instance)
        setattr(model_instance, self.attname, value)
        return value

    def get_value(self, add, model_instance):
        if add and self.auto_created:
            return unix_time_now()

        date_value = getattr(model_instance, self.date_field)
        if date_value is not None:
            try:
                return unix_time_millis(date_value)
            except ValueError:
                pass

        return 0


class AutoDateField(BigIntegerField):
    def __init__(self, *args, **kwargs):
        if 'default' not in kwargs:
            kwargs['default'] = 0

        if 'editable' not in kwargs:
            kwargs['editable'] = False

        BigIntegerField.__init__(self, *args, **kwargs)

    def pre_save(self, model_instance, add):
        value = get_unique_id()
        setattr(model_instance, self.attname, value)
        return value


class AutoIDField(BigIntegerField):
    def __init__(self, *args, **kwargs):
        if 'primary_key' not in kwargs:
            kwargs['primary_key'] = True

        if 'editable' not in kwargs:
            kwargs['editable'] = False

        BigIntegerField.__init__(self, *args, **kwargs)

    def pre_save(self, model_instance, add):
        if add:
            value = get_unique_id()
            setattr(model_instance, self.name, value)
            return value

        return BigIntegerField.pre_save(self, model_instance, add)
