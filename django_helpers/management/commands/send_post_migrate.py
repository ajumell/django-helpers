# coding=utf-8
from __future__ import unicode_literals

from django.apps import apps
from django.core.management.base import BaseCommand
from django.db import models


def emit_post_migrate_signal(verbosity=1, db='default'):
    # Emit the post_migrate signal for every application.
    for app_config in apps.get_app_configs():
        if app_config.models_module is None:
            continue
        if verbosity >= 2:
            print("Running post-migrate handlers for application %s" % app_config.label)
        models.signals.post_migrate.send(
            sender=app_config,
            app_config=app_config,
            verbosity=verbosity,
            interactive=False,
            using=db)


class Command(BaseCommand):
    args = '<app.model> <field>'
    help = 'Re-generates thumbnails for all instances of the given model, for the given field.'
    
    def handle(self, *args, **options):
        self.args = args
        self.options = options
        
        if len(args) == 0:
            db = 'default'
        else:
            db = args[0]
        emit_post_migrate_signal(db=db)
