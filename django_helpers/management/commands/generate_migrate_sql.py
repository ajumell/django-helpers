# coding=utf-8
from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from django.db import connections
from django.db.migrations.executor import MigrationExecutor


def get_migrations_code(db):
    connection = connections[db]
    executor = MigrationExecutor(connection)
    graph = executor.loader.graph
    
    shown = set()
    for node in graph.leaf_nodes():
        for plan_node in graph.forwards_plan(node):
            if plan_node not in shown:  # and plan_node[0] == app_label:
                print plan_node
                shown.add(plan_node)
            else:
                print 'else', plan_node
    
    migrations = []
    done = set()
    
    for app_label, title in shown:
        migration = executor.loader.get_migration(app_label, title)
        target = (app_label, migration.name)
        if target in done:
            continue
        
        done.add(target)
        plan = [(graph.nodes[target], False)]
        sql_statements = executor.collect_sql(plan)
        new_statements = []
        for sql in sql_statements:
            new_sql = sql
            if new_sql:
                new_statements.append(new_sql)
        
        migrations.append(new_statements)
    
    for migration in migrations:
        for line in migration:
            print line


class Command(BaseCommand):
    args = '<app.model> <field>'
    help = 'Re-generates thumbnails for all instances of the given model, for the given field.'
    
    def handle(self, *args, **options):
        self.args = args
        self.options = options
        
        if len(args) == 0:
            db = 'default'
        else:
            db = args[0]
        get_migrations_code(db)
