# coding=utf-8
from django_helpers.apps.static_manager.utils import get_js_files, get_css_files
from django_helpers.utils.models import get_value


class JavascriptPlugin(object):
    add_data_attrs = True
    need_instance = False
    
    def get_attrs(self, instance=None):
        raise NotImplementedError()
    
    def apply(self, attrs, instance=None):
        """
        :param attrs: The dictionary of attribute to which the attributes has to be copied.
        :type attrs: dict
        """
        if self.need_instance and instance is None:
            raise Exception("Instance is needed.")
        
        data_attrs = self.get_attrs(instance)
        for key, value in data_attrs.items():
            if self.add_data_attrs:
                if not key.startswith('data-'):
                    key = 'data-' + key
                
                if key in attrs:
                    continue
            
            if value is True:
                value = 'true'
            
            if value is None:
                continue
            
            attrs[key] = value
    
    def render(self):
        from django.forms.utils import flatatt
        
        attrs = {}
        self.apply(attrs)
        
        return flatatt(attrs)
    
    #
    # Repr
    #
    def __unicode__(self):
        return self.render()
    
    def __str__(self):
        return self.render()
    
    def __repr__(self):
        return str(self.__class__)
    
    #
    # Static Files
    #
    def js_files(self):
        pass
    
    def css_files(self):
        pass


class GenericJavascriptPlugin(JavascriptPlugin):
    def get_attrs(self, instance=None):
        return self.attrs
    
    def __init__(self):
        self.attrs = {}
    
    # noinspection PyMethodMayBeStatic
    def sanitize_key(self, key):
        hipens = ["_", " ", "(", ")", "[", "]", "{", "}"]
        for h in hipens:
            key = key.replace(h, "-")
        return key
    
    def add(self, key, value=None):
        key = self.sanitize_key(key)
        self.attrs[key] = value
    
    def data(self, key, value=None):
        self.add("data-" + key, value)
    
    def datas(self, dictionary):
        for key, value in dictionary.items():
            self.add("data-" + key, value)


class JavascriptPlugins(object):
    def __init__(self):
        self.plugins = []
    
    def add(self, plugin):
        """
        :type plugin: JavascriptPlugin
        """
        self.can_add(plugin)
        plugins = self.plugins
        plugins.append(plugin)
    
    def can_add(self, new_plugin):
        from django.conf import settings
        if not settings.DEBUG:
            return
        
        for plugin in self.plugins:
            if type(plugin) == type(new_plugin):
                raise Exception("A plugin with same class is already existing.")
    
    def apply(self, dictionary, instance=None):
        plugins = self.plugins
        for plugin in plugins:
            plugin.apply(dictionary, instance)
    
    def render(self, instance=None):
        from django.forms.utils import flatatt
        
        attrs = {}
        self.apply(attrs, instance)
        
        return flatatt(attrs)
    
    def __unicode__(self):
        return self.render()
    
    def __str__(self):
        return self.render()
    
    def __repr__(self):
        return self.render()
    
    def has_plugin(self):
        return len(self.plugins) > 0
    
    def js_files(self):
        js_files = []
        for plugin in self.plugins:
            js_files += get_js_files(plugin)
        
        return js_files
    
    def css_files(self):
        css_files = []
        for plugin in self.plugins:
            css_files += get_css_files(plugin)
        
        return css_files
    
    #
    # Plugins
    #
    def set_ajax(self, processing_text=None):
        if processing_text is None:
            processing_text = 'Loading...'
        
        ajax_obj = AjaxLinkPlugin()
        ajax_obj.loading = processing_text
        
        self.add(ajax_obj)
    
    def set_modal(self, loading_text='Loading...', title=None):
        modal = ModalLinkPlugin()
        modal.title = title
        modal.loading = loading_text
        
        self.add(modal)
    
    # noinspection PyAttributeOutsideInit
    def set_confirm(self, question, title=None, yes_text="Yes", no_text="No", yes_color="success", no_color=""):
        confirm = ConfirmPlugin()
        confirm.need_confirm = True
        
        confirm.question = question
        confirm.title = title
        
        confirm.yes_text = yes_text
        confirm.yes_color = yes_color
        
        confirm.no_text = no_text
        confirm.no_color = no_color
        
        self.add(confirm)


class ConfirmPlugin(JavascriptPlugin):
    question = None
    title = ''
    
    yes_text = 'Yes'
    yes_color = 'success'
    
    no_text = 'No'
    no_color = 'danger'
    
    # For Inheritance Purpose
    need_confirm = True
    
    def get_attrs(self, instance=None):
        if not self.need_confirm:
            return {}
        
        return {
            'confirm-question': self.question,
            'confirm-title': self.title or '',
            'confirm-yes-text': self.yes_text,
            'confirm-no-text': self.no_text,
            'confirm-yes-color': self.yes_color,
            'confirm-no-color': self.no_color
        }
    
    def js_files(self):
        return [
            'django-helpers-core-js'
        ]


class AjaxLinkPlugin(ConfirmPlugin):
    loading = 'Loading...'
    need_confirm = False
    
    def get_attrs(self, instance=None):
        attrs = ConfirmPlugin.get_attrs(self)
        
        attrs['has-ajax'] = 'yes'
        attrs['ajax-processing'] = self.loading
        
        return attrs


class ModalLinkPlugin(ConfirmPlugin):
    loading = 'Loading...'
    need_confirm = False
    is_ajax = True
    content = ''
    title = ''
    
    def get_attrs(self, instance=None):
        attrs = ConfirmPlugin.get_attrs(self)
        
        attrs['show-modal'] = True
        attrs['ajax-modal'] = self.is_ajax
        attrs['modal-title'] = self.title
        attrs['ajax-loading-content'] = self.loading
        
        return attrs


class DropZonePlugin(JavascriptPlugin):
    need_instance = True
    
    def __init__(self, dropzone):
        self.dropzone = dropzone
        self.dropzone.key = ''
    
    def js_files(self):
        return get_js_files(self.dropzone)
    
    def css_files(self):
        return get_css_files(self.dropzone)
    
    def get_attrs(self, instance=None):
        attrs = {}
        dropzone = self.dropzone
        
        fields = []
        if dropzone.url_search_parameters:
            fields.extend(dropzone.url_search_parameters)
        
        kwargs = {}
        for field in fields:
            kwargs[field] = get_value(instance, field)
        
        attrs['data-drop-zone-button'] = 'true'
        attrs['drop-zone-url'] = dropzone.save_url(kwargs)
        attrs['file-field'] = dropzone.file_field
        
        return attrs


class HotKeyPlugin(JavascriptPlugin):
    def __init__(self, keys):
        self.keys = keys
    
    def get_attrs(self, instance=None):
        return {
            'hot-key': self.keys
        }


class TipsyPlugin(GenericJavascriptPlugin):
    def __init__(self, title, is_html=False, position=None):
        GenericJavascriptPlugin.__init__(self)
        self.data('tipsy-js', 'true')
        self.data('tipsy-title', title)
        self.data('is-html', is_html)
        self.data('position', position)
    
    def js_files(self):
        return ['jquery-tipsy-js']
    
    def css_files(self):
        return ['jquery-tipsy-css']
