# coding=utf-8

from django_helpers.js_plugins.core import GenericJavascriptPlugin


class MediumToolBar(object):
    """
    toolbar: {
        /* These are the default options for the toolbar,
           if nothing is passed this is what is used */
        allowMultiParagraphSelection: true,
        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
        diffLeft: 0,
        diffTop: -10,
        firstButtonClass: 'medium-editor-button-first',
        lastButtonClass: 'medium-editor-button-last',
        standardizeSelectionStart: false,
        static: false,
        relativeContainer: null,
        /* options which only apply when static is true */
        align: 'center',
        sticky: false,
        updateOnEmptySelection: false
    }
    """

    def __init__(self):
        self.buttons = []

    def add_button(self, name):
        if name not in self.buttons:
            self.buttons.append(name)

    def add_buttons(self, *names):
        for name in names:
            self.add_button(name)

    def has_buttons(self):
        return len(self.buttons) > 0

    def render(self, attrs):
        """
        :type attrs:GenericJavascriptPlugin
        """
        i = 0
        attrs.add('toolbar-buttons-count', len(self.buttons))
        for button in self.buttons:
            attrs.add('toolbar-button-{}'.format(i), button)
            i += 1


class MediumEditorOptions(GenericJavascriptPlugin):
    def __init__(self):
        GenericJavascriptPlugin.__init__(self)
        self.toolbar = MediumToolBar()
        self.setup()

    def setup(self):
        pass

    def render(self):
        self.toolbar.render(self)
        return GenericJavascriptPlugin.render(self)


class MinimalMediumEditorOptions(MediumEditorOptions):
    def setup(self):
        self.toolbar.add_buttons('bold', 'italic', 'underline')
