jQuery && jQuery(function ($) {
    var sUrl = "https://baasil.io/socketcluster.js";
    var fnRunAJAXPreProcessors = $.dj.fnRunAJAXPreProcessors;
    var fnCallback = function () {
        var oSocket = socketCluster.connect({
            query: {
                serviceKey: '{{ key }}:main'
            },
            channelPrefix: '{{ key }}:main#',
            hostname: 'service.baasil.io',
            port: 443,
            secure: true
        });

        var myChannel = oSocket.subscribe('django-helpers');

        myChannel.watch(function (data) {
            fnRunAJAXPreProcessors(data);
        });

    };
    $.getScript(sUrl, fnCallback);
});