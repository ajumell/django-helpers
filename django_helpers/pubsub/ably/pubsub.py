# coding=utf-8
from django_helpers.pubsub.base import PubSubBase

try:
    from Crypto.Cipher import AES
except ImportError:
    import crypto
    import sys

    sys.modules['Crypto'] = crypto
    from crypto.Cipher import AES

import os
import sys

CUR_DIR = os.path.dirname(__file__)
TEMPLATE_PATH = os.path.join(CUR_DIR, 'template.js')

LIB_PATH = os.path.join(CUR_DIR, '..', '..', '..', 'libraries')
LIB_PATH = os.path.realpath(LIB_PATH)

if LIB_PATH not in sys.path:
    sys.path.append(LIB_PATH)


class AblyPubSub(PubSubBase):
    @classmethod
    def get_key(cls):
        raise NotImplementedError()

    @classmethod
    def iter_js_files(cls):
        contents = cls.render_template(TEMPLATE_PATH, {
            'key': cls.get_key()
        })

        dependencies = ['django-helpers-core-js']

        yield "ably.js", contents, dependencies

    @classmethod
    def iter_css_files(cls):
        pass

    @classmethod
    def publish(cls, name, data):
        from ably import AblyRest

        data = cls.sanitize_data(data)

        client = AblyRest(cls.get_key())
        channel = client.channels.get(name)
        response = channel.publish('event', data)

        print dir(response)
