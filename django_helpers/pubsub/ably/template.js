jQuery && jQuery(function ($) {
    var fnConnected;
    var fnCallback;
    var fnRunAJAXPreProcessors;
    var sUrl;

    sUrl = "http://cdn.ably.io/lib/ably.min.js";

    fnRunAJAXPreProcessors = $.dj.fnRunAJAXPreProcessors;

    fnCallback = function () {
        var ably = new Ably.Realtime('{{ key | safe }}');
        ably.connection.on('connected', function () {
            fnConnected(ably);
        });
    };

    fnConnected = function (ably) {
        var channel = ably.channels.get('django-helpers');
        channel.subscribe(function (message) {
            var jsonData = $.parseJSON(message.data);
            fnRunAJAXPreProcessors(jsonData);
        });
    };

    $.getScript(sUrl, fnCallback);
});