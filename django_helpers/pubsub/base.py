# coding=utf-8
from django_helpers.core.theme import BaseThemeAddon


class PubSubBase(BaseThemeAddon):
    @classmethod
    def sanitize_data(cls, data):
        from json import dumps

        typ = type(data)

        if typ in (str, unicode):
            return data

        if typ in (dict, list, tuple):
            return dumps(data)

        from django_helpers.core.views import JSONResponse

        if typ == JSONResponse:
            return dumps(data.data)

    @classmethod
    def publish(cls, name, data):
        raise NotImplementedError()

    @classmethod
    def publish_django_helpers(cls, data):
        return cls.publish('django-helpers', data)
