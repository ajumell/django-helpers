# coding=utf-8
from django.template.loader import render_to_string

from django_helpers.apps.static_manager import register_js, register_css
from exceptions import *

register_css('pretty-photo-css', 'pretty-photo/css/prettyPhoto.css')
register_js('pretty-photo-js', 'pretty-photo/js/jquery.prettyPhoto.js', dependancies=('jquery',))


class PrettyPhotoItem:
    def __init__(self, thumbnail, full_photo, title=None, description=None):
        self.thumbnail = thumbnail
        self.full_photo = full_photo
        self.title = title
        self.description = description


class PrettyPhoto:
    gallery_id = None
    javascript_template = 'pretty-photo/pretty-photo.js'
    html_template = 'pretty-photo/pretty-photo-bootstrap.html'
    image_style = "rounded"
    has_js = True

    #
    #   API Options
    #

    animation_speed = 'fast'
    slideshow = 5000
    autoplay_slideshow = False
    opacity = 0.80
    show_title = True
    allow_resize = True
    default_width = 500
    default_height = 344
    counter_separator_label = '/'
    theme = 'pp_default'
    horizontal_padding = 20
    hideflash = False
    wmode = 'opaque'
    autoplay = True
    modal = False
    deeplinking = True
    overlay_gallery = True
    keyboard_shortcuts = True

    enable_social_tools = False

    def __init__(self):
        if self.gallery_id is None:
            raise GalleryIDNotFoundException()

        self.items = []

        self.js_files = ('pretty-photo-js',)
        self.css_files = ('pretty-photo-css',)

        self.setup()

    def setup(self):
        pass

    def render(self):
        return render_to_string(self.html_template, {
            'items': self.items,
            'image_style': self.image_style,
            "gallery_id": self.gallery_id
        })

    def render_js(self):
        return render_to_string(self.javascript_template, {
            "animation_speed": self.animation_speed,
            "slideshow": self.slideshow,
            "autoplay_slideshow": self.autoplay_slideshow,
            "opacity": self.opacity,
            "show_title": self.show_title,
            "allow_resize": self.allow_resize,
            "default_width": self.default_width,
            "default_height": self.default_height,
            "counter_separator_label": self.counter_separator_label,
            "theme": self.theme,
            "horizontal_padding": self.horizontal_padding,
            "hideflash": self.hideflash,
            "wmode": self.wmode,
            "autoplay": self.autoplay,
            "modal": self.modal,
            "deeplinking": self.deeplinking,
            "overlay_gallery": self.overlay_gallery,
            "keyboard_shortcuts": self.keyboard_shortcuts,

            "gallery_id": self.gallery_id
        })

    def add_item(self, thumbnail, full_photo, title=None, description=None):
        item = PrettyPhotoItem(thumbnail, full_photo, title, description)
        self.items.append(item)
        return item

    def __unicode__(self):
        return self.render()

    def __str__(self):
        return self.render()

    def __repr__(self):
        return self.render()
