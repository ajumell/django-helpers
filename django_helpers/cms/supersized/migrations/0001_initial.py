# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

import django_helpers.apps.forms.fields.thumbnail


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SliderImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', django_helpers.apps.forms.fields.thumbnail.ThumbnailImageField(upload_to=b'supersized')),
                ('caption', models.CharField(max_length=255, blank=True)),
                ('link', models.URLField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SuperSizedSlide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', django_helpers.apps.forms.fields.thumbnail.ThumbnailImageField(upload_to=b'supersized')),
                ('caption', models.CharField(max_length=255, blank=True)),
                ('link', models.URLField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SuperSizedSlider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=75)),
                ('autoplay', models.BooleanField(default=True)),
                ('fit_always', models.BooleanField(default=False)),
                ('fit_landscape', models.BooleanField(default=False)),
                ('fit_portrait', models.BooleanField(default=True)),
                ('horizontal_center', models.BooleanField(default=False)),
                ('image_protect', models.BooleanField(default=True)),
                ('keyboard_nav', models.BooleanField(default=True)),
                ('min_height', models.IntegerField(default=0)),
                ('min_width', models.IntegerField(default=0)),
                ('new_window', models.BooleanField(default=True)),
                ('pause_hover', models.BooleanField(default=False)),
                ('performance', models.IntegerField(default=1)),
                ('random', models.BooleanField(default=False)),
                ('slideshow', models.BooleanField(default=True)),
                ('slide_interval', models.IntegerField(default=5000)),
                ('slide_links', models.BooleanField(default=False)),
                ('start_slide', models.IntegerField(default=1)),
                ('stop_loop', models.BooleanField(default=False)),
                ('thumb_links', models.BooleanField(default=True)),
                ('thumbnail_navigation', models.BooleanField(default=True)),
                ('transition', models.CharField(default=b'fade', max_length=25,
                                                choices=[(b'carouselLeft', b'carouselLeft'), (b'carouselRight', b'carouselRight'), (b'fade', b'fade'), (b'none', b'none'),
                                                         (b'slideBottom', b'slideBottom'), (b'slideLeft', b'slideLeft'), (b'slideRight', b'slideRight'),
                                                         (b'slideTop', b'slideTop')])),
                ('transition_speed', models.IntegerField(default=750)),
                ('vertical_center', models.BooleanField(default=True)),
                ('progress_bar', models.BooleanField(default=True)),
                ('mouse_scrub', models.BooleanField(default=True)),
                ('show_thumbnails', models.BooleanField(default=False)),
                ('hide_controls', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='supersizedslide',
            name='slider',
            field=models.ForeignKey(to='supersized.SuperSizedSlider'),
            preserve_default=True,
        ),
    ]
