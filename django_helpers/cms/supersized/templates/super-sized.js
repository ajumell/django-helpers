jQuery(function ($) {
    $.supersized.themeVars.image_path = '{{ STATIC_URL }}super-sized/img/';
    $.supersized({

        // Functionality
        slideshow :{{ slideshow }},
        autoplay :{{ autoplay }},
        start_slide :{{ start_slide }},
        stop_loop :{{ stop_loop }},
        random :{{ random }},
        slide_interval :{{ slide_interval }},
        transition : '{{ transition }}',
        transition_speed :{{ transition_speed }},
        new_window :{{ new_window }},
        pause_hover :{{ pause_hover }},
        keyboard_nav :{{ keyboard_nav }},
        performance :{{ performance }},
        image_protect :{{ image_protect }},
        min_width :{{ min_width }},
        min_height :{{ min_height }},
        vertical_center :{{ vertical_center }},
        horizontal_center :{{ horizontal_center }},
        fit_always :{{ fit_always }},
        fit_portrait :{{ fit_portrait }},
        fit_landscape :{{ fit_landscape }},

        // Components
        slide_links :{{ slide_links }},
        thumb_links :{{ thumb_links }},
        thumbnail_navigation :{{ thumbnail_navigation }},
        slides : [
            {% for slide in slides %}
            {
                image : '{{ slide.image }}',
                title : '{{ slide.title }}',
                thumb : '{{ slide.thumb }}',
                url : '{{ slide.url }}'},
            {% endfor %}
        ],

        // Theme Options
        progress_bar :{{ progress_bar }}, // Timer for each slide
        mouse_scrub :{{ mouse_scrub }}

    });
    {% if show_thumbnails %}
    setTimeout(function () {
        {% if hide_controls %}
        $("#thumb-tray").stop().animate({bottom : 0});
        {% else %}
        $("#tray-button").click();
        {% endif %}
    }, 500);
    {% endif %}
});
