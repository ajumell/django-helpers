# coding=utf-8
from django import forms

from models import *


class AddSliderImageForm(forms.ModelForm):
    class Meta:
        model = SliderImage
        fields = (
            'image',
            'caption',
            'link',
        )


class EditSliderImageForm(forms.ModelForm):
    class Meta:
        model = SliderImage
        fields = (
            'caption',
            'link',
        )
