# coding=utf-8
from django_helpers.apps.data_tables import DataTable
from django_helpers.apps.data_tables.columns import DataTableColumn, BootstrapButtonSetColumn
from django_helpers.apps.data_tables.columns import ImageField
from models import *


class SliderImageDataTable(DataTable):
    table_id = "slider-image-data-table"

    def get_columns(self, user):
        from views import SliderImageCRUDView as View

        columns = []

        image_column = ImageField("image", "Image")
        columns.append(image_column)

        caption_column = DataTableColumn("caption", "Caption", True, True)
        columns.append(caption_column)
        caption_column.make_editable()

        link_column = DataTableColumn("link", "Link", True, True)
        columns.append(link_column)
        link_column.make_editable()

        actions = BootstrapButtonSetColumn()
        actions.add_button(View.listable.edit_button(user))
        actions.add_button(View.listable.delete_button(user))

        if actions.has_buttons():
            columns.append(actions)
        return columns

    def get_query(self, request, kwargs=None):
        return SliderImage.objects.all()
