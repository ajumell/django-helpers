# coding=utf-8
from django_helpers import create_attr_from_obj
from django_helpers.helpers.views import render_to_string


class SuperSizedTransitions(object):
    none = 'none'
    fade = 'fade'
    slideTop = 'slideTop'
    slideRight = 'slideRight'
    slideLeft = 'slideLeft'
    slideBottom = 'slideBottom'
    carouselRight = 'carouselRight'
    carouselLeft = 'carouselLeft'


TransitionChoices = []

for item in dir(SuperSizedTransitions):
    if not item.startswith('__'):
        val = getattr(SuperSizedTransitions, item)
        if TransitionChoices.count(val) == 0:
            TransitionChoices.append((item, val))


class SuperSized(object):
    has_js = True
    autoplay = True
    fit_always = False
    fit_landscape = False
    fit_portrait = True
    horizontal_center = False
    image_protect = True
    keyboard_nav = True
    min_height = 0
    min_width = 0
    new_window = True
    pause_hover = False
    performance = 1
    random = False
    slideshow = True
    slide_interval = 5000
    slide_links = False
    start_slide = 1
    stop_loop = False
    thumb_links = True
    thumbnail_navigation = True
    transition = SuperSizedTransitions.fade
    transition_speed = 750
    vertical_center = True
    progress_bar = True
    mouse_scrub = True

    show_thumbnails = False
    hide_controls = False

    def __init__(self):
        self.js_files = ('super-sized-shutter-js',)
        self.css_files = ('super-sized-shutter-css', 'super-sized-css')
        self._slides = []

        self.setup()

    def setup(self):
        pass

    def add_slide(self, image, title='', thumb='', link=''):
        if not thumb:
            thumb = image

        self._slides.append({
            'image': image,
            'title': title,
            'thumb': thumb,
            'url': link
        })

    def render(self):
        from django.conf import settings

        attrs = create_attr_from_obj(self)
        attrs['slides'] = self._slides
        attrs['show_thumbnails'] = self.show_thumbnails
        attrs['hide_controls'] = self.hide_controls
        attrs['STATIC_URL'] = settings.STATIC_URL
        return render_to_string('super-sized.html', attrs)

    def render_js(self):
        from django.conf import settings

        attrs = create_attr_from_obj(self)
        attrs['slides'] = self._slides
        attrs['show_thumbnails'] = self.show_thumbnails
        attrs['hide_controls'] = self.hide_controls
        attrs['STATIC_URL'] = settings.STATIC_URL
        return render_to_string('super-sized.js', attrs)

    def __unicode__(self):
        return self.render()

    def __str__(self):
        return self.render()


def get_slider_by_name(name):
    from models import SuperSizedSlider

    slider_obj = SuperSizedSlider.objects.get(name=name)
    slider = SuperSized()
    for attr in dir(slider_obj):
        if not attr.startswith('__'):
            if hasattr(slider, attr):
                value = getattr(slider_obj, attr)
                setattr(slider, attr, value)

    for img in slider_obj.slides():
        url = img.image.url
        try:
            thumb = img.image.thumbnail
        except Exception, dt:
            print 'Error :', dt
            thumb = url
        caption = img.caption
        link = img.link
        slider.add_slide(url, caption, thumb, link)
    return slider


class MainSlider(SuperSized):
    slider_id = 'home-page-slider'
    pause_hover = False
    show_thumbnails = False
    thumbnail_navigation = False
    progress_bar = False
    hide_controls = True

    def setup(self):
        from models import SliderImage

        images = SliderImage.objects.all()

        for img in images:
            url = img.image.url
            try:
                thumb = img.image.thumbnail
            except Exception, dt:
                print 'Error :', dt
                thumb = url
            caption = img.caption
            link = img.link
            self.add_slide(url, caption, thumb, link)
