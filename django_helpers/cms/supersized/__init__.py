# coding=utf-8
"""

0 or 'none' - No transition effect
1 or 'fade' - Fade effect (Default)
2 or 'slideTop' - Slide in from top
3 or 'slideRight' - Slide in from right
4 or 'slideBottom' - Slide in from bottom
5 or 'slideLeft' - Slide in from left
6 or 'carouselRight' - Carousel from right to left
7 or 'carouselLeft' - Carousel from left to right


"""
from django_helpers.apps.static_manager import register_js, register_css

__author__ = 'ajumell'

register_css('super-sized-css', 'super-sized/css/supersized.css')
register_css('super-sized-shutter-css', 'super-sized/theme/supersized.shutter.css')

register_js('super-sized-js', 'super-sized/js/supersized.3.2.7.min.js', ('jquery', 'jquery-easing'))
register_js('super-sized-shutter-js', 'super-sized/theme/supersized.shutter.min.js', ('super-sized-js',))

from slider import get_slider_by_name, SuperSized, SuperSizedTransitions
