# coding=utf-8
from django.conf.urls import url

import views

urlpatterns = [
    url(r'^', views.db_view, name='db-view'),
]
