# coding=utf-8
from django.db import models

REGISTER = set()


def register(view):
    def decorator(model):
        REGISTER.add(model)
        module = view.__module__
        name = view.__name__

        model.DB_VIEW = "{}.{}".format(module, name)

        url = models.CharField(max_length=2000, default='')
        model.add_to_class("url", url)

        return model

    return decorator
