# coding=utf-8
from django.http import Http404
from django_helpers.utils.importlib import import_module


def get_view(view_name):
    i = view_name.rfind('.')
    module_name = view_name[:i]
    name = view_name[i + 1:]
    module_obj = import_module(module_name)
    view_name = getattr(module_obj, name)

    return view_name


def db_view(request):
    from register import REGISTER
    path = request.path[1:]
    for model in REGISTER:
        try:
            instance = model.objects.get(url=path)
            view_name = model.DB_VIEW
            view = get_view(view_name)

            return view(request, instance)
        except model.DoesNotExist:
            pass

    raise Http404()
