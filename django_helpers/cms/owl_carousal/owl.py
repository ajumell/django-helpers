# coding=utf-8
from django_helpers.apps.static_manager import register_js, register_css
from django_helpers.widgets import Div

#
# Owl Carousal
#
register_css('owl-carousal-css', "js-plugins/owl-carousel/owl.carousel.css")
register_css('owl-carousal-theme-css', "js-plugins/owl-carousel/owl.theme.css")
register_css('owl-carousal-transitions-css', "js-plugins/owl-carousel/owl.transitions.css")

register_js('owl-carousal-js', 'js-plugins/owl-carousel/owl.carousel.min.js', (
    'jquery',
    'owl-carousal-css',
    'owl-carousal-theme-css',
    'owl-carousal-transitions-css',
))

register_js('owl-carousal-main-js', 'js-plugins/owl-carousel/owl.main.js', (
    'owl-carousal-js',
))


class OwlCarousal(object):
    def render(self):
        pass


class TextCarousal(Div):
    js_files = ['owl-carousal-main-js']

    auto_speed = 3000

    def get_items(self):
        return []

    def render_children(self):
        code = []
        for text in self.get_items():
            child = Div(text)
            code.append(child.render())

        return "\n".join(code)

    def render(self):
        self.data('owl-carousal', 'yes')
        self.data('single', 'true')
        self.data('auto-speed', self.auto_speed)
        return Div.render(self)
