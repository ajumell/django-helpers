# coding=utf-8
from django.conf.urls import url

import views

urlpatterns = [
    url(r'^save/', views.save, name='content-blocks-save'),
]
