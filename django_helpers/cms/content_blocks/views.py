# coding=utf-8
from django.http import HttpResponse


def save(request):
    from django_helpers.cms.content_blocks import get_block
    gt = request.POST.get

    name = gt('name')
    content = gt('content')

    block = get_block(name)
    block.content = content
    block.save()

    return HttpResponse("OK")
