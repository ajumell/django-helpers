(function ($) {
    var selector;
    var editor;
    var $area;

    selector = '#' + '{{ id }}';
    $area = $(selector);

    editor = new MediumEditor(selector, {{ options.render|safe }});

    editor.subscribe('editableBlur', function (ev, elem) {
        $.ajax({
            url: "{% url "content-blocks-save" %}",
            data: {
                "name": "{{ name }}",
                "content": elem.innerHTML
            },
            dataType: "json",
            type: "post"
        });
    })
    $area.data('medium-editor', editor);

}(jQuery));
