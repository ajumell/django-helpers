# coding=utf-8
from django.db import models


class ContentBlock(models.Model):
    name = models.CharField(max_length=32)
    content = models.TextField(blank=True)
    language = models.CharField(default='en', max_length=2)

    def __unicode__(self):
        return "%s - %s" % (self.name, self.language)

    class Meta:
        unique_together = (
            ('name', 'language'),
        )
