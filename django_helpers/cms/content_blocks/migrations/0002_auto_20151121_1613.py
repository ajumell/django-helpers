# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('content_blocks', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contentblock',
            name='content',
            field=models.TextField(blank=True),
            preserve_default=True,
        ),
    ]
