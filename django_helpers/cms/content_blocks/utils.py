# coding=utf-8


def get_block(name, language='en', create=False):
    from models import ContentBlock

    try:
        return ContentBlock.objects.get(name=name, language=language)
    except ContentBlock.DoesNotExist:
        if create is True:
            block = ContentBlock()
            block.name = name
            block.language = language
            block.save()

            return block
