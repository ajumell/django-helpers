# coding=utf-8
import urls
from django_helpers import add_app_url
from utils import get_block
from widgets.medium import MediumContentBlock, MinimalMediumContentBlock

add_app_url('content-block', urls)

__all__ = [
    'get_block',
    'MediumContentBlock',
    'MinimalMediumContentBlock'
]
