jQuery && jQuery(function ($) {

    var dj,
        fnFindButtons,
        registerDataWidget;

    dj = $.dj;
    registerDataWidget = dj.registerDataWidget;

    fnFindButtons = function (jqObj) {
        var count, value, key, i, arrButtons;
        count = jqObj.data("toolbarButtonsCount")
        console.log(jqObj.data());
        arrButtons = [];
        for (i = 0; i < count; i += 1) {
            key = "data-toolbar-button-" + i;
            value = jqObj.attr(key);
            arrButtons.push(value);
        }

        return arrButtons;
    };


    registerDataWidget('content-blocks-medium', function (obj) {
        var
            options,
            editor,
            name,
            url,
            buttons;

        buttons = fnFindButtons(obj);
        url = obj.data("contentBlocksUrl");
        name = obj.data("contentBlocksName");

        options = {"toolbar": {"buttons": buttons}};
        editor = new MediumEditor(obj, options);
        editor.subscribe('editableBlur', function (ev, elem) {
            $.ajax({
                url: url,
                data: {
                    "name": name,
                    "content": elem.innerHTML
                },
                dataType: "json",
                type: "post"
            });
        })
        obj.data('medium-editor', editor);

    });

});