# coding=utf-8
from django.core.urlresolvers import reverse

from django_helpers.apps.static_manager import register_js
from django_helpers.cms.content_blocks import get_block
from django_helpers.helpers.views import render_to_string
from django_helpers.js_plugins.medium_editor import MediumEditorOptions, MinimalMediumEditorOptions

__all__ = ['MinimalMediumEditorOptions', 'MediumContentBlock']

register_js('content-blocks-medium-js', 'content-blocks/medium.js', (
    'medium-editor-js',
    'jquery-django-ajax-csrf',
    'django-helpers-core-js',
))


class MediumContentBlock(object):
    js_files = ['content-blocks-medium-js']
    css_files = ['medium-editor-css']

    def __init__(self, request, block_name, options=None):
        if options is None:
            options = MediumEditorOptions()

        self.options = options
        self.request = request
        perm = self.has_perm()
        block = get_block(block_name, create=perm)

        if block:
            name = block.name
            self.name = name
            self.html_id = "medium-content-block-" + name
            self.content = block.content
        else:
            self.html_id = "medium-content-block-" + block_name
            self.content = ""
            self.name = block_name

        options.add('content-blocks-medium', 'true')
        options.add('content-blocks-url', reverse('content-blocks-save'))
        options.add('content-blocks-name', block_name)

    def has_perm(self):
        user = self.request.user

        if user.is_staff or user.is_superuser:
            return True

        return False

    def render(self):
        tmpl = '<div id="{0}" {2}>{1}</div>'
        html_id = self.html_id
        content = self.content
        content = content.encode('utf-8')

        if not self.has_perm():
            return tmpl.format(html_id, content, "")

        if not content:
            content = "<br />Click to edit the content<br />"

        content = tmpl.format(html_id, content, self.options.render())

        return content

    def render_js(self):
        if True:
            return ""

        if not self.has_perm():
            return ""

        return render_to_string('content-blocks/medium.js', {
            "id": self.html_id,
            "name": self.name,
            "options": self.options
        })


class MinimalMediumContentBlock(MediumContentBlock):
    def __init__(self, request, block_name, options=None):
        if options is None:
            options = MinimalMediumEditorOptions()
        MediumContentBlock.__init__(self, request, block_name, options)
