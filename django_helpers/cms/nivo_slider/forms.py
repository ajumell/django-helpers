# coding=utf-8
from django import forms

from models import *


class AddSliderImageForm(forms.ModelForm):
    class Meta:
        model = SliderImage
        fields = (
            'image',
            'link',
            'caption',
            'transition',
        )


class EditSliderImageForm(forms.ModelForm):
    class Meta:
        model = SliderImage
        fields = (
            'link',
            'caption',
            'transition',
        )
