# coding=utf-8
def add_slider_image_items(menu):
    from views import SliderImageCRUDView

    list_url = SliderImageCRUDView.get_url_name_for_action('list')
    item = menu.add_user_item("Slider Images", list_url)
    SliderImageCRUDView.get_nav(item, 'list', 'add')
