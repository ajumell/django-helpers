# coding=utf-8
from django_helpers.apps.static_manager import register_js, register_css

__author__ = 'ajumell'

register_js('nivo-slider-js', 'nivo-slider/js/jquery.nivo.slider.pack.js', ('jquery',))
register_css('nivo-slider-css', 'nivo-slider/css/nivo-slider.css')

register_css('nivo-slider-theme-bar-css', 'nivo-slider/css/themes/bar/bar.css')
register_css('nivo-slider-theme-dark-css', 'nivo-slider/css/themes/dark/dark.css')
register_css('nivo-slider-theme-default-css', 'nivo-slider/css/themes/default/default.css')
register_css('nivo-slider-theme-light-css', 'nivo-slider/css/themes/light/light.css')
register_css('nivo-slider-theme-wds-css', 'nivo-slider/css/themes/wds/wds.css')

from slider import Effects, Themes, NivoSlider, NivoSliderSlide, get_slider_with_name
