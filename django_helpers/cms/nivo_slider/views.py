# coding=utf-8
from forms import *
from tables import *
from xeo_theme_base.crud import CRUDView


class SliderImageCRUDView(CRUDView):
    model = SliderImage
    check_permissions = True

    url_prefix = "slider-image"

    # List
    list_permission = 'change'
    list_data_table = SliderImageDataTable
    list_title = "List Slider Images"

    # Insert
    add_permission = 'add'
    add_form = AddSliderImageForm
    add_title = "Add SliderImage"
    add_success_message = '"{0}" added successfully.'
    add_success_redirect = 'list'

    # Edit
    edit_permission = 'change'
    edit_form = EditSliderImageForm
    edit_title = 'Edit "{0}"'
    edit_success_message = '"{0}" updated successfully.'
    edit_success_redirect = 'list'

    # Delete
    delete_permission = 'delete'
    delete_title = 'Confirm Delete "{0}"'
    delete_success_redirect = 'list'
    delete_cancel_link = 'list'
