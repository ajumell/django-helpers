# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('nivo_slider', '0002_auto_20150119_0842'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='nivosliderslide',
            options={'ordering': ('order',)},
        ),
        migrations.AddField(
            model_name='nivosliderslide',
            name='disabled',
            field=models.BooleanField(default=False, editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='nivosliderslide',
            name='order',
            field=models.PositiveIntegerField(default=1, editable=False, db_index=True),
            preserve_default=False,
        ),
    ]
