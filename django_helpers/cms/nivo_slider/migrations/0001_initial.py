# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NivoSlider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=32)),
                ('effect', models.CharField(max_length=75, choices=[(b'boxRain', b'boxRain'), (b'boxRainGrow', b'boxRainGrow'), (b'boxRainGrowReverse', b'boxRainGrowReverse'),
                                                                    (b'boxRainReverse', b'boxRainReverse'), (b'boxRandom', b'boxRandom'), (b'fade', b'fade'), (b'fold', b'fold'),
                                                                    (b'random', b'random'), (b'sliceDown', b'sliceDown'), (b'sliceDownLeft', b'sliceDownLeft'),
                                                                    (b'sliceUp', b'sliceUp'), (b'sliceUpDown', b'sliceUpDown'), (b'sliceUpDownLeft', b'sliceUpDownLeft'),
                                                                    (b'sliceUpLeft', b'sliceUpLeft'), (b'slideInLeft', b'slideInLeft'), (b'slideInRight', b'slideInRight')])),
                ('slices', models.SmallIntegerField(default=15)),
                ('box_cols', models.SmallIntegerField(default=8)),
                ('box_rows', models.SmallIntegerField(default=4)),
                ('animation_speed', models.SmallIntegerField(default=700)),
                ('pause_time', models.SmallIntegerField(default=3000)),
                ('direction_nav', models.BooleanField(default=True)),
                ('control_nav', models.BooleanField(default=True)),
                ('control_bav_thumbs', models.BooleanField(default=False)),
                ('pause_on_hover', models.BooleanField(default=True)),
                ('manual_advance', models.BooleanField(default=False)),
                ('prev_text', models.CharField(default=b'Prev', max_length=32)),
                ('next_text', models.CharField(default=b'Next', max_length=32)),
                ('random_start', models.BooleanField(default=False)),
                ('theme_name',
                 models.CharField(default=b'bar', max_length=75, choices=[(b'bar', b'bar'), (b'dark', b'dark'), (b'default', b'default'), (b'light', b'light'), (b'wds', b'wds')])),
                ('width', models.IntegerField(null=True, blank=True)),
                ('height', models.IntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NivoSliderSlide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'nivo-slider')),
                ('link', models.URLField(blank=True)),
                ('caption', models.CharField(max_length=255, blank=True)),
                ('transition', models.CharField(blank=True, max_length=75,
                                                choices=[(b'boxRain', b'boxRain'), (b'boxRainGrow', b'boxRainGrow'), (b'boxRainGrowReverse', b'boxRainGrowReverse'),
                                                         (b'boxRainReverse', b'boxRainReverse'), (b'boxRandom', b'boxRandom'), (b'fade', b'fade'), (b'fold', b'fold'),
                                                         (b'random', b'random'), (b'sliceDown', b'sliceDown'), (b'sliceDownLeft', b'sliceDownLeft'), (b'sliceUp', b'sliceUp'),
                                                         (b'sliceUpDown', b'sliceUpDown'), (b'sliceUpDownLeft', b'sliceUpDownLeft'), (b'sliceUpLeft', b'sliceUpLeft'),
                                                         (b'slideInLeft', b'slideInLeft'), (b'slideInRight', b'slideInRight')])),
                ('slider', models.ForeignKey(to='nivo_slider.NivoSlider')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
