# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

import django_helpers.apps.forms.fields.thumbnail


class Migration(migrations.Migration):
    dependencies = [
        ('nivo_slider', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SliderImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(editable=False, db_index=True)),
                ('disabled', models.BooleanField(default=False, editable=False)),
                ('image', django_helpers.apps.forms.fields.thumbnail.ThumbnailImageField(upload_to=b'nivo-slider')),
                ('link', models.URLField(blank=True)),
                ('caption', models.CharField(max_length=255, blank=True)),
                ('transition', models.CharField(blank=True, max_length=75,
                                                choices=[(b'boxRain', b'boxRain'), (b'boxRainGrow', b'boxRainGrow'), (b'boxRainGrowReverse', b'boxRainGrowReverse'),
                                                         (b'boxRainReverse', b'boxRainReverse'), (b'boxRandom', b'boxRandom'), (b'fade', b'fade'), (b'fold', b'fold'),
                                                         (b'random', b'random'), (b'sliceDown', b'sliceDown'), (b'sliceDownLeft', b'sliceDownLeft'), (b'sliceUp', b'sliceUp'),
                                                         (b'sliceUpDown', b'sliceUpDown'), (b'sliceUpDownLeft', b'sliceUpDownLeft'), (b'sliceUpLeft', b'sliceUpLeft'),
                                                         (b'slideInLeft', b'slideInLeft'), (b'slideInRight', b'slideInRight')])),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='nivosliderslide',
            name='image',
            field=django_helpers.apps.forms.fields.thumbnail.ThumbnailImageField(upload_to=b'nivo-slider'),
        ),
    ]
