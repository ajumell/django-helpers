jQuery(function ($) {
    $("#slider_" + "{{ slider_id }}").nivoSlider({
        effect : '{{ effect }}',
        slices : {{ slices }},
        boxCols : {{ box_cols }},
        boxRows : {{ box_rows }},
        animSpeed : {{ animation_speed }},
        pauseTime : {{ pause_time }},
        startSlide : {{ start_slide }},
        directionNav : {{ direction_nav }},
        controlNav : {{ control_nav }},
        controlNavThumbs : {{ control_nav_thumbs }},
        pauseOnHover : {{ pause_on_hover }},
        manualAdvance : {{ manual_advance }},
        prevText : '{{ prev_text }}',
        nextText : '{{ next_text }}',
        randomStart : {{ random_start }}
    });
});