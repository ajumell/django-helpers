# coding=utf-8
__author__ = 'ajumell'

import datetime

import gdata.analytics.client
import gdata.gauth
from django.conf import settings
from django.core.cache import cache


def get_result(query):
    my_client = gdata.analytics.client.AnalyticsClient()
    token = gdata.gauth.ClientLoginToken(settings.GAs_TOKEN)
    my_client.auth_token = token
    result = my_client.GetDataFeed(query)
    return result


def get_page_visits(profile_id=None):
    visits = cache.get('ga-page-visits', None)

    if visits is not None:
        return visits

    if profile_id is None:
        profile_id = getattr(settings, 'GA_PROFILE_ID', None)

    if profile_id is None:
        raise Exception('Profile ID is required.')

    start = datetime.date.today() - datetime.timedelta(days=3 * 365)
    today = datetime.date.today()

    data_query = gdata.analytics.client.DataFeedQuery({
        'ids': 'ga:%d' % profile_id,
        'start-date': start,
        'end-date': today,
        'metrics': 'ga:visits',
        'sort': 'ga:visits',
        'max-results': '1000'
    })

    result = get_result(data_query)
    visits = result.aggregates.metric[0].value
    cache.set('ga-page-visits', visits, 3600 * 12)

    return visits
