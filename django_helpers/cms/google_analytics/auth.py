# coding=utf-8

from httplib2 import ServerNotFoundError
from oauth2client.service_account import ServiceAccountCredentials

__all__ = [
    'get_token',
    'load_credentials',
]

scope = 'https://www.googleapis.com/auth/analytics.readonly'

CREDENTIALS = {}


def get_token(key_file_path=None):
    try:
        # The scope for the OAuth2 request.
        credentials = get_credentials(key_file_path)
        return credentials.get_access_token().access_token
    except ServerNotFoundError:
        return ""


def get_credentials(key_file_path=None):
    if key_file_path is None:
        from django.conf import settings
        key_file_path = getattr(settings, 'GOOGLE_ANALYTICS_KEY_PATH')

    if key_file_path in CREDENTIALS:
        return CREDENTIALS[key_file_path]

    credentials = load_credentials(key_file_path)
    CREDENTIALS[key_file_path] = credentials

    return credentials


def load_credentials(key_file_path=None, client_email=None):
    if key_file_path is None:
        from django.conf import settings
        key_file_path = getattr(settings, 'GOOGLE_ANALYTICS_KEY_PATH')

    if client_email is None:
        from django.conf import settings
        client_email = getattr(settings, 'GOOGLE_ANALYTICS_CLIENT_EMAIL')

    credentials = ServiceAccountCredentials.from_p12_keyfile(client_email, key_file_path, scopes=scope)
    return credentials
