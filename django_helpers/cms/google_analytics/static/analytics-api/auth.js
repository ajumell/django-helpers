jQuery(function ($) {
    gapi && gapi.analytics.ready(function () {
        gapi.analytics.auth.authorize({
            'serverAuth': {
                'access_token': window.ANALYTICS_CLIENT_ID
            }
        });
    });
});