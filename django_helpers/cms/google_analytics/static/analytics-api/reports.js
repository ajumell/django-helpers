(function (window, document, undefined, $) {
    gapi.analytics.ready(function () {
        $.fn.GACountryChart = function (options) {
            if (options === undefined) options = {};
            this.each(function () {
                var view_id;
                var $this;
                var chart;
                var query;
                var dataChart;

                $this = $(this);
                view_id = options['view_id'] || $this.data('view-id');

                query = {
                    ids: 'ga:' + view_id,
                    metrics: 'ga:sessions',
                    dimensions: 'ga:country',
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                    'max-results': options['max-result'] || 10,
                    sort: '-ga:sessions'
                };

                chart = {
                    container: this,
                    type: 'PIE',
                    options: {
                        width: '100%',
                        pieHole: 4 / 9
                    }
                };
                dataChart = new gapi.analytics.googleCharts.DataChart({
                    query: query,
                    chart: chart
                });


                dataChart.execute();
                $this.data('ga-country-chart', dataChart);
            })
        };

        $.fn.GASessionsChart = function (options) {
            if (options === undefined) options = {};
            this.each(function () {

                var chart;
                var query;
                var dataChart;
                var view_id;
                var $this;

                $this = $(this);
                view_id = options['view_id'] || $this.data('view-id');

                query = {
                    ids: 'ga:' + view_id,
                    metrics: 'ga:sessions',
                    dimensions: 'ga:date',
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday'
                };

                chart = {
                    container: this,
                    type: 'LINE',
                    options: {
                        width: '100%'
                    }
                };

                dataChart = new gapi.analytics.googleCharts.DataChart({
                    query: query,
                    chart: chart
                });

                dataChart.execute();
                $this.data('ga-sessions-chart', dataChart);
            })
        };

        $.fn.GAActiveUsers = function (options) {
            if (options === undefined) options = {};
            this.each(function () {

                var view_id;
                var obj;
                var interval;
                var $this;

                $this = $(this);
                interval = options['interval'] || $this.data('interval') || 5;
                view_id = options['view_id'] || $this.data('view-id');

                console.log(view_id);

                obj = new gapi.analytics.ext.ActiveUsers({
                    container: this,
                    pollingInterval: interval
                });

                obj.set({
                    ids: "ga:" + view_id
                }).execute();

                $this.data('ga-active-users-info', obj);
            })
        };


        $("[data-ga-country-widget]").GACountryChart();
        $("[data-ga-sessions-widget]").GASessionsChart();
        $("[data-ga-active-users-widget]").GAActiveUsers();
    });

}(window, document, undefined, jQuery));