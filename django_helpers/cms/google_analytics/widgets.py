# coding=utf-8
from auth import get_token
from django_helpers.widgets import Div


class AnalyticsWidget(Div):
    analytic_code = ''

    def __init__(self, view_id, widget_id=None):
        Div.__init__(self)

        self.widget_id = widget_id
        self.view_id = view_id
        self.js_files = ['analytics-api-reports']

        self.attr('data-view-id', self.view_id)
        if self.analytic_code:
            self.attr(self.analytic_code, 'true')

        if self.widget_id is not None:
            self.attr('id', self.widget_id)

    def js_global_vars(self):
        return {
            'ANALYTICS_CLIENT_ID': get_token()
        }


class CountryWidget(AnalyticsWidget):
    analytic_code = 'data-ga-country-widget'


class SessionsWidget(AnalyticsWidget):
    analytic_code = 'data-ga-sessions-widget'


class ActiveUsersWidget(AnalyticsWidget):
    analytic_code = 'data-ga-active-users-widget'
    tag = 'span'

    def __init__(self, *args, **kwargs):
        super(ActiveUsersWidget, self).__init__(*args, **kwargs)
        self.add_text("0")
        self.js_files = ['analytics-api-reports', 'analytics-api-active-users']
