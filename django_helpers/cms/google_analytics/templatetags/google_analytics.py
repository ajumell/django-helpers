# coding=utf-8
from django.template import TemplateSyntaxError
from django.template.defaulttags import register
from django_helpers.cms.google_analytics.reporting import get_page_visits

from django_helpers.helpers.templatetags import parse_args
from django_helpers.helpers.views import render_to_string

__author__ = 'ajumell'


@register.simple_tag()
def analytics_js(parser, token):
    bits = token.split_contents()
    if len(bits) != 2:
        raise TemplateSyntaxError("'%s' needs at least one arguments." % bits[0])
    args, kwargs = parse_args(bits, parser)
    code = args[0]
    return render_to_string('google-analytics.html', {
        'CODE': code
    })


@register.simple_tag()
def visits(parser, token):
    bits = token.split_contents()
    if len(bits) != 2:
        raise TemplateSyntaxError("'%s' needs at least one arguments." % bits[0])
    args, kwargs = parse_args(bits, parser)
    return get_page_visits(*args, **kwargs)
