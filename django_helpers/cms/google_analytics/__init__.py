# coding=utf-8
from django_helpers.apps.static_manager import register_js

__author__ = 'ajumell'

register_js('analytics-api-loader', 'analytics-api/loader.js')
register_js('analytics-api-auth', 'analytics-api/auth.js', ('analytics-api-loader', 'jquery'))
register_js('analytics-api-active-users', 'analytics-api/src/active-users.js', ('analytics-api-auth', 'jquery'))
register_js('analytics-api-reports', 'analytics-api/reports.js', ('analytics-api-auth', 'jquery'))
