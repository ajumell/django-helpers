# coding=utf-8

import httplib2
from googleapiclient.discovery import build
from django.core.management.base import BaseCommand

from django_helpers.cms.google_analytics.auth import load_credentials

__author__ = 'ajumell'


class Command(BaseCommand):
    args = ''
    help = 'Lists all the views in the given key file.'

    def handle(self, *args, **options):
        service = self.get_service('analytics', 'v3')
        self.get_first_profile_id(service)

    def get_service(self, api_name, api_version):
        credentials = load_credentials()
        http = credentials.authorize(httplib2.Http())
        service = build(api_name, api_version, http=http)
        return service

    def get_first_profile_id(self, service):
        # Use the Analytics service object to get the first profile id.

        # Get a list of all Google Analytics accounts for this user
        accounts = service.management().accounts().list().execute()

        if accounts.get('items'):
            # Get the first Google Analytics account.
            account = accounts.get('items')[0].get('id')

            # Get a list of all the properties for the first account.
            properties = service.management().webproperties().list(accountId=account).execute()

            properties = properties.get('items')
            if properties:
                for property in properties:
                    print 'Property :', property.get('name')
                    property_id = property.get('id')

                    # Get a list of all views (profiles) for the first property.
                    profiles = service.management().profiles().list(
                        accountId=account,
                        webPropertyId=property_id).execute()

                    profiles_items = profiles.get('items')
                    if profiles_items:
                        for profiles_item in profiles_items:
                            print "--Name :", profiles_item.get('name')
                            print "--URL :", profiles_item.get('websiteUrl')
                            print "--ID :", profiles_item.get('id')
