# coding=utf-8

from django.conf.urls import url
from django.http import HttpResponse

import urls
from django_helpers import add_app_url
from django_helpers.utils.importlib import import_module

__author__ = 'ajumell'

_URL_PREFIX = 'x-editable-'

_EDITABLES = dict()

add_app_url('x-editable', urls)


def create_reg(name):
    return "(?P<%s>.*)/" % name


def get_editable(name, pk):
    obj = _EDITABLES.get(name)

    if isinstance(obj, str):
        i = obj.rfind('.')
        module_name = obj[:i]
        name = obj[i + 1:]
        module_obj = import_module(module_name)
        obj = getattr(module_obj, name)

    return obj(pk=pk)


def save(request, name):
    if request.method != 'POST':
        return HttpResponse()

    gt = request.POST.get
    pk = gt('pk')
    editable = get_editable(name, pk)
    if editable is None:
        return HttpResponse()
    field_name = gt('name')
    results = editable.save(request, field_name)
    return results


def list_data(request, name, field_name, pk):
    # gt = request.POST.get
    # pk = gt('pk')
    editable = get_editable(name, pk)
    if editable is None:
        return HttpResponse()
    results = editable.list(field_name)
    return results


def register_name(class_name, name):
    if name is None:
        return

    if name in _EDITABLES:
        old_name = _EDITABLES[name]

        msg = "You tried to register a editable with id " + name + " for " + class_name + ".\n"
        msg += "But " + name + " is already registered for " + old_name + ".\n"
        msg += "Try giving a new ID for " + class_name
        raise Exception(msg)

    _EDITABLES[name] = class_name

    list_reg = "%s/" % name
    list_reg += create_reg('field_name')
    list_reg += create_reg('pk')
    list_reg += "list/$"

    save_reg = "%s/" % name
    save_reg += "save/$"

    save_pattern = url(r"%s" % save_reg, save, name=name + '-save', kwargs={
        "name": name
    })

    list_pattern = url(r"%s" % list_reg, list_data, name=name + '-list', kwargs={
        "name": name
    })

    urls.urlpatterns.append(save_pattern)
    urls.urlpatterns.append(list_pattern)
