# coding=utf-8
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db.models import ForeignKey
from django.utils import six

from django_helpers.apps.static_manager.utils import get_js_files, get_css_files
from django_helpers.core.exceptions import IDMissingException
from django_helpers.core.views import JSONResponse
from django_helpers.helpers.views import render_json

# noinspection PyUnresolvedReferences
import static

__author__ = 'ajumell'


class XEditableMeta(type):
    def __new__(mcs, name, bases, attrs):
        from views import register_name

        new_class = type.__new__(mcs, name, bases, attrs)

        module_name = attrs['__module__']
        class_name = module_name + '.' + name
        editable_id = attrs.get('editable_id', None)
        register_name(class_name, editable_id)

        return new_class


class BaseXEditable(object):
    #
    # ID
    #
    def get_widget_id(self, field, pk):
        raise NotImplementedError()

    #
    # URL Functions
    #
    def get_list_url(self, name):
        raise NotImplementedError()

    def get_save_url(self):
        raise NotImplementedError()

    #
    # AJAX Functions
    #
    def save(self, request, name):
        raise NotImplementedError()

    def list(self, name):
        field = self.get_editable_with_name(name)
        if field is None:
            return self.field_does_not_exist_error()
        return render_json(field.list())

    #
    # Response
    #
    def get_response(self, data):
        request = getattr(self, 'request', None)
        json = JSONResponse(request, data)

        return json

    def error(self, message):
        data = {'err': True, 'msg': str(message)}
        json = self.get_response(data)
        self.error_hook(json)

        return json

    def success(self, content):
        data = {'err': False, 'msg': content}
        json = self.get_response(data)
        self.success_hook(json)

        return json

    def field_does_not_exist_error(self):
        return self.error('Server Error : Field does not exists.')

    def value_none_error(self):
        return self.error('Server error : Value is None.')

    def error_hook(self, json_response):
        """
        Hook to send some extra data in error response
        """
        pass

    def success_hook(self, json_response):
        """
        Hook to send some extra data in success response
        """

    #
    # Fields
    #
    def get_editable_with_name(self, name):
        raise NotImplementedError()

    #
    # Value
    #
    def get_value(self, field_name):
        raise NotImplementedError()

    def get_pk(self):
        raise NotImplementedError()


# noinspection PyAbstractClass
class BaseModelXEditable(BaseXEditable):
    editable_id = None

    #
    # Instance and model functions
    #
    def get_instance(self):
        """
        :return: Returns the current model instance
        """
        raise NotImplementedError()

    def get_model(self):
        """
        :return: Returns the model class for the editable
        """
        raise NotImplementedError()

    #
    # Model Field
    #

    def get_model_field_type(self, name):
        """
        @param name: name of the field
        @return: the value returned by the get_internal_type
        function of the model field.
        """
        name = self.clean_field_name(name)
        cache_name = '_model_field_type_cache'
        if not hasattr(self, cache_name):
            setattr(self, cache_name, {})

        cache = getattr(self, cache_name)
        if name not in cache:
            field = self.get_model_field(name)
            val = field.get_internal_type()
            cache[name] = val
        else:
            val = cache[name]
        return val

    def get_model_field(self, name):
        """
        @param name: name of the field
        @return: returns the corresponding model field.
        """
        name = self.clean_field_name(name)
        cache_name = '_model_field_cache'
        if not hasattr(self, cache_name):
            setattr(self, cache_name, {})

        cache = getattr(self, cache_name)
        if name not in cache:
            model = self.get_model()
            meta = getattr(model, "_meta")
            val = meta.get_field_by_name(name)[0]
            cache[name] = val
        else:
            val = cache[name]
        return val

    #
    # Getters
    #

    def get_value(self, name):
        """
        @param name: name of the field
        @return: the python object of the field from the model instance
        """
        instance = self.get_instance()
        return getattr(instance, name, None)

    def get_pk(self):
        raise NotImplementedError()

    #
    # Queryset Modifier
    #
    def get_queryset_modifier(self):
        return None
    
    #
    # Validation Functions
    #
    def validate(self, name, value):
        """
        Performs some basic validations. If a custom validation function
        is created for a field then it will be called.

        @param name: name of the field.
        @param value: the current python object for the field.
        @return: the cleaned value.
        """
        custom_validator = getattr(self, '%s_clean' % name, None)
        if hasattr(custom_validator, '__call__'):
            return custom_validator()

        self.model_validate(name, value)

        return value

    def model_validate(self, name, value):
        model_field = self.get_model_field(name)
        instance = self.get_instance()
        try:
            if isinstance(model_field, ForeignKey):
                _value = value.pk
            else:
                _value = value
            model_field.validate(_value, instance)
        except ValidationError, dt:
            raise Exception(', '.join(dt.messages))

    # noinspection PyMethodMayBeStatic
    def clean_field_name(self, name):
        i = name.find("__")
        if i > 0:
            name = name[:i]

        return name

    #
    # Rendering
    #
    def render_value_for_field(self, name, value):
        editable_field = self.get_editable_with_name(name)
        return editable_field.to_string(value)

    #
    # AJAX Functions
    #
    def save(self, request, name):
        self.request = request
        field_name = self.clean_field_name(name)

        editable_field = self.get_editable_with_name(name)
        is_multivalued = getattr(editable_field, 'is_multivalued', False)

        if editable_field is None:
            return self.field_does_not_exist_error()

        if is_multivalued:
            value = request.POST.getlist('value')
        else:
            value = request.POST.get('value')

        if value is None:
            # If no value came from request
            return self.value_none_error()

        try:
            value = editable_field.to_python(value)
            value = self.validate(field_name, value)
            self.save_field(field_name, value, request)
            op = self.render_value_for_field(name, value)
        except Exception, dt:
            return self.error(dt)

        return self.success(op)

    def save_field(self, model_field_name, value, request, save=True):
        instance = self.get_instance()
        setattr(instance, model_field_name, value)
        try:
            instance.clean()
        except ValidationError, ex:
            raise Exception(', '.join(ex.messages))

        self.save_extra(instance, request)
        if save is True:
            instance.save()

    def save_extra(self, instance, request):
        pass


class XEditableBase(six.with_metaclass(XEditableMeta, BaseModelXEditable)):
    has_js = True
    model = None
    editable_id = None
    _js_files = []

    mode = "bootstrap"

    def __init__(self, instance=None, pk=None, is_ajax=False):
        if self.editable_id is None:
            raise IDMissingException()

        self.is_ajax = is_ajax
        model = self.model
        if instance is not None:
            if not isinstance(instance, model):
                raise Exception()

            self.instance = instance
            self.pk = instance.pk
        else:
            self.instance = None
            self.pk = pk

        self.fields = {}
        self.preprocess_editables()

    #
    # Fields
    #
    def get_fields(self):
        raise NotImplementedError()

    def preprocess_editables(self):
        fields = self.get_fields()
        new_fields = {}

        for field in fields:
            field_name = field.field_name
            field.set_controller(self)

            new_fields[field_name] = field
            setattr(self, field_name, field)

        self.fields = new_fields

    def get_editable_with_name(self, name):
        return self.fields[name]

    #
    # Rendering
    #
    def render_field(self, name):
        field = self.fields[name]
        return field.render()

    def __getitem__(self, item):
        return self.render_field(item)

    #
    # Static Files
    #

    def js_files(self):
        files = self._js_files[:]

        if 'django-xeditable-js' not in files:
            files.append('django-xeditable-js')

        files.append("xeditable-%s-base" % self.mode)
        fields = self.fields
        for field_name in fields:
            field = self.fields[field_name]
            files += get_js_files(field)

        return files

    def css_files(self):
        files = []

        fields = self.fields

        for field_name in fields:
            field = self.fields[field_name]
            css = get_css_files(field)
            if css and len(css) > 0:
                files += css

        return files

    #
    # Model and Instance
    #

    def get_instance(self):
        instance = self.instance
        if instance is None:
            pk = self.get_pk()
            if pk is None:
                raise
            instance = self.get_model().objects.get(pk=pk)
            self.instance = instance
        return instance

    def get_model(self):
        return getattr(self, 'model')

    #
    # Getters
    #

    def get_widget_id(self, field, pk=None):
        if pk is None:
            pk = self.get_pk()

        return "editable-{}-{}-{}".format(self.editable_id, field, pk)

    def get_list_url(self, name):
        return reverse(self.editable_id + "-list", kwargs={
            'field_name': name,
            'pk': self.pk
        })

    def get_save_url(self):
        return reverse(self.editable_id + "-save")

    def get_pk(self):
        return self.pk


# noinspection PyAbstractClass
class BootstrapXEditable(XEditableBase):
    mode = 'bootstrap'


# noinspection PyAbstractClass
class Bootstrap3XEditable(XEditableBase):
    mode = 'bootstrap3'
