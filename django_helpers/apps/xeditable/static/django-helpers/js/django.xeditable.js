jQuery(function ($) {
    var dj,
        registerDataWidget;

    dj = $.dj;
    registerDataWidget = dj.registerDataWidget;

    dj = $.dj;
    if (!dj) {
        return;
    }
    $.fn.editableform.loading = '<div><i class="icon-spinner icon-spin icon-large"></i></div>';


    registerDataWidget('dj-editable', function (jqElement) {
        /**
         *
         * @param dElement The element in which the editable has to be applied
         * @param dTarget The target element in which the edited value has to be updated
         * @param sURL URL to which the save function has to be called.
         * @param dOptions The options for xeditable
         * @param dClickElement The element which should trigger the editing.
         */
        var
            jqTarget,
            jqClickElement,

            oDefaultOptions,

            sValue,
            sClickElement;

        oDefaultOptions = {
            "container": "body",
            ajaxOptions: {
                dataType: 'json'
            },
            display: function (value, sourceData, response) {
                var _target = jqTarget;

                if (!_target) {
                    _target = jqElement;
                }

                if (response === undefined) {
                    response = sourceData;
                }

                if (response && response.msg) {
                    _target.html(response.msg);
                }
            },
            success: function (response) {
                if (response && response.err === true) {
                    return response.msg;
                }
            }
        };

        sClickElement = jqElement.data("editableClickElement");


        /* Find a method to add value to Javascript */
        if (sValue) {
            oDefaultOptions.value = sValue;
        }

        if (sClickElement !== undefined) {
            jqClickElement = $(sClickElement);
            oDefaultOptions.toggle = 'manual';
            jqClickElement.click(function (e) {
                e.stopPropagation();
                jqElement.editable('toggle');
            });
        }

        jqElement.editable(oDefaultOptions);
    });
});