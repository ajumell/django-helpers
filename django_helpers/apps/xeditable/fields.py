# coding=utf-8
import datetime

from django.db import router
from django.db.models import ForeignKey
from django.forms.utils import from_current_timezone
from django.utils import formats, six, datetime_safe
from django.utils.encoding import force_text

from django_helpers.apps.forms.widgets.bootstrap import eternicode_date_format
from django_helpers.widgets import HTMLNode

__author__ = 'ajumell'


class EditableField(HTMLNode):
    listable = False

    field_type = None
    js_template = None
    tag = 'a'

    _has_render_prepared = False

    popup_placement = 'right'
    title = ''

    click_element = None
    click_element_placement = 'before'

    def __init__(self, field_name=None, required=None, mode='popup'):
        HTMLNode.__init__(self, '')
        self.field_name = field_name
        self.required = required
        self.mode = mode

    def to_python(self, value):
        """
        This function will handle the convertion of the
        string to its corresponding python object.
        @param value: The string value given from the http post
        @return: the converted value. In normal cases it will be same.
        """
        return value

    def to_string(self, value):
        """
        This function will convert the python object to
        the string representation
        @param value: python object
        @return: string representation of the python object
        """
        return value

    #
    # Render
    #
    def render(self, reset=False):
        self.render_prepare(reset)
        return self.render_widget()

    def render_widget(self):
        widget_html = HTMLNode.render(self)
        click_element = self.click_element

        if not click_element:
            return widget_html

        click_element_placement = self.click_element_placement
        if click_element_placement == 'none' or click_element_placement is None:
            return widget_html

        click_html = click_element.render()
        if click_element_placement == 'before':
            return click_html + widget_html

        return widget_html + click_html

    def render_prepare(self, reset=False):
        if self._has_render_prepared and not reset:
            return

        self._has_render_prepared = True
        data = self.generate_render_data()
        for key, value in data.items():
            if value is not None:
                self.data(key, value)

    def generate_render_data(self):
        controller = self.controller
        if controller is None:
            raise Exception("Controller is Missing.")

        value = controller.get_value(self.field_name)
        if value is not None:
            text = self.to_string(value)
            self.set_text(text)

        if self.tag == 'a':
            self.attr('href', "javascript:;")

        pk = controller.get_pk()
        save_url = controller.get_save_url()

        html_id = controller.get_widget_id(self.field_name, pk)
        self.attr('id', html_id)
        data = {
            'dj-editable': 'true',
            'pk': pk,
            'url': save_url,
            'title': self.title,
            'name': self.field_name,
            'type': self.field_type,
            'placement': self.popup_placement,
            'mode': self.mode
        }

        if self.listable is True:
            list_url = controller.get_list_url(self.field_name)
            data['source'] = list_url

        if self.click_element is not None:
            self.click_element.data("dj-editable-element", html_id)
            data['editable-click-element'] = "[data-dj-editable-element={}]".format(html_id)

        return data

    def set_click_element(self, elem, placement='before'):
        self.click_element = elem
        self.click_element_placement = placement


class ModelEditableField(EditableField):
    def get_model_field(self):
        controller = self.controller
        return controller.get_model_field(self.field_name)

    def get_model_field_type(self):
        controller = self.controller
        return controller.get_model_field_type(self.field_name)

    def load_validations_from_model(self):
        """
        Loads the Client side validations from the model.
        :return:
        """
        field = self.get_model_field()
        # TODO: Add more validations.
        if self.required is None:
            self.required = not field.blank

    def controller_changed(self, old_controller, new_controller):
        super(ModelEditableField, self).controller_changed(old_controller, new_controller)
        self.load_validations_from_model()


#
# Text Editable
#
class TextEditable(EditableField):
    field_type = 'text'
    js_files = ['xeditable-input-text-js']

    def __init__(self, field_name=None, required=None, mode='popup', placeholder='', clear=True):
        super(TextEditable, self).__init__(field_name, required, mode)
        self.clear = clear
        self.placeholder = placeholder

    def generate_render_data(self):
        data = EditableField.generate_render_data(self)
        data['placeholder'] = self.placeholder
        data['clear'] = self.clear
        return data


class TextAreaEditable(TextEditable):
    field_type = 'textarea'

    def __init__(self, field_name=None, required=None, mode='popup', placeholder='', clear=True, rows=7):
        super(TextAreaEditable, self).__init__(field_name, required, mode, placeholder, clear)
        self.rows = rows
        self.js_files = ("xeditable-input-textarea-js",)

    def generate_render_data(self):
        data = TextEditable.generate_render_data(self)
        data['rows'] = self.rows
        return data


#
# Select Fields
#
class SelectEditable(EditableField):
    js_files = ("xeditable-input-select-js",)

    source_cache = True
    source_error = "Error occured when loading data."
    prepend = False
    source = None


class BaseChoiceField(EditableField):
    listable = True

    def __init__(self, field_name=None, required=None, choices=None, mode='popup'):
        EditableField.__init__(self, field_name, required, mode)
        self.choices = choices

    def _get_choices(self):
        return self.choices

    def list(self):
        result = []
        choices = self._get_choices()
        for choice in choices:
            pk, value = choice[0], choice[1]

            result.append({
                'value': pk,
                'text': value
            })
        return result


class BaseModelChoiceField(ModelEditableField):
    listable = True

    def __init__(self, field_name=None, required=None, queryset=None, mode='popup'):
        ModelEditableField.__init__(self, field_name, required, mode)
        self.queryset = queryset

    def _get_choices(self):
        queryset = self.queryset

        model_field = self.get_model_field()
        if queryset is None:
            # Find if model has relation or choices
            is_foreign = isinstance(model_field, ForeignKey)
            if not is_foreign:
                # Borrowed from the django code base.
                choices = model_field.choices
                self.choices = choices
                return choices
            else:
                # Borrowed from the django code base.
                # django/db/models/fields/related.py Line 1088, v:1.5.1
                # TODO Find a method to implement using multiple db.
                manager = model_field.rel.to._default_manager
                queryset = manager.using(None).complex_filter(model_field.rel.limit_choices_to),
                if type(queryset) is tuple and len(queryset) == 1:
                    queryset = queryset[0]
                self.queryset = queryset
        modifier = self.controller.get_queryset_modifier()
        if modifier is not None:
            queryset = modifier.apply_queryset(queryset, self.field_name)

        return queryset

    def list(self):
        result = []
        choices = self._get_choices()
        for choice in choices:
            pk = choice.pk
            value = str(choice)

            result.append({
                'value': pk,
                'text': value
            })
        return result


class SimpleSelectField(BaseChoiceField):
    field_type = 'select'
    js_files = ('xeditable-input-select-js',)


class ModelSelectField(BaseModelChoiceField):
    field_type = 'select'
    js_files = ('xeditable-input-select-js',)

    def to_python(self, value):
        field = self.get_model_field()
        controller = self.controller
        model_instance = controller.get_instance()
        using = router.db_for_read(model_instance.__class__, instance=model_instance)
        # noinspection PyProtectedMember
        qs = field.rel.to._default_manager.using(using).filter(
                **{field.rel.field_name: value}
        )
        return qs.complex_filter(field.rel.limit_choices_to)[0]

    def to_string(self, value):
        return unicode(value)


#
# Date Fields
#
class BaseTemporalField(TextEditable):
    date_format = None

    def to_python(self, value):
        # Model cannot handle the conversion since it does not
        # know the format.
        unicode_value = force_text(value, strings_only=True)
        if isinstance(unicode_value, six.text_type):
            value = unicode_value.strip()

        if isinstance(value, six.text_type):
            try:
                return self.strptime(value, self.date_format)
            except (ValueError, TypeError):
                raise
        raise Exception('Invalid format')

    # noinspection PyShadowingBuiltins
    def strptime(self, value, format):
        field_type = self.get_model_field_type()
        val = datetime.datetime.strptime(value, format)
        if field_type == 'DateField':
            val = val.date()
        elif field_type == 'TimeField':
            val = val.time()
        else:
            val = from_current_timezone(val)
        return val

    def to_string(self, value):
        if hasattr(value, 'strftime'):
            value = datetime_safe.new_datetime(value)
            return value.strftime(self.date_format)
        return value


class DatePickerEditable(BaseTemporalField):
    field_type = 'date'

    def __init__(self, field_name=None, required=None, date_format=None, view_format=None, mode='popup'):
        TextEditable.__init__(self, field_name, required, mode)

        if date_format is None:
            date_format = formats.get_format('DATE_INPUT_FORMATS')[0]

        if view_format is None:
            view_format = date_format

        self.date_format = date_format
        self.view_format = view_format

        self.js_files = (
            # "xeditable-input-combodate-js",
            "xeditable-input-abstract-js",
            'eternicode-bootstrap-datepicker-js',
            "xeditable-input-date-js",
            "xeditable-input-datefield-js",
        )

        self.css_files = (
            'bootstrap-datepicker-css',
        )

    def generate_render_data(self):
        data = TextEditable.generate_render_data(self)
        data['format'] = eternicode_date_format(self.date_format)
        data['view_format'] = eternicode_date_format(self.view_format)
        return data
