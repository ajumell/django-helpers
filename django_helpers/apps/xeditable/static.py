# coding=utf-8
from django_helpers.apps.static_manager import register_js, register_css

source_folder = "js-plugins/x-editable-1.5.1/src/"

register_css("xeditable-editable-form-css", source_folder + "editable-form/editable-form.css")
register_css("xeditable-editable-container-css", source_folder + "containers/editable-container.css")
register_css("xeditable-editable-element-css", source_folder + "element/editable-element.css")

#
# Base
#

register_js("xeditable-editable-form-js", source_folder + "editable-form/editable-form.js", (
    'jquery',
    "xeditable-editable-form-css",
))

register_js("xeditable-editable-form-utils-js", source_folder + "editable-form/editable-form-utils.js")

register_js("xeditable-editable-container-js", source_folder + "containers/editable-container.js", dependancies=(
    "xeditable-editable-container-css",
))
register_js("xeditable-editable-inline-js", source_folder + "containers/editable-inline.js")

register_js("xeditable-editable-element-js", source_folder + "element/editable-element.js")

register_js('xeditable-base', None, (
    "xeditable-editable-form-js",
    "xeditable-editable-form-utils-js",
    "xeditable-editable-container-js",
    "xeditable-editable-inline-js",
    "xeditable-editable-element-js"
))

#
#   Standard Inputs
#

register_js("xeditable-input-abstract-js", source_folder + "inputs/abstract.js", (
    "xeditable-base",
    "xeditable-editable-element-css",
))

register_js("xeditable-input-list-js", source_folder + "inputs/list.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-text-js", source_folder + "inputs/text.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-textarea-js", source_folder + "inputs/textarea.js", (
    "xeditable-input-text-js",
))

register_js("xeditable-input-select-js", source_folder + "inputs/select.js", (
    "xeditable-input-list-js",
))

register_js("xeditable-input-checklist-js", source_folder + "inputs/checklist.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-html5types-js", source_folder + "inputs/html5types.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-select2-js", source_folder + "inputs/select2/select2.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-combodate-lib-js", source_folder + "inputs/combodate/lib/combodate.js", (
    "moment-js",
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-combodate-js", source_folder + "inputs/combodate/combodate.js", (
    "xeditable-input-combodate-lib-js",
))

#
#   Bootstrap
#

register_js("xeditable-editable-form-bootstrap-js", source_folder + "editable-form/editable-form-bootstrap.js", (
    'twitter-bootstrap-js',
    'twitter-bootstrap-css',
    'xeditable-base',
))

register_js("xeditable-editable-popover-js", source_folder + "containers/editable-popover.js", (
    'twitter-bootstrap-js',
    'twitter-bootstrap-css',
    'xeditable-base',
))

register_js('xeditable-bootstrap-base', None, (
    "xeditable-input-abstract-js",
    "xeditable-editable-form-bootstrap-js",
    "xeditable-editable-popover-js",
))

#
#   Bootstrap 3
#
register_js("xeditable-editable-form-bootstrap3-js", source_folder + "editable-form/editable-form-bootstrap3.js", (
    'twitter-bootstrap-js',
    'twitter-bootstrap-css',
))

register_js("xeditable-editable-popover3-js", source_folder + "containers/editable-popover3.js", (
    'twitter-bootstrap-js',
    'twitter-bootstrap-css',
))

register_js('xeditable-bootstrap3-base', None, (
    "xeditable-input-abstract-js",
    "xeditable-editable-form-bootstrap3-js",
    "xeditable-editable-popover3-js",
))

#
#   Extra Plugins
#

register_js("xeditable-input-date-js", source_folder + "inputs/date/date.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-datefield-js", source_folder + "inputs/date/datefield.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-datetime-js", source_folder + "inputs/datetime/datetime.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-datetimefield-js", source_folder + "inputs/datetime/datetimefield.js", (
    "xeditable-input-abstract-js",
))

register_js("xeditable-input-typeahead-js", source_folder + "inputs/typeahead.js", (
    "xeditable-input-abstract-js",
))

#
#   Django XEditable
#
register_js('django-xeditable-js', 'django-helpers/js/django.xeditable.js', (
    'jquery-django-ajax-csrf',
    'xeditable-base',
))
