jQuery(function ($) {
    var options = {{ data|safe }};
    $.dj.xEditable('{{ selector }}', '{{ selector }}', '{{ save_url }}', options);
});
