(function () {
    Dropzone.options.{{ capital_id }} = {
        paramName: "{{ file_field }}"
    };
}());