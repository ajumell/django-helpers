# coding=utf-8
from django import forms
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils import six

from django_helpers.core.views import JSONResponse
from django_helpers.apps.static_manager import register_js, register_css
from django_helpers.widgets import Form, CSRFTokenInput

register_js('drop-zone-js', 'js-plugins/drop-zone-js/dropzone.min.js')
register_js('django-drop-zone-js', 'js-plugins/drop-zone-js/dropzone.django.js', ('jquery', 'drop-zone-js'))
register_css('drop-zone-css', 'js-plugins/drop-zone-js/dropzone.min.css')
register_css('drop-zone-basic-css', 'js-plugins/drop-zone-js/basic.css')


class DropZoneMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        from views import register_name

        obj = type.__new__(mcs, name, bases, attrs)
        module_name = attrs['__module__']
        class_name = module_name + '.' + name
        table_id = attrs.get('html_id', None)
        params = attrs.get('url_search_parameters', None)

        if params is None:
            for b in bases:
                params = getattr(b, 'url_search_parameters', None)
                if params is not None:
                    break

        register_name(class_name, table_id, params)
        return obj


# noinspection PyMethodMayBeStatic
class DropZone(six.with_metaclass(DropZoneMetaClass, Form)):
    model = None

    html_id = None

    file_field = None

    form = None

    key = 'drop-zone-form'

    hidden_fields = None

    url_search_parameters = None

    def __init__(self, request, **kwargs):
        Form.__init__(self)
        self.request = request
        self.extra_fields = kwargs

        self.kwargs = kwargs

        self._html_id = None

        self.had_setup = False
        self.had_setup_id = False

    # noinspection PyAttributeOutsideInit
    def setup_static(self):
        self.js_files = ['django-drop-zone-js']
        self.css_files = ['drop-zone-css', 'drop-zone-basic-css']

    #
    # Manual Params
    #
    def set_kwargs(self, kwargs):
        self.kwargs = kwargs
        self.had_setup_id = False
        self.setup_id()

        if self.had_setup:
            html_id = self.get_html_id()
            self.attr('id', html_id)

        self.form = None

    #
    # IDs
    #

    def get_html_id(self):
        if not self.had_setup_id:
            self.setup_id()

        return self._html_id

    def setup_id(self):
        if self.had_setup_id:
            return

        html_id = self.html_id
        if self.kwargs:
            for k, v in self.kwargs.items():
                html_id = "{}-{}-{}".format(html_id, k, v)

        capital_id = html_id
        capital_id = capital_id.replace("_", " ")
        capital_id = capital_id.replace("-", " ")
        capital_id = capital_id.title()
        capital_id = capital_id.replace(" ", "")
        first_letter = capital_id[0:1]
        remaining = capital_id[1:]

        self._html_id = html_id
        self.had_setup_id = True
        self._capital_id = first_letter.lower() + remaining

    #
    # Render
    #

    def render(self):
        self.render_setup()
        html = Form.render(self)
        return html

    def render_children(self):
        html = [Form.render_children(self)]

        form = self.create_form()
        hidden_fields = form.hidden_fields()
        # noinspection PyTypeChecker
        for field in hidden_fields:
            html.append(str(field))

        return '\n'.join(html)

    def render_setup(self):
        if self.had_setup:
            return

        request = self.request
        csrf_token_input = CSRFTokenInput(request)
        self.add(csrf_token_input)
        html_id = self.get_html_id()

        self.attr('id', html_id)

        if self.key:
            self.data(self.key, html_id)

        self.attr('action', self.save_url())
        self.data('file-field', self.file_field)

        self.had_setup = True

    #
    # Save
    #
    def save_url(self, kwargs=None):
        url_kwargs = {}
        parameters = self.url_search_parameters

        if kwargs is None:
            kwargs = self.kwargs

        for arg in parameters:
            url_kwargs[arg] = kwargs[arg]

        return reverse(self.html_id + '-save', kwargs=url_kwargs)

    def save(self):
        request = self.request
        files = request.FILES
        post = request.POST
        kwargs = self.kwargs

        parameters = self.url_search_parameters
        instance = None

        if parameters:
            model = self.model
            query_args = {}
            manager = getattr(model, '_default_manager')
            for parameter in parameters:
                query_args[parameter] = kwargs[parameter]
            instance = manager.get(**query_args)

        form_cls = self.get_form()
        form = form_cls(post, files, instance=instance)

        if form.is_valid():
            obj = self.save_form(form)
            return self.render_success(obj)
        else:
            return HttpResponse('FAIL', status=500)

    def process_success_json(self, json):
        pass

    def render_success(self, obj):
        response = self.success_message(obj)

        if response:
            messages.success(self.request, response)

        json = JSONResponse(self.request)
        self.process_success_json(json)
        json.refresh_table()

        return json

    def save_form(self, form):
        obj = form.save()
        return obj

    def success_message(self, obj):
        return 'File uploaded successfully.'

    #
    # Form
    #

    def create_form(self):
        if self.form is not None:
            return self.form

        kwargs = self.kwargs
        form_cls = self.get_form()

        if kwargs:
            form = form_cls(initial=kwargs)
        else:
            form = form_cls()

        self.form = form

        return form

    @classmethod
    def get_form(cls):
        form = getattr(cls, 'form_cls', None)
        if form is not None:
            return form

        file_field = cls.file_field
        form_fields = [file_field]
        form_widgets = {}
        hidden_fields = cls.hidden_fields

        if hidden_fields:
            for field_name in hidden_fields:
                form_fields.append(field_name)
                form_widgets[field_name] = forms.HiddenInput

        class Form(forms.ModelForm):
            class Meta:
                model = cls.model
                fields = form_fields
                widgets = form_widgets

        cls.form_cls = Form

        return Form

    def __repr__(self):
        return 'DropZone'
