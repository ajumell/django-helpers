# coding=utf-8
from django.conf.urls import url
from django.http import HttpResponse
from django_helpers.utils.importlib import import_module

import urls
from django_helpers import add_app_url

add_app_url('drop-zones', urls)

_URL_PREFIX = 'drop-zone-'

_REGISTERED_DROP_ZONES = {}


def has_registered(name):
    return name in _REGISTERED_DROP_ZONES


def get_zone(name, request, **kwargs):
    obj = _REGISTERED_DROP_ZONES.get(name)

    if isinstance(obj, str):
        i = obj.rfind('.')
        module_name = obj[:i]
        name = obj[i + 1:]
        module_obj = import_module(module_name)
        obj = getattr(module_obj, name)

    if getattr(obj, 'url_search_parameters', False):
        instance = obj(request, **kwargs)
    else:
        instance = obj(request)
    return instance


def save(request, name, **kwargs):
    zone = get_zone(name, request, **kwargs)
    if zone is None:
        return HttpResponse()

    results = zone.save()
    return results


def create_reg(name):
    return "(?P<%s>.+?)/" % name


def register_name(class_name, name, url_search_parameters):
    if name is None:
        return

    if name in _REGISTERED_DROP_ZONES:
        old_name = _REGISTERED_DROP_ZONES[name]

        msg = "You tried to register a drop zone with id " + name + " for " + class_name + ".\n"
        msg += "But " + name + " is already registered for " + old_name + ".\n"
        msg += "Try giving a new ID for " + class_name

        raise Exception(msg)

    _REGISTERED_DROP_ZONES[name] = class_name

    save_reg = "^%s/save/" % name

    extra_url_parameters = url_search_parameters
    if extra_url_parameters is not None:
        for parameter in extra_url_parameters:
            save_reg += create_reg(parameter)

    save_reg += "$"

    save_pattern = url(r"%s" % save_reg, save, name=name + '-save', kwargs={
        "name": name
    })

    urls.urlpatterns.append(save_pattern)
