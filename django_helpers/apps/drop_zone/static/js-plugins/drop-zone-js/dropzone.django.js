jQuery && jQuery(function ($) {
    var dj = $.dj;

    var generateDataOptions = $.dj.generateDataOptions;
    var registerDataWidget = $.dj.registerDataWidget;
    var fnGetOptions;

    if (!Dropzone) {
        return;
    }

    fnGetOptions = function (obj, mappings) {
        var options;

        options = generateDataOptions(obj, mappings);
        options['success'] = function (ev, data) {
            try {
                dj.fnRunAJAXPreProcessors(data);
                dj.fnRunElementAJAXPreProcessors(obj, data);
            } catch (e) {
            }

        };

        options['headers'] = {
            'X-CSRFToken': window.csrftoken
        };

        options['previewTemplate'] = '<div style="display: none;"></div>';
        options['createImageThumbnails'] = false;

        return options;
    };


    registerDataWidget('drop-zone-form', function (obj) {
        var options,
            mappings;

        mappings = {
            "dropZoneForm": "drop_zone_id",
            "fileField": "paramName"
        };

        options = fnGetOptions(obj, mappings);
        new Dropzone("#" + options['drop_zone_id'], options);


    });


    registerDataWidget('drop-zone-button', function (obj) {
        var options,
            mappings;

        mappings = {
            "dropZoneUrl": "url",
            "fileField": "paramName"
        };

        options = fnGetOptions(obj, mappings);
        options['clickable'] = obj[0];
        new Dropzone(obj[0], options);
    });


    $(document).on('click', 'a[data-drop-zone-button]', function () {
        return false;
    });

});