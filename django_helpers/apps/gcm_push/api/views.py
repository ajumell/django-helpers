from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from django_helpers.helpers.views import render_json as render_json_original


def render_json(request, d):
    if settings.DEBUG:
        d['http_method'] = request.method
        d['http_post'] = request.POST
        d['http_get'] = request.GET

    return render_json_original(d)


@csrf_exempt
def register(request):
    from django_helpers.apps.gcm_push.models import GCMUser
    gt = request.POST.get
    token = gt('token')
    device_id = gt('device_id')

    GCMUser.add(token, device_id, request.user)

    return render_json(request, {
        'err': False,
        'msg': 'User logged in successfully.',

    })
