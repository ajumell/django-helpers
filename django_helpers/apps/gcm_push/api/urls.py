# coding=utf-8
from django.conf.urls import url

import views

urlpatterns = [
    url(r'^gcm/register/$', views.register, name='api-gcm-register'),
]
