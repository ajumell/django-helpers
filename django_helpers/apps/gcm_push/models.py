from django.core.exceptions import ValidationError
from django.db import models
from django.conf import settings


class GCMUserManager(models.Manager):
    def send_data(self, data):
        from gcm import GCM

        tokens = self.values_list("token")

        api_key = getattr(settings, 'GCM_API_KEY', None)

        if api_key is None:
            raise ValidationError("API Key is missing.")

        gcm = GCM(api_key)
        gcm.json_request(registration_ids=tokens, data=data)


class GCMUser(models.Model):
    token = models.CharField(max_length=2048)
    device_od = models.CharField(max_length=64)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)

    objects = GCMUserManager()

    @classmethod
    def add(cls, token, device_od, user):

        try:
            instance = GCMUser.objects.get(device_od=device_od)
        except GCMUser.DoesNotExist:
            instance = GCMUser(device_od=device_od)

        instance.token = token

        if user and user.is_authenticated():
            instance.user = user

        instance.save()

    @classmethod
    def set_user(cls, device_od, user):
        try:
            instance = GCMUser.objects.get(device_od=device_od)
        except GCMUser.DoesNotExist:
            instance = GCMUser(device_od=device_od)

        if user and user.is_authenticated():
            instance.user = user

        instance.save()
