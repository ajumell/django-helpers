# coding=utf-8
"""
Views and functions for serving static files. These are only to be used
during development, and SHOULD NOT be used in a production setting.
"""
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.shortcuts import render


def offline(request, app_name=None):
    try:
        redirect_url = reverse('home')
    except:
        redirect_url = '/'

    return render(request, 'static-manager/offline.html', {
        "app_name": app_name,
        'redirect': redirect_url
    })
