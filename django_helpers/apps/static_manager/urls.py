# coding=utf-8
from django.conf.urls import patterns, url

import views

urlpatterns = [
    url(r'^(?P<app_name>.*)/offline.html$', views.offline),
    url(r'^offline.html$', views.offline),
]
