jQuery && jQuery(function ($) {
    var fnMakeMaterial = function (target) {
        var $material = $('.material-control', target);

        function toggle_label(elem, val) {
            $(elem).parents('.material-form-group').toggleClass('focused', val);
        }

        $material.on('focus blur', function (e) {
            var val = (e.type === 'focus' || this.value.length > 0);
            toggle_label(this, val);
        })

        setTimeout(function () {
            $material.each(function () {
                toggle_label(this, this.value.length > 0);
            });
        }, 300);
    };

    $(document).on('content-changed', "body", function (ev) {
        fnMakeMaterial(ev.target);
    });
    fnMakeMaterial(document);

});