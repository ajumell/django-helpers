jQuery && jQuery(function ($) {

    if ($.dj && $.dj.modal) {

        $.dj.modal.setAdapter(function (title, content, buttons) {

            var box = bootbox.dialog({
                message: content,
                title: title,
                buttons: buttons,
                onEscape: true
            });

            return {
                'close': function () {
                    return box.modal('hide');
                },

                'header': box.find('.modal-header'),
                'footer': box.find('.modal-footer'),
                'body': box.find('.modal-body .bootbox-body')
            }
        });
    }

});