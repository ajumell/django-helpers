jQuery && jQuery(function ($) {

    var dj;

    dj = $.dj;

    dj.confirm.setAdapter(function (title, question, buttons, yes_callback, no_callback) {
        var
            no_label = buttons['no_label'] || "No",
            yes_label = buttons['yes_label'] || "Yes",

            no_color = buttons['no_color'] || "btn-danger",
            yes_color = buttons['yes_color'] || "yes-danger";

        bootbox.dialog({
            message: question,
            title: title,
            buttons: {
                yes: {
                    label: yes_label,
                    className: yes_color,
                    callback: yes_callback
                },
                no: {
                    label: no_label,
                    className: no_color,
                    callback: no_callback
                }
            }
        });
    })


});