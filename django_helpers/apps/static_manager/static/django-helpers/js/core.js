/*
 * jQuery Hotkeys Plugin
 * Copyright 2010, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Based upon the plugin by Tzury Bar Yochay:
 * https://github.com/tzuryby/jquery.hotkeys
 *
 * Original idea by:
 * Binny V A, http://www.openjs.com/scripts/events/keyboard_shortcuts/
 */
!function(t){function e(e){if("string"==typeof e.data&&(e.data={keys:e.data}),e.data&&e.data.keys&&"string"==typeof e.data.keys){var a=e.handler,s=e.data.keys.toLowerCase().split(" ");e.handler=function(e){if(this===e.target||!(t.hotkeys.options.filterInputAcceptingElements&&t.hotkeys.textInputTypes.test(e.target.nodeName)||t.hotkeys.options.filterContentEditable&&t(e.target).attr("contenteditable")||t.hotkeys.options.filterTextInputs&&t.inArray(e.target.type,t.hotkeys.textAcceptingInputTypes)>-1)){var n="keypress"!==e.type&&t.hotkeys.specialKeys[e.which],i=String.fromCharCode(e.which).toLowerCase(),r="",o={};t.each(["alt","ctrl","shift"],function(t,a){e[a+"Key"]&&n!==a&&(r+=a+"+")}),e.metaKey&&!e.ctrlKey&&"meta"!==n&&(r+="meta+"),e.metaKey&&"meta"!==n&&r.indexOf("alt+ctrl+shift+")>-1&&(r=r.replace("alt+ctrl+shift+","hyper+")),n?o[r+n]=!0:(o[r+i]=!0,o[r+t.hotkeys.shiftNums[i]]=!0,"shift+"===r&&(o[t.hotkeys.shiftNums[i]]=!0));for(var p=0,l=s.length;l>p;p++)if(o[s[p]])return a.apply(this,arguments)}}}}t.hotkeys={version:"0.2.0",specialKeys:{8:"backspace",9:"tab",10:"return",13:"return",16:"shift",17:"ctrl",18:"alt",19:"pause",20:"capslock",27:"esc",32:"space",33:"pageup",34:"pagedown",35:"end",36:"home",37:"left",38:"up",39:"right",40:"down",45:"insert",46:"del",59:";",61:"=",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9",106:"*",107:"+",109:"-",110:".",111:"/",112:"f1",113:"f2",114:"f3",115:"f4",116:"f5",117:"f6",118:"f7",119:"f8",120:"f9",121:"f10",122:"f11",123:"f12",144:"numlock",145:"scroll",173:"-",186:";",187:"=",188:",",189:"-",190:".",191:"/",192:"`",219:"[",220:"\\",221:"]",222:"'"},shiftNums:{"`":"~",1:"!",2:"@",3:"#",4:"$",5:"%",6:"^",7:"&",8:"*",9:"(",0:")","-":"_","=":"+",";":": ","'":'"',",":"<",".":">","/":"?","\\":"|"},textAcceptingInputTypes:["text","password","number","email","url","range","date","month","week","time","datetime","datetime-local","search","color","tel"],textInputTypes:/textarea|input|select/i,options:{filterInputAcceptingElements:!0,filterTextInputs:!0,filterContentEditable:!0}},t.each(["keydown","keyup","keypress"],function(){t.event.special[this]={add:e}})}(jQuery||this.jQuery||window.jQuery);


jQuery && jQuery(function ($) {
    var navigate;
    var dj;
    var registerDataWidget;
    var generateDataOptions;
    var getConfirmButtons;
    var fnConfirmAction;
    var fnAjaxForms;
    var refreshParentTable;
    var fnRunAJAXPreProcessors;
    var fnRunElementAJAXPreProcessors;
    var startsWith;

    if ($.dj === undefined) {
        $.dj = {};
    }

    dj = $.dj;


    startsWith = function (a, s) {
        return a.slice(0, s.length) === s;
    };

    navigate = function (url) {
        window.location = url;
    };

    registerDataWidget = function (attribute, converter, completeCallback) {
        var convert_all = function (target) {
            var selector, already_widget_selector;
            selector = "[data-" + attribute + "]";
            already_widget_selector = 'is-already-' + attribute;
            $(selector, target).each(function () {
                var obj = $(this);
                if (!obj.data(already_widget_selector)) {
                    converter(obj);
                    obj.data(already_widget_selector, true);
                }
            });
        };

        $(document).on('content-changed', function (ev) {
            convert_all(ev.target);
        });

        convert_all(document);

        if (completeCallback) {
            completeCallback();
        }
    };

    generateDataOptions = function (obj, mappings) {
        var options, key, mapping, value, data;
        options = {};
        data = obj.data();
        for (key in mappings) {
            if (mappings.hasOwnProperty(key)) {
                mapping = mappings[key];
                value = data[key];
                if (value == "True") {
                    options[mapping] = true;
                } else if (value == "False") {
                    options[mapping] = false;
                } else if (value) {
                    options[mapping] = value;
                }
            }
        }
        return options;
    };

    getConfirmButtons = function (dElement) {
        var
            yes_label = dElement.data('confirm-yes-text'),
            yes_color = "btn-" + dElement.data('confirm-yes-color'),
            no_label = dElement.data('confirm-no-text'),
            no_color = "btn-" + dElement.data('confirm-no-color');

        return {
            yes_color: yes_color,
            yes_label: yes_label,
            no_color: no_color,
            no_label: no_label
        };

    };

    fnConfirmAction = function (dElement, fnYesCallback, fnNoCallback) {
        var
            oConfirmButtons,
            sQuestion,
            sTitle;

        if (fnNoCallback == undefined) {
            fnNoCallback = function () {
            };
        }

        sQuestion = dElement.data('confirm-question');

        if (sQuestion) {
            sTitle = dElement.data('confirm-title');

            oConfirmButtons = getConfirmButtons(dElement);

            dj.confirm.show(sTitle, sQuestion, oConfirmButtons, fnYesCallback, fnNoCallback)

        } else {
            fnYesCallback();
        }
    };

    refreshParentTable = function ($this) {
        var table, oTable;

        if (!$this) {
            return;
        }

        table = $this.parents('table');
        if (table.length) {
            oTable = table.data("oTable");
            if (oTable) {
                oTable.fnReloadAjax(null, null, true)
            }
        }
    };

    /*
     *
     * Custom Modules
     *
     * */

    // Display Toggle
    (function () {
        $("[data-display-toggle]").on('click', function () {
            var sSelector,
                jqElement,
                jqTarget;

            jqTarget = $(this);
            sSelector = jqTarget.attr('data-display-toggle');
            jqElement = $(sSelector);


            jqElement.toggle();

        });
        $("[data-fade-toggle]").on('click', function () {
            var sSelector,
                jqElement,
                jqTarget;

            jqTarget = $(this);
            sSelector = jqTarget.attr('data-fade-toggle');
            jqElement = $(sSelector);


            jqElement.fadeToggle();

        });
        $("[data-slide-toggle]").on('click', function () {
            var sSelector,
                jqElement,
                jqTarget;

            jqTarget = $(this);
            sSelector = jqTarget.attr('data-slide-toggle');
            jqElement = $(sSelector);


            jqElement.slideToggle();

        });
    }());

    // Design Controls
    (function () {
        var jqWindow = $(window);
        var jqBody = $('body');
        var jqFullScreenElements = $("[data-full-page]");
        var fnResize = function () {
            var iWidth = jqBody.width();
            var iHeight = jqBody.height();

            jqFullScreenElements.height(iHeight);
            jqFullScreenElements.width(iWidth);
        };

        jqWindow.on('resize', fnResize);
        fnResize();
    }());

    // Confirm
    (function () {

        var currentAdapter;

        dj.confirm = dj.confirm || {};

        dj.confirm.setAdapter = function (adapter) {
            currentAdapter = adapter;
        };

        dj.confirm.show = function (title, question, params, yes_callback, no_callback) {
            if (currentAdapter) {
                currentAdapter(title, question, params, yes_callback, no_callback);
            }
        };

    }());

    // Messages
    (function () {

        var currentAdapter;

        dj.messages = dj.messages || {};

        dj.messages.setAdapter = function (adapter) {
            currentAdapter = adapter;
        };

        dj.messages.show = function (title, msg, tags) {
            if (currentAdapter) {
                currentAdapter(title, msg, tags);
            }
        };

        dj.messages.showSuccess = function (title, msg) {
            dj.messages.show(title, msg, "success");
        };

        dj.messages.showError = function (title, msg) {
            dj.messages.show(title, msg, "error");
        };

        dj.messages.showWarning = function (title, msg) {
            dj.messages.show(title, msg, "warning");
        };

    }());

    // AJAX Buttons
    (function () {
        var performAjax, actions;

        actions = {};

        dj.ajaxButtons = dj.ajaxButtons || {};

        dj.ajaxButtons.addAction = function (name, fn) {
            actions[name] = fn;
        }

        dj.ajaxButtons.removeAction = function (name) {
            actions[name] = undefined;
        }

        performAjax = function (url, $this, processing, original) {
            var original_trimmed = original.trim();
            var can_set_text = original_trimmed && original_trimmed !== "";

            if (can_set_text) {
                $this.text(processing);
            }

            $.ajax({
                url: url,
                dataType: 'json',
                success: function (response) {
                    if (can_set_text) {
                        $this.text(original);
                    }

                    (function (response) {
                        var parent_selector;
                        var parent;
                        if (response['remove-item'] === true) {

                            parent_selector = $this.data('parent-selector');
                            if (!parent_selector) {
                                parent_selector = "[data-listing-item]";
                            }

                            parent = $this.parents(parent_selector);
                            if (parent.length) {
                                parent.fadeOut('slow', function () {
                                    parent.remove();
                                });
                            }
                        }
                    }(response));

                    (function (response) {
                        fnRunElementAJAXPreProcessors($this, response);
                    }(response));
                }
            });
        };

        (function () {
            var selector = 'a[data-has-ajax]';
            $(document).on('click', selector, function () {

                var yes_callback,
                    no_callback,
                    title,
                    question,
                    processing,
                    url,
                    $this,
                    original,
                    confirm_buttons;

                url = this.href;
                $this = $(this);
                original = $this.text();
                processing = $this.data('ajax-processing');
                question = $this.data('confirm-question');

                if (question) {
                    title = $this.data('confirm-title');

                    confirm_buttons = getConfirmButtons($this);

                    no_callback = function () {
                    };

                    yes_callback = function () {
                        performAjax(url, $this, processing, original);
                    };

                    dj.confirm.show(title, question, confirm_buttons, yes_callback, no_callback)

                } else {
                    performAjax(url, $this, processing, original);
                }
                return false;
            });
        }());
    }());

    // Confirm Buttons
    (function () {

        /*TODO: Add support for Other tags*/
        var selector = 'a[data-confirm-question]';
        $(document).on('click', selector, function () {

            var yes_callback,
                no_callback,
                title,
                question,
                url,
                $this,
                confirm_buttons;

            url = this.href;

            $this = $(this);

            if ($this.data('has-ajax') || $this.data('show-modal')) {
                /* If there is AJAX then this button will be processed by AJAX Buttons*/
                return;
            }

            question = $this.data('confirm-question');

            title = $this.data('confirm-title');

            no_callback = function () {
            };

            yes_callback = function () {
                navigate(url);
            };

            confirm_buttons = getConfirmButtons($this);

            dj.confirm.show(title, question, confirm_buttons, yes_callback, no_callback)


            return false;
        });

    }());

    // AJAX Pre Processors
    (function () {

        var oPreProcessors;
        var oElementPreProcessors;
        var refresh_table;
        var process_redirect;
        var process_messages;
        var keys_redirect = "dj-redirect";
        var keys_messages = "dj-messages";
        var keys_refresh_tables = "dj-refresh-tables";

        fnRunAJAXPreProcessors = function (data) {
            $.each(oPreProcessors, function (sName, fnCallback) {
                fnCallback(data);
            });
        };

        fnRunElementAJAXPreProcessors = function (dElement, data) {
            $.each(oElementPreProcessors, function (sName, fnCallback) {
                if (data[sName]) {
                    fnCallback(dElement);
                }
            });
        };

        process_messages = function (data) {
            var count, i,
                message,
                messages = data[keys_messages];
            if (messages) {
                count = messages.length;
                for (i = 0; i < count; i += 1) {
                    message = messages[i];
                    dj.messages.show(undefined, message.message, message.tags);
                }
            }
        };

        process_redirect = function (data) {
            var url = data[keys_redirect];
            if (url) {
                window.location = url;
            }
        };

        refresh_table = function (data) {
            var count, i,
                table_id,
                table,
                oTable,
                tables = data[keys_refresh_tables];

            if (tables) {
                count = tables.length;
                for (i = 0; i < count; i += 1) {
                    table_id = tables[i];
                    table = $("#" + table_id);
                    if (table.length) {
                        oTable = table.data("oTable");
                        if (oTable) {
                            oTable.fnReloadAjax(null, null, true)
                        }
                    }
                }
            }
        };

        oPreProcessors = {
            'messages': process_messages,
            'redirect': process_redirect,
            'refresh_table': refresh_table
        };

        oElementPreProcessors = {
            'dj-refresh-table': function (dElement) {
                refreshParentTable(dElement);
            }
        }

        dj.fnRegisterPreProcessor = function (sName, fnPreProcessor) {
            if (oPreProcessors[sName]) {
                throw new Error("Preprocessor already registered.");
            }
            oPreProcessors[sName] = fnPreProcessor;
        };

        dj.fnUnRegisterPreProcessor = function (sName) {
            oPreProcessors[sName] = undefined;
        };

        dj.fnRegisterElementPreProcessor = function (sName, fnPreProcessor) {
            if (oElementPreProcessors[sName]) {
                throw new Error("Preprocessor already registered.");
            }
            oElementPreProcessors[sName] = fnPreProcessor;
        };

        dj.fnUnRegisterElementPreProcessor = function (sName) {
            oElementPreProcessors[sName] = undefined;
        };

        $(document).ajaxSuccess(function (evt, jqXHR, ajaxOptions, data) {
            if (ajaxOptions.dataType === "json") {
                fnRunAJAXPreProcessors(data);
            }
        });

    }());

    // Modal
    (function () {

        var dj, currentAdapter, show, showAjax;

        dj = $.dj;

        dj.modal = dj.modal || {};

        dj.modal.setAdapter = function (adapter) {
            currentAdapter = adapter;
        };

        show = function (title, content, buttons, invoker) {
            if (currentAdapter) {
                var modal = currentAdapter(title, content, buttons, invoker);
                // Give a small waiting time for the modal to show
                setTimeout(function () {
                    modal.header.contentUpdated();
                    modal.footer.contentUpdated();
                    modal.body.contentUpdated();
                }, 200);

                return modal;
            }
        };

        showAjax = dj.modal.showAJAX = function (title, url, buttons, loading_message, invoker) {
            if (!loading_message) {
                loading_message = 'Loading...'
            }

            var modal = show(title, loading_message, buttons, invoker);
            var body = modal.body;

            $.ajax({
                url: url,
                headers: {'dj-modal': true},
                success: function (response) {
                    body.html(response);
                    // Give a small waiting time for the modal to show
                    setTimeout(function () {
                        body.contentUpdated();
                        // Find forms and attach ajax events
                        body.find('form').each(function () {
                            fnAjaxForms($(this), undefined, modal, invoker);
                        });
                    }, 200);


                }
            });
        };

        dj.modal.show = show;

        (function () {

            var showModal = function ($this) {
                var bIsAjax;
                var sTitle;
                var sContent;
                var sUrl;
                var sLoading;

                bIsAjax = $this.data('ajax-modal');
                sTitle = $this.data('modal-title')

                if (bIsAjax) {
                    sLoading = $this.data('ajax-loading-content');
                    sUrl = $this.attr('href');
                    showAjax(sTitle, sUrl, undefined, sLoading, $this);
                } else {
                    sContent = $this.data('modal-content')
                    show(sTitle, sContent, undefined, $this);
                }

            };

            $(document).on('click', 'a[data-show-modal]', function () {
                var dElement,
                    fnYesCallback;

                dElement = $(this);

                fnYesCallback = function () {
                    showModal(dElement);
                };

                fnConfirmAction(dElement, fnYesCallback);

                return false;
            });
        }());

        dj.fnRegisterPreProcessor('show-modal', function (response) {
            var data = response['dj-open-modal'];
            if (data) {
                showAjax(data['title'], data['url']);
            }
        });


    }());

    // Data Binding
    (function () {
        /**
         * Simple HTML Only Data Binding
         *
         * Init
         * 1. Find all data bound elements
         * 2. Attach events onChange, contentChanged, keyDown
         * 3. When the event occures find the other bounded elements
         * 4. Change the value of the other bounded elements accordingly
         */
        var bInit,
            fnFindBindingParams,
            fnContentChanged,
            fnDataChanged,
            fnActionValue,
            fnActionHide,
            fnActionAddClass;

        fnFindBindingParams = function (dElement) {
            var sSelector,
                aAttributes,
                i,
                len,
                sAttribute,
                sAttributeName,
                sAttributeValue;

            sSelector = "";

            aAttributes = dElement.attributes;
            for (i = 0, len = aAttributes.length; i < len; i++) {
                sAttribute = aAttributes[i];
                sAttributeName = sAttribute.name;
                sAttributeValue = sAttribute.value;
                if (startsWith(sAttributeName, 'data-dom-binding-param')) {
                    sSelector += "[" + sAttributeName + "=" + sAttributeValue + "]";
                }
            }

            return sSelector;
        };

        fnActionValue = function (dElement, jqElement, sValue, sBindingParamSelector) {
            var sSelector, ajqElements;

            if (!bInit) {
                return;
            }

            sSelector = jqElement.data('dom-binding-value-selector');
            if (!sSelector) {
                sSelector = "[data-dom-binding=\"" + jqElement.data("dom-binding") + "\"]";
                sSelector += sBindingParamSelector;
                sSelector += "[data-dom-action-value]"
                jqElement.data('dom-binding-value-selector', sSelector);
            }

            ajqElements = $(sSelector);

            ajqElements.each(function () {
                var
                    jqChildElement,
                    sFunctionName;

                if (dElement == this) {
                    return;
                }

                jqChildElement = $(this);
                sFunctionName = jqChildElement.data('dom-binding-function-name');
                if (!sFunctionName) {
                    if (jqChildElement.is(":checkbox, :radio, :input, select")) {
                        sFunctionName = 'val';
                    } else {
                        sFunctionName = 'html';
                    }
                    jqChildElement.data('dom-binding-function-name', sFunctionName);
                }

                if (sFunctionName == 'val') {
                    if (jqChildElement.is(":checkbox, :radio")) {
                        if (jqChildElement.val() == sValue) {
                            jqChildElement.prop('checked', true);
                        }
                    } else {
                        jqChildElement[sFunctionName](sValue);
                    }
                } else {
                    jqChildElement[sFunctionName](sValue);
                }
            });
        };

        fnActionHide = function (dElement, jqElement, sValue, sBindingParamSelector) {
            var sSelector, ajqElements;

            sSelector = jqElement.data('dom-binding-hide-selector');
            if (!sSelector) {
                sSelector = "[data-dom-binding=\"" + jqElement.data("dom-binding") + "\"]";
                sSelector += sBindingParamSelector;
                sSelector += "[data-dom-action-hide]"
                jqElement.data('dom-binding-hide-selector', sSelector);
            }


            ajqElements = $(sSelector);

            ajqElements.each(function () {
                var jqChildElement;

                if (dElement == this) {
                    return;
                }

                jqChildElement = $(this);

                if (jqChildElement.data("dom-action-hide") == sValue) {
                    jqChildElement.hide();
                } else {
                    jqChildElement.show();
                }

            });
        };


        fnDataChanged = function (ev) {
            var
                dElement,
                jqElement,
                sValue,
                sBindingParamSelector;

            dElement = ev.target;
            if (!dElement) {
                dElement = ev;
            }
            jqElement = $(dElement);

            sBindingParamSelector = fnFindBindingParams(dElement);


            if (jqElement.is(":checkbox, :radio, :input, select")) {
                sValue = jqElement.val();
            } else {
                sValue = jqElement.html();
            }

            fnActionValue(dElement, jqElement, sValue, sBindingParamSelector);
            fnActionHide(dElement, jqElement, sValue, sBindingParamSelector);
        };

        fnContentChanged = function (ev) {
            var jqElements,
                dTarget;

            if (ev) {
                dTarget = ev.target;
            } else {
                dTarget = document.body;
            }

            jqElements = $('[data-dom-binding]', dTarget);
            jqElements.each(function () {
                fnDataChanged(this);
            });
        };

        registerDataWidget('dom-binding', function (obj) {
            if (obj.is(":checkbox, :radio, select")) {
                obj.on('change', fnDataChanged);
            } else if (obj.is(":input")) {
                obj.on('keyup', fnDataChanged);
            }

            var aAttributes,
                i,
                len,
                sAttribute,
                sAttributeName;

            aAttributes = obj[0].attributes;
            for (i = 0, len = aAttributes.length; i < len; i++) {
                sAttribute = aAttributes[i];
                sAttributeName = sAttribute.name;
                if (startsWith(sAttributeName, 'data-dom-action')) {
                    return;
                }
            }
            obj.attr('data-dom-action-value', true);


        }, function () {
            $(document).on('content-changed', "body", fnContentChanged);
            fnContentChanged();
            bInit = true;
        });


    }());

    // Widgets
    (function () {
        dj.fnRegisterPreProcessor('dj-update-widget', function (data) {
            var oWidgets = data["dj-update-widgets"];

            if (oWidgets) {
                $.each(oWidgets, function (key, value) {
                    var jqElements = $("[data-widget-id=" + key + "]");
                    if (jqElements) {
                        jqElements.html(value);
                    }
                });
            }
        });

    }());

    fnAjaxForms = function ($form, url, modal, invoker) {

        var oValidator;
        var fnSubmitHandler;
        var submit_button;
        submit_button = $form.find('[type=submit]');

        if ($form.attr('action')) {
            url = $form.attr('action');
        }

        fnSubmitHandler = function (form) {
            var fnErrorHandler;
            var fnSuccessHandler;
            var formData;

            $form.fadeTo(200, 0.3);
            submit_button.button("loading");
            formData = new FormData(form);
            fnSuccessHandler = function (data) {

                var error_messages = data.errors,
                    field,
                    fieldName,
                    errorMessage,
                    errorPlacement = $form.data("validator");

                $form.stop().fadeTo(200, 1);

                if (data.success) {
                    if (data['dj-reset-form']) {
                        $form[0].reset();
                    }

                    if (data['dj-close-modal'] && modal && modal.close) {
                        modal.close();
                    }

                    fnRunElementAJAXPreProcessors(invoker, data);


                } else {
                    for (fieldName in error_messages) {
                        if (error_messages.hasOwnProperty(fieldName)) {
                            errorMessage = error_messages[fieldName].join("<br />");
                            field = $form.find("[name=" + fieldName + "]")[0];

                            errorPlacement.showLabel(field, errorMessage);
                        }
                    }
                }
                submit_button.button("reset");
            };
            fnErrorHandler = function (data) {
                $form.stop().fadeTo(200, 1);
                submit_button.button("reset");
                dj.messages.showError("Error", data.message);
            };

            $.ajax({
                url: url,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
                type: 'POST',

                success: fnSuccessHandler,
                error: fnErrorHandler
            });
            return false;
        };
        oValidator = $form.data('validator');
        if (oValidator) {
            oValidator.settings.submitHandler = fnSubmitHandler
        }
        else {
            $form.on('submit', function () {
                fnSubmitHandler($form[0]);
                return false;
            });
        }

        setTimeout(function(){
            $form.find(":input:visible:enabled:first").focus();
        }, 300);

    };

    // AJAX Forms
    (function () {
        registerDataWidget('ajax-form', function (obj) {
            if (!obj.is("form")) {
                return;
            }

            fnAjaxForms(obj);
        });
    }());

    // Console.log
    (function () {
        dj.fnRegisterPreProcessor("console.log", function (data) {
            if (!console && !console.log) {
                return;
            }

            var oWidgets = data["console-log"];

            if (oWidgets) {
                $.each(oWidgets, function (string, value) {
                    console.log(string);
                });
            }
        });
    }());

    // Hot Keys
    (function () {
        registerDataWidget('hot-key', function(obj) {
            var keys = obj.data('hotKey');
            $(document).bind('keydown', keys, function(){
                if(obj.is(":visible")) {
                    obj.click();
                }
            });
        });
    }());

    // Template
    (function () {
    }());

    dj.navigate = navigate;
    dj.registerDataWidget = registerDataWidget;
    dj.fnRunAJAXPreProcessors = fnRunAJAXPreProcessors;
    dj.fnRunElementAJAXPreProcessors = fnRunElementAJAXPreProcessors;
    dj.refreshParentTable = refreshParentTable;
    dj.generateDataOptions = generateDataOptions;

    /* jQuery Plugins */
    $.fn.contentUpdated = function () {
        this.trigger('content-changed');
    };
});