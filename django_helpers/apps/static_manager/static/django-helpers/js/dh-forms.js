jQuery && jQuery(function ($) {

    var registerDataWidget;
    var generateDataOptions;
    var dj;

    dj = $.dj;
    registerDataWidget = dj.registerDataWidget;
    generateDataOptions = dj.generateDataOptions;

    if ($.fn.clockpicker) {
        registerDataWidget('clock-picker', function (obj) {
            obj.clockpicker();
        })
    }

    if ($.fn.datepicker) {
        registerDataWidget('bootstrap-date-picker', function (obj) {

            var options,
                mappings;

            mappings = {
                "week-start": "weekStart",
                "calendar-weeks": "calendarWeeks",
                "format": "format",
                "start-date": "startDate",
                "end-date": "endDate",
                "day-of-week-disabled": "dayOfWeekDisabled",
                "auto-close": "autoClose",
                "start-view": "startView",
                "min_view-mode": "minViewMode",
                "today-btn": "todayBtn",
                "today-highlight": "todayHighlight",
                "keyboard-navigation": "keyboardNavigation",
                "language": "language",
                "is-component": "isComponent",
                "force-parse": "forceParse"
            };

            options = generateDataOptions(obj, mappings)

            if (options['isComponent']) {
                obj = obj.parent();
            }

            obj.datepicker(options);
        });
    }

    if ($.fn.intlTelInput) {
        registerDataWidget('telephone-input', function (obj) {
            obj.intlTelInput();
        });
    }

});