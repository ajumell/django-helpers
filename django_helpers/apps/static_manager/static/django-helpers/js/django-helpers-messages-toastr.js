jQuery && jQuery(function ($) {

    if ($.dj && $.dj.messages) {
        var tagsMapping = {
                "success": "Success",
                "error": "Error",
                "warning": "Warning"
            },
            sanitizeTitle = function (title, tags) {
                if (title) {
                    return title;
                }

                if (!tags) {
                    return "";
                }

                if (tags) {
                    return tagsMapping[tags];
                }
            }

        $.dj.messages.setAdapter(function (title, msg, tags) {
            var fn;
            title = sanitizeTitle(title, tags);
            fn = toastr[tags];
            if (fn) {
                fn(title, msg);
            }
        });
    }

});