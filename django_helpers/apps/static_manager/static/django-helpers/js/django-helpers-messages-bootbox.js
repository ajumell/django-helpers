jQuery && jQuery(function ($) {

    if ($.dj && $.dj.messages) {
        var tagsMapping = {
                "success": "Success",
                "error": "Error",
                "warning": "Warning"
            },
            sanitizeTitle = function (title, tags) {
                if (title) {
                    return title;
                }

                if (!tags) {
                    return "";
                }

                if (tags) {
                    return tagsMapping[tags];
                }
            }

        $.dj.messages.setAdapter(function (title, msg, tags) {
            title = sanitizeTitle(title, tags);
            bootbox.dialog({
                "message": msg,
                "title": title,
                "buttons": {
                    "ok": {
                        label: "OK",
                        callback: function () {
                        }
                    }
                }
            });
        });
    }

});