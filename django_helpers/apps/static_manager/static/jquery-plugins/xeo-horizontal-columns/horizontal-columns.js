jQuery && jQuery(function ($) {

    var fnResize;
    var fnResizeProxy;
    var registerDataWidget;
    var iTimeout;

    registerDataWidget = $.dj.registerDataWidget;
    fnResize = function (jqObj) {

        var fColumnWidth;
        var iDefaultValue;
        var iChildrenCount;
        var fActualWidth;
        var fColumnWidthPercentage;
        var iColumns;
        var jqChildren;
        var iClientWidth;
        var iGutter;

        iClientWidth = document.body.clientWidth;

        if (iClientWidth >= 1200) {
            iColumns = jqObj.data('colLg');
            iDefaultValue = 4;
        } else if (iClientWidth >= 992) {
            iColumns = jqObj.data('colMd');
            iDefaultValue = 3;
        } else if (iClientWidth >= 767) {
            iColumns = jqObj.data('colSm');
            iDefaultValue = 2;
        } else {
            iColumns = jqObj.data('colXs');
            iDefaultValue = 1;
        }


        iGutter = jqObj.data('colGutter');
        if (iGutter === undefined) {
            iGutter = 10;
        } else {
            iGutter = parseInt(iGutter, 10);
        }

        if (iColumns === undefined) {
            iColumns = iDefaultValue;
        } else {
            iColumns = parseInt(iColumns, 10);
        }


        fColumnWidthPercentage = 100 / iColumns;
        jqChildren = jqObj.children(":visible");
        iChildrenCount = jqChildren.length;
        fColumnWidth = fColumnWidthPercentage * iClientWidth / 100;

        fActualWidth = (fColumnWidth + iGutter) * iChildrenCount;
        fActualWidth += iGutter;

        console.log(fActualWidth, fColumnWidth);

        jqChildren.css({
            "max-width": fColumnWidth + "px",
            "min-width": fColumnWidth + "px",
            marginLeft: iGutter + "px",
            float: 'left'
        });

        jqObj.css({
            width: fActualWidth + "px"
        });

        jqObj.parent().css({
            "overflow-x": "scroll"
        })

    };

    fnResizeProxy = function (jqObj, iPause) {
        if (!iPause) {
            iPause = 400;
        }

        clearTimeout(iTimeout);

        iTimeout = setTimeout(function () {
            fnResize(jqObj);
        }, iPause);
    };

    registerDataWidget('horizontal-columns', function (jqObj) {
        clearTimeout(iTimeout);
        iTimeout = setTimeout(function () {
            fnResize(jqObj);
        }, 400);
    });

    $(window).on('resize', function () {
        $("[data-horizontal-columns]").each(function () {
            fnResizeProxy($(this));
        })
    });

    $("[data-horizontal-columns]").on('content-changed', "*", function (ev) {
        fnResizeProxy($(this), 200);
    });

});