# coding=utf-8
jQueryURL = 'js/jquery-1.9.1.min.js'
AjaxCsrfURL = 'js/django-ajax-csrf.js'
jQueryEasingURL = 'js/jquery.easing.min.js'
jQueryUIURL = 'js/jquery-ui.js'
BootstrapURL = 'js/bootstrap.min.js'


class StaticRequirement(object):
    def __init__(self):
        self.js_files = []
        self.css_files = []
        self.ignore_files = []
        self.rename_mapping = {}

    def add_js_files(self, *js_files):
        self.js_files.extend(js_files)

    def add_css_files(self, *css_files):
        self.css_files.extend(css_files)

    def add_ignore_files(self, *ignore_files):
        self.ignore_files.extend(ignore_files)

    def add_rename_mapping(self, name, new_path):
        self.rename_mapping[name] = new_path


def add_js_requirement(*js_files):
    obj = StaticRequirement()
    obj.js_files.extend(js_files)
    return obj


def add_css_requirement(*css_files):
    obj = StaticRequirement()
    obj.css_files.extend(css_files)
    return obj


def add_ignore_requirement(*ignore_files):
    obj = StaticRequirement()
    obj.ignore_files.extend(ignore_files)
    return obj
