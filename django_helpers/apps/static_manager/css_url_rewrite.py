# coding=utf-8
import re
from os.path import relpath, join, dirname

from utils import read_utf8

regexp = re.compile("url\s*\(\s*(.+?)\s*\)")


def get_all_urls(contents):
    sanitized_urls = []
    urls = regexp.findall(contents)
    for url in urls:
        url = url.strip(" \"'")
        ext_index = url.rfind('.')

        # Special cases
        i = url.find("#", ext_index)
        if i > 0:
            url = url[0:i]

        # Special cases
        i = url.find("?", ext_index)
        if i > 0:
            url = url[0:i]

        url = url.strip(" \"'?")
        if url not in sanitized_urls:
            sanitized_urls.append(url)

    return sanitized_urls


def generate_rewrited_contents(file_path, relative_path, new_path, cache_files):
    contents = read_utf8(file_path)
    urls = get_all_urls(contents)

    relative_dir = dirname(relative_path)
    new_dir = dirname(new_path)
    static_dir = dirname(new_dir)

    for url in urls:
        relative_url = join(relative_dir, url)
        new_url = relpath(relative_url, new_dir)

        cache_url = relpath(relative_url, static_dir)
        cache_files.append(cache_url)

        print 'Renaming', url, 'to', new_url
        contents = contents.replace(url, new_url)

    return contents


def main():
    get_all_urls("""
background-image: url('test/test.gif');
background: url("test2/test2.gif");
background-image: url(test3/test3.gif);
background: url   ( test4/ test4.gif );
background: url( " test5/test5.gif"   );

background-image: url('../test/test.gif');
background: url("/test2/test2.gif");
background-image: url(../../test3/test3.gif);
background: url   ( ./test4/ test4.gif );
background: url( " test5/test5.gif"   );
""")


if __name__ == "__main__":
    main()
