# coding=utf-8
from json import dumps

from django import template
from django.utils.safestring import mark_safe

from django_helpers import get_settings_val
from django_helpers.helpers.views import render_to_string
from django_helpers.templatetags.jsmin import remove_blank_lines
from .. import settings
from .. import static_file
from .. import storage
from ..utils import generate_js_from_context, append_static_arr, \
    get_js_files_from_context, get_css_files_from_context, get_ignore_files_from_context, \
    get_rename_mappings_from_context, js_global_vars

__author__ = 'ajumell'

STATIC_URL = settings.STATIC_URL
SAVE_FILE = get_settings_val('STATIC_MANAGER_MAKE_FILE', True)
register = template.Library()


def _render_code(files, html_template, static_url=None):
    if static_url is None:
        static_url = STATIC_URL

    code = []
    for f in files:
        f = unicode(f)
        if f.startswith("http://") or f.startswith("https://"):
            s = ""
        else:
            s = static_url
        code.append(html_template % (s, f))
    return mark_safe('\n'.join(code))


def has_ajax_rendering(context):
    return 'ajax_block_register' in context


def ajax_render_js(op, app_name=None, static_url=settings.STATIC_URL):
    if app_name is None:
        raise

    for block in op:
        files = block['files']
        new_files = static_file.get_real_paths('js', *files)

        # jQuery path should be removed from the
        # js files. Its already included. There is
        # no need to include it again.
        jquery_path = static_file.static_file_path('jquery')
        while jquery_path in new_files:
            new_files.remove(jquery_path)

        block['files'] = append_static_arr(new_files)

    generated_code = render_to_string('static-manager/render-js.html', {
        'code_blocks': op,
        'STATIC_URL': static_url,
        'app_name': app_name
    })
    return mark_safe(generated_code)


@register.simple_tag(takes_context=True)
def cache_manifest(context, app_name=None):
    from ..storage import get_build_path

    if settings.DEBUG:
        return ""

    tmpl = ' manifest="{0}{1}"'.format(settings.STATIC_URL, get_build_path(app_name, "manifest"))
    return tmpl


@register.simple_tag(takes_context=True)
def render_js(context, app_name=None):
    op = generate_js_from_context(context)

    if has_ajax_rendering(context):
        return ajax_render_js(op, app_name)
    else:
        generated_code = '<script type="text/javascript">'
        for block in op:
            generated_code += block['js_code']
        generated_code = remove_blank_lines(generated_code)
        return mark_safe(generated_code + '</script>')


def render_js_global_vars(context):
    variables = js_global_vars(context)
    generated_code = '<script type="text/javascript">'

    for key, val in variables.items():
        generated_code += "window.%s = %s;" % (key, dumps(val))
    generated_code = remove_blank_lines(generated_code)
    generated_code += '</script>'

    return mark_safe(generated_code)


@register.simple_tag(takes_context=True)
def js_files(context, app_name=None):
    if app_name is None:
        app_name = context.get('static_app')

    if has_ajax_rendering(context):
        return render_js(context, app_name)
    else:
        html_template = '<script type="text/javascript" src="%s%s"></script>'
        static_url = context.get('STATIC_URL', STATIC_URL)
        if not settings.DISABLE_MINIFYING:
            path = storage.get_build_path(app_name, 'js')
            return render_js_global_vars(context) + _render_code([path], html_template, static_url) + render_js(context, app_name)

        files = get_js_files_from_context(context)
        ignore_files = get_ignore_files_from_context(context)
        rename_mappings = get_rename_mappings_from_context(context)
        files = static_file.get_real_paths('js', ignore_files, rename_mappings, *files)
        storage.add_files('js', app_name, files)
        storage.save_data()
        static_file.save_data()
        if not settings.STATIC_DEBUG_MODE:
            files = storage.get_files('js', app_name)
        return render_js_global_vars(context) + _render_code(files, html_template, static_url) + render_js(context, app_name)


@register.simple_tag(takes_context=True)
def css_files(context, app_name=None):
    if app_name is None:
        app_name = context.get('static_app')

    html_template = '<link rel="stylesheet" href="%s%s" />'
    static_url = context.get('STATIC_URL', STATIC_URL)
    if not settings.DISABLE_MINIFYING:
        path = storage.get_build_path(app_name, 'css')
        return _render_code([path], html_template, static_url)

    files = get_css_files_from_context(context)
    new_files = list(files)
    new_files.extend(get_js_files_from_context(context))
    ignore_files = get_ignore_files_from_context(context)
    rename_mappings = get_rename_mappings_from_context(context)
    files = static_file.get_real_paths('css', ignore_files, rename_mappings, *new_files)
    storage.add_files('css', app_name, files)
    storage.save_data()
    static_file.save_data()
    if not settings.STATIC_DEBUG_MODE:
        files = storage.get_files('css', app_name)
    return _render_code(files, html_template, static_url)
