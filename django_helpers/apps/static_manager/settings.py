# coding=utf-8
from django.conf import settings

STATIC_URL = settings.STATIC_URL

DEBUG = settings.DEBUG

SAVE_FILE = getattr(settings, 'STATIC_MANAGER_MAKE_FILE', True)

INCLUDE_CSS = getattr(settings, 'STATIC_MANAGER_INCLUDE_CSS', True)

INCLUDE_JS = getattr(settings, 'STATIC_MANAGER_INCLUDE_JS', True)

DISABLE_JS_SYNTAX_CHECKING = getattr(settings, 'STATIC_MANAGER_DISABLE_JS_SYNTAX_CHECKING', False)

STATIC_DEBUG_MODE = getattr(settings, 'STATIC_DEBUG_MODE', False)

DISABLE_MINIFYING = DEBUG