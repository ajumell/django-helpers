# coding=utf-8
import os
from json import load, dump

from django_helpers.utils.importlib import import_module

from django_helpers import get_settings_val
from django_helpers.apps.static_manager.utils import static_file_exists
from settings import DEBUG, STATIC_DEBUG_MODE

REGISTRY = None
JSON_REGISTRY = None
HAS_CHANGED = True
_FILE_NAME = get_settings_val('STATIC_FILES_REGISTER_FILE_NAME', 'STATIC_FILES_REGISTER')


def _get_file_path():
    """
    Calculates the file path for the storage of the static files register.
    @return: The absolute file name for the storage file name.
    @rtype str
    """
    settings = os.environ.get('DJANGO_SETTINGS_MODULE')
    module = import_module(settings)
    root = os.path.dirname(module.__file__)
    root = os.path.abspath(root)
    return os.path.join(root, _FILE_NAME)


def load_data():
    """
    @summary Loads the data from the file.
    @permission private
    """
    global JSON_REGISTRY, REGISTRY
    path = _get_file_path()
    REGISTRY = {}
    try:
        fp = file(path)
        JSON_REGISTRY = load(fp)
        fp.close()

        for key in JSON_REGISTRY:
            REGISTRY[key] = StaticFile(**JSON_REGISTRY[key])

    except Exception:
        JSON_REGISTRY = {}


def save_data():
    """
    @summary Saves the data to the file.
    @return:
    @rtype:
    """

    global HAS_CHANGED
    if not HAS_CHANGED:
        return

    path = _get_file_path()
    fp = file(path, 'w')
    if not STATIC_DEBUG_MODE:
        dump(JSON_REGISTRY, fp, indent=4, sort_keys=True)
    else:
        fp.write('')
    fp.close()

    HAS_CHANGED = False


def exists(name):
    return name in REGISTRY


def get_file(name):
    try:
        entry = REGISTRY[name]
    except:
        raise Exception('Static File {0} is not registered.'.format(name))
    return entry


def register(name, path, file_type='js', dependancies=None, fail_if_existe=False):
    """
    Registers a static file with a name. We can use this name to get the files
    later.
    """
    global HAS_CHANGED
    if not DEBUG:
        return

    static_file = StaticFile(name, path, file_type, dependancies)
    if REGISTRY is None:
        load_data()

    if name in REGISTRY:
        if fail_if_existe:
            raise
        else:
            return

    REGISTRY[name] = static_file

    JSON_REGISTRY[name] = {
        "name": name,
        "path": path,
        "file_type": file_type,
        "dependancies": dependancies
    }

    HAS_CHANGED = True


def register_js(name, path, dependancies=None, fail_if_existe=False):
    register(name, path, dependancies=dependancies, fail_if_existe=fail_if_existe)


def register_css(name, path, fail_if_existe=False):
    register(name, path, 'css', fail_if_existe=fail_if_existe)


def ignore(name, should=True):
    static_file = get_file(name)
    static_file.ignored = should


def change_path(name, path):
    static_file = get_file(name)
    static_file.set_path(path)


def get_real_paths(file_type, ignore_names, rename_mappings, *names):
    result = []
    for name in names:
        static_file = get_file(name)
        static_file.get_files(result, file_type, ignore_names, rename_mappings)
    return result


def static_file_path(name):
    static_file = get_file(name)
    return static_file.path


# noinspection PyAttributeOutsideInit
class StaticFile:
    ignored = False

    def __init__(self, name, path, file_type, dependancies=None):
        self.name = name
        self.file_type = file_type
        if path is None and len(dependancies) == 1:
            entry = get_file(dependancies[0])
            path = entry.path
            dependancies = entry.dependancies

        self.set_path(path)
        self.set_dependancies(dependancies)

    def set_path(self, path):
        # static_file_exists(path)
        self.path = path

    def set_dependancies(self, dependancies):
        if dependancies is None:
            self.dependancies = None
            return

        if type(dependancies) not in (tuple, list):
            raise Exception('Dependancies should be a list or tuple.')

        self.dependancies = dependancies

    def remove_dependancies(self, **args):
        dependancies = self.dependancies
        if dependancies is None:
            return

        for dependency in args:
            if dependency in dependancies:
                dependancies.remove(dependency)

    def add_dependancies(self, **args):
        dependancies = self.dependancies
        if dependancies is None:
            dependancies = self.dependancies = []

        for dependency in args:
            if dependency not in dependancies:
                dependancies.append(dependency)

    def get_files(self, result, file_type, ignore_names, rename_mappings):
        # if self.ignored or self.file_type != file_type:
        #     return

        dependancies = self.dependancies
        if dependancies is not None:
            for dependency in dependancies:
                if not exists(dependency):
                    raise Exception('Dependency {0} is not registered.'.format(dependency))

                static_file = get_file(dependency)
                static_file.get_files(result, file_type, ignore_names, rename_mappings)

        if self.path is None:
            return

        name = self.name
        # Add the file path to result only if  the file is not in ignore list.
        if self.ignored is False and self.file_type == file_type and name not in ignore_names:
            path = rename_mappings.get(name, self.path)
            static_file_exists(path)
            if path not in result:
                result.append(path)
