# coding=utf-8
__author__ = 'ajumell'

from django.contrib.staticfiles.finders import find
from django.core.management.base import BaseCommand


def read_entirely(file_name):
    from ...utils import read_file

    return ";" + read_file(file_name) + ";"


def mkdir(path):
    from os import mkdir as orig

    try:
        orig(path)
    except:
        pass


class Command(BaseCommand):
    help = 'Concatenate the javascript files and minify it.'
    args = 'App name'

    def handle(self, *args, **options):
        from ...storage import get_files, get_build_path
        from ...appcache import generate_app_cache
        from ...css_url_rewrite import generate_rewrited_contents
        from ...utils import write_utf8, write_file
        from django.conf import settings
        from os.path import join, abspath, dirname

        args = list(args)
        args.insert(0, None)
        static_dirs = settings.STATICFILES_DIRS

        if len(static_dirs) == 0:
            raise Exception("Static Files Dir is not configured.")

        static_dir = abspath(static_dirs[0])

        for name in args:
            cache_files = []
            manifest_files = []
            for file_type in ('css', 'js'):
                path_name = get_build_path(name, file_type)
                cache_files.append(path_name)

                path = join(static_dir, path_name)
                mkdir(dirname(path))
                file_names = get_files(file_type, name)
                contents = []

                for x in file_names:
                    manifest_files.append(x)
                    print "Adding :", x
                    original_path = find(x)
                    if file_type == 'js':
                        content = read_entirely(original_path)
                    else:
                        content = generate_rewrited_contents(original_path, x, path_name, cache_files)
                    contents.append(content)
                if file_type == "css":
                    write_utf8(path, contents)
                else:
                    write_file(path, contents)

            generate_app_cache(name, manifest_files, static_dir)
