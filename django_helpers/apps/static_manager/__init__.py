# coding=utf-8


from helpers import *
from static_file import register_js, register_css, ignore, change_path

__author__ = 'ajumell'
#
# jQuery
#
register_js('jquery-1.9.1', 'js/jquery-1.9.1.min.js')

register_js('jquery', None, dependancies=('jquery-1.9.1',))

#
# Twitter Bootstrap
#
register_js('twitter-bootstrap-js', 'js-plugins/twitter-bootstrap-3/js/bootstrap.min.js', ('jquery',))
register_css('twitter-bootstrap-css', 'js-plugins/twitter-bootstrap-3/css/bootstrap.min.css')
register_css('twitter-bootstrap-theme', 'js-plugins/twitter-bootstrap-3/css/bootstrap-theme.min.css')
register_css('boot-plus-cards', 'bootplus/css/boot-plus-cards.css')
register_css('bootcards-css', 'bootcards/bootcards-desktop-lite.css')

"""
@deprecated
"""
register_css('twitter-bootstrap-responsive-css', 'css/bootstrap-responsive.min.css')

#
# jQuery UI
#
register_js('jquery-ui-js', 'js/jquery-ui.js', ('jquery',))
register_css('jquery-ui-base-css', 'css/jquery-ui/jquery-ui.min.css')

#
# Misc
#
register_js('swig-js', 'js/swig.min.js')
register_js('jquery-django-ajax-csrf', 'django-helpers/js/django-ajax-csrf.js', ('jquery',))
register_js('jquery-easing', 'js/jquery.easing.min.js', ('jquery',))
register_js('jquery-jeditable-js', 'js/jquery.jeditable.js', ('jquery',))
register_js('prettify-js', 'js/prettify.min.js')

#
#   Twitter Bootstrap Plugins
#
register_js('bootbox-js', 'js-plugins/bootstrap-bootbox/bootbox.min.js', ('twitter-bootstrap-js',))

#
#   Tipsy Plugin
#
register_js('jquery-tipsy-js', 'js-plugins/jquery-tipsy/jquery.tipsy.js', dependancies=('jquery',))
register_css('jquery-tipsy-css', 'js-plugins/jquery-tipsy/tipsy.css')

#
#   Isotope Plugin
#
register_js('images-loaded-js', 'js-plugins/images-loaded/js/imagesloaded.pkgd.min.js')
register_js('jquery-isotope-js', 'js-plugins/isotope/jquery.isotope.min.js', dependancies=('jquery', 'images-loaded-js'))
register_js('jquery-isotope-sloppy-masonry-js', 'js-plugins/isotope/jquery.isotope.sloppy-masonry.min.js', dependancies=('jquery-isotope-js',))
register_css('jquery-isotope-css', 'js-plugins/isotope/css/style.css')

register_css('bootstrap-datepicker-css', 'css/datepicker3.css')
register_css('bootstrap-multi-select-css', 'css/bootstrap-multiselect.css')
register_css('bootstrap-switch-css', 'css/bootstrap3/bootstrap-switch.min.css')

register_js('moment-js', 'js/moment.min.js')
register_js('eternicode-bootstrap-datepicker-js', 'js/eternicode-bootstrap-datepicker.js', ('twitter-bootstrap-js',))
register_js('bootstrap-datepicker-js', 'js/bootstrap-datepicker.js', ('twitter-bootstrap-js',))
register_js('bootstrap-datetimepicker-js', 'js/bootstrap-datetimepicker.js', ('twitter-bootstrap-js',))
register_js('bootstrap-multiselect-js', 'js/bootstrap-multiselect.js', ('twitter-bootstrap-js',))
register_js('bootstrap-daterangepicker-js', 'js/daterangepicker.js', ('twitter-bootstrap-js',))
register_js('bootstrap-switch-js', 'js/bootstrap-switch.min.js', ('twitter-bootstrap-js',))
register_js('bootstrap-clockface-js', 'js/bootstrap-clockface.js', ('twitter-bootstrap-js',))

register_js('jquery-masked-input-js', 'django-helpers/forms/js/jquery.maskedinput-1.3.min.js', ('jquery-ui-js',))
register_js('jquery-ui-spinner-js', 'django-helpers/forms/js/jquery-ui-spinner.js', ('jquery-ui-js',))

#
# Color Picker Widget
#
register_js('django-helpers-forms-js', 'django-helpers/js/dh-forms.js', ('django-helpers-core-js',))

#
# Color Picker Widget
#
register_css('bootstrap-colorpicker-css', 'css/color-picker.css')
register_js('bootstrap-colorpicker-js', 'js/bootstrap-colorpicker.js', ('twitter-bootstrap-js',))

#
# Clock Picker Widget
#
register_css('bootstrap-clock-picker-css', 'js-plugins/clock-picker/bootstrap-clockpicker.min.css')
register_js('bootstrap-clock-picker-js', 'js-plugins/clock-picker/bootstrap-clockpicker.min.js', ('twitter-bootstrap-js', 'django-helpers-forms-js'))

#
# Django Helpers Core
#
register_js("django-helpers-core-js", "django-helpers/js/core.js", ("jquery",))

#
# BootBox
#
register_js("django-helpers-messages-bootbox-js", "django-helpers/js/django-helpers-messages-bootbox.js", ("django-helpers-core-js", 'bootbox-js'))
register_js("django-helpers-confirm-bootbox-js", "django-helpers/js/django-helpers-confirm-bootbox.js", ("django-helpers-core-js", 'bootbox-js'))
register_js("django-helpers-modal-bootbox-js", "django-helpers/js/django-helpers-modal-bootbox.js", ("django-helpers-core-js", 'bootbox-js'))

#
# Toaster Plugin
#
register_js("toastr-js", "js-plugins/toastr/toastr.js", ("jquery",))
register_css("toastr-css", "js-plugins/toastr/toastr.css")
register_js("django-helpers-messages-toastr-js", "django-helpers/js/django-helpers-messages-toastr.js", ("django-helpers-core-js", 'toastr-css', 'toastr-js'))

#
# Trumbowyg
#
register_css('trumbowyg-css', "js-plugins/trumbowyg/dist/ui/trumbowyg.min.css")
register_js('trumbowyg-js', "js-plugins/trumbowyg/dist/trumbowyg.min.js", ('jquery',))

#
# Medium Editor
#

register_css('medium-editor-css', "js-plugins/medum-editor/css/medium-editor.min.css")
register_js('medium-editor-js', "js-plugins/medum-editor/js/medium-editor.min.js")

#
# Form
#
register_js('jquery-validate-js', 'form-renderer/js/jquery.validate.js', ('jquery',))
register_js('jquery-validate-additional-methods-js', 'form-renderer/js/additional-methods.js', ('jquery-validate-js',))
register_js('sprintf-js', 'form-renderer/js/sprintf.min.js')
register_js('jquery-validate-django-js', 'form-renderer/js/jquery.validate.django.js', ('jquery-validate-js', 'sprintf-js'))
register_js("bs3-form-validation-options-js", "django-helpers/js/bs3-validation-options.js", ("jquery-validate-django-js",))
register_js('django-helpers-validate-js', 'form-renderer/js/django-helpers-validate.js', ('jquery-validate-django-js',))

#
# Bootstrap Components
#

register_css('bootstrap-css-components-chat', 'bootstrap-css-components/chat.css')
register_css('bootstrap-css-components-time-line', 'bootstrap-css-components/time-line.css')

register_css('bootstrap-responsive-margin', 'bootstrap-css-components/responsive-margin.css')
register_css('bootstrap-responsive-border', 'bootstrap-css-components/responsive-border.css')
register_css('bootstrap-responsive-masonry', 'bootstrap-css-components/masonry-css.css')
register_css('bootstrap-responsive-padding', 'bootstrap-css-components/responsive-padding.css')
register_css('bootstrap-row-eq-height', 'bootstrap-css-components/row-eq-height.css')

#
# Material Forms
#
register_js('material-forms-js', 'material-forms/material-forms.js')
register_css('material-forms-css', 'material-forms/material-forms.css')

#
# Responsive Tabs
#
register_css('responsive-tabs-css', 'jquery-plugins/responsive-tabs/responsive-tabs.css')
register_js('responsive-tabs-js', 'jquery-plugins/responsive-tabs/jquery.responsiveTabs.min.js', (
    'jquery',
))

#
# Responsive Navigation Slick Nav
#
register_css('slick-nav-css', 'jquery-plugins/SlickNav/dist/slicknav.min.css')
register_js('slick-nav-js', 'jquery-plugins/SlickNav/dist/jquery.slicknav.min.js', ('jquery',))

#
# Horizontal Columns
#
register_js('horizontal-columns-js', 'jquery-plugins/xeo-horizontal-columns/horizontal-columns.js', ('jquery',))

#
# Light Boxes
#
register_css('light-box-2-css', 'jquery-plugins/light-box-2/css/lightbox.min.css')
register_js('light-box-2-js', 'jquery-plugins/light-box-2/js/lightbox.min.js', ('jquery',))

#
# Perfect Scroll Bar
#
register_css('perfect-scrollbar-css', 'jquery-plugins/perfect-scrollbar/perfect-scrollbar.min.css')
register_js('perfect-scrollbar-js', 'jquery-plugins/perfect-scrollbar/perfect-scrollbar.min.js', ('jquery',))
register_js('perfect-scrollbar-django-js', 'jquery-plugins/perfect-scrollbar/perfect-scrollbar-django.min.js', ('perfect-scrollbar-js',))
