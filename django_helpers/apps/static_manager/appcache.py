# coding=utf-8
from django_helpers.helpers.views import render_to_string

__author__ = 'ajumell'

from os.path import join, dirname
from django.contrib.staticfiles import views
from django.contrib.staticfiles.finders import find
from storage import get_build_path
from utils import write_file, append_static_arr

original_serve = views.serve


def mkdir(path):
    from os import mkdir as orig

    try:
        orig(path)
    except:
        pass


def patched_serve_patcher(request, path, insecure=False, **kwargs):
    meta = request.META
    http_referer = meta.get('HTTP_REFERER')

    if http_referer is not None:
        pass

    return original_serve(request, path, insecure, **kwargs)


views.serve = patched_serve_patcher


def generate_app_cache(name, files, static_dir):
    manifest_path = get_build_path(name, 'manifest')
    path = join(static_dir, manifest_path)
    mkdir(dirname(path))
    new_files = []
    for file_name in files:
        if find(file_name) is not None:
            new_files.append(file_name)
    files = append_static_arr(new_files)

    contents = render_to_string("static-manager/app-cache.manifest", {
        "cached_urls": files
    })

    write_file(path, contents)
