# coding=utf-8
import codecs
from types import StringTypes

from django.contrib.staticfiles import finders

from django_helpers import get_settings_val
from exceptions import PYESPRIMAException, SyntaxErrorException
from exceptions import StaticFileNotFoundException
from settings import DISABLE_JS_SYNTAX_CHECKING, DEBUG


def static_file_exists(name):
    """
    Checks if the given static file exists or not.
    """
    if finders.find(name) is None:
        if get_settings_val('RAISE_STATIC_EXCEPTIONS', True):
            raise StaticFileNotFoundException(name)
        else:
            print StaticFileNotFoundException(name)
            return False
    else:
        return True


def resolve_file_name(file_name):
    t = type(file_name)
    if t in (list, tuple):
        if len(file_name) is 1:
            return file_name[0]
        else:
            raise
    if t in StringTypes:
        return file_name

    raise


def remove_duplicates(arr):
    """
    @summary Removes duplicate entries in the list and
    append the static dir prefix if add static is True.

    @author Muhammed K K
    @rtype list


    @param arr: The list from which duplicates has to be removed.
    @return: The newly created array with duplicates removed.
    """
    new_arr = []
    for js in arr:
        if js not in new_arr:
            new_arr.append(js)
    return new_arr


def append_static_arr(arr):
    """
    Prepends static url to every entry in the app
    """
    static_url = get_settings_val('STATIC_URL')
    return [static_url + a for a in arr]


# noinspection PyProtectedMember
def has_js(obj):
    """
    @summary Checks whether a object has javascript in it or not.
    @param obj: The object from which has javascript to be checked.
    @return: yes or no
    """

    has_meta = hasattr(obj, '_meta')
    _has_js = hasattr(obj, 'render_js') and getattr(obj, 'has_js', True)

    if not has_meta:
        return _has_js

    _meta = obj._meta
    _has_js_meta = hasattr(_meta, 'render_js') and getattr(_meta, 'has_js', True)

    return _has_js or _has_js_meta


def get_js_files(obj):
    """
    @param obj: The object from which the list of javascript files has to be found.
    @return: A list of required javascript file.
    """
    if hasattr(obj, 'setup_static'):
        obj.setup_static()

    if not hasattr(obj, 'js_files'):
        return []

    files = obj.js_files

    if hasattr(files, '__call__'):
        files = obj.js_files()

    if type(files) not in (list, tuple):
        raise Exception('js_files is not in correct format in %s.' % str(type(obj)))

    files = remove_duplicates(files)
    return files


def get_css_files(obj):
    """
    @param obj: The object from which the list of css files has to be found.
    @return: A list of required css file.
    """
    if not hasattr(obj, 'css_files'):
        return []

    files = obj.css_files

    if hasattr(files, '__call__'):
        files = obj.css_files()

    if files is None:
        return []

    if type(files) not in (list, tuple):
        raise Exception('css_files is not in correct format in %s.' % str(type(obj)))

    files = remove_duplicates(files)
    return files


def get_ignore_files(obj):
    """
    @param obj: The object from which the list of css files has to be found.
    @return: A list of required javascript file.
    """
    if not hasattr(obj, 'ignore_files'):
        return []

    files = obj.ignore_files

    if type(files) not in (list, tuple):
        raise Exception('ignore_files is not in correct format in %s.' % str(type(obj)))

    files = remove_duplicates(files)
    return files


def get_rename_mappings(obj, mapping):
    """
    @param obj: The object from which the list of css files has to be found.
    @return: A list of required javascript file.
    """
    if not hasattr(obj, 'rename_mapping'):
        return

    files = obj.rename_mapping

    if type(files) is not dict:
        raise Exception('rename_mapping is not in correct format in %s.' % str(type(obj)))

    for key, value in files.items():
        mapping[key] = value


def get_js_files_from_context(context):
    """
    Loads all the javascript files form the context.
    """
    js_arr = []
    for dictionary in context.dicts:
        for key, val in dictionary.items():
            js_arr += get_js_files(val)
    return js_arr


def get_css_files_from_context(context):
    """
    Loads all the css files form the context.
    """
    css_arr = []
    for dictionary in context.dicts:
        for key, val in dictionary.items():
            css_arr += get_css_files(val)
    return css_arr


def get_ignore_files_from_context(context):
    """
    Loads the list of files which has to be ignored from
    the context
    """
    ignore_arr = []
    for dictionary in context.dicts:
        for key, val in dictionary.items():
            ignore_arr += get_ignore_files(val)
    return ignore_arr


def get_rename_mappings_from_context(context):
    rename_mapping = {}
    for dictionary in context.dicts:
        for key, val in dictionary.items():
            get_rename_mappings(val, rename_mapping)
    return rename_mapping


# noinspection PyUnresolvedReferences
def validate_javascript(code):
    if DISABLE_JS_SYNTAX_CHECKING or not DEBUG:
        return True
    try:
        import pyesprima
    except ImportError:
        if not DEBUG:
            raise PYESPRIMAException()
        else:
            return True
    try:
        pyesprima.parse(code)
        return True
    except Exception:
        return False


def generate_js_from_context(context):
    """
    Generates the Javascript code form the context.
    """
    js_arr = []
    for dictionary in context.dicts:
        for key, val in dictionary.items():
            if has_js(val):
                js_files = get_js_files(val)
                try:
                    js_code = val.render_js().strip()
                except Exception, ex:
                    print ex
                    js_code = ''

                if not validate_javascript(js_code):
                    print '\n' * 3
                    print '--' * 50
                    print js_code
                    print '--' * 50
                    print '\n' * 3
                    raise SyntaxErrorException(val)

                js_arr.append({
                    'files': js_files,
                    'js_code': js_code
                })
    return js_arr


def js_global_vars(context):
    js_arr = {}
    for dictionary in context.dicts:
        for key, val in dictionary.items():
            fn = getattr(val, 'js_global_vars', None)
            if hasattr(fn, '__call__'):
                res = fn()
                if type(res) is dict:
                    js_arr.update(res)
    return js_arr


def read_utf8(path):
    try:
        fp = codecs.open(path, 'r', 'utf-8')
        contents = fp.read()
        fp.close()
        return contents
    except Exception, ex:
        print ex
        raise


def write_utf8(path, contents):
    if type(contents) in (list, tuple):
        contents_list = contents
        contents = ""
        for content in contents_list:
            contents += content
            contents += "\n"

    fp = codecs.open(path, 'w', 'utf-8')
    fp.write(contents)
    fp.close()


def read_file(path):
    fp = open(path)
    contents = fp.read()
    fp.close()
    return contents


def write_file(path, contents):
    fp = open(path, 'w')
    if type(contents) in (list, tuple):
        contents_list = contents
        contents = ""
        for content in contents_list:
            contents += content
            contents += "\n"
    fp.write(contents)
    fp.close()
