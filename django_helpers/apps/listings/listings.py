# coding=utf-8

from django.core.urlresolvers import reverse
from django.utils import six
from django.utils.safestring import mark_safe

from django_helpers.apps.data_tables.data_table import DataTableMetaClass
from django_helpers.apps.static_manager import register_js
from django_helpers.core.listable import BaseModelEditableListable
from django_helpers.helpers.views import render_to_string
from django_helpers.js_plugins.core import GenericJavascriptPlugin

__author__ = 'ajumell'

register_js('object-diff-js', 'js/listings/diff.min.js')
register_js('django-listing-js', 'js/listings/listings.js', ['jquery', 'django-helpers-core-js', 'object-diff-js'])


class ListingMetaClass(DataTableMetaClass):
    def __new__(mcs, name, bases, attrs):
        from views import register_name
        
        obj = type.__new__(mcs, name, bases, attrs)
        listing_id = attrs.get('listing_id', None)
        params = attrs.get('url_search_parameters', None)
        if params is None:
            for b in bases:
                params = getattr(b, 'url_search_parameters', None)
                if params is not None:
                    break
        
        register_name(obj, listing_id, params)
        return obj


# noinspection PyAttributeOutsideInit
class Listing(six.with_metaclass(ListingMetaClass, BaseModelEditableListable)):
    template = 'django-helpers-listings/contents.html'
    search_template = 'django-helpers-listings/search.html'
    pagination_template = 'django-helpers-listings/pagination.htm'
    pagination_info_template = 'django-helpers-listings/paging-info.html'
    
    js_template = 'django-helpers-listings/listing.js'
    
    listing_id = None
    mode = "paged"
    isotope_support = True
    item_id = "pk"
    
    container_selector = None
    hide_parent_if_empty = False
    
    show_header = True
    show_footer = True
    
    def setup_static_files(self):
        super(Listing, self).setup_static_files()
        js_files = self.js_files
        css_files = self.css_files
        
        js_files.insert(0, 'django-listing-js')
        if self.isotope_support:
            js_files.append('jquery-isotope-js')
            css_files.append('jquery-isotope-css')
    
    def __init__(self, request=None):
        super(Listing, self).__init__(request)
        self.setup_static_files()
    
    def get_item_pk(self):
        return self.item_id
    
    def id(self):
        return self.listing_id
    
    def page_data(self, query, data_dict=None, start=None, max_items=None):
        if self.mode == ListingModes['COMPLETE']:
            return query
        if start is None and max_items is None:
            gt = data_dict.get
            start = int(gt('iDisplayStart', 0))
            max_items = int(gt('iDisplayLength', self.display_length))
        
        if max_items is None:
            max_items = self.display_length
        
        return query[start:start + max_items]
    
    def get_search_term(self, data_dict, search_term):
        if data_dict is not None and search_term is None:
            gt = data_dict.get
            search_term = gt('sSearch', None)
        
        return search_term
    
    def do_form_filter(self, query, data_dict):
        return query
    
    def sort(self, query, data_dict):
        return query
    
    #
    # Rendering
    #
    def reset_params(self):
        self._widget_id = None
        self._data = None
    
    def generate_render_data(self):
        if self._data is None:
            kwargs = self.url_kwargs
            
            table_id = self.id()
            ajax_source = self.get_ajax_url(kwargs, table_id)
            
            attrs = GenericJavascriptPlugin()
            attrs.add('ajax-source', ajax_source)
            attrs.add('display-length', self.display_length)
            
            attrs.add('data-django-listings', "true")
            
            data = {
                'show_header': self.show_header,
                'show_footer': self.show_footer,
                'attrs': attrs,
                'display_length': self.display_length,
                'ajax_source': ajax_source,
            }
            
            for column in self.fields:
                column.table = self
            
            widget_id = self.widget_id()
            
            attrs.add('listing-id', widget_id)
            attrs.add('listing-mode', self.mode)
            attrs.add('listing-isotope-support', self.isotope_support)
            attrs.add('container-selector', self.container_selector)
            attrs.add('hide-container', self.hide_parent_if_empty)
            
            attrs.add('listable-class', self.listable_class)
            params = self.get_params()
            if params:
                for key, value in params.items():
                    attrs.add('listable-' + key.replace("_", '-'), value)
            
            data['listing_id'] = widget_id
            data['listing_mode'] = self.mode
            data['isotope_support'] = self.isotope_support
            
            self._data = data
        
        return self._data
    
    def initial_data(self, kwargs):
        request = self.request
        
        if self.request is None:
            raise Exception('Request is required for rendering initial data')
        data = self._data
        
        if 'rows' in data:
            return
        
        query = self._get_query(request, kwargs)
        
        prev_total = None
        total_length = query.count()
        paginator = self.initial_pagination(request, total_length)
        query = self.only(query)
        query = self.page_data(query, start=paginator['from'] - 1)
        
        data['attrs'].add('total', total_length)
        
        data['rows'] = self.generate_initial_data_dict(query, request)
        data['total'] = total_length
        data['qs'] = paginator['qs']
        data['request'] = request
        data['paginator'] = paginator
        data['prev_total'] = prev_total
    
    def generate_initial_data_dict(self, query, request):
        rows = []
        i = 0
        row_id = self.get_item_pk()
        for record in query:
            self._instance = record
            pk = getattr(record, row_id)
            self.pk = pk
            rows.append(self.render_item(record, request))
            i += 1
        return rows
    
    def render(self, template=None):
        
        if template is None:
            template = self.template
        
        kwargs = self.url_kwargs
        self.generate_render_data()
        self.initial_data(kwargs)
        return self.render_a_template(template)
    
    def render_search(self):
        return self.render(self.search_template)
    
    def render_pagination(self):
        return self.render(self.pagination_template)
    
    def render_pagination_info(self):
        return self.render(self.pagination_info_template)
    
    #
    # Item Rendering
    #
    def item_template(self, row):
        raise NotImplementedError()
    
    def render_item(self, record, request=None):
        template = self.item_template(record)
        
        row_id = self.get_item_pk()
        row_id = str(getattr(record, row_id))
        
        columns = self.fields
        cells = {'pk': row_id}
        
        for column in columns:
            html = column.render(request, record)
            title = column.get_field_title()
            if column.is_string:
                cells[title] = mark_safe(html)
            else:
                cells[title] = html
        
        attrs = GenericJavascriptPlugin()
        attrs.add('listing-item', self.widget_id())
        cells['attrs'] = attrs
        
        html = render_to_string(template, cells, request)
        return html
    
    #
    # AJAX
    #
    
    def generate_ajax_json(self, query, request, data_dict, total_length, current_length):
        datas = []
        
        # start = int(gt('iDisplayStart'))
        
        gt = data_dict.get
        echo = gt('sEcho')
        
        row_id = self.get_item_pk()
        
        for record in query:
            self._instance = record
            pk = getattr(record, row_id)
            self.pk = pk
            datas.append(self.render_item(record, request))
        
        data = {
            "aData": datas,
            "sEcho": echo,
            "iTotalRecords": total_length,
            "iTotalDisplayRecords": current_length
        }
        
        return data
    
    def get_ajax_url(self, kwargs, table_id):
        try:
            ajax_source = reverse(self.listing_id, kwargs=kwargs)
            self.save_url = reverse(self.listing_id + "-save", kwargs=kwargs)
        except Exception, ex:
            from views import has_registered
            
            if not has_registered(table_id):
                t = type(self)
                m = t.__module__
                n = t.__name__
                raise Exception('This listing "%s.%s" is not registered.' % (m, n))
            raise Exception('Django helpers is not added in root urls.')
        return ajax_source
    
    # noinspection PyAttributeOutsideInit
    def get_list_url(self, name):
        if not hasattr(self, 'url_kwargs'):
            raise
        
        kwargs = {
            'field_name': name,
            'pk': self.get_pk()
        }
        
        if self.url_kwargs is not None:
            kwargs.update(self.url_kwargs)
        
        return reverse(self.id() + "-list", kwargs=kwargs)
    
    def get_save_url(self):
        if not hasattr(self, 'save_url'):
            kwargs = self.url_kwargs
            self.save_url = reverse(self.id() + "-save", kwargs=kwargs)
        
        return self.save_url


# noinspection PyAbstractClass
class SimpleListing(Listing):
    template = 'django-helpers-listings/basic-listing.html'
    show_footer = False
    show_header = False
    isotope_support = False
    mode = 'complete'


ListingModes = {"PAGED": "paged", 'INFINITE': "infinite", 'COMPLETE': "complete"}
