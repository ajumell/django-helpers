# coding=utf-8
from django.conf.urls import url
from django.http import HttpResponse
from django_helpers.utils.importlib import import_module

import urls
from django_helpers import add_app_url

add_app_url('listings', urls)

_URL_PREFIX = 'data-listing-'

_REGISTERED_LISTINGS = {}


def has_registered(name):
    return name in _REGISTERED_LISTINGS


def get_listing(name, request, **kwargs):
    obj = _REGISTERED_LISTINGS.get(name)

    if isinstance(obj, str):
        i = obj.rfind('.')
        module_name = obj[:i]
        name = obj[i + 1:]
        module_obj = import_module(module_name)
        obj = getattr(module_obj, name)
    if getattr(obj, 'needs_kwargs', False):
        instance = obj(request, **kwargs)
    else:
        instance = obj(request)
    return instance


def get_data(request, name, **kwargs):
    listing = get_listing(name, request, **kwargs)
    if listing is None:
        return HttpResponse()

    listing.set_params(**kwargs)
    results = listing.get_data(request)
    return results


def save(request, name, **kwargs):
    if request.method != 'POST':
        return HttpResponse()

    gt = request.POST.get
    pk = gt('pk')
    listing = get_listing(name, request, **kwargs)

    if listing is None:
        return HttpResponse()

    listing.set_params(**kwargs)
    listing.pk = pk
    field_name = gt('name')
    results = listing.save(request, field_name)
    return results


def list_data(request, name, field_name, pk, **kwargs):
    # gt = request.POST.get
    # pk = gt('pk')
    listing = get_listing(name, request, **kwargs)
    if listing is None:
        return HttpResponse()

    listing.set_params(**kwargs)
    listing.pk = pk
    results = listing.list(field_name)
    return results


def create_reg(name):
    return "(?P<%s>.+?)/" % name


def register_name(class_name, name, url_search_parameters):
    if name is None:
        return

    if name in _REGISTERED_LISTINGS:
        old_name = _REGISTERED_LISTINGS[name]

        msg = "You tried to register a data listing with id " + name + " for " + class_name + ".\n"
        msg += "But " + name + " is already registered for " + old_name + ".\n"
        msg += "Try giving a new ID for " + class_name

        raise Exception(msg)

    _REGISTERED_LISTINGS[name] = class_name

    data_reg = "^%s/data/" % name
    save_reg = "^%s/save/" % name
    list_reg = "^%s/list/" % name
    list_reg += create_reg('field_name')

    extra_url_parameters = url_search_parameters
    if extra_url_parameters is not None:
        for parameter in extra_url_parameters:
            data_reg += create_reg(parameter)
            save_reg += create_reg(parameter)
            list_reg += create_reg(parameter)

    list_reg += create_reg('pk')

    data_reg += "$"
    save_reg += "$"
    list_reg += "$"

    data_pattern = url(r"%s" % data_reg, get_data, name=name, kwargs={
        "name": name
    })

    save_pattern = url(r"%s" % save_reg, save, name=name + '-save', kwargs={
        "name": name
    })

    list_pattern = url(r"%s" % list_reg, list_data, name=name + '-list', kwargs={
        "name": name
    })

    urls.urlpatterns.append(data_pattern)
    urls.urlpatterns.append(save_pattern)
    urls.urlpatterns.append(list_pattern)
