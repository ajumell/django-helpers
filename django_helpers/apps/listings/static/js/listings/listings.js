(function ($) {
    /**
     * Sorting
     * No items available
     */
    var
        oDefaultOptions,
        fnPerformSearch,
        fnListing,
        fnGeneratePagination,
        fnInitPagination,
        fnUpdateContents,
        fnGenerateServerParams,
        fnGenerateOPagination,
        fnUpdatePageInfo,
        fnFindElement,
        fnRemoveElement,
        fnPerformAction,

        fnHideContainerIfEmpty,
        fnProcessFilters,
        fnSanitizeFilterText,

        dDocument,
        dWindow,
        aScrollCallbacks,
        fnScrollEventAction,
        fnInvokeScrollCallback,
        fnUpdateOPagination;

    aScrollCallbacks = [];

    dDocument = $(document);

    dWindow = $(window);

    oDefaultOptions = {
        sUrl: undefined,

        /*
         * Paged
         * Infinite
         * Complete
         * */
        sMode: 'paged',

        /**
         * Boolean Flags
         */
        bHasPagination: false,

        bHasInitialData: false,

        bClientSideOnly: false,

        bHideContainerIfEmpty: false,

        sContainerSelector: false,

        /**
         * Selectors
         */
        sItemsContainer: undefined,

        sItemSelector: '[data-listing-item]',

        sPaginationContainer: undefined,

        sInfoContainer: undefined,

        sLoadingContainer: undefined,

        sSearchBox: undefined,

        fnServerParams: function (aData) {

        },

        sAjaxMethod: 'GET',

        /*
         * Functions
         */
        fnSetHTML: function (oContainer, sHTML) {
            oContainer.html(sHTML);
        },

        fnAppendHTML: function (oContainer, sHTML) {
            oContainer.append(sHTML);
        },

        fnShowLoadingMessage: function (oLoading) {
            if (oLoading) {
                oLoading.stop().fadeIn('fast');
            }
        },

        fnHideLoadingMessage: function (oLoading) {
            if (oLoading) {
                oLoading.stop().fadeOut('fast');
            }
        },

        /**
         * Search
         */


        fnSearch: function (dItem, sSearchTerm) {
            return dItem.text().toLowerCase().indexOf(sSearchTerm) >= 0;
        },

        fnHideItem: function (dItem) {
            if (dItem) {
                dItem.stop().slideUp();
            }
        },

        fnShowItem: function (dItem) {
            if (dItem) {
                dItem.stop().show();
            }
        },

        /**
         * Events
         */
        fnContentsUpdated: function (oContainer) {
        },

        /**
         * Pagination
         */
        iDisplayLength: 10,

        iTotal: undefined,

        iBufferPixel: 40,

        /**
         * Isotope Support
         */
        bIsotopeSupport: false,

        fnSetHTMLIsotope: function (oOptions, dContainer, sHTML) {
            var aChildren = dContainer.children();
            dContainer.isotope('remove', aChildren);
            oOptions.fnAppendHTMLIsotope(oOptions, dContainer, sHTML);
        },

        fnAppendHTMLIsotope: function (oOptions, dContainer, aHTML) {
            var aElements = [];
            $.each(aHTML, function (iIndex, sItem) {
                var dItem = $(sItem);
                if (dItem && dItem.length) {
                    aElements.push(dItem[0]);
                }
            });

            dContainer.append(aElements);
            dContainer.isotope('appended', aElements);

            if (imagesLoaded) {
                imagesLoaded(dContainer[0], function () {
                    dContainer.isotope('layout');
                });
            }
        },

        fnHideItemIsotope: function (dContainer, dItem) {
            dItem.each(function () {
                var oItem = dContainer.isotope('getItem', this);
                if (!oItem.isHidden) {
                    dContainer.isotope('hide', [oItem]);
                }
            });
        },

        fnShowItemIsotope: function (dContainer, dItem) {
            dItem.each(function () {
                var oItem = dContainer.isotope('getItem', this);
                if (oItem.isHidden) {
                    dContainer.isotope('reveal', [oItem]);
                }
            });
        }

    };

    fnFindElement = function (dObject, sSelector) {
        if (!sSelector) {
            return;
        }
        var dItem = jQuery(sSelector);
        if (!dItem.length) {
            return;
        }
        return dItem;
    };

    fnRemoveElement = function (dElement) {
        if (!dElement) {
            return;
        }
        if (!dElement.length) {
            return;
        }
        dElement.remove();
    };

    fnGenerateServerParams = function (aData, oPagination, iEcho) {
        aData.push({
            name: "iDisplayStart",
            value: oPagination.iStart
        });

        aData.push({
            name: "iDisplayLength",
            value: oPagination.iLength
        });

        aData.push({
            name: "sEcho",
            value: iEcho
        });
    };

    fnPerformSearch = function (aData, dSearchBox, dFilters) {
        if (dSearchBox) {
            aData.push({
                name: "sSearch",
                value: dSearchBox.val()
            });
        }

        if (dFilters.length) {
            dFilters.each(function () {
                var $this = $(this);
                aData.push({
                    name: $this.attr("name"),
                    value: $this.val()
                });
            });
        }

    };

    fnUpdateContents = function (dContainer, sHTML, oOptions, dLoadingMessagePanel, oPagination) {
        var sMode,
            iStart;

        iStart = oPagination.iStart;
        sMode = oOptions.sMode;

        if (sMode === 'paged' || iStart === 0) {
            if (oOptions.bIsotopeSupport) {
                oOptions.fnSetHTMLIsotope(oOptions, dContainer, sHTML);
            } else {
                oOptions.fnSetHTML(dContainer, sHTML);
            }
        } else if (sMode === 'infinite') {
            if (oOptions.bIsotopeSupport) {
                oOptions.fnAppendHTMLIsotope(oOptions, dContainer, sHTML);
            } else {
                oOptions.fnAppendHTML(dContainer, sHTML);
            }
        }

        dContainer.contentUpdated();
        oOptions.fnContentsUpdated(dContainer);
        fnHideContainerIfEmpty(oOptions, dContainer);
        fnProcessFilters(oOptions, dContainer);

        if (dLoadingMessagePanel) {
            oOptions.fnHideLoadingMessage(dLoadingMessagePanel)
        }
    };

    fnHideContainerIfEmpty = function (oOptions, dContainer) {
        var jqParent;
        var sSelector;

        if (oOptions.bHideContainerIfEmpty) {
            sSelector = oOptions.sContainerSelector;
            jqParent = dContainer.parents(sSelector);
            if (dContainer.children().length == 0) {
                jqParent.hide();
            } else {
                jqParent.show();
            }
        }
    };

    fnSanitizeFilterText = function (sText) {
        var sTextId;
        var aChars;
        var cChar;
        var iIndex;
        var iCount;

        if (!sText) {
            return;
        }

        aChars = [" ", "(", ")", "_", "[", "]"]
        iCount = aChars.length;
        sTextId = sText.toLowerCase();

        sTextId = sTextId.replace(/\s+/, "");
        sTextId = sTextId.trim();

        for (iIndex = 0; iIndex < iCount; iIndex += 1) {
            cChar = aChars[iIndex];
            sTextId = sTextId.split(cChar).join("-");
        }

        return sTextId;
    };

    fnProcessFilters = function (oOptions, dContainer) {
        var oCurrentFilters;
        var aFilters;
        var jqFilterItems;
        var jqChildren;

        if (!oOptions.aFilters) {
            oOptions.aFilters = [];
        }

        if (!oOptions.oCurrentFilters) {
            oOptions.oCurrentFilters = {};
        }

        aFilters = oOptions.aFilters;
        // TODO: This is In Complete
        oCurrentFilters = oOptions.oCurrentFilters;

        if (!oOptions.bHasAttachedEvent) {
            oOptions.bHasAttachedEvent = true;
            dContainer.on("click", "[data-filter-label]", function (oEvent) {
                var sFilterLabel;
                var sFilterValue;
                var jqElem;

                jqElem = $(this);

                sFilterValue = jqElem.attr("data-filter-value");
                sFilterLabel = jqElem.attr("data-filter-label");

                dContainer.children().each(function () {
                    var jqChild = $(this);
                    var sSelector = "[data-filter-" + sFilterLabel + "=" + sFilterValue + "]";

                    if (jqChild.is(sSelector)) {
                        jqChild.show();
                    } else {
                        jqChild.hide();
                    }
                });

                return false;
            });

        }

        jqChildren = dContainer.children();

        jqChildren.each(function () {
            var jqChild = $(this);

            jqFilterItems = jqChild.find("[data-filter-label]");
            jqFilterItems.each(function () {
                var jqLabel;
                var sLabel;
                var sLabelId;
                var sText;
                var sTextId;

                jqLabel = $(this);
                sLabel = jqLabel.attr("data-filter-label");

                if (aFilters.indexOf(sLabel) < 0) {
                    aFilters.push(sLabel);
                }

                sText = jqLabel.text();
                sTextId = fnSanitizeFilterText(sText);
                sLabelId = fnSanitizeFilterText(sLabel);

                jqChild.attr("data-filter-" + sLabelId, sTextId);
                jqLabel.attr("data-filter-value", sTextId);
                jqLabel.attr("data-filter-label", sLabelId);
            });

        });
    };

    /**
     * Pagination
     */

    fnGenerateOPagination = function (options) {
        var total,
            length,
            end,
            pages;

        total = options.iTotal;
        length = options.iDisplayLength;

        pages = parseInt((total - 1) / length, 10) + 1

        if (total < length) {
            end = total;
        } else {
            end = length;
        }

        return {
            iEnd: end,
            iFilteredTotal: total,
            iLength: length,
            iPage: 0,
            iStart: 0,
            iTotal: total,
            iTotalPages: pages
        };
    }

    fnUpdateOPagination = function (oPagination, oResponse) {
        var total,
            length,
            end,
            start,
            page,
            iFilteredTotal,
            pages;

        //noinspection JSUnresolvedVariable
        iFilteredTotal = oResponse.iTotalDisplayRecords

        //noinspection JSUnresolvedVariable
        total = oResponse.iTotalRecords;

        length = oPagination.iLength;
        start = oPagination.iStart;
        end = start + length;
        page = Math.ceil(start / length)

        pages = parseInt((iFilteredTotal - 1) / length, 10) + 1

        if (end > iFilteredTotal) {
            end = iFilteredTotal;
        }

        $.extend(oPagination, {
            iEnd: end,
            iFilteredTotal: iFilteredTotal,
            iLength: length,
            iPage: page,
            iTotal: total,
            iTotalPages: pages
        });
    };

    fnInitPagination = function (oPagination, dPagination, oApi) {
        if (!dPagination) {
            return;
        }

        var oLang = {
            sPrevious: "Prev",
            sNext: "Next"
        };

        var fnClickHandler = function (e) {
            e.preventDefault();
            oApi.fnChangePage(e.data.action);
            return false;
        };

        dPagination.html(
            '<li class="prev disabled"><a href="#">&larr; ' + oLang.sPrevious + '</a></li>' +
            '<li class="next disabled"><a href="#">' + oLang.sNext + ' &rarr; </a></li>'
        );
        var els = $('a', dPagination);
        $(els[0]).bind('click.DT', {action: "previous"}, fnClickHandler);
        $(els[1]).bind('click.DT', {action: "next"}, fnClickHandler);

    };

    fnGeneratePagination = function (oPagination, dPagination, oApi) {
        if (!dPagination) {
            return;
        }

        var iListLength = 5;
        var oPaging = oPagination;
        var an = dPagination;
        var i, ien, j, sClass, iStart, iEnd, iHalf = Math.floor(iListLength / 2);

        if (oPaging.iTotalPages < iListLength) {
            iStart = 1;
            iEnd = oPaging.iTotalPages;
        }
        else if (oPaging.iPage <= iHalf) {
            iStart = 1;
            iEnd = iListLength;
        } else if (oPaging.iPage >= (oPaging.iTotalPages - iHalf)) {
            iStart = oPaging.iTotalPages - iListLength + 1;
            iEnd = oPaging.iTotalPages;
        } else {
            iStart = oPaging.iPage - iHalf + 1;
            iEnd = iStart + iListLength - 1;
        }

        for (i = 0, ien = an.length; i < ien; i++) {
            // Remove the middle elements
            $('li:gt(0)', an[i]).filter(':not(:last)').remove();

            if (oPagination.iFilteredTotal > 0) {

                // Add the new list items and their event handlers
                for (j = iStart; j <= iEnd; j++) {
                    sClass = (j == oPaging.iPage + 1) ? 'class="active"' : '';
                    $('<li ' + sClass + '><a href="#">' + j + '</a></li>')
                        .insertBefore($('li:last', an[i])[0])
                        .bind('click', function (e) {
                            e.preventDefault();
                            oPagination.iStart = (parseInt($('a', this).text(), 10) - 1) * oPaging.iLength;
                            oApi.fnDraw();
                        });
                }
            }

            // Add / remove disabled classes from the static elements
            if (oPaging.iPage === 0) {
                $('li:first', an[i]).addClass('disabled');
            } else {
                $('li:first', an[i]).removeClass('disabled');
            }

            if (oPaging.iPage === oPaging.iTotalPages - 1 || oPaging.iTotalPages === 0) {
                $('li:last', an[i]).addClass('disabled');
            } else {
                $('li:last', an[i]).removeClass('disabled');
            }
        }

    };

    fnUpdatePageInfo = function (oPagination, dInfoPanel) {

        if (!dInfoPanel) {
            return;
        }
        var msg = "Showing " + (oPagination.iStart + 1) + " to " + oPagination.iEnd + " of " + oPagination.iFilteredTotal + " entries";

        if (oPagination.iFilteredTotal !== oPagination.iTotal) {
            msg = msg + "(filtered from " + oPagination.iTotal + " total entries)";
        }

        dInfoPanel.html(msg);
    };

    /**
     * Infinite Scrolling
     */
    fnScrollEventAction = function () {
        var i,
            len;

        len = aScrollCallbacks.length;
        for (i = 0; i < len; i += 1) {
            fnInvokeScrollCallback(aScrollCallbacks[i]);
        }

    };

    fnInvokeScrollCallback = function (oScrollCallback) {
        var dContainer,
            iBufferPixel,
            iDocumentHeight,
            iContainerHeight,
            iContainerTop,
            iPixelsFromWindowBottomToBottom,
            iPixelsFromNavToBottom,
            iWindowHeight, iWindowScrollTop;

        dContainer = oScrollCallback.dContainer;
        iBufferPixel = oScrollCallback.iBufferPixel;
        iContainerHeight = dContainer.height();
        iContainerTop = dContainer.offset().top;
        iDocumentHeight = dDocument.height();

        iPixelsFromNavToBottom = iDocumentHeight - (iContainerTop + iContainerHeight);

        iWindowHeight = dWindow.height();
        iWindowScrollTop = dWindow["scrollTop"]();
        iPixelsFromWindowBottomToBottom = iDocumentHeight - iWindowScrollTop - iWindowHeight;

        if (iPixelsFromWindowBottomToBottom - iBufferPixel < iPixelsFromNavToBottom) {
            oScrollCallback.oApi.fnChangePage('next');
        }
    };

    dWindow.on('scroll', fnScrollEventAction);

    /**
     *
     */

    /**
     * Plugin
     */
    fnListing = function (dObj, oOptions) {
        var
            bIsotopeSupport,
            sMode,
            aPreviousData,
            iEcho,
            oApi,
            fnGeneratePreviousData,
            iSearchTimer,
            dContainer,
            dInfoPanel,
            dSearchBox,
            dFilters,
            dLoadingMessagePanel,
            dPagination,
            oPagination;

        iEcho = 0;

        dContainer = jQuery(oOptions.sItemsContainer);
        dObj.attr("data-is-listing", true);

        if (dContainer.length < 1) {
            // If not container is found then fail silently
            return;
        }

        dInfoPanel = fnFindElement(dObj, oOptions.sInfoContainer);
        dSearchBox = fnFindElement(dObj, oOptions.sSearchBox);
        dFilters = fnFindElement(dObj, oOptions.sFilters);
        dPagination = fnFindElement(dObj, oOptions.sPaginationContainer);
        dLoadingMessagePanel = fnFindElement(dObj, oOptions.sLoadingContainer);

        bIsotopeSupport = oOptions.bIsotopeSupport;
        if (bIsotopeSupport && !jQuery.fn.isotope) {
            bIsotopeSupport = oOptions.bIsotopeSupport = false;
        }

        if (bIsotopeSupport) {
            dContainer.isotope({
                itemSelector: oOptions.sItemSelector
            });
        }

        oOptions.fnHideLoadingMessage(dLoadingMessagePanel);
        if (dSearchBox) {
            dSearchBox.on("keyup blur", function () {
                oApi.fnSearch();
            });
        }

        if (dFilters.length) {
            dFilters.each(function () {
                var $this = $(this);
                $this.on("keyup blur", function () {
                    oApi.fnSearch();
                });
            });
        }

        sMode = oOptions.sMode;
        if (sMode === "complete" || sMode === "infinite") {
            fnRemoveElement(dPagination);
            fnRemoveElement(dInfoPanel);
        }

        oPagination = fnGenerateOPagination(oOptions);


        fnGeneratePreviousData = function (aData) {
            var iIndex, iCount;
            aPreviousData = [];
            if (aData === undefined) {
                fnPerformSearch(aPreviousData, dSearchBox, dFilters);
                oOptions.fnServerParams(aPreviousData);
            } else {
                iCount = aData.length;
                for (iIndex = 0; iIndex < iCount; iIndex += 1) {
                    aPreviousData.push(aData[iIndex]);
                }
            }

        };

        fnGeneratePreviousData();

        oApi = {
            fnSearch: function () {
                oPagination.iStart = 0;
                var sMode = oOptions.sMode;
                if (sMode === "complete") {
                    oApi.fnSearchLocal();
                } else if (sMode === 'infinite' && oPagination.iEnd === oPagination.iTotal) {
                    oApi.fnSearchLocal();
                } else {
                    oApi.fnDraw(true);
                }
            },

            fnSearchLocal: function () {
                var sSearchTerm = dSearchBox.val();
                dContainer.children().each(function () {
                    var dItem = $(this);

                    if (oOptions.fnSearch(dItem, sSearchTerm)) {
                        if (bIsotopeSupport) {
                            oOptions.fnShowItemIsotope(dContainer, dItem)
                        } else {
                            oOptions.fnShowItem(dItem);
                        }
                    } else {
                        if (bIsotopeSupport) {
                            oOptions.fnHideItemIsotope(dContainer, dItem)
                        } else {
                            oOptions.fnHideItem(dItem);
                        }
                    }

                });

                if (bIsotopeSupport) {
                    dContainer.isotope('layout');
                }
            },

            fnDrawActual: function (bCheckParams) {
                var aData = [];
                var aDiff;

                oOptions.fnShowLoadingMessage(dLoadingMessagePanel);

                fnPerformSearch(aData, dSearchBox, dFilters);
                oOptions.fnServerParams(aData);

                if (bCheckParams === true) {
                    aDiff = DeepDiff.diff(aData, aPreviousData);
                    if (aDiff === undefined) {
                        return;
                    }
                }

                fnGeneratePreviousData(aData);

                iEcho += 1;
                fnGenerateServerParams(aData, oPagination, iEcho);
                $.ajax({
                    url: oOptions.sUrl,
                    type: oOptions.sAjaxMethod,
                    data: aData,
                    dataType: "json",
                    success: function (oResponse) {
                        var sHTML;

                        if (oResponse['sEcho'] !== iEcho + "") {
                            return;
                        }

                        sHTML = oResponse["aData"];
                        fnUpdateContents(dContainer, sHTML, oOptions, dLoadingMessagePanel, oPagination);
                        fnUpdateOPagination(oPagination, oResponse);
                        fnGeneratePagination(oPagination, dPagination, oApi);
                        fnUpdatePageInfo(oPagination, dInfoPanel);
                        dContainer.trigger('content-changed');
                    }
                });
            },

            fnDraw: function (bCheckParams) {
                clearTimeout(iSearchTimer);
                iSearchTimer = setTimeout(function () {
                    oApi.fnDrawActual(bCheckParams);
                }, 200);
            },

            fnUpdateItem: function (pk, html) {

            },

            fnChangePage: function (page) {
                var iTotal,
                    iStart,
                    iPages,
                    iPrevStart,
                    iCurrentPage;

                iCurrentPage = oPagination.iPage;
                iPrevStart = oPagination.iStart;
                iPages = oPagination.iTotalPages;

                if (page === 'next') {
                    iCurrentPage += 1;
                } else if (page === 'previous') {
                    iCurrentPage -= 1;
                } else {
                    iCurrentPage = page;
                }

                if (iCurrentPage < 0) {
                    iCurrentPage = 0;
                }

                if (iCurrentPage > iPages) {
                    iCurrentPage = iPages;
                }

                iTotal = oPagination.iFilteredTotal;
                iStart = iCurrentPage * oPagination.iLength;

                if (iStart > iTotal) {
                    return;
                }

                if (iStart < 0) {
                    iStart = 0;
                }

                if (iPrevStart === iStart) {
                    return;
                }

                oPagination.iStart = iStart;
                oApi.fnDraw();
            }
        };

        fnInitPagination(oPagination, dPagination, oApi);
        fnGeneratePagination(oPagination, dPagination, oApi);
        fnUpdatePageInfo(oPagination, dInfoPanel);

        if (sMode === "infinite") {
            aScrollCallbacks.push({
                'dContainer': dContainer,
                'iBufferPixel': oOptions.iBufferPixel,
                'oApi': oApi
            });
        }

        if (bIsotopeSupport && imagesLoaded) {
            imagesLoaded(dContainer[0], function () {
                dContainer.isotope('layout');
            });
        }

        oOptions.fnContentsUpdated(dContainer);
        fnHideContainerIfEmpty(oOptions, dContainer);
        fnProcessFilters(oOptions, dContainer);

        dObj.data("oListingApi", oApi);

        return oApi;
    };

    fnPerformAction = function (dObj, sAction, aParams) {
        var oApi,
            fnAction;

        oApi = dObj.data("oListingApi");
        if (!oApi) {
            return false;
        }
        fnAction = oApi[sAction];
        if (fnAction) {
            fnAction(aParams);
        }
    };

    $.fn.listings = function (options, param) {
        if ($.type(options) === 'string') {
            return fnPerformAction(this, options, param);
        }
        options = $.extend({}, oDefaultOptions, options);
        if (this.length === 1) {
            return fnListing(this, options);
        } else if (this.length > 1) {
            this.each(function () {
                fnListing($(this), options);
            });
        }
    };

    $.fn.listings.defaultOptions = oDefaultOptions;

}(jQuery));

jQuery && jQuery(function ($) {

    var dj = $.dj;
    if (!dj) {
        return;
    }


    var fnRegisterPreProcessor = dj["fnRegisterPreProcessor"];

    var fnRegisterElementPreProcessor = dj["fnRegisterElementPreProcessor"];

    if (fnRegisterPreProcessor) {
        fnRegisterPreProcessor("dj-refresh-listings", function (data) {
            var count,
                i,
                sListingID,
                dListing,
                aListings = data["dj-refresh-listings"];

            if (aListings) {
                count = aListings.length;
                for (i = 0; i < count; i += 1) {
                    sListingID = aListings[i];
                    dListing = $("[data-listing-id=" + sListingID + "]");
                    if (dListing.length && dListing.listings) {
                        dListing.listings("fnDraw");
                    }
                }
            }
        });

        fnRegisterPreProcessor("dj-refresh-listables", function (data) {
            var count,
                i,
                sListingID,
                dListings,
                aListings = data["dj-refresh-listables"];

            if (aListings) {
                count = aListings.length;
                for (i = 0; i < count; i += 1) {
                    sListingID = aListings[i];
                    dListings = $(sListingID);

                    dListings.each(function () {
                        var dListing = $(this);
                        if (dListing.listings) {
                            dListing.listings("fnDraw");
                        }
                    })

                }
            }
        });
    }

    if (fnRegisterElementPreProcessor) {
        var refreshParentListing = function ($this) {
            var listing, oTable;

            if (!$this) {
                return;
            }

            listing = $this.parents('[data-is-listing]');
            if (listing.length) {
                oTable = listing.data("oListingApi");
                if (oTable) {
                    oTable.fnDraw();
                }
            }
        };

        fnRegisterElementPreProcessor("dj-refresh-listing", function (dElement) {
            refreshParentListing(dElement);
        });
    }

});

jQuery && jQuery(function ($) {
    var dj,
        registerDataWidget,
        generateDataOptions;

    dj = $.dj;
    registerDataWidget = dj.registerDataWidget;
    generateDataOptions = dj.generateDataOptions;


    registerDataWidget('django-listings', function (obj) {
        var mappings, options;

        mappings = {
            'listingId': 'listingID',
            'listingMode': 'sMode',
            'total': 'iTotal',
            'displayLength': 'iDisplayLength',
            'ajaxSource': 'sUrl',

            'hideContainer': 'bHideContainerIfEmpty',
            'containerSelector': 'sContainerSelector',

            'listingIsotopeSupport': 'bIsotopeSupport'
        };

        options = generateDataOptions(obj, mappings);

        options["sItemSelector"] = "[data-listing-item=" + options.listingID + "]";
        options["sPaginationContainer"] = "[data-listing-pagination=" + options.listingID + "]";
        options["sInfoContainer"] = "[data-listing-info=" + options.listingID + "]";
        options["sItemsContainer"] = "[data-listing-container=" + options.listingID + "]";
        options["sLoadingContainer"] = ".loading-animation";
        options["sSearchBox"] = "[data-listing-search=" + options.listingID + "]";
        options["sFilters"] = "[data-" + options.listingID + "-filter]";

        var oApi = obj.listings(options);

    });

});
