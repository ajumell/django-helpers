# coding=utf-8
__author__ = 'ajumell'

from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    args = '<app.model> <field>'
    help = 'Re-generates thumbnails for all instances of the given model, for the given field.'

    def handle(self, *args, **options):
        self.args = args
        self.options = options

        self.validate_input()
        self.parse_input()
        self.regenerate_thumbs()

    def validate_input(self):
        num_args = len(self.args)

        if num_args < 2:
            raise CommandError("Please pass the app.model and the field to generate thumbnails for.")
        if num_args > 2:
            raise CommandError("Too many arguments provided.")

        if '.' not in self.args[0]:
            raise CommandError("The first argument must be in the format of: app.model")

    def parse_input(self):
        """
        Go through the user input, get/validate some important values.
        """
        app_split = self.args[0].split('.')
        app = app_split[0]
        model_name = app_split[1].lower()

        try:
            self.model = ContentType.objects.get(app_label=app, model=model_name)
            self.model = self.model.model_class()
        except ContentType.DoesNotExist:
            raise CommandError("There is no app/model combination: %s" % self.args[0])

        # String field name to re-generate.
        self.field = self.args[1]

    def regenerate_thumbs(self):
        from ...fields.thumbnail import ThumbnailImageFieldFile

        model_class = self.model
        instances = model_class.objects.all()
        for instance in instances:
            image_file = getattr(instance, self.field)

            if isinstance(image_file, ThumbnailImageFieldFile):
                image_file.generate_thumbnails()
