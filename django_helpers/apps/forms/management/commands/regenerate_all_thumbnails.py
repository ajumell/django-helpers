# coding=utf-8
from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand

__author__ = 'ajumell'

get_models = apps.get_models
get_app = apps.get_app_config


class Command(BaseCommand):
    help = 'Re generates thumbnails for all instances of all models.'
    
    def handle(self, *args, **options):
        apps = settings.INSTALLED_APPS
        for app_name in apps:
            try:
                if '.' in app_name:
                    i = app_name.rfind('.')
                    app_name = app_name[i + 1:]
                app = get_app(app_name)
            except Exception, ex:
                print ex
                continue
            
            models = get_models(app)
            for model in models:
                self.regenerate_thumbs(model)
    
    # noinspection PyMethodMayBeStatic,PyProtectedMember
    def regenerate_thumbs(self, model_class):
        from ...fields.thumbnail import ThumbnailImageField
        
        model_meta = model_class._meta
        
        all_fields = model_meta.fields
        image_fields = []
        for field in all_fields:
            if isinstance(field, ThumbnailImageField):
                image_fields.append(field.name)
        
        if len(image_fields) == 0:
            return
        
        print 'Found Thumbnail Field in model {0}, app {1}'.format(model_meta.model_name, model_meta.app_label)
        
        instances = model_class.objects.all()
        count = 0
        for instance in instances:
            for field in image_fields:
                fp = getattr(instance, field)
                try:
                    fp.generate_thumbnails(ignore_existing=True)
                    count += 1
                except ValueError:
                    pass
                except Exception, ex:
                    print ex
                    raise
        
        print 'Generated thumbnails for {0} out of {1} instances.'.format(count, len(instances))
