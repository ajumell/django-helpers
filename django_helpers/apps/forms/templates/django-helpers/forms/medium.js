(function () {
    var selector;
    var editor;
    var $area;

    selector = '#' + '{{ id }}';
    $area = $(selector);
    $area.removeClass('form-control');

    editor = new MediumEditor(selector, {{ options.render|safe }});
    $area.data('medium-editor', editor);

}())