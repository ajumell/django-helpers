jQuery(function ($) {
    $('#' + '{{ id }}').clockface({
        format : '{{ format }}',
        trigger : '{{ trigger }}'
    });
});