jQuery(function ($) {
    $('#' + '{{ id }}').daterangepicker({
        format : '{{ format }}'
    });
});