jQuery(function ($) {
    var obj;

    obj = $('#' + '{{ id }}');

    obj.bootstrapSwitch({
        animate : {% if animate %}true{% else %}false{% endif %},
        {% if onColor %}onColor : "{{ onColor }}", {% endif %}
        {% if offColor %}offColor : "{{ offColor }}", {% endif %}
        {% if labelText %}labelText : "{{ labelText|safe }}", {% endif %}
        {% if onText %}onText : "{{ onText|safe }}", {% endif %}
        {% if offText %}offText : "{{ offText|safe }}", {% endif %}
    });
});