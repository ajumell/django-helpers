jQuery && jQuery(function () {
    $("#" + "{{ html_id }}").popupForm({
        "link": "{{ link }}",
        "modal": "#" + "{{ modal_id }}",
        "form_id": "#" + "{{ form_id }}",
        "submitURL": "{{ submitURL }}"
    });
});