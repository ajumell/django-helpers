jQuery && jQuery(function ($) {
    $("#" + "{{ html_id }}").array_fields({
        {% if not can_add %}add_btn_selector : false, {% endif %}
        {% if not can_add %}can_add : false, {% endif %}
        {% if not can_delete %}delete_btn_selector : false, {% endif %}
        render_js : function (elem) {
            {% autoescape off %}{{ js_code|default:"" }}{% endautoescape %}
        }
    })
});