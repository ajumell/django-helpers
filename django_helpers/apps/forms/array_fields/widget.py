# coding=utf-8
import copy
from inspect import getargspec

from django import forms
from django.forms.forms import pretty_name

from django_helpers.apps.static_manager import register_js
from django_helpers.helpers.views import render_to_string

__author__ = 'ajumell'
__all__ = ['ArrayFieldTableWidget']

register_js("array-field-js", "js/array-field/array-field.js", ['jquery'])


class ArrayFieldTableWidget(forms.Widget):
    js_files = ['array-field-js']
    has_js = True
    template = 'array-field/table-html.html'

    def __init__(self, fields, widths=None, can_add=True, can_delete=True, initial_field=1, attrs=None):
        self.initial_field = initial_field
        self.can_add = can_add
        self.can_delete = can_delete
        self.widths = widths
        self.fields = fields
        self.html_id = None

        super(ArrayFieldTableWidget, self).__init__(attrs)

    def render_field(self, field, key, field_name, item=None):
        field = copy.deepcopy(field)
        if item is not None:
            field_value = item[key]
        else:
            field_value = None

        field_html_name = field_name + "_" + key + "[]"
        widget = field.widget
        render_function = widget.render

        def render(name, value, attrs=None):
            if type(attrs) is dict:
                if 'id' in attrs:
                    print attrs.pop('id')
            return render_function(name, value, attrs)

        widget.render = render
        html = widget.render(field_html_name, field_value)
        widget.render = render_function
        return html

    def render_field_js(self, field, field_name):
        field = copy.deepcopy(field)
        name = self.name

        widget = field.widget
        has_js = getattr(field, 'render_js', None)
        field_html_name = name + "_" + field_name + "[]"
        if has_js is None:
            has_js = getattr(widget, 'render_js', None)
            widget.name = field_html_name
            widget.value = None
        else:
            field.name = field_html_name
            field.value = None

        if has_js is None:
            return None

        argspec = getargspec(has_js)
        arg_names = argspec.args
        kwargs = {}

        if 'array_field' in arg_names:
            kwargs['array_field'] = True

        if 'name_prefix' in arg_names:
            kwargs['name_prefix'] = name

        code = has_js(**kwargs)
        return code

    def render(self, name, value, attrs=None):
        columns = []
        widths = self.widths
        fields = self.fields
        self.name = name

        if widths is None:
            widths = {}

        initial_data = []
        fields_items = fields.items()
        if value is not None:
            for item in value:
                data = []
                for key, field in fields_items:
                    html = self.render_field(field, key, name, item)
                    data.append(html)

                initial_data.append(data)

        for field_name, field in fields_items:
            field = copy.deepcopy(field)
            column = {
                "title": field.label or pretty_name(field_name),
                "width": widths.get(field_name),
                "html": self.render_field(field, field_name, name)
            }
            columns.append(column)

        html_id = None
        if attrs is not None:
            html_id = attrs.pop('id')

        self.html_id = html_id
        return render_to_string(self.template, {
            "can_add": self.can_add,
            "can_delete": self.can_delete,
            'columns': columns,
            'initial_data': initial_data,
            'html_id': html_id,
            "initial_field_count": range(self.initial_field)
        })

    def render_js(self):
        js_code = []
        fields = self.fields
        for field_name, field in fields.items():
            code = self.render_field_js(field, field_name)
            if code is not None:
                js_code.append(code)

        js = render_to_string("array-field/table-js.js", {
            "can_add": self.can_add,
            "can_delete": self.can_delete,
            "html_id": self.html_id,
            "js_code": ';\n'.join(js_code)
        })
        return js

    def value_from_datadict(self, data, files, name):
        fields = self.fields
        collected_data = {}
        data_list = []

        count = 0
        for field_name, field in fields.items():
            field_html_name = name + "_" + field_name + "[]"
            field_data = data.getlist(field_html_name)
            collected_data[field_name] = field_data
            count = len(field_data)

        i = 0
        while i < count:
            item = {}
            for key in collected_data:
                item[key] = collected_data[key][i]

            if not all(v == "" for v in item.values()):
                data_list.append(item)

            i += 1

        return data_list


class ArrayFieldBootstrapTableWidget(ArrayFieldTableWidget):
    template = 'array-field/table-html-bootstrap.html'
