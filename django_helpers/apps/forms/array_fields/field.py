# coding=utf-8
import copy
from collections import OrderedDict

from django.forms.forms import pretty_name

from widget import ArrayFieldTableWidget, ArrayFieldBootstrapTableWidget

__author__ = 'ajumell'

# Array Field
# ------------
#
# (Done) An array field should have one or more fields present in it.
# (Done) It can be added and removed by the user at client side.
# It should have a javascript version and non javascript version.
#
# Form Renderer
# -------------
#
# (Done) The Form renderer should render it properly.
# (Done) The forms with Javascript widget should be able to add dynamically.
# (Done) The add button should be configurable at the renderer.
# (Done) The form should be able to render with out form renderer.
#
#
#
#
#
#
#

from django import forms


class ArrayField(forms.Field):
    widget = ArrayFieldTableWidget

    def __init__(self, fields=None, form=None, can_add=True, can_delete=True, widths=None, initial_field=1, widget=None, *args, **kwargs):
        self.can_add = can_add
        self.can_delete = can_delete
        self.initial_field = initial_field
        self.require_all_fields = kwargs.pop('require_all_fields', True)

        if form is not None:
            fields = OrderedDict()
            base_fields = form.base_fields
            for field_name in base_fields.keys():
                field = base_fields[field_name]
                fields[field_name] = field

            fields = copy.deepcopy(fields)

        if type(fields) not in (dict, OrderedDict):
            raise Exception("The fields must be a dictionary.")

        if widget is None:
            widget = self.widget

        is_type = isinstance(widget, type)

        if is_type:
            if widget != self.widget and not issubclass(widget, self.widget):
                raise Exception('Invalid widget class.')
        else:
            if not isinstance(widget, self.widget):
                raise Exception('Invalid widget class.')

        if is_type:
            widget_parms = {
                'fields': fields,
                'widths': widths,
                "can_add": can_add,
                "initial_field": initial_field,
                "can_delete": can_delete
            }
            widget = self.widget(**widget_parms)
            kwargs['widget'] = widget

        super(ArrayField, self).__init__(*args, **kwargs)

        self.fields = fields

    def __deepcopy__(self, memo):
        result = super(ArrayField, self).__deepcopy__(memo)
        result.fields = {}
        for x, y in self.fields.items():
            result.fields[x] = y.__deepcopy__(memo)

        return result

    def to_python(self, value):
        fields = self.fields.items()
        new_values = []
        for row in value:
            new_item = {}
            for field_name, field in fields:
                field = copy.deepcopy(field)
                current_value = row.get(field_name)
                new_value = field.to_python(current_value)
                new_item[field_name] = new_value
            new_values.append(new_item)
        return new_values

    def validate(self, value):
        errors = []
        fields = self.fields.items()
        index = 0

        for row in value:
            index += 1
            for field_name, field in fields:
                field = copy.deepcopy(field)
                current_value = row.get(field_name)
                try:
                    field.validate(current_value)
                except forms.ValidationError, ex:
                    label = field.label or pretty_name(field_name)
                    args = (label, index, ex.message)
                    msg = "Error occurred in field %s at position %s : %s" % args
                    errors.append(msg)

        if len(errors) > 0:
            raise forms.ValidationError(errors)

        return value

    def _has_changed(self, initial, data):
        return True


class BootstrapArrayField(ArrayField):
    widget = ArrayFieldBootstrapTableWidget
