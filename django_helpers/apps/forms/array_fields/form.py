# coding=utf-8
from django import forms
from django.forms.models import model_to_dict

__author__ = 'ajumell'

__all__ = ['ModelForm']


def queryset_name(field_name):
    field_name = field_name.lower()
    while "_" in field_name:
        field_name = field_name.replace("_", '')

    return field_name + "_set"


def get_parent_field(model, parent_model):
    from django.db import models

    parent_field = None
    # noinspection PyProtectedMember
    fields = model._meta.fields
    for field in fields:
        if isinstance(field, models.ForeignKey):
            related_model = field.related.parent_model
            if related_model == parent_model:
                if parent_field is not None:
                    raise Exception("Array Field cannot be used with relations with more than one foreign key.")
                parent_field = field.name
    return parent_field


def construct_instance(cleaned_data, instance, fields=None, exclude=None):
    """
    Constructs and returns a model instance from the bound ``form``'s
    ``cleaned_data``, but does not save the returned instance to the
    database.
    """
    from django.db import models

    # noinspection PyProtectedMember
    opts = instance._meta

    file_field_list = []
    for f in opts.fields:
        if not f.editable or isinstance(f, models.AutoField) \
                or f.name not in cleaned_data:
            continue
        if fields is not None and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        # Defer saving file-type fields until after the other fields, so a
        # callable upload_to can use the values from other fields.
        if isinstance(f, models.FileField):
            file_field_list.append(f)
        else:
            f.save_form_data(instance, cleaned_data[f.name])

    for f in file_field_list:
        f.save_form_data(instance, cleaned_data[f.name])

    return instance


def save_instance(form, instance, cleaned_data, fields=None, fail_message='saved',
                  commit=True, exclude=None, construct=True):
    if construct:
        instance = construct_instance(cleaned_data, instance, fields, exclude)

    # noinspection PyProtectedMember
    opts = instance._meta
    if form.errors:
        raise ValueError("The %s could not be %s because the data didn't"
                         " validate." % (opts.object_name, fail_message))

    # Wrap up the saving of m2m data as a function.
    def save_m2m():
        # Note that for historical reasons we want to include also
        # virtual_fields here. (GenericRelation was previously a fake
        # m2m field).
        for f in opts.many_to_many + opts.virtual_fields:
            if not hasattr(f, 'save_form_data'):
                continue
            if fields and f.name not in fields:
                continue
            if exclude and f.name in exclude:
                continue
            if f.name in cleaned_data:
                f.save_form_data(instance, cleaned_data[f.name])

    if commit:
        # If we are committing, save the instance and the m2m data immediately.
        instance.save()
        save_m2m()
    else:
        # We're not committing. Add a method to the form to allow deferred
        # saving of m2m data.
        instance.save_m2m = save_m2m
    return instance


class ModelForm(forms.ModelForm):
    def find_array_fields(self, load_initial):
        from field import ArrayField

        self.array_fields = array_fields = []
        self.model_array_fields = model_array_fields = []
        initial = self.initial
        instance = self.instance
        fields = self.fields
        for field_name, field in fields.items():
            if isinstance(field, ArrayField):
                array_fields.append(field_name)
                query_set = getattr(instance, queryset_name(field_name), None)
                if query_set is not None:
                    # noinspection PyUnresolvedReferences
                    model_array_fields.append((field_name, query_set.model))

                    if load_initial is True:
                        # noinspection PyUnresolvedReferences
                        query = query_set.all()
                        values = []
                        for record in query:
                            value = model_to_dict(record, field.fields, None)
                            values.append(value)

                        initial[field_name] = values

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=None, label_suffix=None,
                 empty_permitted=False, instance=None, load_initial=False):

        kwargs = {
            'label_suffix': label_suffix,
            'empty_permitted': empty_permitted,
            'instance': instance,
        }

        if error_class is not None:
            kwargs['error_class'] = error_class

        super(ModelForm, self).__init__(data, files, auto_id, prefix, initial, **kwargs)
        self.find_array_fields(load_initial)

    def save(self, commit=True):
        op = super(ModelForm, self).save(commit)
        model_array_fields = self.model_array_fields
        cleaned_data = self.cleaned_data
        instances = []
        for field_name, model in model_array_fields:
            values = cleaned_data.get(field_name)
            parent_field = get_parent_field(model, self._meta.model)
            for value in values:
                fail_message = 'created'
                instance = model()
                setattr(instance, parent_field, op)
                save_instance(self, instance, value, fail_message=fail_message, commit=commit, construct=True)
                instances.append((instance, parent_field))

        def save_array_fields():
            # TODO: Implement Save m2m here.
            for obj, pf in instances:
                setattr(obj, pf, op)
                obj.validate_unique()
                obj.save()

        self.save_array_fields = save_array_fields

        return op
