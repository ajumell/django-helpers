# coding=utf-8
from django.forms import widgets

from django_helpers.apps.forms.widgets import Widget
from django_helpers.helpers.views import render_to_string
from django_helpers.js_plugins.medium_editor import MediumEditorOptions


class MediumEditorWidget(Widget, widgets.Textarea):
    def __init__(self, options=None, *args, **kwargs):
        if options is None:
            options = MediumEditorOptions()
        self.options = options
        self.js_files = ['medium-editor-js']
        self.css_files = ['medium-editor-css']

        widgets.Textarea.__init__(self, *args, **kwargs)

    def render_js(self):
        return render_to_string('django-helpers/forms/medium.js', {
            "id": self.html_id,
            "options": self.options
        })
