# coding=utf-8
from itertools import chain

from django.forms import Select
from django.forms.utils import flatatt
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from django_helpers.apps.static_manager import register_js

register_js('connected-selects-js', 'js/connected-selects.js', ('jquery',))


class ExtraAttrSelect(Select):
    allow_multiple_selected = False
    
    js_files = ['connected-selects-js']
    
    def __init__(self, attrs=None, choices=(), option_attrs=None):
        Select.__init__(self, attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)
        self.option_attrs = option_attrs
    
    def render(self, name, value, attrs=None, choices=()):
        if value is None:
            value = ''
        
        fields = []
        for k in self.option_attrs:
            fields.append(k)
        
        attrs['data-dependent-fields'] = ','.join(fields)
        attrs['data-connected-selects'] = 'true'
        
        final_attrs = self.build_attrs(attrs, name=name)
        output = [format_html('<select{}>', flatatt(final_attrs))]
        options = self.render_options(choices, [value])
        if options:
            output.append(options)
        output.append('</select>')
        return mark_safe('\n'.join(output))
    
    def render_options(self, choices, selected_choices):
        # Normalize to strings.
        selected_choices = set(force_text(v) for v in selected_choices)
        output = []
        for choice in chain(self.choices, choices):
            print choice
            if type(choice) is dict:
                self.render_options_dict(choice, output, selected_choices)
            else:
                self.render_options_normal(choice, output, selected_choices)
        return '\n'.join(output)
    
    def render_options_dict(self, choice, output, selected_choices):
        option_html = self.render_option_dict(selected_choices, choice)
        output.append(option_html)
    
    def render_options_normal(self, choice, output, selected_choices):
        option_value, option_label = choice
        if isinstance(option_label, (list, tuple)):
            output.append(format_html('<optgroup label="{0}">', force_text(option_value)))
            for option in option_label:
                output.append(self.render_option(selected_choices, *option))
            output.append('</optgroup>')
        else:
            output.append(self.render_option(selected_choices, option_value, option_label))
    
    def render_option_dict(self, selected_choices, option):
        """
        :type option: dict
        """
        option_value = option.pop('value')
        option_label = option.pop('label')
        
        if option_value is None:
            option_value = ''
        
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        
        extra_attrs = flatatt(option)
        
        if extra_attrs:
            selected_html = " " + extra_attrs
        
        return format_html('<option value="{0}"{1}>{2}</option>',
                           option_value,
                           mark_safe(selected_html),
                           force_text(option_label))


class NoEmptySelect(Select):
    def render(self, name, value, attrs=None, choices=()):
        if len(self.choices) > 0:
            first_value, first_label = self.choices[0]
            if first_value == '' and first_label.startswith('---'):
                self.choices.pop(0)
        
        return Select.render(self, name, value, attrs, choices)
