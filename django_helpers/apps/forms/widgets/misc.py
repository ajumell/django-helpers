# coding=utf-8
from django.forms import fields
from django.forms.utils import flatatt
from django.utils.safestring import mark_safe
from django_helpers.apps.static_manager import register_js
from django_helpers.helpers.views import render_to_string
from . import Widget

register_js('jquery-raty-js', 'django-helpers/forms/js/jquery.raty.min.js', ('jquery',))


class RatingWidget(Widget, fields.TextInput):
    def __init__(self, number=5, cancel=False, half=False, size=16, *args, **kwargs):
        # TODO: Add more options from plugin
        self.js_files = ['jquery-raty-js']

        fields.TextInput.__init__(self, *args, **kwargs)
        self.number = number
        self.cancel = cancel
        self.half = half
        self.size = size

    def render(self, name, value, attrs=None):
        if value is None:
            value = 0

        self.input_type = "hidden"
        hidden = fields.TextInput.render(self, name, value)
        final_attrs = self.build_attrs(attrs)
        self.value = value
        self.name = name
        return mark_safe(u'<div%s></div>' % flatatt(final_attrs)) + hidden

    def render_js(self):
        return render_to_string('django-helpers/forms/raty.js', {
            "number": self.number,
            "half": self.half,
            "size": self.size,
            "name": self.name,
            "id": self.html_id,
            "score": self.value,
            "cancel": self.cancel
        })
