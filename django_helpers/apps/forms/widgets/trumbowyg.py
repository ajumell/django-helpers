# coding=utf-8
from django.forms import widgets

from django_helpers.apps.forms.widgets import Widget
from django_helpers.helpers.views import render_to_string


class TrumbowygOptions(object):
    pass


class TrumbowygWidget(Widget, widgets.Textarea):
    def __init__(self, *args, **kwargs):
        self.js_files = ['trumbowyg-js']
        self.css_files = ['trumbowyg-css']

        widgets.Textarea.__init__(self, *args, **kwargs)

    def render_js(self):
        return render_to_string('django-helpers/forms/trumbowyg.js', {
            "id": self.html_id
        })
