# coding=utf-8
from django.forms import fields

from django_helpers.apps.static_manager import register_css, register_js

register_css('intl-tele-input-css', 'js-plugins/intl-tele-input/css/intlTelInput.css')
register_js('intl-tele-input-js', 'js-plugins/intl-tele-input/js/intlTelInput.js', ('django-helpers-forms-js', 'intl-tele-input-css'))


# noinspection PyShadowingBuiltins
class PhoneNumberWidget(fields.TextInput):
    def __init__(self, attrs=None):
        fields.TextInput.__init__(self, attrs)
        self.js_files = ['intl-tele-input-js']
        self.css_files = ['intl-tele-input-css']

    def render(self, name, value, attrs=None):
        if attrs is None:
            attrs = {}

        attrs['data-telephone-input'] = True
        attrs['type'] = 'number'
        
        return fields.TextInput.render(self, name, value, attrs)
