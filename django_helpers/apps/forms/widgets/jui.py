# coding=utf-8
from django.forms import fields

from django_helpers.helpers.views import render_to_string
from . import Widget


class DateInput(Widget, fields.DateInput):
    def __init__(self, attrs=None, date_format=None, max_date=None,
                 min_date=None, change_year=None, change_month=None):
        self.js_files = ['jquery-ui-js']
        self.css_files = ['jquery-ui-base-css']

        fields.DateInput.__init__(self, attrs, date_format)

        self.min_date = min_date
        self.max_date = max_date
        self.change_month = change_month
        self.change_year = change_year

    def render_js(self):
        return render_to_string('django-helpers/forms/date-field.js', {
            "max_date": self.max_date,
            "min_date": self.min_date,
            "change_year": self.change_year,
            "change_month": self.change_month,
            "id": self.html_id,
            "date_format": self.format
        })


class MaskedInput(Widget, fields.TextInput):
    def __init__(self, mask, placeholder="_", *args, **kwargs):
        self.js_files = ['jquery-masked-input-js']

        fields.TextInput.__init__(self, *args, **kwargs)
        self.mask = mask
        self.placeholder = placeholder

    def render_js(self):
        return render_to_string('django-helpers/forms/mask-input.js', {
            "mask": self.mask,
            "id": self.html_id,
            "placeholder": self.placeholder
        })


class SpinnerInput(Widget, fields.TextInput):
    def __init__(self, min_value=None, max_value=None, places=None, prefix="",
                 step=1, large_step=10, *args, **kwargs):
        # TODO: Add more options from plugin
        self.js_files = ['jquery-ui-spinner-js']

        fields.TextInput.__init__(self, *args, **kwargs)
        self.min_value = min_value
        self.max_value = max_value
        self.places = places
        self.prefix = prefix
        self.step = step
        self.large_step = large_step

    def render(self, name, value, attrs=None):
        self.value = value
        return fields.TextInput.render(self, name, self.prefix + unicode(value), attrs)

    def render_js(self):
        return render_to_string('django-helpers/forms/mask-input.js', {
            "min": self.min_value,
            "max": self.max_value,
            "prefix": self.prefix,
            "places": self.places,
            "value": self.value,
            "step": self.step,
            "large_step": self.large_step,
            "id": self.html_id
        })
