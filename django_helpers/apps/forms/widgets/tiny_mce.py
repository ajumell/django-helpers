# coding=utf-8
from django.forms import widgets

from django_helpers.apps.forms.widgets import Widget
from django_helpers.apps.static_manager import register_js
from django_helpers.helpers.views import render_to_string

__author__ = 'ajumell'

register_js('tiny-mcs-js', 'tiny_mce/tiny_mce.js')
register_js('jquery-tiny-mcs-js', 'tiny_mce/jquery.tinymce.js', ('jquery', 'tiny-mce-js'))


class TinyMCEEditorOptions(object):
    pass


class TinyMCEWidget(Widget, widgets.Textarea):
    def __init__(self, *args, **kwargs):
        self.js_files = ['tiny-mce-js']
        widgets.Textarea.__init__(self, *args, **kwargs)

    def render_js(self):
        return render_to_string('django-helpers/forms/tiny_mce.js', {
        })
