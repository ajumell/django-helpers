# coding=utf-8
try:
    from ckeditor.widgets import CKEditorWidget as BaseWidget, json_encode
except ImportError, err:
    BaseWidget = None
    json_encode = None
    print err
    raise Exception('CKEditor widget requires django-ckeditor-updated Package.')

from django.conf import settings
from django.core.urlresolvers import reverse
from django.forms.utils import flatatt
from django.template import Template, Context
from django.utils.encoding import force_text
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

from django_helpers.apps.static_manager import register_js


def render_string(string, opts):
    t = Template(string)
    c = Context(opts)
    return t.render(c)


register_js('ck-editor-js', 'ck-editor/ckeditor.js', ('jquery',))


class CKEditorWidget(BaseWidget):
    js_render_attrs = None
    has_js = True

    js_files = ('ck-editor-js',)

    def render(self, name, value, attrs=None):
        if attrs is None:
            attrs = {}

        if value is None:
            value = ''

        final_attrs = self.build_attrs(attrs, name=name)

        self.config.setdefault('filebrowserUploadUrl', reverse('ckeditor_upload'))
        self.config.setdefault('filebrowserBrowseUrl', reverse('ckeditor_browse'))
        self.js_render_attrs = opts = {
            'final_attrs': flatatt(final_attrs),
            'value': conditional_escape(force_text(value)),
            'id': final_attrs['id'],
            'config': json_encode(self.config)
        }
        return mark_safe(render_string('<p><textarea{{ final_attrs|safe }}>{{ value }}</textarea></p>', opts))

    # noinspection PyMethodMayBeStatic
    def js_global_vars(self):
        return {
            "CKEDITOR_BASEPATH": '%sck-editor/' % settings.STATIC_URL
        }

    def render_js(self):
        if not self.js_render_attrs:
            return ''
        return render_string('CKEDITOR.replace("{{ id }}", {{ config|safe }});', self.js_render_attrs)
