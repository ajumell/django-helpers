# coding=utf-8
from django.forms import Widget as OrginalWidget
from django.utils.safestring import mark_safe


def js_bool(a):
    if a:
        return "true"
    return 'false'


def js_date(a):
    if a is None:
        return None

    return mark_safe("new Date(\"" + a.isoformat() + "\")")


# noinspection PyCallByClass,PyClassHasNoInit
class Widget:
    attrs = None
    has_js = True

    def build_attrs(self, extra_attrs=None, **kwargs):
        """
        Normally this does not need to change from now.
        But be cautious in future updates of widgets.
        """
        attrs = OrginalWidget.build_attrs(self, extra_attrs, **kwargs)

        if "id" in attrs:
            self.html_id = attrs["id"]
        return attrs


        # class BootstrapDateTimeWidget(Widget, fields.DateTimeInput):
        #     def __init__(self, input_format=None, *args, **kwargs):
        #         fields.DateTimeInput.__init__(self, *args, **kwargs)
        #         self.js_files = ['bootstrap-datetimepicker-js']
        #
        #         self.input_format = input_format
        #
        #     def render_js(self):
        #         return render_to_string('django-helpers/forms/bootstrap-datetime.js', {
        #             "format": self.input_format,
        #             "id": self.html_id
        #         })
