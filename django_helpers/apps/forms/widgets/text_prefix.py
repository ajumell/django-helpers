# coding=utf-8
from django.forms import fields

from django_helpers.apps.static_manager import register_css
from django_helpers.apps.static_manager import register_js

register_css('text-box-prefix-css', 'js-plugins/text-box-prefix/text-box-prefix.css')
register_js('text-box-prefix-js', 'js-plugins/text-box-prefix/text-box-prefix.js', ['jquery'])


# noinspection PyShadowingBuiltins
class PrefixedTextBoxWidget(fields.TextInput):
    def __init__(self, prefix=None, attrs=None):
        fields.TextInput.__init__(self, attrs)
        self.css_files = ['text-box-prefix-css']
        self.js_files = ['text-box-prefix-js']
        
        self.prefix = prefix
    
    def render(self, name, value, attrs=None):
        if attrs is None:
            attrs = {}
        
        prefix = self.prefix
        if prefix is not None:
            attrs['data-text-box-prefix'] = prefix
        
        return fields.TextInput.render(self, name, value, attrs)


# noinspection PyShadowingBuiltins
class PrefixedNumberBoxWidget(PrefixedTextBoxWidget):
    input_type = 'number'
