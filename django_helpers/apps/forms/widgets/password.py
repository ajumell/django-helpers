# coding=utf-8
from django.forms import widgets

from django_helpers.apps.static_manager import register_css
from django_helpers.apps.static_manager import register_js

register_css('sauron-css', 'js-plugins/sauron/css/style.css')
register_js('sauron-js', 'js-plugins/sauron/js/jquery.sauron.js', ('django-helpers-forms-js', 'sauron-css'))


class ShowPasswordWidget(widgets.PasswordInput):
    def __init__(self, attrs=None):
        widgets.PasswordInput.__init__(self, attrs)
        self.js_files = ['sauron-js']
        self.css_files = ['sauron-css']
    
    def render(self, name, value, attrs=None):
        if attrs is None:
            attrs = {}
        
        attrs['data-sauron'] = True
        return widgets.PasswordInput.render(self, name, value, attrs)
