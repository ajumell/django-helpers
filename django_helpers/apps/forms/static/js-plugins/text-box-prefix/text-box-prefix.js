jQuery && jQuery(function ($) {

    var fn = function (obj, text) {

        var class_name;
        var flagsContainer;
        var selectedFlagInner;

        class_name = "text-box-prefix";
        if (obj.css("display") == "block") {
            class_name += " block"
        }

        obj.wrap($("<div>", {
            "class": class_name
        }));

        flagsContainer = $("<div>", {
            "class": "prefix-container"
        }).insertBefore(obj);
        var selectedFlag = $("<div>", {
            "class": "prefix-text"
        }).appendTo(flagsContainer);

        selectedFlagInner = $("<div>", {
            "class": "content"
        }).text(text).appendTo(selectedFlag);

    };


    $("[data-text-box-prefix]").each(function () {
        var $this = jQuery(this);
        var text = $this.attr("data-text-box-prefix");

        fn($this, text);
    });

});