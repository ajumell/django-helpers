jQuery && jQuery(function ($) {
    $.fn.connectedSelects = function (oOptions) {
        this.each(function () {
            var jqObj;
            var aFields;
            var dataChanged;

            jqObj = $(this);
            aFields = oOptions['fields'];
            dataChanged = function () {
                var selector = 'option';
                $.each(aFields, function () {
                    var val = $("[name=" + this + "]").val();
                    selector += "[" + this + "=" + val + "]";
                });

                var filteredOptions = jqObj.find(selector);
                var options = jqObj.find("option").not(filteredOptions);

                filteredOptions.show();
                options.hide();
            };

            $.each(aFields, function () {
                $("[name=" + this + "]").on("change", dataChanged);
            });
        });
    };
});

jQuery && jQuery(function () {
    var registerDataWidget;
    var dj;

    dj = $.dj;
    registerDataWidget = dj.registerDataWidget;

    registerDataWidget('connected-selects', function (obj) {
        var fields = obj.data('dependentFields').split(",");
        obj.connectedSelects({'fields': fields});
    });
});