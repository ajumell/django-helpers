jQuery && jQuery(function ($) {

    $.fn.array_fields = function (options) {

        var current_count,
            tmpl,
            opts,
            $table,
            $tbody,
            rows,
            add_btn_selector,
            can_add,
            add_new_row_button,
            add_disabled,
            template_row,
            render_js,
            add_new_row,
            delete_selector;

        opts = $.extend({}, $.fn.array_fields.defaults, options);

        $table = this;
        $tbody = $("tbody", $table);
        rows = $("tr", $table);
        current_count = rows.length - 1;
        delete_selector = opts.delete_btn_selector;
        render_js = opts.render_js;
        template_row = $(rows[current_count]);
        tmpl = template_row.clone();
        can_add = opts.can_add;

        if (delete_selector !== false) {
            $table.on("click", delete_selector, function (e) {
                if (current_count == 1) {
                    return false;
                }

                var parent = $(this).parents("tr");
                parent.fadeOut('fast', function () {
                    parent.remove()
                });
                current_count -= 1;
                e.preventDefault();
                return false;
            });
        }

        rows.each(function () {
            render_js(this);
        });

        if (can_add) {
            add_btn_selector = opts.add_btn_selector;
            if (add_btn_selector !== false) {
                add_new_row_button = $table.siblings(add_btn_selector);
            }

            add_new_row = function (f) {
                if (add_disabled) {
                    return false;
                }
                add_disabled = true;

                var new_row = tmpl.clone();
                $tbody.append(new_row);
                new_row.hide();
                new_row.fadeIn();

                if (f !== false) {
                    new_row.find("input, select, textarea").first().focus();
                }
                render_js(new_row);
                current_count += 1;

                setTimeout(function () {
                    add_disabled = false;
                }, 200);

                return false;
            };

            add_new_row_button.on("click", add_new_row);

            $tbody.on('click focus', 'td > *:not(' + delete_selector + ')', function () {
                var parent = $(this).parents('tr');
                var last_row = $tbody.find("tr:last");

                if (parent.is(last_row)) {
                    add_new_row(false);
                }
            });
        }

    };

    $.fn.array_fields.defaults = {
        add_btn_selector : "a[data-array-add-btn]",
        can_add : true,
        delete_btn_selector : "a[data-array-remove-btn]",
        render_js : function (elem) {
        }
    };

});