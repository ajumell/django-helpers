# coding=utf-8
from django import forms
from django.forms import ChoiceField
from django.forms.models import ModelChoiceIterator

from django_helpers.apps.forms.widgets import bootstrap
from django_helpers.apps.forms.widgets.widgets import ExtraAttrSelect

__author__ = 'ajumell'


# noinspection PyAbstractClass
class DateField(forms.DateField):
    widget = bootstrap.DatePickerWidget

    def __init__(self, input_formats=None, *args, **kwargs):
        super(DateField, self).__init__(input_formats, *args, **kwargs)
        self.widget.format = self.input_formats[0]


# noinspection PyAbstractClass
class DateTimeField(forms.DateTimeField):
    widget = bootstrap.DateTimePickerWidget
    
    def __init__(self, input_formats=None, *args, **kwargs):
        super(DateTimeField, self).__init__(input_formats, *args, **kwargs)
        self.widget.format = input_formats[0]


# noinspection PyAbstractClass
class TimeField(forms.TimeField):
    widget = bootstrap.TimePickerWidget
    
    def __init__(self, input_formats=None, *args, **kwargs):
        super(TimeField, self).__init__(input_formats, *args, **kwargs)
        self.widget.format = input_formats[0]


class ColorField(forms.CharField):
    widget = bootstrap.ColorPickerWidget


#
# Choice Field with Extra Attrs in Options
#

class ExtraAttrsModelChoiceIterator(ModelChoiceIterator):
    def choice(self, obj):
        field = self.field
        extra_option_attrs = getattr(field, 'extra_option_attrs', None)
        if extra_option_attrs is None:
            return (
                field.prepare_value(obj),
                field.label_from_instance(obj)
            )
        else:
            choice = {
                "value": field.prepare_value(obj),
                "label": field.label_from_instance(obj)
            }
            for attr_name, value in extra_option_attrs.items():
                attr = getattr(obj, value, None)
                if attr is not None:
                    choice[attr_name] = attr
            
            return choice


class ExtraAttrsModelChoiceField(forms.ModelChoiceField):
    widget = ExtraAttrSelect
    
    def __init__(self, queryset, empty_label="---------", cache_choices=False,
                 required=True, widget=None, label=None, initial=None,
                 help_text='', to_field_name=None, limit_choices_to=None,
                 extra_option_attrs=None,
                 *args, **kwargs):
        
        forms.ModelChoiceField.__init__(
            self,
            queryset,
            empty_label,
            required,
            widget,
            label,
            initial,
            help_text,
            to_field_name,
            limit_choices_to,
            *args,
            **kwargs
        )
        
        if extra_option_attrs and type(extra_option_attrs) is not dict:
            raise Exception("extra_option_attrs should be a dict.")
        
        self.widget.option_attrs = extra_option_attrs
        self.extra_option_attrs = extra_option_attrs
    
    def _get_choices(self):
        # Copied from parent class.
        if hasattr(self, '_choices'):
            return self._choices
        return ExtraAttrsModelChoiceIterator(self)
    
    choices = property(_get_choices, ChoiceField._set_choices)
