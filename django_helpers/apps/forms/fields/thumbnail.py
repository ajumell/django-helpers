# coding=utf-8

__author__ = 'ajumell'

from django.db.models.fields.files import ImageFieldFile

from django_helpers import get_settings_val
from lib.thumbnail import ImageWithThumbsFieldFile, ImageWithThumbsField

try:
    from django_helpers.storages.flickr import FlickrStorage
except:
    FlickrStorage = None

DEFAULT_SIZES = get_settings_val('THUMBNAIL_SIZES')


class ThumbnailImageFieldFile(ImageWithThumbsFieldFile):
    def _get_thumbnail(self):
        storage = self.storage
        if hasattr(storage, 'thumbnail'):
            return self.storage.thumbnail(self.name)
        sizes = self.field.sizes
        w, h = sizes[0]
        return getattr(self, 'url_%sx%s' % (w, h))

    thumbnail = property(_get_thumbnail)

    def save(self, name, content, save=True):
        storage = self.storage
        # In Flickr storage there is no need for making thumbnails
        # manually. It will be done automatically.
        if FlickrStorage is not None and isinstance(storage, FlickrStorage):
            ImageFieldFile.save(self, name, content, save)
        else:
            ImageWithThumbsFieldFile.save(self, name, content, save)


class ThumbnailImageField(ImageWithThumbsField):
    attr_class = ThumbnailImageFieldFile

    def __init__(self, verbose_name=None, name=None, width_field=None, height_field=None, sizes=None, **kwargs):
        if sizes is None:
            sizes = DEFAULT_SIZES or ((150, 150),)
        ImageWithThumbsField.__init__(self, verbose_name, name, width_field, height_field, sizes, **kwargs)


try:
    # noinspection PyUnresolvedReferences
    from south.modelsinspector import add_introspection_rules

    rules = [
        (
            [ThumbnailImageField],
            [],
            {
                "sizes": ["sizes", {"default": None}],
                "verbose_name": ["verbose_name", {"default": None}],
                "name": ["name", {"default": None}],
                "width_field": ["width_field", {"default": None}],
                "height_field": ["height_field", {"default": None}],
            },
        ),
    ]

    patterns = ["^django_helpers.apps.forms.fields.thumbnail.ThumbnailImageField"]
    add_introspection_rules(rules, patterns)
except ImportError:
    pass
