# coding=utf-8

from django_helpers.helpers.views import render_to_string
from django_helpers.apps.static_manager import register_js, register_css
from django_helpers.widgets import Div

__author__ = 'ajumell'

register_js('jcrop-js', 'django-jcrop/jquery.Jcrop.js', ['jquery'])
register_js('jcrop-django-js', 'django-jcrop/django.Jcrop.js', ['jcrop-js', 'django-helpers-core-js'])
register_css('jcrop-css', 'django-jcrop/jquery.Jcrop.css')


class Cropper(object):
    id = "cropper"

    min_width = 100

    min_height = 100

    cancel_url = None

    aspectRatio = 1

    save_text = 'Save'

    cancel_text = 'Cancel'

    action_url = ''

    js_files = ('jcrop-django-js',)

    css_files = ('jcrop-css',)

    def __init__(self, photo_url, width, height, request):
        self.height = height
        self.width = width
        self.photo_url = photo_url

        if request.method == "POST":
            post = request.POST.get
            self.selected_height = self.get_val(post, "height")
            self.selected_width = self.get_val(post, "width")
            self.selected_x = self.get_val(post, "x")
            self.selected_y = self.get_val(post, "y")
        else:
            self.selected_height = 0
            self.selected_width = 0
            self.selected_x = 0
            self.selected_y = 0

        self.request = request

        self.data = None

    def get_data(self):
        if self.data is None:
            self.data = {
                "id": self.id,
                "url": self.photo_url,
                "save_text": self.save_text,
                "cancel_url": self.cancel_url,
                "cancel_text": self.cancel_text,
                'action_url': self.action_url
            }
        return self.data

    def memory_file(self, img, name, img_format='JPEG'):
        from StringIO import StringIO
        from django.core.files.uploadedfile import InMemoryUploadedFile

        io = StringIO()
        img.save(io, format=img_format)

        content_type = 'image/jpeg'
        return InMemoryUploadedFile(io, None, name, content_type, io.len, None)

    def crop_image(self, photo):
        from PIL import Image

        box = self.get_crop_box()
        image = Image.open(photo)
        cropped = image.crop(box)

        return self.memory_file(cropped, photo.name)

    def get_val(self, post, name):
        try:
            return int(float(post(self.id + "-" + name)))
        except ValueError:
            return 0

    def is_valid(self):
        if self.min_height > self.selected_height:
            return False

        if self.min_width > self.selected_width:
            return False

        ar = int(self.aspectRatio)
        c_ar = int(self.selected_width / self.selected_height)
        diff = ar - c_ar

        if diff < 0:
            diff *= -1

        if diff > 2:
            return False

        return True

    def get_crop_box(self):
        return (
            self.selected_x,
            self.selected_y,
            self.selected_width + self.selected_x,
            self.selected_height + self.selected_y
        )

    def render(self):
        template = "django-jcrop/cropper.html"
        content = render_to_string(template, self.get_data(), self.request)
        div = Div(content)
        data = {
            "j-cropper": 'true',
            "name": self.id,
            "width": self.width,
            "height": self.height,
            "aspect-ratio": self.aspectRatio,
            "min-width": self.min_width,
            "min-height": self.min_height
        }

        for k, v in data.items():
            div.data(k, v)

        return div.render()

    def __unicode__(self):
        return self.render()

    def __str__(self):
        return self.render()

    def __repr__(self):
        return self.render()


class FacebookCropper(Cropper):
    min_height = 706

    min_width = 679

    aspectRatio = 242.0 / 251.0
