jQuery && jQuery(function ($) {
    var
        jCrop_api,
        target = $("#" + "{{ id }}" + "-image");

    target.Jcrop({
        trueSize : [{{ width }}, {{ height }}],
        aspectRatio : {{ aspectRatio }},
        minSize : [{{ min_width }}, {{ min_height }}],
        onChange : function (c) {
            $("input[name=" + "{{ id }}" + "-height]").val(c.h);
            $("input[name=" + "{{ id }}" + "-width]").val(c.w);
            $("input[name=" + "{{ id }}" + "-x]").val(c.x);
            $("input[name=" + "{{ id }}" + "-y]").val(c.y);
        },
        onRelease : function () {
            jCrop_api.setSelect([
                parseInt($("input[name=" + "{{ id }}" + "-x]").val(), 10),
                parseInt($("input[name=" + "{{ id }}" + "-y]").val(), 10),
                parseInt($("input[name=" + "{{ id }}" + "-width]").val(), 10),
                parseInt($("input[name=" + "{{ id }}" + "-height]").val(), 10)
            ])
        }
    }, function () {
        jCrop_api = this;
        jCrop_api.setSelect([0, 0, {{ min_width }}, {{ min_height }}]);
    });
});