jQuery && jQuery(function ($) {

    var generateDataOptions = $.dj.generateDataOptions;
    var registerDataWidget = $.dj.registerDataWidget;

    if ($.fn.Jcrop) {
        registerDataWidget('j-cropper', function (obj) {
            var boxWidth,
                jCrop_api,
                mappings,
                options,
                dHeightObj,
                dWidthObj,
                dXObj,
                dYObj,
                target = obj.find("img");

            mappings = {
                "name": "name",
                "width": "width",
                "height": "height",
                "aspectRatio": "aspectRatio",
                "minWidth": "min_width",
                "minHeight": "min_height"
            };

            options = generateDataOptions(obj, mappings)

            dHeightObj = obj.find("input[name=" + options.name + "-height]");
            dWidthObj = obj.find("input[name=" + options.name + "-width]");
            dXObj = obj.find("input[name=" + options.name + "-x]");
            dYObj = obj.find("input[name=" + options.name + "-y]");

            boxWidth = obj.width();
            target.Jcrop({
                trueSize: [options.width, options.height],
                aspectRatio: options.aspectRatio,
                boxWidth: boxWidth,
                minSize: [options.min_width, options.min_height],
                onChange: function (c) {
                    dHeightObj.val(c.h);
                    dWidthObj.val(c.w);
                    dXObj.val(c.x);
                    dYObj.val(c.y);
                },
                onRelease: function () {
                    jCrop_api.setSelect([
                        parseInt(dXObj.val(), 10),
                        parseInt(dYObj.val(), 10),
                        parseInt(dWidthObj.val(), 10),
                        parseInt(dHeightObj.val(), 10)
                    ])
                }
            }, function () {
                jCrop_api = this;
                jCrop_api.setSelect([0, 0, options.min_width, options.min_height]);
            });

        });
    }
});
