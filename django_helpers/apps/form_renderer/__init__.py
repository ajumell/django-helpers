# coding=utf-8
import urls
from django_helpers import add_app_url
from renderer import FormRenderer, MaterialFormRenderer

add_app_url('form-renderer', urls)
