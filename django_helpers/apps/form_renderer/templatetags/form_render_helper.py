# coding=utf-8
from django import template
from django.forms import CheckboxInput, HiddenInput, MultipleHiddenInput

register = template.Library()


@register.filter(name='is_checkbox')
def is_checkbox(field):
    return field.field.widget.__class__.__name__ == CheckboxInput().__class__.__name__


@register.filter(name='is_hidden')
def is_hidden(field):
    names = [
        MultipleHiddenInput().__class__.__name__,
        HiddenInput().__class__.__name__
    ]
    return field.field.widget.__class__.__name__ in names
