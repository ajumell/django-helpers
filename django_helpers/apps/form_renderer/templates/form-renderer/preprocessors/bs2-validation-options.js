jQuery(function ($) {
    $.dj.setErrorPlacement('#' + '{{ form_id }}', function (error, element) {

        var parent = element.parents(".controls"),
            existing = parent.find('label.help-block, label.error');

        parent.parent().addClass('error');
        error = $(error);
        error.addClass('help-block');
        error.fadeTo(0, 0.1);
        if (existing.length) {
            existing.fadeOut('normal', function () {
                parent.after(error);
                error.fadeTo(300, 1);
            });
        } else {
            parent.after(error);
            error.fadeTo(300, 1);
        }
    });

    $.dj.setSuccess('#' + '{{ form_id }}', function (error, element) {
        element = $(element);
        var parent = element.parents(".controls");
        parent.parent().removeClass('error');
        error.remove()
    });

});