jQuery(function ($) {

    $.dj.setGetContainer('#' + '{{ form_id }}', function (elem) {
        var parent;
        if (elem) {
            if (elem.length === undefined) {
                elem = jQuery(elem);
            }

            parent = elem.parents(".form-group");
            parent = parent.parent();

            return parent;
        }

    });

});