jQuery(function ($) {
    var form = $("#" + "{{ form_id }}"),
        prefix = "{{ form.prefix|default:"" }}";

    if (prefix) {
        prefix = prefix + "-";
    }

    form.attr('data-form-prefix', prefix);
});