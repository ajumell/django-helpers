definitions = {}
class Builder

  constructor: (@canvasSelector, @widgetsSelector, @fieldsSelector, @data)->
    @canvasDomElement = jQuery(@canvasSelector)
    @widgetsDomElement = widgets(@widgetsSelector)
    @fieldsDomElement = jQuery(@fieldsSelector)


  getId: () ->
    @_id += 1
    return @_id

  render: ->
    @_id = 0
    @renderChildren(@data, @canvasElement)

  renderWidget: (widget, parentWidget) ->
    unless widget["__id"]
      item_id = @getId()
      widget["__id"] = item_id
      widgets[item_id] = widget
    else
      item_id = widget["__id"]

    widget["__parent_id"] = parentWidget.__id if parentWidget

    type = widget['type']
    definition = Builder.getDefinition(type)

    renderer = definition.renderer
    template = renderer.template
    element = jQuery(template)

    element.data("widget-id", item_id)
    element.data("widget-type", type)

    if definition.is_editable and widget.content
      editableSelector = renderer.editable_selector
      if editableSelector
        editableElement = element.find(editableSelector)
      else
        editableElement = element
      editableElement.html(widget.content)

    @renderChildren(widget.children, element, widget)
    return element

  renderChildren: (widgets, parentElement, parentWidget) ->
    return unless widgets

    for widget in widgets
      childElement = @renderWidget(widget, parentWidget)
      parentElement.append(childElement)

  getWidget: (element) ->
    element = jQuery(element) unless element.data
    widget_id = element.data('widget-id')
    return widgets[widget_id]


Builder =

  init: (canvas, widgets, fields, data) ->
    builder = Builder(canvas, widgets, fields, data)

    return builder

  addDefinition: (properties) ->
    name = properties["type"]

    throw Error("Type is not provided.") unless name
    throw Error("Duplicate entry") if definitions[name]

    definitions[name] = properties

  getDefinitions: () ->
    return definitions

  getDefinition: (type) ->
    return definitions[type]


@Builder = formBuilder