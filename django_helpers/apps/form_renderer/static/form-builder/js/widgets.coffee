addDefinition
  name: 'Container'
  type: 'container'
  renderer:
    template: '<div class="container-fluid"></div>'

  is_container: true
  icon: 'plus'
  allowed_children: ['row', 'alert']

addDefinition
  name: 'Row'
  type: 'row'
  renderer:
    template: '<div class="row"></div>'

  allowed_children: ['container', 'alert']
  is_container: true
  icon: 'minus'

addDefinition
  name: 'Alert Danger'
  type: 'alert_danger'
  renderer:
    template: '<div class="alert alert-danger"></div>'
  is_container: true
  icon: 'remove-sign'

addDefinition
  name: 'Alert Success'
  type: 'alert_success'
  renderer:
    template: '<div class="alert alert-success"></div>'
  is_container: true
  icon: 'ok-sign'

addDefinition
  name: 'Alert Warning'
  type: 'alert_warning'
  renderer:
    template: '<div class="alert alert-warning"></div>'
  is_container: true
  icon: 'ok-sign'