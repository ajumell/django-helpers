jQuery ->
  dj = jQuery.dj;

  startsWith = (a, s) -> a.slice(0, s.length) == s
  endsWith = (a, s) -> s == '' or a.slice(-s.length) == s

  convert = ->
    form = $(this)
    return if form.data("validator")

    controls = form.find(":input, select")
    rules = {}
    messages = {}

    controls.each ->
      return if rules[this.name]

      dControl = $(this)
      rule = {}
      message = {}

      validators = [
        'required',
        'date',
        'integer',
        'number',
        'max_digit',
        'max_whole_digits',
        'max_decimal_places',
        'email',
        'url',
        'minlength',
        'maxlength',
        'minvalue',
        'maxvalue',
        'regex',
        'equalto',
      ]

      mapping =
        'equalto': 'equalTo'

      for validator in validators
        if dControl.data('validation-' + validator)
          if mapping[validator]
            rule_function = mapping[validator]
          else
            rule_function = validator
          rule[rule_function] = dControl.data('validation-' + validator)
          fn = dControl.data("validation-#{validator}-function")
          msg = dControl.data("validation-#{validator}-message")
          msg = $.dj.msgs[fn](msg) if fn

          message[rule_function] = msg

      if not jQuery.isEmptyObject(rule)
        rules[this.name] = rule
        messages[this.name] = message

    if not jQuery.isEmptyObject(rules)
      dj.validate(form, rules, messages)


    # Toggles

    toggles = []
    controls.each ->
      return if toggles[this.name]
      dControl = $(this)

      attributes = this.attributes
      elements = []

      for attribute in attributes
        name = attribute.name
        if startsWith(name, 'data-toggle-') and endsWith(name, '-dest')
          name = name.substr('data-toggle-'.length)
          name = name.substr(0, name.length - '-dest'.length)
          elements.push(name)

      for element in elements
        toggle_dest = dControl.data('toggle-' + element + '-dest')
        toggle_value = dControl.data('toggle-' + element + '-value')
        toggle_condition = dControl.data('toggle-' + element + '-condition')
        toggle_action = dControl.data('toggle-' + element + '-action')

        toggle_value = JSON.parse(toggle_value) if toggle_condition == 'in'

        toggles.push
          src: this.name
          dest: toggle_dest
          value: toggle_value
          action: toggle_action
          condition: toggle_condition

    dj.formRendererToggles(toggles, form)

  convert_all = (target)->
    setTimeout () ->jQuery('form[data-validate-form]', target).each(convert), 50

  $(document).on 'content-changed', (ev) ->
    convert_all(ev.target)


  convert_all(document)




