jQuery(function ($) {

    var dj,
        attachEvent,
        evaluate,
        getElement,
        getInputElement,
        actions;

    dj = $.dj;

    if (!dj) {
        dj = $.dj = {};
    }

    getInputElement = function (dest_elem) {
        var $this;
        var elem;

        $this = $(dest_elem);
        if ($this.is(':input')) {
            elem = $this;
        } else {
            elem = $this.find(":input");
        }

        return elem;
    }

    actions = {}

    actions['disable_true'] = function (src_elem, dest_elem, form_id) {
        dest_elem.each(function () {
            var elem = getInputElement(this);
            elem.prop("disabled", true);
        });
    };

    actions['disable_false'] = function (src_elem, dest_elem, form_id) {
        dest_elem.each(function () {
            var elem = getInputElement(this);
            elem.prop("disabled", false);
        });
    };

    actions['hide_true'] = function (src_elem, dest_elem, form_id) {
        dest_elem.each(function () {
            var elem = $(this);
            if (elem.is(':input')) {
                $.dj.getGetContainer(form_id)(this).hide();
            } else {
                elem.hide();
            }
        });
    };

    actions['hide_false'] = function (src_elem, dest_elem, form_id) {
        dest_elem.each(function () {
            var elem = $(this);
            if (elem.is(':input')) {
                $.dj.getGetContainer(form_id)(this).show();
            } else {
                elem.show();
            }
        });
    };

    actions['clear_true'] = function (src_elem, dest_elem, form_id) {
        dest_elem.each(function () {
            var elem = getInputElement(this);
            elem.val("");
            elem.trigger('change');
        });
    };

    actions['clear_false'] = function (src_elem, dest_elem, form_id) {

    };

    evaluate = function (src, val, condition, value, type) {
        var i;
        var c;

        if (type === "" || type === "string") {
            val = val;
            if (value === undefined)
                value = "";
            else
                value = value.toString();
        }

        if (src.is(":checkbox") && val == "on") {
            return src.is(':checked');
        } else if (src.is(":checkbox")) {
            return !src.is(':checked');
        }

        if (condition === "==" || condition === "" || condition === "===") {
            return val === value;
        } else if (condition === "!=") {
            return val !== value;
        } else if (condition === "<") {
            return val < value;
        } else if (condition === ">") {
            return val > value;
        } else if (condition === ">=") {
            return val >= value;
        } else if (condition === "<=") {
            return val <= value;
        } else if (condition === "in") {
            if (val.indexOf(value) >= 0) {
                return true;
            }
        } else if (condition === "nin") {
            if (val.indexOf(value) < 0) {
                return true;
            }
        }
        return false;
    };

    getElement = function (name, form_prefix, form) {
        if ($.isArray(name)) {
            var selector,
                i,
                count;

            count = name.length;
            selector = "";

            for (i = 0; i < count; i += 1) {
                if (selector) {
                    selector = selector + ", ";
                }
                selector = selector + "[name=" + form_prefix + name[i] + "]";
            }
            return form.find(selector);

        } else {
            return form.find("[name=" + form_prefix + name + "]");
        }
    };

    attachEvent = function (src, value, dest, condition, action, form_prefix, form, form_id) {
        var elem, dest_elem;

        elem = getElement(src, form_prefix, form);
        if (elem && elem.length) {
            var fnEvt = function () {
                setTimeout(function () {
                    var fn;
                    var src_value;

                    if (elem.length > 1) {
                        src_value = elem.filter(":checked").val();
                    } else {

                        src_value = elem.val();
                    }


                    if (!dest_elem) {
                        dest_elem = getElement(dest, form_prefix, form);
                    }

                    if (evaluate(elem, value, condition, src_value, "")) {
                        fn = actions[action + "_true"];
                    } else {
                        fn = actions[action + "_false"];
                    }

                    if (fn) {
                        fn(elem, dest_elem, form_id);
                    }

                }, 50);
            };

            elem.on("change", fnEvt);
            elem.on("keydown", fnEvt);

            fnEvt();
        }
    };

    dj.formRendererToggles = function (rules, form) {

        var count,
            i,
            src,
            dest,
            condition,
            action,
            value,
            rule,
            form_id,
            form_prefix;

        count = rules.length;

        if (typeof form == 'string') {
            form = $("#" + form);
        }


        form_prefix = form.data('form-prefix');
        form_id = form.attr('id');

        if (!form_prefix) {
            form_prefix = '';
        }

        for (i = 0; i < count; i += 1) {
            rule = rules[i];
            src = rule.src;
            dest = rule.dest;
            condition = rule.condition;
            action = rule.action;
            value = rule.value;

            attachEvent(src, value, dest, condition, action, form_prefix, form, form_id);

        }

    };

});