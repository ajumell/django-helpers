// Generated by CoffeeScript 1.9.3
(function () {
    jQuery(function () {
        var convert, convert_all, dj, endsWith, startsWith;
        dj = jQuery.dj;
        startsWith = function (a, s) {
            return a.slice(0, s.length) === s;
        };
        endsWith = function (a, s) {
            return s === '' || a.slice(-s.length) === s;
        };
        convert = function () {
            var controls, form, messages, rules, toggles;
            form = $(this);
            if (form.data("validator")) {
                return;
            }
            controls = form.find(":input, select");
            rules = {};
            messages = {};
            controls.each(function () {
                var dControl, fn, i, len, mapping, message, msg, rule, rule_function, validator, validators;
                if (rules[this.name]) {
                    return;
                }
                dControl = $(this);
                rule = {};
                message = {};
                validators = ['required', 'date', 'integer', 'number', 'max_digit', 'max_whole_digits', 'max_decimal_places', 'email', 'url', 'minlength', 'maxlength', 'minvalue', 'maxvalue', 'regex', 'equalto'];
                mapping = {
                    'equalto': 'equalTo'
                };
                for (i = 0, len = validators.length; i < len; i++) {
                    validator = validators[i];
                    if (dControl.data('validation-' + validator)) {
                        if (mapping[validator]) {
                            rule_function = mapping[validator];
                        } else {
                            rule_function = validator;
                        }
                        rule[rule_function] = dControl.data('validation-' + validator);
                        fn = dControl.data("validation-" + validator + "-function");
                        msg = dControl.data("validation-" + validator + "-message");
                        if (fn) {
                            msg = $.dj.msgs[fn](msg);
                        }
                        message[rule_function] = msg;
                    }
                }
                if (!jQuery.isEmptyObject(rule)) {
                    rules[this.name] = rule;
                    return messages[this.name] = message;
                }
            });
            if (!jQuery.isEmptyObject(rules)) {
                dj.validate(form, rules, messages);
            }
            toggles = [];
            controls.each(function () {
                var attribute, attributes, dControl, element, elements, i, j, len, len1, name, results, toggle_action, toggle_condition, toggle_dest, toggle_value;
                if (toggles[this.name]) {
                    return;
                }
                dControl = $(this);
                attributes = this.attributes;
                elements = [];
                for (i = 0, len = attributes.length; i < len; i++) {
                    attribute = attributes[i];
                    name = attribute.name;
                    if (startsWith(name, 'data-toggle-') && endsWith(name, '-dest')) {
                        name = name.substr('data-toggle-'.length);
                        name = name.substr(0, name.length - '-dest'.length);
                        elements.push(name);
                    }
                }
                results = [];
                for (j = 0, len1 = elements.length; j < len1; j++) {
                    element = elements[j];
                    toggle_dest = dControl.data('toggle-' + element + '-dest');
                    toggle_value = dControl.data('toggle-' + element + '-value');
                    toggle_condition = dControl.data('toggle-' + element + '-condition');
                    toggle_action = dControl.data('toggle-' + element + '-action');

                    if(toggle_condition === 'in' || toggle_condition === 'nin') {
                        toggle_value = toggle_value.split("'").join('"')
                        toggle_value = JSON.parse(toggle_value)
                    } else {
                        toggle_value = toggle_value.toString();
                    }

                    results.push(toggles.push({
                        src: this.name,
                        dest: toggle_dest,
                        value: toggle_value,
                        action: toggle_action,
                        condition: toggle_condition
                    }));
                }
                return results;
            });
            return dj.formRendererToggles(toggles, form);
        };
        convert_all = function (target) {
            return setTimeout(function () {
                jQuery('form[data-validate-form]', target).each(convert);
            }, 50);
        };
        $(document).on('content-changed', function (ev) {
            return convert_all(ev.target);
        });
        return convert_all(document);
    });

}).call(this);

//# sourceMappingURL=django-helpers-validate.js.map
