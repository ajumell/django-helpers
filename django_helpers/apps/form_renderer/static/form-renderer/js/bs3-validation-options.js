jQuery(function ($) {
    var get_parent = function (element) {
        var parent = element.parent();
        var p_parent = parent.parent();

        if (parent.hasClass("input-group")) {
            parent = p_parent;
        }

        if (p_parent.hasClass("form-group")) {
            parent = p_parent;
        }

        if (parent.is('label')) {
            parent = parent.parent();
            if (parent.hasClass('radio')) {
                parent = parent.parent();
            }
        }
        return parent;
    };

    var get_group = function (element) {
        var parent = element.parents(".form-group");
        return parent;
    };

    $.dj.setErrorPlacement(undefined, function (error, element) {

        var group_parent = get_group(element),
            parent = get_parent(element),
            existing = parent.find('label.help-block, label.error');

        group_parent.addClass('has-error');
        error = $(error);
        error.addClass('help-block');
        error.fadeTo(0, 0.1);
        if (existing.length) {
            existing.fadeOut('normal', function () {
                element.after(error);
                error.fadeTo('normal', 1);
            });
        } else {
            parent.append(error);
            error.fadeTo('normal', 1);
        }
    });

    $.dj.setSuccess(undefined, function (error, element) {
        element = $(element);
        var group_parent = get_group(element);

        group_parent.removeClass('has-error');
        error.remove();
    });

    $.dj.setGetContainer(undefined, function (elem) {
        var parent;
        if (elem) {
            elem = jQuery(elem);

            parent = elem.parents(".form-group");
            parent = parent.parent();

            return parent;
        }

    });


});