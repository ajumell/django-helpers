# coding=utf-8

from django_helpers.js_plugins.core import JavascriptPlugin
from django.core.validators import *
from django.forms import *

SPRINTF_PARAMS = 'sprintf_params'
SPRINTF_VALUE = 'sprintf_value'
SPRINTF_LENGTH = 'sprintf_length'


class JavascriptValidator(JavascriptPlugin):
    message = None
    function = None
    name = None
    parameter = None

    def get_attrs(self, instance=None):
        prefix = 'validation-{0}'.format(self.name)
        return {
            prefix: self.parameter,
            prefix + '-message': self.message,
            prefix + '-function': self.function
        }


class JSRequiredValidator(JavascriptValidator):
    name = 'required'
    parameter = True


class JSDateValidator(JavascriptValidator):
    name = 'date'


class JSIntegerValidator(JavascriptValidator):
    name = 'integer'


class JSNumberValidator(JavascriptValidator):
    name = 'number'


class JSMaxDigitsValidator(JavascriptValidator):
    function = SPRINTF_PARAMS
    name = 'max_digits'


class JSMaxWholeDigitsValidator(JavascriptValidator):
    function = SPRINTF_PARAMS
    name = 'max_whole_digits'


class JSMaxDecimalPlacesValidator(JavascriptValidator):
    name = 'max_decimal_places'
    function = SPRINTF_PARAMS


class JSEmailValidator(JavascriptValidator):
    name = 'email'
    function = SPRINTF_VALUE


class JSURLValidator(JavascriptValidator):
    name = 'url'


class JSMinLengthValidator(JavascriptValidator):
    name = 'minlength'
    function = SPRINTF_LENGTH


class JSMaxLengthValidator(JavascriptValidator):
    name = 'maxlength'
    function = SPRINTF_LENGTH


class JSMinValueValidator(JavascriptValidator):
    name = 'minvalue'
    function = SPRINTF_VALUE


class JSMaxValueValidator(JavascriptValidator):
    name = 'maxvalue'
    function = SPRINTF_VALUE


class JSRegexValidator(JavascriptValidator):
    name = 'regex'


class JSEqualToValidator(JavascriptValidator):
    name = 'equalTo'


class JSToggle(JavascriptPlugin):
    dest_element = None
    value = None
    condition = None
    action = None

    def get_attrs(self, instance=None):
        element = self.dest_element
        return {
            'toggle-' + element + '-dest': element,
            'toggle-' + element + '-value': self.value,
            'toggle-' + element + '-condition': self.condition,
            'toggle-' + element + '-action': self.action,
        }


def validate(self):
    """
    @type self: django_helpers.apps.form_renderer.FormRenderer
    @param self:
    """

    form = self.instance
    compare_validations = self.compare_validations
    toggles = self.toggles

    for name, field in form.fields.items():
        widget = field.widget
        error_messages = field.error_messages

        validators = []

        if field.required:
            validator = JSRequiredValidator()
            validator.message = error_messages['required']
            validators.append(validator)

        if isinstance(field, DateField):
            validator = JSDateValidator()
            validator.message = error_messages['invalid']
            validators.append(validator)

        if isinstance(field, DecimalField):
            validator = JSNumberValidator()
            validator.message = error_messages['invalid']
            validators.append(validator)

            digits = field.max_digits
            if digits is not None:
                validator = JSMaxDigitsValidator()
                validator.parameter = digits
                validator.message = error_messages.get('max_digits', "Fix this")
                validators.append(validator)

            decimals = field.decimal_places
            if decimals is not None:
                validator = JSMaxDecimalPlacesValidator()
                validator.parameter = decimals
                validator.message = error_messages.get('max_decimal_places', "Fix this")
                validators.append(validator)

            if decimals is not None and digits is not None:
                whole_nums = digits - decimals
                validator = JSMaxWholeDigitsValidator()
                validator.parameter = whole_nums
                validator.message = error_messages.get('max_whole_digits', "Fix this")
                validators.append(validator)

        elif isinstance(field, FloatField):
            validator = JSNumberValidator()
            validator.message = error_messages['invalid']
            validators.append(validator)

        elif isinstance(field, IntegerField):
            validator = JSIntegerValidator()
            validator.message = error_messages['invalid']
            validators.append(validator)

        for validator in field.validators:

            if isinstance(validator, DecimalValidator):
                # TODO : Fix this.
                continue

            obj = getattr(validator, 'limit_value', True)
            validator_message = getattr(validator, "message", "")
            validator_message = unicode(validator_message)
            error_msg_msg = error_messages.get(validator.code, "Fix this")
            error_message = validator_message or error_msg_msg

            if isinstance(validator, EmailValidator):
                validator = JSEmailValidator()
                validator.message = error_message
                validators.append(validator)

            elif isinstance(validator, URLValidator):
                validator = JSURLValidator()
                validator.message = error_message
                validators.append(validator)

            elif isinstance(validator, MinLengthValidator):
                validator = JSMinLengthValidator()
                validator.message = error_message
                validator.parameter = obj
                validators.append(validator)

            elif isinstance(validator, MaxLengthValidator):
                validator = JSMaxLengthValidator()
                validator.message = error_message
                validator.parameter = obj
                validators.append(validator)

            elif isinstance(validator, MinValueValidator):
                validator = JSMinValueValidator()
                validator.message = error_message
                validator.parameter = obj
                validators.append(validator)

            elif isinstance(validator, MaxValueValidator):
                validator = JSMaxValueValidator()
                validator.message = error_message
                validator.parameter = obj
                validators.append(validator)

            elif isinstance(validator, RegexValidator):
                validator = JSRegexValidator()
                validator.message = error_message
                validator.parameter = obj
                validators.append(validator)

        compare_validation = compare_validations.get(name)
        if compare_validation:
            compare_field_name = compare_validation['field']
            compare_field = form[compare_field_name]

            if field is not None:
                message = compare_validation['message']
                obj = "#" + compare_field.auto_id

                validator = JSEqualToValidator()
                validator.parameter = obj
                validator.message = message
                validators.append(validator)

        validations = self.field_validation(name)
        if 'required' in validations:
            validator = JSRequiredValidator()
            validator.message = validations['required']
            validators.append(validator)

        for validator in validators:
            attrs = widget.attrs
            if attrs is None:
                attrs = {}
                widget.attrs = attrs

            validator.apply(attrs)

    for toggle_obj in toggles:
        name = toggle_obj.get('src')
        field = form.fields.get(name)

        if field is None:
            continue

        toggle = JSToggle()

        toggle.action = toggle_obj.get('action')
        toggle.dest_element = toggle_obj.get('dest')
        toggle.condition = toggle_obj.get('condition')
        toggle.value = toggle_obj.get('value')

        attrs = field.widget.attrs
        if attrs is None:
            attrs = {}
            field.widget.attrs = attrs

        toggle.apply(attrs)
