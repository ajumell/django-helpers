# coding=utf-8
from django_helpers.widgets import HTMLNode, Div, BlockHTMLMixin
from django_helpers.widgets import Row, Column, BootstrapButtonMixin, Panel


# noinspection PyUnresolvedReferences
class HorizontalSectionMixin(object):
    def add_horizontal_section(self, col_span):
        section = FormHorizontalSection(col_span)
        self.add(section)
        return section


# noinspection PyUnresolvedReferences
class ColMixin(object):
    def add_col(self, field=None, **kwargs):
        col = FormCol(**kwargs)
        self.add(col)

        if field is not None:
            col.add_field(field)

        return col


# noinspection PyUnresolvedReferences
class RowMixin(object):
    def add_row(self, name=None):
        row = FormRow()

        if name is not None:
            row.name(name)

        self.add(row)
        return row


class FormField(HTMLNode):
    def __init__(self, field, parent=None, controller=None):
        HTMLNode.__init__(self, parent=parent, controller=controller)
        self.field = field

    def render(self):
        try:
            from django.template.loader import render_to_string

            renderer = self.controller

            fields_data = {}

            renderer.field_context(self.field, fields_data)

            parent_data = {}
            parent = getattr(self, 'parent', None)

            while parent is not None:
                fn = getattr(parent, 'field_context', None)
                if hasattr(fn, '__call__'):
                    fn(parent_data)

                parent = getattr(parent, 'parent', None)

            fields_data.update(parent_data)
            template = renderer.field_template

            return render_to_string(template, fields_data)
        except KeyError:
            return ""

    def controller_changed(self, old_controller, new_controller):
        if new_controller:
            new_controller.mark_field(self.field)


class FormErrorRenderer(HTMLNode):
    def __init__(self, controller=None):
        HTMLNode.__init__(self, controller=controller)

    def render(self):
        from django.template.loader import render_to_string

        renderer = self.controller

        fields_data = {
            'form': renderer.instance
        }

        template = renderer.error_template
        return render_to_string(template, fields_data)


class FormHorizontalSection(Div, RowMixin, ColMixin):
    def __init__(self, label_width, name=None, parent=None, controller=None):
        Div.__init__(self, None, parent, controller)
        if name is not None:
            self.name(name)
        self.label_width = label_width

    def setup(self):
        self.add_class('form-horizontal')

    def field_context(self, data):
        width = self.label_width

        data['label_span'] = width
        data['field_span'] = 12 - width

    def name(self, name=None):
        if name is not None:
            self.attr('name', name)


class FormRow(Row, BootstrapButtonMixin, HorizontalSectionMixin, BlockHTMLMixin):
    def __init__(self, name=None, parent=None, controller=None):
        Row.__init__(self, None, parent, controller)
        if name is not None:
            self.name(name)

    def name(self, name=None):
        if name is not None:
            self.attr('name', name)

    def add_col(self, field=None, **kwargs):
        col = FormCol(**kwargs)
        self.add(col)

        if field is not None:
            col.add_field(field)

        return col


class FormCol(Column, HorizontalSectionMixin):
    def add_field(self, name):
        field = FormField(name)
        self.add(field)
        return field


class FormPanel(Panel, RowMixin, ColMixin):
    def __init__(self, title, color=None, name=None, parent=None, controller=None):
        Panel.__init__(self, title, color, parent, controller)
        if name is not None:
            self.name(name)

    def name(self, name=None):
        if name is not None:
            self.attr('name', name)
