# coding=utf-8
import types

from django_helpers.apps.static_manager.utils import get_js_files, get_css_files
from exceptions import CSRFTokenMissingException, FormIDMissingException
from preprocessors.bootstrap import add_form_control_class_preprocessor
from preprocessors.extra_attrs import extra_widget_args, extra_widget_class_names, extra_container_class_names
from preprocessors.replace_widgets import replace_widgets
from preview import Preview
from validation import validate
from widgets import FormErrorRenderer

__author__ = 'ajumell'


class FormRenderer(Preview):
    has_js = True
    
    # Text for the submit button
    form_submit = 'Submit'
    
    # If focus_first is set to True then the
    # first input element will be focused using js
    focus_first = None
    
    # A Unique identifier for this form
    form_id = None
    
    # Name of the template to be rendered.
    template = 'bootstrap/form-renderer/form.html'
    
    field_template = 'bootstrap/form-renderer/field.html'
    
    error_template = 'bootstrap/form-renderer/errors.html'
    
    # Method of the form
    method = 'POST'
    
    # URL (not name of the url) to which this form has to be submitted.
    form_action = ''
    
    # A set of functions which will be executed when render function is called.
    # The form renderer will be passed as a parameter to this function
    pre_processors = [
        # This one has to be before any bootstrap pre-processors.
        replace_widgets,
        add_form_control_class_preprocessor,
        extra_widget_args,
        extra_widget_class_names,
        extra_container_class_names,
        validate,
    ]
    
    # Validation Pre Processor Options
    ignorable_custom_validations = []
    remote_validations = None
    compare_validations = None
    
    # A list of extra templates that has to be loaded.
    # This helps pre processors to add code to template.
    extra_templates = []
    
    js_templates = []
    
    _required_js_files = []
    _required_css_files = []
    
    form = None
    
    def __init__(self, instance=None, request=None, form_id=None, csrf_token=None, template=None):
        if instance is None:
            if request is None:
                raise
            
            if self.method == 'POST':
                instance = self.form(request.POST or None, request.FILES or None)
            else:
                instance = self.form(request.GET or None)
        elif self.form is not None:
            if self.method == 'POST':
                instance = self.form(request.POST or None, request.FILES or None, instance=instance)
            else:
                instance = self.form(request.GET or None, instance=instance)
        
        Preview.__init__(self, instance, template, request)
        
        self.control_classes = ['form-control']
        
        self.csrf_token = csrf_token
        self.buttons = []
        self.toggles = []
        self.added_fields = []
        
        self.class_names = {}
        self.widget_attrs = {}
        self.replace_widgets = {}
        self.compare_validations = {}
        self.container_class_names = {}
        self.help_texts = {}
        self.labels = {}
        self.validations = {}
        
        # Extra context dict
        self.extra_context_dict = {}
        
        if template is not None:
            self.template = template
        
        if self.request is None and csrf_token is None:
            raise CSRFTokenMissingException()
        
        if self.pre_processors is not None:
            self.pre_processors = self.pre_processors[:]
        
        if self.js_templates is None:
            self.js_templates = []
        else:
            self.js_templates = self.js_templates[:]
        self.js_templates.append('form-renderer/widget-js.js')
        
        if form_id is not None:
            self.form_id = form_id
        self.data = None
        
        self.html_elements = []
        
        self.appends = {}
        self.prepends = {}
        
        self.append_buttons = {}
        self.prepend_buttons = {}
        
        self.no_labels = {}
        
        self.button_color = 'primary'
        
        self.has_run_setup = False
    
    def setup_static(self):
        self.add_js_requirement('twitter-bootstrap-js')
        self.add_js_requirement('django-form-toggles-js')
        self.add_js_requirement('django-helpers-validate-js')
        self.add_css_requirement('twitter-bootstrap-css')
    
    #
    # Form Related Functions
    #
    def is_valid(self, *args, **kwargs):
        return self.instance.is_valid(*args, **kwargs)
    
    def save(self, *args, **kwargs):
        return self.instance.save(*args, **kwargs)
    
    def save_m2m(self, *args, **kwargs):
        instance = self.instance
        if not hasattr(instance, 'save_m2m'):
            return
        
        return instance.save_m2m(*args, **kwargs)
    
    #
    # Static Files
    #
    def add_js_requirement(self, files):
        t = type(files)
        if t in (tuple, list):
            for f in files:
                self._required_js_files.append(f)
        elif t in types.StringTypes:
            self._required_js_files.append(files)
    
    def add_css_requirement(self, files):
        t = type(files)
        if t in (tuple, list):
            for f in files:
                self._required_css_files.append(f)
        elif t in types.StringTypes:
            self._required_css_files.append(files)
    
    def js_files(self):
        js = self._required_js_files[:]
        for name, field in self.instance.fields.items():
            widget = field.widget
            js += get_js_files(widget)
        return js
    
    def css_files(self):
        css = self._required_css_files[:]
        for name, field in self.instance.fields.items():
            widget = field.widget
            css += get_css_files(widget)
        return css
    
    def render_prepare(self):
        if self.form_id is None:
            raise FormIDMissingException()
        
        if self.data is not None:
            return
        
        if not self.has_run_setup:
            self.has_run_setup = True
            self.setup()
        
        self.pre_process()
        
        if not self.html_elements:
            self.add_all()
            self.add_submit_button(self.form_submit)
        else:
            self.add_missing()
        
        self.add_form_name()
        
        data = {
            'errors': FormErrorRenderer(self),
            'buttons': self.buttons,
            'form': self.instance,
            'form_submit': self.form_submit,
            'focus_first': self.focus_first,
            'form_id': self.form_id,
            'method': self.method,
            'form_action': self.form_action,
            'csrf_token_html': self.csrf_token,
            'htmls': self.html_elements
        }
        
        if type(self.extra_context_dict) is dict:
            data.update(self.extra_context_dict)
        
        self.data = data
    
    def pre_process(self):
        if getattr(self, 'has_pre_processed', False):
            return
        
        setattr(self, 'has_pre_processed', True)
        
        pre_processors = self.pre_processors
        if pre_processors is not None:
            for pre_processor in pre_processors:
                pre_processor(self)
    
    def render_js(self):
        return self.render_templates(self.js_templates[:])
    
    def js_global_vars(self):
        js_arr = {}
        for name, field in self.instance.fields.items():
            widget = field.widget
            fn = getattr(widget, 'js_global_vars', None)
            if hasattr(fn, '__call__'):
                res = fn()
                if type(res) is dict:
                    js_arr.update(res)
        return js_arr
    
    #
    # Buttons
    #
    
    def create_default_button(self):
        self.add_submit_button(self.form_submit)
    
    def add_submit_button(self, value, *args, **kwargs):
        btn = self.create_submit_button(value, *args, **kwargs)
        self.buttons.append(btn)
        return btn
    
    def add_reset_button(self, value, *args, **kwargs):
        btn = self.create_reset_button(value, *args, **kwargs)
        self.buttons.append(btn)
        return btn
    
    def add_link_button(self, text, link, *args, **kwargs):
        btn = self.create_link_button(text, link, *args, **kwargs)
        self.buttons.append(btn)
        return btn
    
    #
    # Bootstrap Buttons
    #
    
    # noinspection PyMethodOverriding
    def create_submit_button(self, value, color=None, size=''):
        from django_helpers.widgets import BootstrapSubmitButton
        
        if color is None:
            color = self.button_color
        
        btn = BootstrapSubmitButton(value, color, size)
        return btn
    
    # noinspection PyMethodOverriding
    def create_reset_button(self, value, color=None, size=''):
        from django_helpers.widgets import BootstrapResetButton
        
        if color is None:
            color = self.button_color
        
        btn = BootstrapResetButton(value, color, size)
        return btn
    
    # noinspection PyMethodOverriding
    def create_link_button(self, text, link, color=None, size=None):
        from django_helpers.widgets import LinkButton
        if color is None:
            color = self.button_color
        
        btn = LinkButton(text, link, color, size)
        return btn
    
    #
    # Toggles
    #
    
    def toggle(self, src, value, dest, condition, action):
        self.toggles.append({
            'src': src,
            'value': value,
            'dest': dest,
            'condition': condition,
            'action': action
        })
    
    #
    # Clear Toggle
    #
    
    def clear_if_checked(self, src, dest):
        self.toggle(src, 'on', dest, '==', 'clear')
    
    def clear_if_unchecked(self, src, dest):
        self.toggle(src, 'off', dest, '==', 'clear')
    
    def clear_if_equals(self, src, value, dest):
        self.toggle(src, value, dest, '==', 'clear')
    
    def clear_if_not_equals(self, src, value, dest):
        self.toggle(src, value, dest, '!=', 'clear')
    
    def clear_if_less_than(self, src, value, dest):
        self.toggle(src, value, dest, '<', 'clear')
    
    def clear_if_greater_than(self, src, value, dest):
        self.toggle(src, value, dest, '>', 'clear')
    
    def clear_if_less_than_equals(self, src, value, dest):
        self.toggle(src, value, dest, '>=', 'clear')
    
    def clear_if_greater_than_equals(self, src, value, dest):
        self.toggle(src, value, dest, '>=', 'clear')
    
    def clear_if_empty(self, src, dest):
        self.toggle(src, "", dest, '==', 'clear')
    
    def clear_if_not_empty(self, src, dest):
        self.toggle(src, "", dest, '!=', 'clear')

    def clear_if_in(self, src, value, dest):
        self.toggle(src, value, dest, 'in', 'clear')
    
    def clear_if_not_in(self, src, value, dest):
        self.toggle(src, value, dest, 'nin', 'clear')
    
    #
    # Hide Toggle
    #
    
    def hide_if_checked(self, src, dest):
        self.toggle(src, 'on', dest, '==', 'hide')
    
    def hide_if_unchecked(self, src, dest):
        self.toggle(src, 'off', dest, '==', 'hide')
    
    def hide_if_equals(self, src, value, dest):
        self.toggle(src, value, dest, '==', 'hide')
    
    def hide_if_not_equals(self, src, value, dest):
        self.toggle(src, value, dest, '!=', 'hide')
    
    def hide_if_less_than(self, src, value, dest):
        self.toggle(src, value, dest, '<', 'hide')
    
    def hide_if_greater_than(self, src, value, dest):
        self.toggle(src, value, dest, '>', 'hide')
    
    def hide_if_less_than_equals(self, src, value, dest):
        self.toggle(src, value, dest, '>=', 'hide')
    
    def hide_if_greater_than_equals(self, src, value, dest):
        self.toggle(src, value, dest, '>=', 'hide')
    
    def hide_if_empty(self, src, dest):
        self.toggle(src, "", dest, '==', 'hide')
    
    def hide_if_not_empty(self, src, dest):
        self.toggle(src, "", dest, '!=', 'hide')
    
    def hide_if_in(self, src, value, dest):
        self.toggle(src, value, dest, 'in', 'hide')
    
    def hide_if_not_in(self, src, value, dest):
        self.toggle(src, value, dest, 'nin', 'hide')
    
    #
    # Disable
    #
    
    def disable_if_checked(self, src, dest):
        self.toggle(src, 'on', dest, '==', 'disable')
    
    def disable_if_unchecked(self, src, dest):
        self.toggle(src, 'off', dest, '==', 'disable')
    
    def disable_if_equals(self, src, value, dest):
        self.toggle(src, value, dest, '==', 'disable')
    
    def disable_if_not_equals(self, src, value, dest):
        self.toggle(src, value, dest, '!=', 'disable')
    
    def disable_if_less_than(self, src, value, dest):
        self.toggle(src, value, dest, '<', 'disable')
    
    def disable_if_greater_than(self, src, value, dest):
        self.toggle(src, value, dest, '>', 'disable')
    
    def disable_if_less_than_equals(self, src, value, dest):
        self.toggle(src, value, dest, '>=', 'disable')
    
    def disable_if_greater_than_equals(self, src, value, dest):
        self.toggle(src, value, dest, '>=', 'disable')
    
    def disable_if_empty(self, src, dest):
        self.toggle(src, "", dest, '==', 'disable')
    
    def disable_if_not_empty(self, src, dest):
        self.toggle(src, "", dest, '!=', 'disable')

    def disable_if_in(self, src, value, dest):
        self.toggle(src, value, dest, 'in', 'disable')
    
    def disable_if_not_in(self, src, value, dest):
        self.toggle(src, value, dest, 'nin', 'disable')
    
    #
    # Validations
    #
    def field_validation(self, name):
        validations = self.validations
        if name not in validations:
            validations[name] = {}
        
        return validations[name]
    
    def required(self, name, message='This field is required.'):
        validations = self.field_validation(name)
        validations['required'] = message
    
    #
    # Comparison Validator
    #
    def add_comparison_validator(self, first, second, message='Both fields should be same'):
        self.compare_validations[first] = {
            'field': second,
            'message': message
        }
    
    def remove_comparison_validator(self, first):
        del self.compare_validations[first]
    
    #
    # Widget Functions
    #
    
    def replace_widget(self, widget, new_widget):
        self.replace_widgets[widget] = new_widget
    
    def add_class_name(self, field, class_name):
        names = self.class_names
        if field not in names:
            names[field] = []
        
        names[field].append(class_name)
    
    def add_container_class_name(self, field, class_name):
        names = self.container_class_names
        if field not in names:
            names[field] = []
        
        names[field].append(class_name)
    
    def add_widget_attribute(self, field, attribute, value):
        names = self.widget_attrs
        if field not in names:
            names[field] = {}
        
        names[field][attribute] = value
    
    #
    # Append and Prepend
    #
    def _get_append(self, field):
        appends = self.appends
        
        if field not in appends:
            appends[field] = []
        
        return appends[field]
    
    def _get_append_buttons(self, field):
        appends = self.append_buttons
        
        if field not in appends:
            appends[field] = []
        
        return appends[field]
    
    def _get_prepend(self, field):
        prepends = self.prepends
        if field not in prepends:
            prepends[field] = []
        return prepends[field]
    
    def _get_prepend_buttons(self, field):
        prepends = self.prepend_buttons
        if field not in prepends:
            prepends[field] = []
        return prepends[field]
    
    def add_append_text(self, field, text):
        arr = self._get_append(field)
        arr.append(text)
    
    def add_append_checkbox(self, field, name):
        text = '<input type="checkbox" aria-label="{0}" name="{0}">'
        text = text.format(name)
        
        self.add_append_text(field, text)
    
    def add_prepend_text(self, field, text):
        arr = self._get_prepend(field)
        arr.append(text)
    
    def add_prepend_checkbox(self, field, name):
        text = '<input type="checkbox" aria-label="{0}" name="{0}">'
        text = text.format(name)
        
        self.add_prepend_text(field, text)
    
    def add_prepend_button(self, field, btn):
        arr = self._get_prepend_buttons(field)
        arr.append(btn)
    
    def add_append_button(self, field, btn):
        arr = self._get_append_buttons(field)
        arr.append(btn)
    
    #
    # Field Properties
    #
    def remove_label(self, field_name, place_holder=False):
        if field_name not in self.no_labels:
            self.no_labels[field_name] = place_holder
            
            if place_holder is True:
                field = self.instance.fields[field_name]
                place_holder = field.label
            
            if place_holder:
                self.add_widget_attribute(field_name, 'placeholder', place_holder)
    
    def change_help_text(self, field, text):
        self.help_texts[field] = text
    
    def change_label(self, field, label):
        self.labels[field] = label
    
    #
    # Field Rendering
    #
    def __getattr__(self, item):
        from widgets import FormField
        from django_helpers.widgets import HiddenInput
        
        form = self.instance
        fields = form.fields
        
        if item == 'form_id_field':
            return HiddenInput(self.form_id, 'form-id').render()
        
        if item == "errors":
            return FormErrorRenderer(self)
        
        if item not in fields:
            raise AttributeError()
        
        self.pre_process()
        return FormField(item, controller=self).render()
    
    def field_context(self, field_name, context):
        form = self.instance
        field = form[field_name]
        container_class_name = self.container_class_names.get(field_name)
        help_texts = self.help_texts.get(field_name, field.help_text)
        label = self.labels.get(field_name, field.label)
        
        context['field'] = field
        context['label'] = label
        context['id_for_label'] = field.id_for_label
        context['errors'] = field.errors
        context['help_text'] = help_texts
        context['container_class_names'] = container_class_name
        
        if field_name in self.no_labels:
            context['no_label'] = True
        
        appends = self.appends.get(field_name, [])
        prepends = self.prepends.get(field_name, [])
        prepend_buttons = self.prepend_buttons.get(field_name, [])
        append_buttons = self.append_buttons.get(field_name, [])
        
        has_buttons = len(prepend_buttons) > 0 or len(append_buttons) > 0
        has_addons = len(appends) > 0 or len(prepends)
        need_group = has_addons > 0 or has_buttons
        
        context['need_group'] = need_group
        context['prepends'] = prepends
        context['appends'] = appends
        
        context['prepend_buttons'] = prepend_buttons
        context['append_buttons'] = append_buttons
    
    def add_all(self):
        form = self.instance
        for field in form:
            self.add_row(field=field.name)
    
    def mark_field(self, name):
        fields = self.added_fields
        if name not in fields:
            fields.append(name)
    
    def add_missing(self):
        form = self.instance
        added_fields = self.added_fields
        
        for field in form:
            name = field.name
            if name not in added_fields:
                self.add_row(field=name)
    
    def add_form_name(self):
        from django_helpers.widgets import HiddenInput
        
        field = HiddenInput(self.form_id, 'form-id')
        self.html_elements.append(field)


class MaterialFormRenderer(FormRenderer):
    field_template = 'bootstrap/material/field.html'
    
    def __init__(self, *args, **kwargs):
        FormRenderer.__init__(self, *args, **kwargs)
        self.control_classes.append('material-control')
    
    def setup_static(self):
        FormRenderer.setup_static(self)
        self.add_js_requirement('material-forms-js')
        self.add_css_requirement('material-forms-css')
