# coding=utf-8
import types

from django import forms
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

from django_helpers.apps.forms.widgets import bootstrap
from django_helpers.apps.forms.widgets import jui
from django_helpers.apps.static_manager import register_js
from django_helpers.widgets import Icon, BootstrapButton
from extra_attrs import combine_class_names

__author__ = 'ajumell'
__all__ = (
    'prepend_preprocessor',
    'append_preprocessor',
    'make_form_control_excemption'
)

FORM_CONTROL_WIDGETS = (

    jui.DateInput,
    jui.SpinnerInput,
    jui.MaskedInput,

    bootstrap.DatePickerWidget,
    bootstrap.DateRangeInputWidget,
    bootstrap.TimePickerWidget,
    bootstrap.ClockFaceWidget,
    bootstrap.ColorPickerWidget,
    bootstrap.SelectWidget,
    bootstrap.MultiSelectWidget,

    forms.TextInput,
    forms.FileInput,
    forms.Textarea,
    forms.SelectMultiple,
    forms.Select,
    forms.PasswordInput
)

FORM_CONTROL_WIDGETS_EXCEPTIONS = [
    forms.RadioSelect,
    forms.CheckboxInput
]


def make_form_control_excemption(widget):
    FORM_CONTROL_WIDGETS_EXCEPTIONS.append(widget)


def make_addon(text):
    return u'<span class="add-on">%s</span>' % conditional_escape(text)


def get_btn_class(color):
    if color != '':
        color = ' btn-' + color
    return 'btn' + color


def get_contents(text, icon):
    if icon != '':
        icon = Icon(icon)
        icon = icon.render()
    return mark_safe(icon + conditional_escape(text))


def make_button(text, color='', button_type='button', icon=''):
    contents = get_contents(text, icon)
    btn = BootstrapButton(contents)
    btn.color = color
    btn.icon = icon
    btn.attr('type', button_type)

    return btn.render()


def make_link_button(text, url='#', color='', icon=''):
    from django_helpers.widgets import LinkButton
    btn = LinkButton(text, url)
    btn.color = color
    btn.icon = icon

    return btn.render()


def make_text(prop):
    name = 'addon'
    args = prop

    if isinstance(prop, dict):
        assert len(prop.keys()) == 1, 'There cannot be more than one property'
        name, args = prop.items()[0]

    elif type(prop) in (tuple, list):
        name = prop[0]
        args = prop[1:]
        assert len(args) > 0, 'Arguments must be greater than one'

    name = 'make_%s' % name
    if name not in globals():
        return

    func = globals()[name]

    if isinstance(func, types.FunctionType):
        if type(args) in (tuple, list):
            return func(*args)
        elif type(args) is dict:
            return func(**args)
        else:
            return func(args)


def _base_preprocessor(renderer, property_name='addend'):
    form = renderer.instance
    datas = getattr(renderer, '%ss' % property_name, None)
    if type(datas) is not dict:
        return
    for name, data in datas.items():
        field = form.fields[name]
        setattr(field, 'has_%s' % property_name, True)
        if type(data) in (list, tuple):
            text = ''
            for prop in data:
                text += make_text(prop)
        else:
            text = make_text(data)
        setattr(field, '%s_text' % property_name, mark_safe(text))


def prepend_preprocessor(renderer):
    return _base_preprocessor(renderer, 'prepend')


def append_preprocessor(renderer):
    return _base_preprocessor(renderer, 'append')


def add_form_control_class_preprocessor(renderer):
    class_names = getattr(renderer, 'class_names', None)
    if class_names is None:
        class_names = {}
        setattr(renderer, 'class_names', class_names)

    control_classes = getattr(renderer, 'control_classes', None)
    control_classes = " ".join(control_classes)
    form = renderer.instance
    for name, field in form.fields.items():
        widget = field.widget
        if not isinstance(widget, FORM_CONTROL_WIDGETS):
            continue

        if isinstance(widget, tuple(FORM_CONTROL_WIDGETS_EXCEPTIONS)):
            continue

        if getattr(widget, 'no_form_control', False):
            continue

        class_name = class_names.get(name, '')
        if type(class_name) in (list, tuple):
            new_class_name = list(class_name)
            new_class_name.append(control_classes)
        else:
            new_class_name = ' '.join((class_name, control_classes))

        class_names[name] = new_class_name


def _process_container_classes(form_renderer, class_names, name_list):
    container_class_names = getattr(form_renderer, 'container_class_names', 'None')

    if container_class_names == 'None':
        container_class_names = {}
        setattr(form_renderer, 'container_class_names', container_class_names)

    if type(container_class_names) is not dict:
        return

    if type(name_list) not in (list, tuple):
        return

    for name in name_list:
        old_class_names = container_class_names.get(name, '')
        new_class_name = combine_class_names(old_class_names, class_names)
        container_class_names[name] = new_class_name


register_js('django-form-toggles-js', 'form-renderer/js/toggles.js', ('jquery',))


def bs3_toggles_preprocessor(renderer):
    renderer.add_js_requirement('django-form-toggles-js')
    renderer.js_templates.append('form-renderer/preprocessors/get-container-bs3.js')
    renderer.js_templates.append('form-renderer/preprocessors/toggles.js')
