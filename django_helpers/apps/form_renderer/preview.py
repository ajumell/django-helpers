# coding=utf-8
from django.utils.safestring import mark_safe
from django_helpers.helpers.views import render_to_string
from django_helpers.templatetags.jsmin import remove_blank_lines

__author__ = 'ajumell'


class Preview(object):
    # Name of the template to be rendered.
    template = 'bootstrap/preview/form.html'

    field_template = 'bootstrap/preview/field.html'

    def __init__(self, instance, template=None, request=None):
        self.request = request
        self.instance = instance

        self.class_names = {}
        self.container_class_names = {}

        if template is not None:
            self.template = template

        self.data = None

        self.html_elements = []

        self.has_run_setup = False

    def setup(self):
        pass

    def __str__(self):
        return self.render()

    def __unicode__(self):
        return self.render()

    def render_prepare(self):
        if self.data is not None:
            return

        instance = self.instance

        if not self.has_run_setup:
            self.has_run_setup = True
            self.setup()

        if not self.html_elements:
            self.add_all()

        data = {
            'instance': instance,
            'htmls': self.html_elements
        }

        self.data = data

    def render_templates(self, templates):
        self.render_prepare()
        data = self.data
        output = []
        for template in templates:
            op = render_to_string(template, data, self.request)
            output.append(op)
        return mark_safe(remove_blank_lines('\n'.join(output)))

    def render(self):
        op = self.render_templates([self.template])
        return op

    #
    # Bootstrap Containers
    #
    def add_widget(self, widget):
        self.html_elements.append(widget)

    def add_row(self, html_id=None, field=None, **kwargs):
        from widgets import FormRow

        row = FormRow(html_id, controller=self)
        if field is not None:
            row.add_col(field, **kwargs)

        self.html_elements.append(row)
        return row

    def add_panel(self, title, color='default', name=None):
        from widgets import FormPanel

        panel = FormPanel(title, color=color, name=name, controller=self)

        self.html_elements.append(panel)
        return panel

    def add_horizontal_section(self, label_width, html_id=None):
        from widgets import FormHorizontalSection

        child = FormHorizontalSection(label_width, html_id, controller=self)
        self.html_elements.append(child)
        return child

    def add_all(self):
        pass

    #
    # Field Rendering
    #
    def field_context(self, field_name, context):
        instance = self.instance

        meta = getattr(instance, '_meta')
        model_field = meta.get_field(field_name)

        context['label'] = model_field.verbose_name
        context['value'] = getattr(instance, field_name)

    def mark_field(self, name):
        pass
