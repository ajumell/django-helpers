# coding=utf-8
from django.conf.urls import patterns, url

from preprocessors.validation import validate_form

urlpatterns = [
    url(r'^validate-form/$', validate_form, name='validate-form'),
]
