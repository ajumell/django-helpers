jQuery && jQuery(function ($) {

    var dj,
        registerDataWidget,
        generateDataOptions;

    dj = $.dj;
    registerDataWidget = dj.registerDataWidget;
    generateDataOptions = dj.generateDataOptions;

    if (!$.fn.dataTable) {
        return;
    }

    registerDataWidget('data-table', function (obj) {
        var
            form,
            table,
            oTable,
            columns,
            oOptions,
            headers,
            mappings,
            column_filter,
            dataTableOptions,
            oLanguage,
            fnServerParams;

        mappings = {
            'formId': 'form_id',
            'total': 'total',
            'displayLength': 'displayLength',
            'dom': 'dom',
            'ajaxSource': 'ajax_source',
            'displayLengths': 'displayLengths'
        };

        oOptions = generateDataOptions(obj, mappings);

        form = $(oOptions.formId);

        obj.find("[data-pagination]").remove();
        obj.find("[data-search]").remove();
        table = obj.find('table');
        column_filter = table.find("tr[data-column-filter]");
        column_filter.detach();

        columns = [];
        headers = table.find("th");

        headers.each(function () {
            var header = $(this);
            columns.push(generateDataOptions(header, {
                'width': 'sWidth',
                'sortable': 'bSortable',
                'searchable': 'bSearchable'
            }));
        });

        oLanguage = generateDataOptions(table, {
            'infoEmpty': 'sZeroRecords',
            'infoLoading': 'sLoadingRecords',
            'infoProcessing': 'sProcessing'
        })

        dataTableOptions = {
            "fnDrawCallback": function (settings) {
                table.contentUpdated();
            },

            "bProcessing": true,
            "bServerSide": true,
            "iDeferLoading": oOptions.total || 0,
            "iDisplayLength": oOptions.displayLength,
            "bPaginate": true,
            "bDeferRender": true,
            "sAjaxSource": oOptions.ajax_source,
            "aoColumns": columns,
            "oLanguage": oLanguage
        };


        if (oOptions.dom) {
            dataTableOptions["sDom"] = oOptions.dom;
        }

        if (oOptions.displayLengths) {
            var asLengths = oOptions.displayLengths.split(",");
            var aiLengths = [];
            jQuery.each(asLengths, function () {
                aiLengths.push(parseInt(this, 10));
            });
            dataTableOptions['aLengthMenu'] = aiLengths;
        }


        dataTableOptions["fnServerParams"] = function (aData) {
            if (form.length) {
                (function () {
                    var form_data, len;
                    form_data = form.serializeArray();
                    len = form_data.length;
                    while (len--) {
                        aData.push(form_data[len]);
                    }
                }());
            }


            if (column_filter.length) {
                (function () {
                    $("input, select", column_filter).each(function () {
                        var name, val;
                        name = this.name;
                        val = this.value;
                        aData.push({
                            name: name,
                            value: val
                        });
                    });
                }());
            }
        };


        oTable = table.dataTable(dataTableOptions);

        table.data("oTable", oTable);
        if (column_filter) {
            table.find("thead").append(column_filter);
        }

    });


});