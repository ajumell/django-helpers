# coding=utf-8
from types import TypeType

from django.core.urlresolvers import reverse
from django.utils import six
from django.utils.safestring import mark_safe

from django_helpers.apps.static_manager import register_js, register_css
from django_helpers.core.listable import BaseModelListable, BaseModelEditableListable
from django_helpers.js_plugins.core import GenericJavascriptPlugin

__author__ = 'ajumell'

register_js('jquery-data-table-js', 'js-plugins/jquery-data-tables/js/jquery-data-tables-min.1.9.4.js', ('jquery', 'jquery-django-ajax-csrf'))
register_js('jquery-data-table-bootstrap-js', 'js-plugins/jquery-data-tables/js/DT_bootstrap.js', ('jquery-data-table-js',))

register_js('jquery-data-table-standing-draw-js', 'js-plugins/jquery-data-tables/js/dataTable.standing-draw.js', ('jquery-data-table-js',))
register_js('jquery-data-table-ajax-reload-js', 'js-plugins/jquery-data-tables/js/dataTable.ajax.reload.js', ('jquery-data-table-standing-draw-js',))

register_css('jquery-data-table-css', 'js-plugins/jquery-data-tables/css/jquery.dataTables.css')
register_css('jquery-data-table-bootstrap-css', 'js-plugins/jquery-data-tables/css/DT_bootstrap.css')

register_js('django-data-table-js', 'js-plugins/jquery-data-tables/js/django.data-tables.js', ('jquery-data-table-js', 'django-helpers-core-js'))

#
# Scroller
#
register_js('jquery-data-table-scroller-js', 'js-plugins/jquery-data-tables/js/dataTables.scroller.min.js', ('jquery-data-table-js',))
register_css('jquery-data-table-scroller-css', 'js-plugins/jquery-data-tables/plugins/scroller/css/dataTables.scroller.css')

#
# Col Reorder
#
register_js('jquery-data-table-col-reorder-css', 'js-plugins/jquery-data-tables/plugins/col-reorder/js/ColReorder.min.js', ('jquery-data-table-js',))
register_css('jquery-data-table-col-reorder-css', 'js-plugins/jquery-data-tables/plugins/col-reorder/css/ColReorder.css')


class DataTableMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        from views import register_name
        
        obj = type.__new__(mcs, name, bases, attrs)
        table_id = attrs.get('table_id', None)
        params = attrs.get('url_search_parameters', None)
        if params is None:
            for b in bases:
                params = getattr(b, 'url_search_parameters', None)
                if params is not None:
                    break
        
        register_name(obj, table_id, params)
        return obj


# noinspection PyAttributeOutsideInit
class DataTable(six.with_metaclass(DataTableMetaClass, BaseModelEditableListable)):
    needs_kwargs = False
    
    has_js = True
    query = None
    
    width = None
    class_name = ""
    name = None
    fields = []
    table_id = None
    display_lengths = [2, 3, 5, 10, 25, 50, 100]
    loading_message = 'Please wait while data is loading'
    
    scroller = False
    scroller_height = 200
    
    info_empty = None
    info_loading = None
    info_processing = None
    
    row_id = "pk"
    
    dom = None
    template = 'data-tables/table-html.html'
    js_template = 'data-tables/table-js.js'
    extra_fields = None
    
    need_columns_refresh = False
    
    form_filter = None
    
    def __init__(self, request=None):
        super(DataTable, self).__init__(request)
        self.buttons = []
        
        filter_form = self.form_filter
        if filter_form is not None:
            if type(filter_form) is TypeType:
                filter_form = filter_form()
                self.form_filter = filter_form
            
            if filter_form.form_id is None:
                raise
        
        self.setup()
        self.setup_static_files()
    
    def refresh_columns(self, request):
        BaseModelEditableListable.refresh_columns(self, request)
        for field in self.fields:
            plugin = GenericJavascriptPlugin()
            plugin.add_data_attrs = False
            
            if field.title_class:
                plugin.add('class', field.title_class)
            
            if field.title_width:
                plugin.add('style', "width:" + field.title_width + "px")
            
            plugin.add('data-searchable', field.searchable)
            plugin.add('data-sortable', field.sortable)
            plugin.add('data-width', field.title_width)
            
            field.attrs = plugin
    
    def setup(self):
        pass
    
    def id(self):
        return self.table_id
    
    def get_item_pk(self):
        return self.row_id
    
    #
    # Static Files Functions
    #
    def setup_static_files(self):
        
        super(DataTable, self).setup_static_files()
        
        js_files = self.js_files
        css_files = self.css_files
        
        js_files += ['django-data-table-js']
        css_files += ['jquery-data-table-css']
        
        js_files.append('jquery-data-table-bootstrap-js')
        css_files.append('jquery-data-table-bootstrap-css')
        js_files.append('jquery-data-table-ajax-reload-js')
        
        if self.scroller:
            js_files.append('jquery-data-table-scroller-js')
            css_files.append('jquery-data-table-scroller-css')
    
    #
    # URL Functions
    #
    def get_ajax_url(self, kwargs, table_id):
        try:
            ajax_source = reverse(self.id(), kwargs=kwargs)
        except Exception:
            from views import has_registered
            
            if not has_registered(table_id):
                t = type(self)
                m = t.__module__
                n = t.__name__
                raise Exception('This table "%s.%s" is not registered.' % (m, n))
            raise Exception('Django helpers is not added in root urls.')
        return ajax_source
    
    def generate_render_data(self):
        if self._data is None:
            kwargs = self.url_kwargs
            
            table_id = self.id()
            ajax_source = self.get_ajax_url(kwargs, table_id)
            
            attrs = GenericJavascriptPlugin()
            attrs.add('ajax-source', ajax_source)
            attrs.add('display-length', self.display_length)
            attrs.add('dom', self.dom)
            
            attrs.add('info-loading', self.info_loading)
            attrs.add('info-processing', self.info_processing)
            attrs.add('info-empty', self.info_empty)
            attrs.add('display-length', self.display_length)
            
            lengths = [str(x) for x in self.display_lengths]
            attrs.add('display-lengths', ",".join(lengths))
            
            attrs.add('data-data-table', "true")
            attrs.add('data-table-form', "true")
            
            self._data = {
                'attrs': attrs,
                
                'display_length': self.display_length,
                'display_lengths': self.display_lengths,
                'dom': self.dom,
                'ajax_source': ajax_source,
                'columns': self.fields,
                "table_id": self.widget_id(),
                'width': self.width,
                'loading_message': self.loading_message,
                
                'scroller': self.scroller,
                'scroller_height': self.scroller_height,
                
                'info_loading': self.info_loading,
                'info_processing': self.info_processing,
                'info_empty': self.info_empty,
                
                'buttons': self.buttons
            }
            
            filter_form = self.form_filter
            if filter_form is not None:
                self._data['has_form'] = True
                self._data['form_id'] = filter_form.form_id
        
        return self._data
    
    def reset_params(self):
        self._widget_id = None
        self._data = None
    
    #
    # Rendering
    #
    
    def render(self):
        kwargs = self.url_kwargs
        self.generate_render_data()
        self.initial_data(kwargs)
        return self.render_a_template(self.template)
    
    def sort_first_column(self, query):
        column = self.fields[0]
        if column.sortable is False:
            return query
        return self.sort(query, sortings=[column.get_sort_field()])
    
    def sort(self, query, date_dict=None, sortings=None):
        if sortings is None and date_dict is not None:
            gt = date_dict.get
            sortings = []
            columns = self.fields
            total_sort_columns = gt('iSortingCols', 0)
            total_sort_columns = int(total_sort_columns)
            
            if total_sort_columns is 0:
                return self.sort_first_column(query)
            
            while total_sort_columns:
                total_sort_columns -= 1
                column_number = int(gt('iSortCol_%d' % total_sort_columns))
                sort_order = gt('sSortDir_%d' % total_sort_columns)
                column = columns[column_number]
                if column.sortable:
                    field = column.get_sort_field()
                    if sort_order == 'desc':
                        # noinspection PyAugmentAssignment
                        field = '-' + field
                    sortings.append(field)
            sortings.reverse()
        
        if sortings is not None:
            query = query.order_by(*sortings)
        
        return query
    
    def page_data(self, query, data_dict=None, start=None, max_items=None):
        if start is None and max_items is None:
            gt = data_dict.get
            start = int(gt('iDisplayStart', 0))
            max_items = int(gt('iDisplayLength', self.display_length))
        
        if max_items is None:
            max_items = self.display_length
        
        return query[start:start + max_items]
    
    def get_search_term(self, data_dict, search_term):
        if data_dict is not None and search_term is None:
            gt = data_dict.get
            search_term = gt('sSearch', None)
        
        return search_term
    
    #
    # Initial Data For Table rendering
    #
    def initial_data(self, kwargs):
        request = self.request
        
        if self.request is None:
            raise Exception('Request is required for rendering initial data')
        
        query = self._get_query(request, kwargs)
        
        search_term = self.initial_search_term(request)
        prev_total = None
        if search_term:
            prev_total = query.count()
            query = self.search(query, search_term=search_term)
        
        query = self.sort_first_column(query)
        total_length = query.count()
        paginator = self.initial_pagination(request, total_length)
        query = self.only(query)
        query = self.page_data(query, start=paginator['from'] - 1)
        
        data = self._data
        
        data['attrs'].add('total', total_length)
        data['rows'] = self.generate_initial_data_dict(query, request)
        data['total'] = total_length
        data['qs'] = paginator['qs']
        data['request'] = request
        data['paginator'] = paginator
        data['search_term'] = search_term
        data['prev_total'] = prev_total
    
    def generate_initial_data_dict(self, query, request):
        rows = []
        row_id = self.get_item_pk()
        columns = self.fields
        for record in query:
            self._instance = record
            pk = getattr(record, row_id)
            self.pk = pk
            
            row = {}
            if row_id is not None:
                row['row_id'] = pk
            cells = []
            row['cells'] = cells
            for column in columns:
                cells.append(mark_safe(column.render(request, record)))
            rows.append(row)
        
        return rows
    
    def initial_search_term(self, request):
        search_key = self.id() + '-search'
        search_term = request.GET.get(search_key, None)
        return search_term
    
    def initial_search(self, request, query):
        search_key = self.id() + '-search'
        search_term = request.GET.get(search_key, None)
        if search_term is not None:
            query = self.search(query, search_term=search_term)
        return search_term, query
    
    def do_form_filter(self, query, data_dict):
        form_filter = self.form_filter
        if form_filter is not None:
            query = form_filter.filter(query, data_dict)
        return query
    
    #
    # AJAX Table Rendering
    #
    
    def generate_ajax_json(self, query, request, data_dict, total_length, current_length):
        datas = []
        row_id = self.get_item_pk()
        columns = self.fields
        gt = data_dict.get
        echo = gt('sEcho')
        
        for record in query:
            self._instance = record
            pk = getattr(record, row_id)
            self.pk = pk
            
            data = {}
            
            if row_id is not None:
                data['DT_RowId'] = pk
            
            i = 0
            for column in columns:
                data[str(i)] = unicode(column.render(request, record))
                i += 1
            datas.append(data)
        
        data = {
            "aaData": datas,
            "sEcho": echo,
            "iTotalRecords": total_length,
            "iTotalDisplayRecords": current_length
        }
        
        return data
    
    #
    # Fields
    #
    def get_extra_fields(self):
        extra_fields = BaseModelListable.get_extra_fields(self)
        id_ = [self.get_item_pk()]
        if extra_fields is None:
            return id_
        return list(extra_fields + id_)
    
    #
    # Buttons
    #
    def add_button(self, text, action, color=None, icon=None):
        self.buttons.append({
            "text": text,
            "action": action,
            "color": color,
            "icon": icon
        })
    
    #
    # Editable
    #
    
    # noinspection PyAttributeOutsideInit
    def get_list_url(self, name):
        if not hasattr(self, 'url_kwargs'):
            raise
        
        kwargs = {
            'field_name': name,
            'pk': self.get_pk()
        }
        
        if self.url_kwargs is not None:
            kwargs.update(self.url_kwargs)
        
        return reverse(self.id() + "-list", kwargs=kwargs)
    
    def get_save_url(self):
        if not hasattr(self, 'save_url'):
            kwargs = self.url_kwargs
            self.save_url = reverse(self.id() + "-save", kwargs=kwargs)
        
        return self.save_url
