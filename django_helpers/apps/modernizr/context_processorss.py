# coding=utf-8
def modernizer(request):
    return {
        'modernizr': request.modernizr or False
    }
