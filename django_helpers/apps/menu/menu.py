# coding=utf-8
from django.core.urlresolvers import reverse, resolve
from django.template import RequestContext, resolve_variable
from django.utils.safestring import mark_safe

from django_helpers.helpers.views import render_to_string
from django_helpers.js_plugins.core import JavascriptPlugins, ModalLinkPlugin
from exceptions import *


class MenuMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        from register import register_name
        
        obj = type.__new__(mcs, name, bases, attrs)
        menu_id = attrs.get('menu_id', None)
        if menu_id is not None:
            module_name = attrs['__module__']
            class_name = module_name + '.' + name
            register_name(class_name, menu_id)
        return obj


class MenuAndItemBase(object):
    items = None
    parent = None
    level = -1
    
    def add_label(self, text):
        pass
    
    def add_item(self, text, link="#", is_url_name=None, url_args=None, url_kwargs=None):
        items = self.get_items()
        
        cls = getattr(self, "__item_class__", self.__class__)
        item = cls()
        
        if is_url_name is None:
            is_url_name = not self.is_same_page_url(link)
        
        item.text = text
        item.link = link
        item.level = self.level + 1
        item.parent = self
        item.is_url_name = is_url_name
        item.link_args = url_args
        item.link_kwargs = url_kwargs
        
        items.append(item)
        
        return item
    
    def get_items(self):
        items = self.items
        if items is None:
            items = []
            self.items = items
        return items
    
    def add_user_item(self, text, link="#", is_url_name=None, url_args=None, url_kwargs=None):
        """

        :rtype : MenuItem
        """
        item = self.add_item(text, link, is_url_name, url_args, url_kwargs)
        item.allow_guest = False
        
        return item
    
    def add_guest_item(self, text, link="#", is_url_name=None, url_args=None, url_kwargs=None):
        item = self.add_item(text, link, is_url_name, url_args, url_kwargs)
        item.allow_users = False
        item.allow_admin = False
        
        return item
    
    def add_admin_item(self, text, link="#", is_url_name=None, url_args=None, url_kwargs=None):
        item = self.add_item(text, link, is_url_name, url_args, url_kwargs)
        item.allow_users = False
        item.allow_guest = False
        
        return item
    
    def add_guest_user_item(self, text, link="#", is_url_name=None, url_args=None, url_kwargs=None):
        item = self.add_item(text, link, is_url_name, url_args, url_kwargs)
        item.allow_users = True
        item.allow_guest = True
        
        return item
    
    def add_admin_user_item(self, text, link="#", is_url_name=None, url_args=None, url_kwargs=None):
        item = self.add_item(text, link, is_url_name, url_args, url_kwargs)
        item.allow_users = True
        item.allow_admin = True
        
        return item
    
    def has_sub_menu(self):
        items = self.items
        if items is None:
            return False
        return len(items) > 0
    
    def is_same_page_url(self, url):
        if url.startswith("#"):
            return True
        
        if not url:
            return True
        
        if url.startswith("javascript:"):
            return True
        
        return False


# noinspection PyMethodMayBeStatic
class MenuItem(MenuAndItemBase):
    text = None
    text_parameters = None
    
    link = None
    link_args = None
    link_kwargs = None
    is_url_name = True
    
    allow_users = True
    allow_admin = True
    allow_guest = True
    
    icon = None
    badge = None
    
    required_permissions = None
    extra_highlights = None
    
    def __init__(self):
        self.plugins = JavascriptPlugins()
    
    #
    #   Getters
    #
    def get_text(self):
        params = self.text_parameters
        text = self.text
        context = self.context
        
        if not params:
            return text
        
        args = []
        for param in params:
            default = None
            if type(param) in (tuple, list):
                param, default = param
            try:
                op = resolve_variable(param, context)
            except:
                op = default if default is not None else param
            args.append(op)
        return text % tuple(args)
    
    def get_link(self):
        if self.is_url_name and self.link[0] != '/':
            return reverse(self.link, args=self.link_args, kwargs=self.link_kwargs)
        return self.link
    
    def has_valid_link(self):
        return not self.is_same_page_url(self.link)
    
    #
    #   Highlight
    #
    
    def add_highlight_urls(self, *urls):
        if self.extra_highlights is None:
            self.extra_highlights = []
        self.extra_highlights.extend(urls)
    
    def is_current(self):
        url = self.current_path or self.request.get_full_path()
        return self.get_link() == url
    
    def can_highlight(self):
        if self.is_current():
            return True
        
        if self.extra_highlights is not None:
            if self.current_url_name in self.extra_highlights:
                return True
        return False
    
    def is_descent_current(self):
        if self.has_sub_menu():
            for item in self.items:
                if item.can_highlight():
                    return True
        return False
    
    #
    #   Permissions
    #
    
    def has_permission(self):
        request = self.request
        user = request.user
        
        is_admin = user.is_superuser
        is_user = user.is_authenticated() and not is_admin
        is_guest = not is_user and not is_admin
        
        has_perm = True
        perms = self.required_permissions or []
        for perm in perms:
            has_perm = has_perm and user.has_perm(perm)
        
        if not has_perm:
            return False
        
        if self.allow_admin and is_admin:
            return True
        
        if self.allow_users and is_user:
            return True
        
        if self.allow_guest and is_guest:
            return True
        
        return False
    
    def add_permissions(self, *permissions):
        perms = self.required_permissions
        if perms is None:
            perms = []
            self.required_permissions = perms
        
        for perm in permissions:
            if perm:
                perms.append(perm)
    
    def remove_permissions(self, *permissions):
        perms = self.required_permissions
        if perms is None:
            return
        
        for perm in permissions:
            if perm in perms:
                perms.remove(perm)
    
    def allow(self, user=True, admin=True, guest=True):
        self.allow_guest = guest
        self.allow_users = user
        self.allow_admin = admin
    
    def should_render(self):
        has_perm = self.has_permission()
        if not has_perm:
            return False
        
        if not self.has_sub_menu():
            return True
        
        items = self.items
        for item in items:
            if item.should_render():
                return True
        
        if self.has_valid_link():
            return True
        
        return False
    
    #
    #   Render
    #
    
    def get_template(self):
        return 'menu/simple-item.html'
    
    def set_params(self, request, context, path, current_url_name):
        self.request = request
        self.context = context
        self.current_path = path
        self.current_url_name = current_url_name
        
        if self.has_sub_menu():
            for item in self.items:
                item.set_params(request, context, path, current_url_name)
    
    def render(self):
        if not self.should_render():
            return ""
        
        d = self.get_render_context()
        return render_to_string(self.get_template(), d, self.request)
    
    def get_render_context(self):
        return {
            'level': self.level,
            'icon': self.icon,
            'badge': self.badge,
            'attrs': self.plugins,
            'url': self.get_link(),
            'text': self.get_text(),
            'is_current': self.is_current(),
            'decedent_current': self.is_descent_current(),
            'can_highlight': self.can_highlight(),
            'has_sub_menu': self.has_sub_menu(),
            'sub_menu': mark_safe(self.render_sub_items())
        }
    
    def render_sub_start(self):
        return "<ul>"
    
    def render_sub_end(self):
        return "</ul>"
    
    def render_sub_items(self):
        html = ""
        if self.has_sub_menu():
            html += self.render_sub_start()
            for item in self.items:
                html += item.render()
            html += self.render_sub_end()
        return html
    
    #
    # Extras
    #
    def set_badge(self, text, *args, **kwargs):
        self.badge = text
    
    def set_icon(self, name, *args, **kwargs):
        self.icon = name
    
    #
    # Javascript Plugins
    #
    def modal(self, loading=None, title=None):
        modal = ModalLinkPlugin()
        modal.is_ajax = True
        if loading is not None:
            modal.loading = loading
        
        if title is None:
            title = self.text
        
        modal.title = title
        
        self.plugins.add(modal)


class Menu(MenuAndItemBase):
    __metaclass__ = MenuMetaClass
    __item_class__ = MenuItem
    
    menu_classes = []
    has_js = True
    menu_id = None
    highlight_current = True
    ajax = False
    
    def __init__(self, context, menu_id=None, request=None):
        cls = getattr(self, "__item_class__", None)
        
        if cls is None:
            raise MenuItemClassNotProvided()
        
        if menu_id is not None:
            self.menu_id = menu_id
        
        if not isinstance(context, RequestContext):
            raise InvalidContextException()
        
        if self.menu_id is None:
            raise MenuIDNotFoundException()
        
        self.context = context
        if request is None:
            request = self.get_variable('request')
        
        self.current_url = resolve(request.path_info).url_name
        self.path = request.get_full_path()
        
        self.request = request
        self.setup()
    
    def get_variable(self, name):
        return resolve_variable(name, self.context)
    
    def setup(self):
        pass
    
    def render_list(self):
        op = []
        for item in self.items:
            item.set_params(self.request, self.context, self.path, self.current_url)
            op.append(item.render())
        return '\n'.join(op)
    
    def render_menu_start(self):
        classes = self.menu_classes[:]
        return '<ul class="%s" id="%s">' % (' '.join(classes), self.menu_id)
    
    def render_menu_end(self):
        return '</ul>'
    
    def render(self):
        op = ''
        
        op += self.render_menu_start()
        op += self.render_list()
        op += self.render_menu_end()
        
        return op
