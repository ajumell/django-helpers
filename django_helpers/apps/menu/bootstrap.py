# coding=utf-8
__author__ = 'ajumell'

from django_helpers.helpers.views import render_to_string
from menu import Menu, MenuItem


class BootstrapMenuItem(MenuItem):
    def render_sub_start(self):
        return '<ul class="dropdown-menu">'

    def get_template(self):
        level = self.level
        has_sub_menu = self.has_sub_menu()

        if level == 0:
            if has_sub_menu:
                return 'menu/bootstrap-nav-with-drop-down.html'
            else:
                return 'menu/bootstrap-nav.html'

        if level > 0:
            if has_sub_menu:
                return 'menu/bootstrap-drop-down-item-with-sub.html'
            else:
                return 'menu/bootstrap-drop-down-item.html'


class BootstrapPillMenu(Menu):
    __abstract__ = True
    __item_class__ = BootstrapMenuItem

    nav_type = 'pills'
    stacked = False

    has_js = True

    def __init__(self, context):
        Menu.__init__(self, context)
        self.js_files = ['twitter-bootstrap-js']

    def render_menu_start(self):
        classes = self.menu_classes[:]
        classes.insert(0, 'nav')
        classes.insert(1, 'nav-' + self.nav_type)

        if self.stacked:
            classes.insert(2, 'nav-stacked')

        return '<ul class="%s">' % ' '.join(classes)

    def render_js(self):
        op = render_to_string('menu/bootstrap-dropdown.js', {
            'menu_id': self.menu_id,
            'ajax': self.ajax
        })
        return op
