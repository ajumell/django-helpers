# coding=utf-8

from bootstrap import BootstrapPillMenu
from django_helpers.apps.static_manager import register_js
from menu import Menu, MenuItem

__author__ = 'ajumell'

register_js('superfish-js', 'js/superfish.min.js', ('jquery',))

# from django.conf import settings
#
# if 'django_helpers.apps.menu' in settings.INSTALLED_APPS:
#     from django_helpers.utils.autodiscover import autodiscover
#
# autodiscover('menu', 'navigation')
