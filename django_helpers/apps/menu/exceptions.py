# coding=utf-8
__author__ = 'ajumell'


class MenuIDNotFoundException(Exception):
    pass


class InvalidContextException(Exception):
    pass


class MultipleMenuWithSameIDException(Exception):
    pass


class MenuItemClassNotProvided(Exception):
    pass
