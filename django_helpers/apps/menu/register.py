# coding=utf-8
from django_helpers.utils.importlib import import_module

from exceptions import MultipleMenuWithSameIDException

_REGISTERED_MENU = {}


def register_name(class_name, name):
    if name is None:
        return

    if name in _REGISTERED_MENU:
        old_name = _REGISTERED_MENU[name]

        print name
        print old_name

        msg = "You tried to register a menu with id " + name + " for " + class_name + ".\n"
        msg += "But " + name + " is already registered for " + old_name + ".\n"
        msg += "Try giving a new ID for " + class_name

        raise MultipleMenuWithSameIDException(msg)

    _REGISTERED_MENU[name] = class_name


def has_menu(name):
    return name in _REGISTERED_MENU


def get_menu(name):
    obj = _REGISTERED_MENU.get(name)

    if isinstance(obj, str):
        i = obj.rfind('.')
        module_name = obj[:i]
        name = obj[i + 1:]
        module_obj = import_module(module_name)
        obj = getattr(module_obj, name)

    return obj
