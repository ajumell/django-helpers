# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

import django_helpers.apps.preferences.models


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Preference',
            fields=[
                ('user', models.IntegerField(default=0, serialize=False, primary_key=True)),
                ('preference', django_helpers.apps.preferences.models.PreferenceField(default=b'', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
