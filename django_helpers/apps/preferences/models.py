# coding=utf-8
import json

from django.db import models
from django.utils import six

__author__ = 'ajumell'


class PreferenceField(six.with_metaclass(models.SubfieldBase, models.TextField)):
    def __init__(self, *args, **kwargs):
        kwargs['default'] = ''
        kwargs['blank'] = True

        super(PreferenceField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            return {}
        obj = json.loads(value)
        return obj

    def value_to_string(self, obj):
        value = json.dumps(obj)
        return value

    def get_db_prep_save(self, value, connection):
        value = self.value_to_string(value)
        super(PreferenceField, self).get_db_prep_save(value, connection)
        return value

    def pre_save(self, model_instance, add):
        return super(PreferenceField, self).pre_save(model_instance, add)


class Preference(models.Model):
    user = models.IntegerField(default=0, primary_key=True)
    preference = PreferenceField()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        pref = self.preference
        if not isinstance(pref, dict):
            print type(pref), "-", pref, "-"
            raise Exception("Preference object should be a dictionary.")

        for key, value in pref.items():
            if value is None:
                pref.pop(key)

        models.Model.save(self, force_insert, force_update, using, update_fields)


class PreferenceModel(models.Model):
    USER_PREFERENCE = False

    # noinspection PyProtectedMember
    def get_fields(self):
        cls = self.__class__
        # Skip proxies, but keep the origin as the proxy model.
        if cls._meta.proxy:
            cls = cls._meta.concrete_model
        meta = cls._meta
        fields = meta.fields

        name = meta.model_name

        # Pop Primary Key
        return name, fields[1:]

    def __init__(self, request):
        from preference import get_preference

        preference = get_preference(request)
        user = self.USER_PREFERENCE
        self.preference = preference

        prefix, fields = self.get_fields()
        new_kwargs = {}
        for field in fields:
            name = field.attname
            value = preference.get(prefix + name, user=user)

            if value is not None:
                new_kwargs[name] = value

        super(PreferenceModel, self).__init__(**new_kwargs)

    # noinspection PyProtectedMember
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        user = self.USER_PREFERENCE
        pref = self.preference

        prefix, fields = self.get_fields()
        for field in fields:
            name = field.attname
            val = getattr(self, name, None)
            val = field.get_db_prep_value(val, using, False)
            pref.set(prefix + name, val, user=user)

        pref.commit()

    class Meta:
        abstract = True


class UserPreferenceModel(PreferenceModel):
    USER_PREFERENCE = True

    class Meta:
        abstract = True
