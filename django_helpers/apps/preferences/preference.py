# coding=utf-8
__author__ = 'ajumell'


class Preference(object):
    def __init__(self, request=None):
        self.request = request
        self.loaded = False
        self.instance = None
        self.user_instance = None
        self.pref = {}
        self.user_pref = {}
        self.preference_updated = False
        self.user_preference_updated = False

    def get_instance(self):
        self.load()
        return self.instance

    def load(self):
        from models import Preference as Model

        if self.loaded:
            return

        self.loaded = True

        instance = Model.objects.filter(user=0)
        if len(instance) > 0:
            instance = instance[0]
        else:
            instance = Model()

        self.instance = instance
        self.pref = instance.preference

        request = self.request

        if request is None:
            return

        user = request.user
        if not user.is_authenticated():
            return

        user_instance = Model.objects.filter(user=user.id)
        if len(user_instance) > 0:
            user_instance = user_instance[0]
        else:
            user_instance = Model()
            user_instance.user = self.request.user

        self.user_instance = user_instance
        self.user_pref = user_instance.preference

    def get(self, name, default=None, user=True):
        self.load()

        if user and name in self.user_pref:
            return self.user_pref.get(name)

        if name in self.pref:
            return self.pref.get(name)

        return default

    def set(self, name, value, commit=False, user=True):
        self.load()

        if user is True:
            self.user_pref[name] = value
            self.user_preference_updated = True
        else:
            self.pref[name] = value
            self.preference_updated = True
        if commit:
            self.commit()

    def commit(self):
        if self.preference_updated:
            self.instance.save()

        if self.user_preference_updated:
            self.user_instance.save()


def get_preference(request):
    pref = getattr(request, 'preference', None)
    if pref is None:
        pref = Preference(request)
        setattr(request, 'preference', pref)
    return pref
