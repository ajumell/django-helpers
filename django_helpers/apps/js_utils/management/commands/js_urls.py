# coding=utf-8
import json
import re
import sys

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.management.base import BaseCommand
from django.core.urlresolvers import RegexURLPattern, RegexURLResolver
from django.utils.datastructures import SortedDict

RE_KWARG = re.compile(r"(\(\?P<(.*?)>.*?\))")  # Pattern for recognizing named parameters in urls
RE_ARG = re.compile(r"(\(.*?\))")  # Pattern for recognizing unnamed url parameters


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        Create urls.js file by parsing all of the urlpatterns in the root urls.py file
        """
        try:
            file_path = getattr(settings, 'URLS_JS_GENERATED_FILE')
        except:
            raise ImproperlyConfigured('You should provide URLS_JS_GENERATED_FILE setting.')

        js_patterns = SortedDict()
        print "Generating Javascript urls file %s" % file_path
        Command.handle_url_module(js_patterns, settings.ROOT_URLCONF)
        # output to the file
        urls_file = open(file_path, "w")
        urls_file.write("dutils.conf.urls = ")
        json.dump(js_patterns, urls_file)
        print "Done generating Javascript urls file %s" % file_path

    @staticmethod
    def handle_url_module(js_patterns, module_name, prefix=""):
        """
        Load the module and output all of the patterns
        Recurse on the included modules
        """
        if isinstance(module_name, basestring):
            __import__(module_name)
            root_urls = sys.modules[module_name]
            patterns = root_urls.urlpatterns

        else:
            root_urls = module_name
            if hasattr(root_urls, 'urlpatterns'):
                patterns = root_urls.urlpatterns
            else:
                patterns = root_urls

        for pattern in patterns:
            if issubclass(pattern.__class__, RegexURLPattern):
                if pattern.name:
                    full_url = prefix + pattern.regex.pattern
                    for c in ["^", "$"]:
                        full_url = full_url.replace(c, "")
                    # handle kwargs, args
                    kwarg_matches = RE_KWARG.findall(full_url)
                    if kwarg_matches:
                        for el in kwarg_matches:
                            # prepare the output for JS resolver
                            full_url = full_url.replace(el[0], "<%s>" % el[1])
                    # after processing all kwargs try args
                    args_matches = RE_ARG.findall(full_url)
                    if args_matches:
                        for el in args_matches:
                            full_url = full_url.replace(el, "<>")  # replace by a empty parameter name
                    js_patterns[pattern.name] = "/" + full_url

            elif issubclass(pattern.__class__, RegexURLResolver):
                if pattern.urlconf_name:
                    Command.handle_url_module(js_patterns, pattern.urlconf_name, prefix=pattern.regex.pattern)
