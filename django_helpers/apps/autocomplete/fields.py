# coding=utf-8
from django import forms
from django_helpers.apps.autocomplete.widgets import AutoCompleteWidget
from django_helpers.apps.autocomplete.widgets import SimpleAutoCompleteWidget
from django_helpers.apps.autocomplete.widgets import SimpleTypeAheadWidget
from django_helpers.apps.autocomplete.widgets import TokenInputWidget
from django_helpers.apps.autocomplete.widgets import TypeAheadWidget


class AutoCompleteFieldBase(forms.Field):
    def set_url_params(self, url_args=None, url_kwargs=None):
        self.widget.url_kwargs = url_kwargs
        self.widget.url_args = url_args
    
    def __init__(self, lookup, required=True, widget=None, label=None, initial=None,
                 help_text=None, error_messages=None, show_hidden_initial=False,
                 validators=None, localize=False, autocomplete_params=None, widget_parms=None):
        
        if validators is None:
            validators = []
        
        if widget is None:
            widget = self.widget
        is_type = isinstance(widget, type)
        if is_type:
            if widget != self.widget and not issubclass(widget, self.widget):
                raise Exception('Invalid widget class.')
        else:
            if not isinstance(widget, self.widget):
                raise Exception('Invalid widget class.')
        
        if is_type:
            if widget_parms is None:
                widget_parms = {}
            
            widget_parms['lookup'] = lookup
            widget_parms['autocomplete_params'] = autocomplete_params
            widget = self.widget(**widget_parms)
        
        forms.Field.__init__(self, required, widget, label, initial,
                             help_text, error_messages, show_hidden_initial,
                             validators, localize)
        if isinstance(lookup, type):
            lookup = lookup()
        self.auto_complete = lookup


class AutoCompleteField(AutoCompleteFieldBase):
    widget = AutoCompleteWidget
    
    def to_python(self, value):
        auto_complete = self.auto_complete
        try:
            instance = auto_complete.get_instance(value)
            return instance
        except:
            return None
    
    def clean(self, value):
        value = forms.Field.clean(self, value)
        auto_complete = self.auto_complete
        try:
            if not value:
                raise
            self.widget.formatted_value = auto_complete.format_value(value)
            return value
        except Exception:
            self.widget.formatted_value = ""
            if self.required:
                raise forms.ValidationError(self.error_messages['required'])
            return None


class TypeAheadField(AutoCompleteField):
    widget = TypeAheadWidget


class SimpleAutoCompleteField(AutoCompleteField):
    widget = SimpleAutoCompleteWidget
    
    def clean(self, value):
        value = forms.Field.clean(self, value)
        self.widget.formatted_value = value
        return value


class SimpleTypeAheadField(TypeAheadField):
    widget = SimpleTypeAheadWidget
    
    def clean(self, value):
        value = forms.Field.clean(self, value)
        self.widget.formatted_value = value
        return value


class ManyToManyAutoCompleteField(AutoCompleteFieldBase):
    widget = TokenInputWidget
    
    def to_python(self, value):
        try:
            value = value.split(',')
            auto_complete = self.auto_complete
            instances = auto_complete.get_instances(value)
            self.widget.instances = instances
            return instances
        except:
            return None
    
    def clean(self, value):
        value = forms.Field.clean(self, value)
        try:
            if not value:
                raise
            return value
        except Exception:
            self.widget.formatted_value = ""
            if self.required:
                raise forms.ValidationError(self.error_messages['required'])
            return []
