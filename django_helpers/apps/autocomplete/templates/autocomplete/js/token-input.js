{% if array_field %}
$("[" + "name=" + "{{ name }}".replace("[]", "\\[\\]]"), elem).tokenInput("{{ source }}", {
    {% if hint_text %}hintText : "{{ hint_text }}", {% endif %}
    {% if no_result_text %}noResultsText : "{{ no_result_text }}", {% endif %}
    {% if searching_text %}searchingText : "{{ searching_text }}", {% endif %}
    {% if theme %}theme : "{{ theme }}", {% endif %}
    {% if delay %}searchDelay : {{ delay }}, {% endif %}
    {% if min_chars %}minChars : {{ min_chars }}, {% endif %}
    {% if limit %}tokenLimit : {{ limit }}, {% endif %}
    {% if can_add_new %}canAdd : true, {% endif %}
    {% if prevent_duplicates %}preventDuplicates : true, {% endif %}
    {% if search_param %}queryParam : "{{ search_param }}", {% endif %}
    form_params :{{ form_params|safe }},
    {% if values|length %}
    prePopulate : [
        {% for value in values %}
        {id : {{ value.id }}, label : "{{ value.name }}"}{% if not forloop.last %},
        {% endif %}
        {% endfor %}
    ],
    {% endif %}
    propertyToSearch : "{{ label_name }}"
});

{% else %}
jQuery(function ($) {
    var obj = $('#' + '{{ id }}');
    obj.tokenInput("{{ source }}", {
        {% if hint_text %}hintText : "{{ hint_text }}", {% endif %}
        {% if no_result_text %}noResultsText : "{{ no_result_text }}", {% endif %}
        {% if searching_text %}searchingText : "{{ searching_text }}", {% endif %}
        {% if theme %}theme : "{{ theme }}", {% endif %}
        {% if delay %}searchDelay : {{ delay }}, {% endif %}
        {% if min_chars %}minChars : {{ min_chars }}, {% endif %}
        {% if limit %}tokenLimit : {{ limit }}, {% endif %}
        {% if can_add_new %}canAdd : true, {% endif %}
        {% if prevent_duplicates %}preventDuplicates : true, {% endif %}
        {% if search_param %}queryParam : "{{ search_param }}", {% endif %}
        form_params :{{ form_params|safe }},
        {% if values|length %}
        prePopulate : [
            {% for value in values %}
            {id : {{ value.id }}, label : "{{ value.name }}"}{% if not forloop.last %},
            {% endif %}
            {% endfor %}
        ],
        {% endif %}
        propertyToSearch : "{{ label_name }}"
    });
});
{% endif %}
