# coding=utf-8
from django.forms import fields
from django.template.loader import render_to_string

from django_helpers.apps.forms.widgets import js_bool
from django_helpers.apps.static_manager import register_js, register_css
from django_helpers.js_plugins.core import GenericJavascriptPlugin

register_js('django-jquery-autocomplete-js', 'django-js-plugins/js/django.autocomplete.js', ("django-helpers-core-js",))

#
# Type A Head
#
register_css('jquery-typeahead-css', 'js-plugins/css/typeahead.css')
register_js('jquery-typeahead-js', 'js-plugins/js/typeahead.jquery.min.js', ('jquery', 'jquery-typeahead-css'))

#
# Token Input
#
register_js('jquery-tokeninput-js', 'js-plugins/js/jquery.tokeninput.js', ('jquery',))
register_css('jquery-tokeninput-css', 'js-plugins/css/token-input.css')
register_css('jquery-tokeninput-facebook-css', 'js-plugins/css/token-input-facebook.css')
register_css('jquery-tokeninput-mac-css', 'js-plugins/css/token-input-mac.css')


# TODO: Update css for jquery-ui


class AutoCompleteWidgetBase(fields.TextInput):
    def __init__(self, attrs=None, lookup=None,
                 url_args=None, url_kwargs=None, autocomplete_params=None, extra_fields=None):
        fields.TextInput.__init__(self, attrs)
        self.auto_complete = lookup()
        self.url_args = url_args
        self.url_kwargs = url_kwargs
        self.html_id = None
        self.extra_fields = extra_fields
        
        # With this arrays also can be passed as
        # Auto complete params.
        if type(autocomplete_params) in (list, tuple):
            new_params = {}
            for param in autocomplete_params:
                new_params[param] = param
            autocomplete_params = new_params
        
        self.autocomplete_params = autocomplete_params


# noinspection PyAttributeOutsideInit
class AutoCompleteWidget(AutoCompleteWidgetBase):
    formatted_value = None
    mode = 'autocomplete'
    is_simple = False
    template = 'autocomplete/js/auto-complete.js'
    
    js_files = (
        'jquery-ui-js',
        'django-jquery-autocomplete-js',
        # 'js/jquery-ui/jquery.ui.core.min.js',
        # 'js/jquery-ui/jquery.ui.widget.min.js',
        # 'js/jquery-ui/jquery.ui.position.min.js',
        # 'js/jquery-ui/jquery.ui.menu.min.js',
        # 'js/jquery-ui/jquery.ui.autocomplete.min.js',
    )
    
    css_files = (
        'jquery-ui-base-css',
    )
    
    def __init__(self, attrs=None, delay=0, min_length=1, lookup=None,
                 url_args=None, url_kwargs=None, autocomplete_params=None, extra_fields=None):
        AutoCompleteWidgetBase.__init__(self, attrs, lookup, url_args, url_kwargs, autocomplete_params, extra_fields)
        self.min_length = min_length
        self.delay = delay
    
    def get_formatted_value(self, value):
        if not value:
            return ""
        
        if self.is_simple:
            return value
        
        # Edit mode in model field.
        if self.formatted_value is None:
            auto_complete = self.auto_complete
            instance = auto_complete.get_instance(value)
            val = auto_complete.format_value(instance)
        else:
            val = self.formatted_value
        self.formatted_value = val
        self.value = value
        return val
    
    def render(self, name, value, attrs=None):
        if value is None:
            value = ""
        self.value = value
        
        self.input_type = "hidden"
        hidden = fields.TextInput.render(self, name, value)
        self.name = name
        self.input_type = "text"
        value = self.get_formatted_value(value)
        
        attrs = self.generate_attrs(attrs, value)
        print attrs
        display = fields.TextInput.render(self, name + "_display", value, attrs)
        return display + hidden
    
    def generate_attrs(self, attrs, value):
        data_attrs = self.generate_data_attrs(value)
        plugin = GenericJavascriptPlugin()
        plugin.datas(data_attrs)
        if not attrs:
            attrs = {}
        attrs.update(plugin.attrs)
        return attrs
    
    def generate_data_attrs(self, value):
        source = self.auto_complete.get_url(self.url_args, self.url_kwargs)
        data_attrs = {
            "mode": self.mode,
            "django-auto-complete": True,
            "min_length": self.min_length,
            "delay": self.delay,
            "source": source,
            "id": self.html_id,
            "name": self.name,
            "can_add_new": js_bool(self.auto_complete.can_add_new),
            "current-label": self.formatted_value,
            "current-value": value or "",
            "form_params": self.autocomplete_params or "undefined",
            # "array_field": array_field
        }
        
        if self.is_simple:
            data_attrs['is-simple'] = "true"
        
        return data_attrs
    
    def id_for_label(self, id_):
        return '%s_display' % id_


# noinspection PyAttributeOutsideInit
class TypeAheadWidget(AutoCompleteWidget):
    formatted_value = None
    mode = 'typeahead'
    template = 'autocomplete/js/type-a-head.js'
    
    js_files = (
        'jquery-typeahead-js',
        'django-jquery-autocomplete-js',
    )
    
    def __init__(self, attrs=None, min_length=1, highlight=True, lookup=None,
                 url_args=None, url_kwargs=None, autocomplete_params=None, extra_fields=None):
        AutoCompleteWidget.__init__(self, attrs, 0, min_length, lookup, url_args, url_kwargs, autocomplete_params, extra_fields)
        self.highlight = highlight


class SimpleAutoCompleteWidget(AutoCompleteWidget):
    is_simple = True


class SimpleTypeAheadWidget(TypeAheadWidget):
    is_simple = True


# noinspection PyAttributeOutsideInit
class TokenInputWidget(AutoCompleteWidgetBase):
    instances = None
    
    js_files = ['jquery-tokeninput-js']
    css_files = ('jquery-tokeninput-css',)
    
    def __init__(self, lookup, hint_text=None, no_result_text=None, searching_text=None, delay=0,
                 min_chars=1, limit=None, prevent_duplicates=True, theme='facebook', attrs=None,
                 url_args=None, url_kwargs=None, autocomplete_params=None):
        
        AutoCompleteWidgetBase.__init__(self, attrs, lookup, url_args, url_kwargs, autocomplete_params)
        
        self.hint_text = hint_text
        self.no_result_text = no_result_text
        self.searching_text = searching_text
        self.delay = delay
        self.min_chars = min_chars
        self.limit = limit
        self.prevent_duplicates = prevent_duplicates
        self.theme = theme
        
        self.css_files = list(self.css_files)
        self.css_files.append('jquery-tokeninput-%s-css' % theme)
    
    def render(self, name, value, attrs=None):
        self.name = name
        if self.instances is None:
            auto_complete = self.auto_complete
            self.instances = auto_complete.get_instances(value) if value is not None else []
        return fields.TextInput.render(self, name, value, attrs)
    
    def render_js(self, array_field=False, name_prefix=None):
        auto_complete = self.auto_complete
        source = auto_complete.get_url(self.url_args, self.url_kwargs)
        instances = self.instances
        formatted_values = []
        if instances:
            for instance in instances:
                formatted_values.append({
                    "name": auto_complete.format_value(instance),
                    "id": auto_complete.get_pk(instance)
                })
        
        op = render_to_string('autocomplete/js/token-input.js', {
            "hint_text": self.hint_text,
            "no_result_text": self.no_result_text,
            "searching_text": self.searching_text,
            "delay": self.delay,
            "min_chars": self.min_chars,
            "limit": self.limit,
            "theme": self.theme,
            "prevent_duplicates": self.prevent_duplicates,
            "search_param": self.auto_complete.search_param,
            "label_name": self.auto_complete.label_name,
            "can_add_new": self.auto_complete.can_add_new,
            "source": source,
            "id": self.html_id,
            "name": self.name,
            "values": formatted_values,
            "form_params": self.autocomplete_params or "undefined",
            "array_field": array_field
        })
        return op
