# coding=utf-8
from types import StringTypes

from django.core.urlresolvers import reverse
from django.db.models import Q


class AutoCompleteMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        from views import register_name as register

        obj = type.__new__(mcs, name, bases, attrs)
        if name != 'AutoComplete':
            module_name = attrs['__module__']
            class_name = module_name + '.' + name
            autocomplete_name = attrs.get('name', None)
            url_search_parameters = attrs.get('url_search_parameters', None)
            url_name = register(class_name, autocomplete_name, url_search_parameters)
            obj.url_name = url_name
        return obj


# noinspection PyMethodMayBeStatic
class AutoComplete(object):
    __metaclass__ = AutoCompleteMetaClass

    query_set = None

    search_fields = []

    # Parameters required in creating url.
    url_search_parameters = None

    requires_auth = False

    id_name = 'id'

    label_name = 'label'

    required_params = {}

    optional_params = {}

    url_name = None

    max_results = 10

    pk_field = 'pk'

    search_param = "term"

    extra_fields = None

    def __init__(self):
        self.can_add_new = self.save is not None

    # noinspection PyUnusedLocal
    def get_queryset(self, request):
        """
        This function can be overridden to return custom query set.
        @return: The query set for performing extra searches.
        """
        return self.query_set

    def search_term(self, queryset, term):
        """
        Performs a simple database search for the term given.
        This function will performs a __contains in the given
        fields of the query set.

        @param term: str A search term
        @return: django.db.models.query.QuerySet a result query set.
        """
        fields = self.search_fields
        if type(term) not in (list, tuple):
            terms = [term]
        else:
            terms = term

        queries = []
        for term in terms:
            sub_query = None
            for field in fields:
                kwargs = {field + "__contains": term}
                if sub_query is None:
                    sub_query = Q(**kwargs)
                else:
                    sub_query = sub_query | Q(**kwargs)

            queries.append(sub_query)

        if not len(queries):
            return queryset

        return queryset.filter(*queries)

    def search_url_paramters(self, query, url_params):
        """
        This function performs a query lookup with the parameters from the url.
        @param query:
        @param url_params:
        @return:
        """
        url_search_parameters = self.url_search_parameters
        if url_search_parameters is not None:
            for parameter in url_search_parameters:
                value = url_params.get(parameter, '')
                args_dict = {parameter: value}
                query = query.filter(**args_dict)
        return query

    def search_optional_params(self, query_set, data_dict):
        params = self.optional_params
        names = params.keys()
        kwargs = {}
        for name in names:
            if name in data_dict:
                field = params[name]
                value = data_dict[name]
                kwargs[field] = value
        return query_set.filter(**kwargs)

    def search_required_params(self, query_set, data_dict):
        params = self.required_params
        names = params.keys()
        kwargs = {}
        for name in names:
            field = params[name]
            value = data_dict[name]
            kwargs[field] = value
        return query_set.filter(**kwargs)

    def search(self, queryset, term, url_params):
        query = self.search_term(queryset, term)
        return self.search_url_paramters(query, url_params)

    def get_instances(self, ids):
        """
        Returns a queryset with the instances with given list of ids.
        This function is mainly used in many to many fields.

        @param ids: A list/enumerable of primary keys for which the datas has to be returned.
        @return: django.db.models.query.QuerySet
        """
        add_items = []
        filter_items = []
        if self.can_add_new:
            for instance_id in ids:
                if type(instance_id) in StringTypes:
                    if instance_id.startswith("__add__"):
                        instance_id = instance_id[7:]
                        add_items.append(instance_id)
                    else:
                        filter_items.append(instance_id)
                else:
                    filter_items.append(instance_id)
            for item in add_items:
                # noinspection PyNoneFunctionAssignment
                result = self.save(item)
                try:
                    filter_items.append(result.pk)
                except:
                    pass
        else:
            filter_items = ids
        queryset = self.get_queryset(None)
        query_set = queryset.filter(pk__in=filter_items)
        return query_set

    def get_instance(self, pk):
        """

        @param pk: The primary key for which the instance has to be returned.
        @return: A single instance of the current Model.
        """
        if type(pk) in StringTypes and pk.startswith("__add__"):
            pk = pk[7:]
            return self.save(pk)
        return self.get_queryset(None).get(pk=pk)

    def get_url(self, args=None, kwargs=None):
        c = 0
        if args is not None:
            c += len(args)

        if kwargs is not None:
            c += len(kwargs.keys())

        if self.url_search_parameters is not None and len(self.url_search_parameters) != c:
            raise Exception('Parameter required for generating URL is not sufficient.')

        return reverse(self.url_name, args=args, kwargs=kwargs)

    def simple_format(self, instance, string, *fields):
        lst = []
        for term in fields:
            lst.append(getattr(instance, term))
        return string % tuple(lst)

    def format_value(self, instance):
        """
        Returns the formatted values that represents the current instance.
        @param instance:
        @return:
        """
        return unicode(instance)

    def get_pk(self, instance):
        return getattr(instance, self.pk_field)

    def get_search_term(self, request):
        """
        @author Muhammed K K
        @param request: django.http.HttpRequest current django request
        @return: str The term to be searched.
        """
        term = request.GET.get(self.search_param)
        return term

    def check_required_params(self, data_dict):
        params = self.required_params.keys()
        for name in params:
            if name not in data_dict or not data_dict[name]:
                return False
        return True

    def get_field_value(self, instance, field_name):
        val = getattr(instance, field_name, None)
        return str(val)

    def get_dict(self, result):
        val = self.format_value(result)
        pk = self.get_pk(result)
        extra_fields = self.extra_fields

        d = {
            self.id_name: pk,
            self.label_name: val,
        }
        if extra_fields is None:
            return d

        for key, field in extra_fields.items():
            d[key] = self.get_field_value(result, field)

        return d

    def get_results(self, request, params):
        data_dict = request.GET
        if not self.check_required_params(data_dict):
            return []

        term = self.get_search_term(request)
        query = self.get_queryset(request)
        query = self.search(query, term, params)
        query = self.search_optional_params(query, data_dict)
        query = self.search_required_params(query, data_dict)

        # TODO: Implement only function
        query = query.distinct()
        query = query[:self.max_results]
        results = []
        for result in query:
            results.append(self.get_dict(result))
        return results

    save = None
