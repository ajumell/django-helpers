jQuery(function ($) {

    $.dj = $.dj || {};
    $.dj.typeahead = function (parameters) {
        var obj,
            hidden,
            source_url,
            selector,
            name,
            current_value,
            current_label,
            url,
            parent_elem,
            escaped_name,
            data_set_name,
            min_length,
            form_params,
            extra_fields,
            name_prefix,
            can_add;

        selector = parameters["selector"];
        parent_elem = parameters["elem"];
        data_set_name = parameters["name"].replace("[]", "");
        name = parameters["name"]
        escaped_name = name.replace("[", "\\[").replace("]", "\\]");
        current_value = parameters['current_value'];
        current_label = parameters['current_label'];
        url = parameters['url'];
        min_length = parameters['min_length'];
        form_params = parameters['form_params'];
        can_add = parameters['can_add'];
        extra_fields = parameters['extra_fields'];
        name_prefix = parameters['name_prefix'];

        if (parent_elem) {
            parent_elem = $(parent_elem);
        }

        if (parent_elem) {
            obj = $(selector, parent_elem);
        }
        else {
            obj = $(selector);
        }

        if (!obj.length) {
            return;
        }

        if (parent_elem) {
            hidden = $('input[type=hidden][name=' + escaped_name + ']', parent_elem);
        }
        else {
            hidden = $('input[type=hidden][name=' + escaped_name + ']');
        }

        if (!parent_elem) {
            // Make the form parent element for additional
            // processing like automatic extra field values.
            parent_elem = obj.parents('form');
        }

        source_url = function (request, response) {
            var param = { term : request },
                key,
                value,
                form = obj.parents('form'),
                prefix = form.attr('data-form-prefix');

            if (form_params !== undefined) {
                for (key in form_params) {
                    if (form_params.hasOwnProperty(key)) {
                        value = form_params[key];
                        param[key] = $('[name="' + prefix + value + '"]', form).val();
                    }
                }
            }
            $.ajax({
                url : url,
                data : param,
                dataType : "json",
                type : "GET",
                success : function (data) {
                    response(data);
                }
            });
        };

        obj.typeahead({
            minLength : min_length,
            hint : false
        }, {
            name : data_set_name,
            displayKey : "label",
            source : source_url
        });

        obj.on("typeahead:selected typeahead:autocompleted", function (ui, item) {
            var key, item_val;

            obj.val(item.label);
            hidden.val(item.id);

            current_label = item.label;
            current_value = item.id;

            if (extra_fields) {
                for (key in extra_fields) {
                    if (extra_fields.hasOwnProperty(key)) {
                        item_val = item[key];
                        if (name_prefix) {
                            key = name_prefix + "_" + key + '[]';
                            key = key.replace("[", "\\[").replace("]", "\\]");
                        }
                        parent_elem.find('[name=' + key + ']').val(item_val)
                    }
                }
            }

            return false;
        });

        /**
         @author Muhammed K K
         @date-created 9/30/14, 5:43 PM
         This function will remove the current selected value
         from the hidden field  if the value in the text box is
         changed by the user.
         TODO: Updated documentation
         */
        obj.blur(function () {
            var entered_val = obj.val().trim();
            if (current_value) {
                if (entered_val !== current_label) {
                    if (can_add && entered_val) {
                        hidden.val("__add__" + entered_val);
                    } else {
                        hidden.val('');
                        obj.val('');
                    }
                } else {
                    hidden.val(current_value);
                }
            } else if (can_add && entered_val) {
                hidden.val("__add__" + entered_val);
            } else {
                obj.val('');
            }
        });

        setTimeout(function () {
            var parent = obj.parent("span.twitter-typeahead");
            parent.css("display", obj.css("display"));
        }, 0);
    }

});