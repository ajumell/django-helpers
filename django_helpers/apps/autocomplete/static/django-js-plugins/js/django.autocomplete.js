jQuery(function ($) {
    var AUTOCOMPLETE = "autocomplete";
    var TYPEAHEAD = "typeahead";

    var dj,
        registerDataWidget,
        generateDataOptions;

    dj = $.dj;
    registerDataWidget = dj.registerDataWidget;
    generateDataOptions = dj.generateDataOptions;

    dj = $.dj;
    if (!dj) {
        return;
    }

    registerDataWidget('django-auto-complete', function (jqElement) {
        var fnSelected;
        /**
         @author Muhammed K K
         @date-created 23/5/12, 10:24 PM
         This function will remove the current selected value
         from the hidden field  if the value in the text box is
         changed by the user.
         TODO: Updated documentation
         */
        var fnBlurred;
        var mappings, options;

        var jqHiddenInput,
            fnSource,
            sCurrentValue,
            sCurrentLabel,
            sUrl,
            min_length,
            delay,
            form_params,
            extra_fields,
            name_prefix,
            jqParentElement,
            jqFormElement,
            isSimple,
            sMode,
            can_add;

        mappings = {
            'currentValue': 'current_value',
            'currentLabel': 'current_label',

            'source': 'url',

            'minLength': 'min_length',
            'delay': 'delay',
            'canAddNew': 'can_add',
            'isSimple': 'isSimple',

            'formParams': 'form_params',
            'extraFields': 'extra_fields',
            'namePrefix': 'name_prefix',
            'mode': 'mode'
        };

        options = generateDataOptions(jqElement, mappings);

        sMode = options["mode"];

        sCurrentValue = options['current_value'];
        sCurrentLabel = options['current_label'];

        sUrl = options['url'];

        min_length = options['min_length'];
        delay = options['delay'];
        can_add = options['can_add'];
        isSimple = options['isSimple']

        form_params = options['form_params'];
        extra_fields = options['extra_fields'];
        name_prefix = options['name_prefix'];

        jqFormElement = jqElement.parents("form");
        if (jqParentElement) {
            jqParentElement = jqFormElement;
        }

        jqHiddenInput = jqElement.siblings("input[type=hidden]");

        fnSource = function (request, response) {
            var param,
                key,
                value,
                form = jqElement.parents('form'),
                prefix = form.attr('data-form-prefix');
            if (sMode == AUTOCOMPLETE) {
                //noinspection JSUnresolvedVariable
                param = {term: request.term};
            } else if (sMode == TYPEAHEAD) {
                param = {term: request};
            }

            if (form_params !== undefined) {
                for (key in form_params) {
                    if (form_params.hasOwnProperty(key)) {
                        value = form_params[key];
                        param[key] = $('[name="' + prefix + value + '"]', form).val();
                    }
                }
            }

            $.ajax({
                url: sUrl,
                data: param,
                dataType: "json",
                type: "GET",
                success: function (data) {
                    response(data);
                }
            });
        };

        fnBlurred = function () {
            var entered_val = jqElement.val().trim();
            if (sCurrentValue) {
                if (entered_val !== sCurrentLabel) {
                    if (can_add && entered_val) {
                        jqHiddenInput.val("__add__" + entered_val);
                    } else {
                        jqHiddenInput.val('');
                        jqElement.val('');
                    }
                } else {
                    jqHiddenInput.val(sCurrentValue);
                }
            } else if (can_add && entered_val) {
                jqHiddenInput.val("__add__" + entered_val);
            } else {
                jqElement.val('');
            }
        };

        fnSelected = function (ui, item) {
            var key, item_val;

            if (sMode == AUTOCOMPLETE) {
                item = item.item;
            }

            if (isSimple) {
                sCurrentValue = item.label;
                jqElement.val(item.label);
                jqHiddenInput.val(item.label);
            } else {
                jqElement.val(item.label);
                jqHiddenInput.val(item.id);

                sCurrentValue = item.id;
            }
            sCurrentLabel = item.label;


            if (extra_fields) {
                for (key in extra_fields) {
                    if (extra_fields.hasOwnProperty(key)) {
                        item_val = item[key];
                        if (name_prefix) {
                            key = name_prefix + "_" + key + '[]';
                            key = key.replace("[", "\\[").replace("]", "\\]");
                        }
                        jqParentElement.find('[name=' + key + ']').val(item_val)
                    }
                }
            }

            return false;
        };

        if (sMode === AUTOCOMPLETE) {
            //noinspection JSValidateTypes
            jqElement.autocomplete({
                minLength: min_length,
                source: fnSource,
                delay: delay,
                select: fnSelected
            });

            jqElement.blur(fnBlurred);

        } else if (sMode == TYPEAHEAD) {
            //noinspection JSUnresolvedFunction,JSUnresolvedFunction
            jqElement.typeahead({
                minLength: min_length,
                hint: false
            }, {
                name: jqElement.attr("name"),
                displayKey: "label",
                source: fnSource
            });

            jqElement.on("typeahead:selected typeahead:autocompleted", fnSelected);
            jqElement.blur(fnBlurred);
            setTimeout(function () {
                var parent = jqElement.parent("span.twitter-typeahead");
                parent.css("display", jqElement.css("display"));
            }, 0);
        }

    });
});