# coding=utf-8
from django_helpers.utils.importlib import import_module

from json import dumps
from django.http import HttpResponse
from django.conf.urls import url
from django_helpers import add_app_url
import urls

__author__ = 'ajumell'

_REGISTRY = dict()

url_prefix = 'auto-complete-'
add_app_url('auto-complete', urls)


def get_autocomplete(name):
    if name not in _REGISTRY:
        raise Exception('Auto complete is not registered.')

    obj = _REGISTRY.get(name)

    if isinstance(obj, str):
        i = obj.rfind('.')
        module_name = obj[:i]
        name = obj[i + 1:]
        module_obj = import_module(module_name)
        obj = getattr(module_obj, name)

    return obj()


def autocomplete_lookup(request, lookup, **kwargs):
    """

    @param request: django.http.HttpRequest
    @param lookup: name of the lookup
    @param kwargs: extra url parameters
    @return: django.http.HttpResponse
    """
    # If lookup does not exists then return blank array.
    # This is prevent any exceptions at runtime.
    if lookup not in _REGISTRY:
        return HttpResponse("[]")

    auto_complete = get_autocomplete(lookup)

    results = auto_complete.get_results(request, kwargs)
    return HttpResponse(dumps(results))


def create_reg(name):
    return "(?P<%s>.*)/" % name


def register_name(class_name, name, url_search_parameters):
    if not name.startswith(url_prefix):
        name = url_prefix + name

    if name in _REGISTRY:
        old_name = _REGISTRY[name]

        msg = "You tried to register a auto complete with id " + name + " for " + class_name + ".\n"
        msg += "But " + name + " is already registered for " + old_name + ".\n"
        msg += "Try giving a new ID for " + class_name

        raise Exception(msg)

    _REGISTRY[name] = class_name

    reg = "%s/" % name
    extra_url_parameters = url_search_parameters
    if extra_url_parameters is not None:
        for parameter in extra_url_parameters:
            reg += create_reg(parameter)
    reg += "$"

    pattern = url(r"%s" % reg, autocomplete_lookup, name=name, kwargs={
        "lookup": name
    })
    urls.urlpatterns.append(pattern)
    return name
