# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('advanced_auth', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loginhistory',
            name='session',
            field=models.ForeignKey(blank=True, to='sessions.Session', null=True),
        ),
    ]
