# coding=utf-8
from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.db.models import get_model


class MultiModelAuthBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        user = ModelBackend.authenticate(self, username, password, **kwargs)

        if user is None:
            return user

        new_user = self.get_user(user.id, False)
        return new_user or user

    def get_user_base(self, user_id, force):
        if force is False:
            return None
        return ModelBackend.get_user(self, user_id)

    def get_user(self, user_id, force=True):
        models = getattr(settings, 'AUTH_USER_MODELS', None)
        if models is None:
            return self.get_user_base(user_id, force)

        if not isinstance(models, (tuple, list)):
            raise Exception('DO SOMETHING !!!!')

        if models is None:
            return self.get_user_base(user_id, force)

        for model in models:
            i = model.find('.')
            app_label = model[:i]
            model_name = model[i + 1:]
            model_class = get_model(app_label, model_name)
            try:
                return model_class.objects.get(pk=user_id)
            except model_class.DoesNotExist:
                pass

        return self.get_user_base(user_id, force)
