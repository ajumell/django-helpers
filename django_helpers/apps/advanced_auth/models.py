# coding=utf-8
from django.contrib.sessions.models import Session
from django.db import models
from django.utils.datetime_safe import datetime

from django_helpers import get_ip_address_from_request, get_settings_val


class LoginHistoryManager(models.Manager):
    @property
    def active_sessions(self):
        return self.filter(session__expire_date__gte=datetime.now())

    @property
    def expired_sessions(self):
        return self.filter(session__expire_date__lt=datetime.now())

    def get_history(self, user):
        return self.active_sessions.filter(user=user)


class LoginHistory(models.Model):
    user = models.ForeignKey(get_settings_val('AUTH_USER_MODEL'))
    timestamp = models.DateTimeField(auto_now_add=True)
    ip_address = models.GenericIPAddressField(null=True)
    session = models.ForeignKey(Session, blank=True, null=True)
    user_agent = models.CharField(max_length=255, blank=True)

    objects = LoginHistoryManager()

    def is_active(self):
        return self.session.expire_date < datetime.now()

    def logout(self):
        self.session.expire_date = datetime.today()
        self.session.save()


# Connect signal
from django.dispatch import receiver
from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import pre_delete


@receiver(pre_delete, sender=Session)
def session_end_handler(sender, **kwargs):
    session = kwargs.get('instance')
    LoginHistory.objects.filter(session_id=session.pk).update(session=None)


# noinspection PyProtectedMember,PyUnusedLocal
@receiver(user_logged_in)
def save_login_history(request, user, **kwargs):
    session_store = request.session
    session_store.save()
    session_key = session_store._get_or_create_session_key()
    session = Session.objects.get(session_key=session_key)

    history = LoginHistory()
    history.user = user
    history.ip_address = get_ip_address_from_request(request)
    history.session = session
    try:
        history.user_agent = request.META['HTTP_USER_AGENT']
    except:
        pass

    history.save()
