# coding=utf-8
from django_helpers.apps.data_tables import DataTableColumn, DataTable, DateDataTableColumn
from models import LoginHistory


class LoginHistoryDataTable(DataTable):
    table_id = 'advanced-admin-login-history'
    url_search_parameters = ['user_id']

    def get_query(self, request, kwargs=None):
        return LoginHistory.objects.filter(user__id=kwargs.get('user_id'))

    def get_columns(self):
        columns = []

        username_column = DataTableColumn('user__username', 'Username', True, True)
        email_column = DataTableColumn('user__email', 'Email', True, True)
        time_stamp = DateDataTableColumn('timestamp', 'Time', False, True)
        ip_column = DataTableColumn('ip_address', 'IP Address', False, True)

        columns.append(username_column)
        columns.append(email_column)
        columns.append(time_stamp)
        columns.append(ip_column)

        return columns
