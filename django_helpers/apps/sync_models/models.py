# coding=utf-8
from django.db import models
from django.db.models.signals import pre_save
from django.utils.timezone import now

import settings

__author__ = 'ajumell'

__all__ = ['register']

MODEL_REGISTER = []


def setup_server_id(model):
    meta = getattr(model, '_meta')
    pk_field = meta.pk


def server_signal(sender, instance, **kwargs):
    instance.server_time = now()


def client_signal(sender, instance, **kwargs):
    instance.is_dirty = True
    if not instance.pk:
        instance.server_time = None


def register(model):
    if not settings.IS_CLIENT or not settings.IS_SERVER:
        return model

    meta = getattr(model, "_meta")

    app_label = meta.app_label
    model_name = meta.model_names
    model_name = app_label + '.' + model_name
    if model_name not in MODEL_REGISTER:
        MODEL_REGISTER.append(model_name)

    #
    # Create Fields
    #

    server_time = models.DateTimeField(null=True)
    model.add_to_class('server_time', server_time)

    if settings.IS_CLIENT:
        setup_server_id(model)
        pre_save.connect(client_signal, model)

        is_dirty = models.BooleanField(default=True)
        model.add_to_class('is_dirty', is_dirty)

    if settings.IS_SERVER:
        pre_save.connect(server_signal, model)

    return model
