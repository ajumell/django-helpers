# coding=utf-8
from django.conf import settings

IS_CLIENT = getattr(settings, 'OFFLINE_SYNC_CLIENT', False)
IS_SERVER = getattr(settings, 'OFFLINE_SYNC_SERVER', False)
