# coding=utf-8

from django.forms.utils import flatatt
from django.middleware import csrf
from django.utils.html import format_html

#
# Mixins
#

# noinspection PyUnresolvedReferences
from django_helpers.js_plugins.core import AjaxLinkPlugin, ModalLinkPlugin, ConfirmPlugin


class BlockHTMLMixin(object):
    def add_heading_one(self, text):
        heading = HeadingOne(text)
        self.add(heading)

        return heading

    def add_heading_two(self, text):
        heading = HeadingTwo(text)
        self.add(heading)

        return heading

    def add_heading_three(self, text):
        heading = HeadingThree(text)
        self.add(heading)

        return heading

    def add_heading_four(self, text):
        heading = HeadingFour(text)
        self.add(heading)

        return heading

    def add_heading_five(self, text):
        heading = HeadingFive(text)
        self.add(heading)

        return heading

    def add_heading_six(self, text):
        heading = HeadingSix(text)
        self.add(heading)

        return heading

    def add_paragraph(self, text):
        paragraph = Paragraph(text)
        self.add(paragraph)

        return paragraph

    def add_header(self, text):
        header = Header(text)
        self.add(header)

        return header

    def add_footer(self, text):
        item = Footer(text)
        self.add(item)

        return item

    def add_div(self, text):
        div = Div(text)
        self.add(div)

        return div

    def add_section(self, text):
        section = Section(text)
        self.add(section)

        return section


class InlineHTMLMixin(object):
    pass


#
# Nodes
#

class HTMLNode(object):
    self_closing = False
    tag = ""
    __widget__ = True

    def __init__(self, text=None, parent=None, controller=None):
        self.controller = controller
        self.parent = parent
        self.children = []
        self.attrs = {}
        self.class_names = []
        self.js_plugins = []

        if text is not None:
            self.add_text(text)

        self.setup()

    def setup(self):
        pass

    #
    # Class Names
    #

    def get_class_names(self):
        return self.class_names

    def add_class(self, name):
        if name is None:
            return
        if name not in self.class_names:
            self.class_names.append(name)

    def remove_class(self, name):
        if name in self.class_names:
            self.class_names.remove(name)

    #
    # Render
    #

    def render(self):
        start = self.render_start()
        children = self.render_children()
        close = self.render_close()

        return start + children + close

    def render_start(self):
        attrs = self.prepare_attrs()
        self.clean_attrs(attrs)
        attrs = flatatt(attrs)
        if self.self_closing:
            string = u"<{0}{1} />"
        else:
            string = u"<{0}{1}>"

        html = format_html(string, self.tag, attrs)
        return html

    def render_close(self):
        return u"</{0}>".format(self.tag)

    def render_children(self):
        code = []
        for child in self.children:
            code.append(child.render())

        return "\n".join(code)

    def __unicode__(self):
        return self.render()

    def __str__(self):
        return self.render()

    #
    # Attributes
    #

    def remove_attr(self, name):
        if name in self.attrs:
            self.attrs.pop(name)

    def attr(self, name, value=None):
        if type(name) == dict:
            for k, v in name.items():
                self.attr(k, v)

            return

        if name == "class":
            if value is not None:
                return self.add_class(value)
            else:
                return self.get_class_names()

        if value is None:
            return self.attrs.get(name)
        else:
            self.attrs[name] = value

    def data(self, name, value=None):
        name = name.replace("_", "-")
        return self.attr('data-' + name, value)

    def datas(self, **kwargs):
        for name, value in kwargs.items():
            return self.attr('data-' + name, value)

    def prepare_attrs(self):
        attrs = {}
        attrs.update(self.attrs)

        for plugin in self.js_plugins:
            plugin.apply(attrs)

        class_names = self.get_class_names()
        if class_names:
            class_names = ' '.join(class_names)
            class_names = class_names.strip()
            while class_names.find('  ') > 0:
                class_names = class_names.replace('  ', '')

            attrs['class'] = class_names

        return attrs

    def clean_attrs(self, attrs):
        for key, val in attrs.items():
            if val is None:
                del attrs[key]
            elif val == "":
                del attrs[key]

    #
    # Javascript Plugins
    #
    def apply_plugin(self, plugin):
        plugin.apply(self.attrs)

    def add_plugin(self, plugin):
        self.js_plugins.append(plugin)

    #
    # Child Nodes
    #

    def add(self, node):
        if self.self_closing:
            raise Exception("Cannot add elements to self closing tags.")

        if not hasattr(node, '__widget__'):
            node = TextNode(node)

        node.set_parent(self)
        node.set_controller(self.controller)

        self.children.append(node)
        node.added(self, self.controller)

        return node

    def set_parent(self, parent):
        old = self.parent

        if old == parent:
            return

        self.parent = parent
        for child in self.children:
            child.set_parent(self)

        self.parent_changed(old, parent)

    def set_controller(self, controller):
        old = self.controller

        if old == controller:
            return

        self.controller = controller
        for child in self.children:
            child.set_controller(controller)

        self.controller_changed(old, controller)

    def add_text(self, text):
        self.children.append(TextNode(text))

    def set_text(self, text):
        children = self.children

        if len(children) == 0:
            return self.add_text(text)

        child = children[0]
        child.text = text

    #
    # Events
    #
    def added(self, parent, controller):
        pass

    def controller_changed(self, old_controller, new_controller):
        pass

    def parent_changed(self, old, new):
        pass

    #
    # Static Manager
    #
    def render_js(self):
        return ''


class TextNode(HTMLNode):
    def __init__(self, text=None, parent=None, controller=None):
        HTMLNode.__init__(self, None, parent, controller)
        self.text = text

    def render(self):
        return unicode(self.text)


class HeadingOne(HTMLNode):
    tag = "h1"


class HeadingTwo(HTMLNode):
    tag = "h2"


class HeadingThree(HTMLNode):
    tag = "h3"


class HeadingFour(HTMLNode):
    tag = "h4"


class HeadingFive(HTMLNode):
    tag = "h5"


class HeadingSix(HTMLNode):
    tag = "h6"


class Paragraph(HTMLNode):
    tag = "p"


class Span(HTMLNode):
    tag = "span"


class Div(HTMLNode):
    tag = "div"


class Form(HTMLNode):
    tag = "form"


class Section(HTMLNode):
    tag = "section"


class Header(HTMLNode):
    tag = "header"


class Footer(HTMLNode):
    tag = "footer"


class Link(HTMLNode):
    tag = 'a'

    def __init__(self, text, link='#', tooltip='', class_names='', args=None, kwargs=None):
        HTMLNode.__init__(self, text)
        self.link = link
        self.attr('title', tooltip)
        self.attr('class', class_names)
        self.args = args
        self.kwargs = kwargs

    def prepare_attrs(self):
        attrs = HTMLNode.prepare_attrs(self)
        if 'href' not in attrs:
            attrs['href'] = self.get_link()
        return attrs

    def get_link(self):
        from django.core.urlresolvers import reverse

        link = self.link
        if link == "#":
            return link

        if link.startswith("https://"):
            return link

        if link.startswith("http://"):
            return link

        if link.startswith("javascript:"):
            return link

        if link.startswith("javascript;"):
            return link

        args = self.args
        kwargs = self.kwargs
        if link.startswith("/") and args is None and kwargs is None:
            return link

        return reverse(self.link, None, args, kwargs)

    # noinspection PyAttributeOutsideInit
    def set_ajax(self, processing_text=None):
        current = getattr(self, 'ajax_processing_text', 'Loading...')
        if processing_text is None:
            processing_text = current

        ajax_obj = AjaxLinkPlugin()
        ajax_obj.loading = processing_text

        self.js_plugins.append(ajax_obj)

    def set_modal(self, loading_text='Loading...', title=None):
        modal = ModalLinkPlugin()
        modal.title = title
        modal.loading = loading_text

        self.js_plugins.append(modal)

    # noinspection PyAttributeOutsideInit
    def set_confirm(self, question, title=None, yes_text="Yes", no_text="No", yes_color="success", no_color=""):
        confirm = ConfirmPlugin()
        confirm.need_confirm = True

        confirm.question = question
        confirm.title = title

        confirm.yes_text = yes_text
        confirm.yes_color = yes_color

        confirm.no_text = no_text
        confirm.no_color = no_color

        self.js_plugins.append(confirm)


class Button(HTMLNode):
    tag = 'button'

    def __init__(self, text=None, parent=None, controller=None):
        HTMLNode.__init__(self, text, parent, controller)
        self.attr('type', 'button')


class Input(HTMLNode):
    tag = 'input'
    type = 'text'
    self_closing = True

    def __init__(self, value, name=None):
        HTMLNode.__init__(self)
        self.value = value
        self.name = name

    def prepare_attrs(self):
        attrs = HTMLNode.prepare_attrs(self)
        attrs['type'] = self.type
        attrs['value'] = self.value

        if self.name:
            attrs['name'] = self.name

        return attrs


class Label(HTMLNode):
    tag = 'label'


class HiddenInput(Input):
    type = 'hidden'


class CheckBox(Input):
    type = 'checkbox'


class SubmitButton(Input):
    type = 'submit'


class ResetButton(Input):
    type = 'reset'


# Lists


class Ul(HTMLNode):
    tag = 'ul'

    def __init__(self, parent=None, controller=None):
        HTMLNode.__init__(self, None, parent, controller)


class Ol(HTMLNode):
    tag = 'ul'

    def __init__(self, parent=None, controller=None):
        HTMLNode.__init__(self, None, parent, controller)


class Li(HTMLNode):
    tag = 'li'


# Image

class Image(HTMLNode):
    tag = 'img'
    self_closing = True

    def __init__(self, src):
        HTMLNode.__init__(self)
        self.src = src

    def prepare_attrs(self):
        attrs = HTMLNode.prepare_attrs(self)
        attrs['src'] = self.src

        return attrs


# Extras
class CSRFTokenInput(Input):
    type = 'hidden'

    def __init__(self, request):
        Input.__init__(self, '')
        self.request = request

    def render(self):
        self.value = csrf.get_token(self.request)
        self.attr('name', 'csrfmiddlewaretoken')
        return Input.render(self)
