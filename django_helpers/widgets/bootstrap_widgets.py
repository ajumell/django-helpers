# coding=utf-8
from html import Div, Link, HTMLNode, SubmitButton, ResetButton, BlockHTMLMixin, InlineHTMLMixin, Button, Span, Ul, Li


# noinspection PyUnresolvedReferences,PyClassHasNoInit
class BootstrapButtonMixin:
    def add_submit_button(self, text):
        button = BootstrapSubmitButton(text)
        self.add(button)
        return button

    def add_link_button(self, text, link):
        button = LinkButton(text, link)
        self.add(button)
        return button

    def add_reset_button(self, text):
        button = BootstrapResetButton(text)
        self.add(button)
        return button


class Container(Div):
    def setup(self):
        self.add_class('container')


class Row(Div):
    def setup(self):
        self.add_class('row')

    def add_col(self):
        col = Column()
        self.add(col)


class Column(Div, BootstrapButtonMixin, BlockHTMLMixin, InlineHTMLMixin):
    def __init__(self, text=None, parent=None, controller=None, **kwargs):
        Div.__init__(self, text, parent, controller)
        self.span_lg = kwargs.get('span_lg')
        self.span_md = kwargs.get('span_md', 12)
        self.span_sm = kwargs.get('span_sm')
        self.span_xs = kwargs.get('span_xs')

        self.push_lg = None
        self.push_md = None
        self.push_sm = None
        self.push_xs = None

        self.offset_lg = None
        self.offset_md = None
        self.offset_sm = None
        self.offset_xs = None

    def span(self, lg=None, md=None, sm=None, xs=None):
        if lg is not None:
            self.span_lg = lg

        if md is not None:
            self.span_md = md

        if sm is not None:
            self.span_sm = sm

        if xs is not None:
            self.span_xs = xs

    def push(self, lg=None, md=None, sm=None, xs=None):
        if lg is not None:
            self.push_lg = lg

        if md is not None:
            self.push_md = md

        if sm is not None:
            self.push_sm = sm

        if xs is not None:
            self.push_xs = xs

    def offset(self, lg=None, md=None, sm=None, xs=None):
        if lg is not None:
            self.offset_lg = lg

        if md is not None:
            self.offset_md = md

        if sm is not None:
            self.offset_sm = sm

        if xs is not None:
            self.offset_xs = xs

    def get_class_names(self):
        class_names = Div.get_class_names(self)
        class_names = class_names[:]

        if self.span_lg is not None:
            class_names.append('col-lg-{0}'.format(self.span_lg))

        if self.span_md is not None:
            class_names.append('col-md-{0}'.format(self.span_md))

        if self.span_sm is not None:
            class_names.append('col-sm-{0}'.format(self.span_sm))

        if self.span_xs is not None:
            class_names.append('col-xs-{0}'.format(self.span_xs))

        if self.push_lg is not None:
            class_names.append('col-lg-push-{0}'.format(self.push_lg))

        if self.push_md is not None:
            class_names.append('col-md-push-{0}'.format(self.push_md))

        if self.push_sm is not None:
            class_names.append('col-sm-push-{0}'.format(self.push_sm))

        if self.push_xs is not None:
            class_names.append('col-xs-push-{0}'.format(self.push_xs))

        if self.offset_lg is not None:
            class_names.append('col-lg-offset-{0}'.format(self.offset_lg))

        if self.offset_md is not None:
            class_names.append('col-md-offset-{0}'.format(self.offset_md))

        if self.offset_sm is not None:
            class_names.append('col-sm-offset-{0}'.format(self.offset_sm))

        if self.offset_xs is not None:
            class_names.append('col-xs-offset-{0}'.format(self.offset_xs))

        return class_names


class Icon(HTMLNode):
    tag = 'i'

    def __init__(self, icon):
        HTMLNode.__init__(self)
        self.icon = icon

    def render_children(self):
        return ""

    def get_class_names(self):
        class_names = HTMLNode.get_class_names(self)
        class_names.append('fa')
        class_names.append('fa-' + self.icon)
        return class_names


class LinkButton(Link):
    def __init__(self, text, link='#', color=None, size=None, tooltip='', class_names='', args=None, kwargs=None):
        Link.__init__(self, text, link, tooltip, class_names, args, kwargs)
        self.color = color
        self.size = size

    def get_class_names(self):
        class_names = Link.get_class_names(self)
        class_names.append('btn')
        values = [self.color, self.size]

        for value in values:
            if value is not None:
                class_names.append('btn-' + self.color)

        return class_names


class BootstrapSubmitButton(SubmitButton):
    def __init__(self, text, color=None, size=None):
        SubmitButton.__init__(self, text)
        self.color = color
        self.size = size

    def get_class_names(self):
        class_names = SubmitButton.get_class_names(self)
        class_names.append('btn')
        values = [self.color, self.size]

        for value in values:
            if value is not None:
                class_names.append('btn-' + value)

        return class_names


class BootstrapResetButton(ResetButton):
    def __init__(self, text, color=None, size=None):
        ResetButton.__init__(self, text, )
        self.color = color
        self.size = size

    def get_class_names(self):
        class_names = ResetButton.get_class_names(self)
        class_names.append('btn')
        values = [self.color, self.size]

        for value in values:
            if value is not None:
                class_names.append('btn-' + value)

        return class_names


class BootstrapButton(Button):
    def __init__(self, text, color=None, size=None):
        Button.__init__(self, text, )
        self.color = color
        self.size = size

    def get_class_names(self):
        class_names = Button.get_class_names(self)
        class_names.append('btn')
        values = [self.color, self.size]

        for value in values:
            if value is not None:
                class_names.append('btn-' + value)

        return class_names


class PanelHeader(Div):
    def setup(self):
        self.add_class('panel-heading')


class PanelBody(Div):
    def setup(self):
        self.add_class('panel-body')


class PanelFooter(Div):
    def setup(self):
        self.add_class('panel-footer')


class Panel(Div):
    footer = None
    header = None
    body = None

    def __init__(self, text=None, color=None, parent=None, controller=None):
        self.color = color
        self.title = text
        Div.__init__(self, None, parent, controller)

    def setup(self):
        header = PanelHeader(self.title, parent=self, controller=self.controller)
        body = PanelBody(parent=self, controller=self.controller)

        self.children.append(header)
        self.children.append(body)

        self.header = header
        self.body = body

    def get_class_names(self):
        class_names = Div.get_class_names(self)
        class_names.append('panel')
        values = [self.color]

        for value in values:
            if value is not None:
                class_names.append('panel-' + self.color)

        return class_names

    def add(self, node):
        return self.body.add(node)


class DropDownLinkButton(Div):
    def __init__(self, text, color='default', parent=None, controller=None):
        Div.__init__(self, None, parent, controller)
        self.add_class('btn-group')

        btn = Button(text)
        btn.add_class('btn')
        btn.add_class('btn-' + color)
        btn.add_class('dropdown-toggle')
        btn.data('toggle', 'dropdown')

        span = Span()
        span.add_class('caret')

        btn.add(span)
        Div.add(self, btn)

        ul = Ul()
        ul.add_class('dropdown-menu')
        Div.add(self, ul)

        self.ul = ul

    def add(self, node):
        li = Li()
        li.add(node)

        self.ul.add(li)


class BootstrapButtonSet(Div):
    def __init__(self, parent=None, controller=None):
        Div.__init__(self, None, parent, controller)
        self.add_class('btn-group')


class BootstrapVerticalButtonSet(Div):
    def __init__(self, parent=None, controller=None):
        Div.__init__(self, None, parent, controller)
        self.add_class('btn-group-vertical')


class IconLink(Link):
    def __init__(self, text=None, link='#', tooltip='', class_names='', args=None, kwargs=None):
        Link.__init__(self, text, link, tooltip, class_names, args, kwargs)


class EditIcon(IconLink):
    def setup(self):
        self.add(Icon('pencil-square-o'))


class DeleteIcon(IconLink):
    def setup(self):
        self.add(Icon('pencil-square-o'))
