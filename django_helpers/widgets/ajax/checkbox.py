# coding=utf-8

from django.conf.urls import url
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils import six

from django_helpers import add_app_url
from django_helpers.apps.static_manager import register_js
from django_helpers.utils.importlib import import_module
from widget import AJAXWidget

register_js('django-ajax-checkbox', 'widgets/ajax/checkbox.js', ('jquery',))

#
# View Part
#

urlpatterns = []

add_app_url('ajax-check-box', __name__)

_URL_PREFIX = 'ajax-check-box-'

_REGISTERED_CHECKBOXES = {}


def has_registered(name):
    return name in _REGISTERED_CHECKBOXES


def get(name):
    obj = _REGISTERED_CHECKBOXES.get(name)

    if isinstance(obj, str):
        i = obj.rfind('.')
        module_name = obj[:i]
        name = obj[i + 1:]
        module_obj = import_module(module_name)
        obj = getattr(module_obj, name)

    instance = obj()

    return instance


def checked(request, name, **kwargs):
    table = get(name)
    if table is None:
        return HttpResponse()

    results = table.checked(request, **kwargs)
    return results


def un_checked(request, name, **kwargs):
    table = get(name)
    if table is None:
        return HttpResponse()

    results = table.unchecked(request, **kwargs)
    return results


def create_reg(name):
    return "(?P<%s>.+?)/" % name


def register_name(class_name, name, url_search_parameters):
    if name is None:
        return

    if name in _REGISTERED_CHECKBOXES:
        old_name = _REGISTERED_CHECKBOXES[name]

        msg = "You tried to register a Widget with id " + name + " for " + class_name + ".\n"
        msg += "But " + name + " is already registered for " + old_name + ".\n"
        msg += "Try giving a new ID for " + class_name

        raise Exception(msg)

    _REGISTERED_CHECKBOXES[name] = class_name

    checked_reg = "^%s/checked/" % name
    un_checked_reg = "^%s/un-checked/" % name

    extra_url_parameters = url_search_parameters
    if extra_url_parameters is not None:
        for parameter in extra_url_parameters:
            checked_reg += create_reg(parameter)
            un_checked_reg += create_reg(parameter)

    checked_reg += "$"
    un_checked_reg += "$"

    checked_pattern = url(r"%s" % checked_reg, checked, name=name + '-checked', kwargs={
        "name": name
    })

    un_checked_pattern = url(r"%s" % un_checked_reg, un_checked, name=name + '-un-checked', kwargs={
        "name": name
    })

    urlpatterns.append(checked_pattern)
    urlpatterns.append(un_checked_pattern)


#
# Widget Part
#


class CheckboxMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        obj = type.__new__(mcs, name, bases, attrs)

        table_id = attrs.get('checkbox_id', None)
        params = attrs.get('url_search_parameters', None)

        if params is None:
            for b in bases:
                params = getattr(b, 'url_search_parameters', None)
                if params is not None:
                    break

        register_name(obj, table_id, params)
        return obj


class CheckBoxWidget(six.with_metaclass(CheckboxMetaClass, AJAXWidget)):
    checkbox_id = None

    def __init__(self):
        AJAXWidget.__init__(self)

        self.js_files = [
            'django-ajax-checkbox'
        ]

        self.has_checked = False
        self.value = None
        self.name = None

        self.checked_url = None
        self.un_checked_url = None

    def setup(self, name=None, value=None, has_checked=False):
        self.has_checked = has_checked
        self.value = value
        self.name = name

    def render(self):
        widget = self.get_widget()
        self.add_ajax_attrs(widget)

        return widget.render()

    def get_widget(self):
        from django_helpers.widgets import Div

        checkbox = self.get_checkbox_widget()

        div = Div()
        div.add(checkbox)
        div.add(self.name)

        return div

    def add_ajax_attrs(self, div):
        div.data('checked-url', self.get_checked_url())
        div.data('un-checked-url', self.get_un_checked_url())
        div.data('ajax-checkbox', True)

    def get_checkbox_widget(self):
        from django_helpers.widgets import CheckBox

        checkbox = CheckBox(self.value)

        if self.has_checked:
            checkbox.attr('checked', 'checked')

        return checkbox

    def can_set_params(self):
        return True

    def generate_render_data(self):
        pass

    def reset_params(self):
        self._widget_id = None
        self.url_kwargs = None

    def id(self):
        return self.checkbox_id

    #
    # URL Functions
    #
    def get_checked_url(self):
        if not self.checked_url:
            kwargs = self.url_kwargs
            self.checked_url = reverse(self.id() + "-checked", kwargs=kwargs)

        return self.checked_url

    def get_un_checked_url(self):
        if not self.un_checked_url:
            kwargs = self.url_kwargs
            self.un_checked_url = reverse(self.id() + "-un-checked", kwargs=kwargs)

        return self.un_checked_url

    #
    # View Functions
    #

    def checked(self, request, *args, **kwargs):
        raise NotImplementedError()

    def unchecked(self, request, *args, **kwargs):
        raise NotImplementedError()


# noinspection PyAbstractClass
class BootstrapCheckboxWidget(CheckBoxWidget):
    def get_widget(self):
        from django_helpers.widgets import Div, Label

        checkbox = self.get_checkbox_widget()

        div = Div()

        label = Label()
        label.add(checkbox)
        label.add(self.name)

        div.add(label)
        div.add_class('checkbox')

        container = Div()
        container.add_class('form-group')
        container.add(div)

        return container


class CheckboxCollection(list):
    def __init__(self):
        super(CheckboxCollection, self).__init__()

        self.js_files = [
            'django-ajax-checkbox'
        ]

    def add(self, checkbox):
        self.append(checkbox)
