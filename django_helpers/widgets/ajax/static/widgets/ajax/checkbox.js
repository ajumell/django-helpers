jQuery && jQuery(function ($) {

    var registerDataWidget;
    var generateDataOptions;
    var dj;

    dj = $.dj;
    registerDataWidget = dj.registerDataWidget;
    generateDataOptions = dj.generateDataOptions;

    registerDataWidget('ajax-checkbox', function (obj) {

        var options,
            checkbox,
            mappings,
            checkedUrl,
            unCheckedUrl;

        mappings = {
            "checkedUrl": "checkedUrl",
            "unCheckedUrl": "unCheckedUrl"
        };

        options = generateDataOptions(obj, mappings)

        checkedUrl = options['checkedUrl'];
        unCheckedUrl = options['unCheckedUrl'];

        checkbox = obj.find('input[type=checkbox]');

        checkbox.on('mousedown, change', function () {
            if (checkbox.is(":checked")) {
                $.ajax(checkedUrl);
            } else {
                $.ajax(unCheckedUrl);
            }
        });

    });


});