# coding=utf-8


# noinspection PyMethodMayBeStatic
from django.http import HttpResponse
from django.template import loader, RequestContext, Context
from django.utils.safestring import mark_safe

from django_helpers.widgets import Div


class AJAXWidgetMetaClass(type):
    def get_id(cls, attrs):
        return attrs.get('table_id', None)

    def register(cls, class_name, table_id, params):
        raise NotImplementedError()

    def __new__(mcs, name, bases, attrs):
        obj = type.__new__(mcs, name, bases, attrs)

        module_name = attrs['__module__']
        class_name = module_name + '.' + name
        table_id = mcs.get_id(attrs)
        params = attrs.get('url_search_parameters', None)
        if params is None:
            for b in bases:
                params = getattr(b, 'url_search_parameters', None)
                if params is not None:
                    break

        mcs.register(class_name, table_id, params)
        return obj


def sanitize_id(id_str):
    while '_' in id_str:
        id_str = id_str.replace("_", "-")

    while '--' in id_str:
        id_str = id_str.replace("--", "-")

    return id_str


def widget_id(params, name):
    if not params:
        return name
    id_str = "{}-{}"
    arr = [name]

    for k, v in params.items():
        k = str(k).replace("_", "-")
        v = str(v).replace("_", "-")
        arr.append(id_str.format(k, v))

    return sanitize_id('-'.join(arr))


class AJAXWidget(object):
    url_search_parameters = []

    def __init__(self, request=None):
        self.request = request
        self.url_kwargs = None

    #
    # Parameters
    #
    def can_set_params(self):
        """
        This function should raise an error
        if params cannot be set.
        """
        raise NotImplementedError()

    def reset_params(self):
        raise NotImplementedError()

    def set_params(self, *args, **kwargs):
        self.can_set_params()

        if len(args) > 0:
            url_params = self.url_search_parameters
            for value, param in zip(args, url_params):
                if param in kwargs:
                    raise Exception("Cannot set args")
                else:
                    kwargs[param] = value

        self.url_kwargs = kwargs

    def get_params(self):
        return self.url_kwargs

    #
    # ID
    #
    def id(self):
        raise NotImplementedError()

    @classmethod
    def client_id(cls, **params):
        return widget_id(params, cls.id)

    # noinspection PyAttributeOutsideInit
    def widget_id(self):
        _widget_id = getattr(self, '_widget_id', None)
        if _widget_id is None:
            kwargs = self.get_params()
            self_id = self.id()

            _widget_id = widget_id(kwargs, self_id)
            self._widget_id = _widget_id


        return _widget_id

    #
    # Rendering
    #
    def respond(self, string):
        return HttpResponse(string)

    def render_a_template(self, template_name, kwargs=None):
        template = loader.get_template(template_name)
        data = self.generate_render_data()
        request = self.request

        if request is not None:
            context = RequestContext(request, data)
        else:
            context = Context(data)
        return template.render(context)

    def safe_string(self, kwargs=None):
        if kwargs is not None:
            self.set_params(**kwargs)

        self.generate_render_data()
        return mark_safe(self.render())

    def __unicode__(self):
        return self.render()

    def generate_render_data(self):
        raise NotImplementedError()

    def render(self):
        raise NotImplementedError()


class Component(AJAXWidget):
    component_id = None

    html_widget = Div

    def __init__(self, request=None):
        AJAXWidget.__init__(self, request)
        self._widget = None

    def render(self):
        children = self.html()

        widget = self.get_widget()
        client_id = self.widget_id()
        widget.data('widget-id', client_id)

        start = widget.render_start()
        close = widget.render_close()

        return start + children + close

    def get_widget(self):
        if not self._widget:
            self._widget = self.html_widget()

        return self._widget

    def id(self):
        return self.component_id

    def reset_params(self):
        self.url_kwargs = {}

    def can_set_params(self):
        return True

    def generate_render_data(self):
        pass

    def html(self):
        raise NotImplementedError()
