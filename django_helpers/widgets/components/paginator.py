# coding=utf-8

from django_helpers.widgets import HTMLNode


class Paginator(HTMLNode):
    per_page = None
    page = 1
    total = None
