# coding=utf-8
from django.db import connection
from django.db.models.query_utils import DeferredAttribute

try:
    from django.db.models.fields.related import SingleRelatedObjectDescriptor
except ImportError:
    from django.db.models.fields.related_descriptors import ReverseOneToOneDescriptor as SingleRelatedObjectDescriptor

__author__ = 'ajumell'

try:
    import sqlparse
except ImportError:
    sqlparse = None


def query_count():
    return len(connection.queries)


def last_query():
    l = query_count() - 1
    query = connection.queries[l]['sql']
    if sqlparse is not None:
        query = sqlparse.format(query, reindent=True, keyword_case='upper')
    return query


# noinspection PyProtectedMember
def is_deferred(instance, field_name):
    model = instance._meta.model
    fields = model._meta.concrete_fields
    for field in fields:
        if field.name == field_name:
            obj = instance.__class__.__dict__.get(field.attname)
            return isinstance(obj, DeferredAttribute)
    return False


def get_non_deferred_value(instance, field_name, default=None):
    if is_deferred(instance, field_name):
        return default
    return getattr(instance, field_name)


def find_subclasses(model):
    # noinspection PyProtectedMember
    meta = model._meta
    subclasses = getattr(meta, 'model_subclasses', None)
    if subclasses is None:
        subclasses = [o for o in dir(model)
            if isinstance(getattr(model, o), SingleRelatedObjectDescriptor)
               and issubclass(getattr(model, o).related.model, model)]
        
        setattr(meta, 'model_subclasses', subclasses)
    
    return subclasses


def down_cast(instance, re_query=True):
    model = instance.__class__
    subclasses = find_subclasses(model)
    
    if re_query is True:
        # noinspection PyProtectedMember
        query = model._default_manager.filter(pk=instance.pk).select_related(*subclasses)
        obj = query[0]
    else:
        obj = instance
    
    obj = [getattr(obj, s) for s in subclasses if getattr(obj, s, None)] or [obj]
    return obj[0]


def down_cast_queryset(qs):
    model = qs.model
    subclasses = find_subclasses(model)
    new_qs = qs.select_related(*subclasses)
    new_qs.subclasses = subclasses
    return new_qs


# noinspection PyProtectedMember
def reload_instance(instance, *fields):
    model = instance.__class__
    meta = model._meta
    new_instance = model._default_manager.filter(pk=instance.pk)
    
    if len(fields) == 0:
        fields = meta.get_all_field_names()
    
    # for each field in Model
    for field in fields:
        try:
            # update this instances info from returned Model
            setattr(instance, field, getattr(new_instance, field))
        except:
            continue
    
    return instance


def get_single_value(instance, field):
    try:
        value = getattr(instance, field)
    except Exception:
        if field.find('__') > 0:
            fields = field.split('__')
        elif field.find('.') > 0:
            fields = field.split('.')
        else:
            raise
        value = instance
        for field in fields:
            value = getattr(value, field)
            if value is None:
                return None
    return value


def get_value(instance, field, format_expression=None, no_format=False):
    value = None
    
    if type(field) in (list, tuple):
        values = []
        for f in field:
            values.append(get_single_value(instance, f))
        
        if format_expression is None:
            if no_format is True:
                return values
            
            raise Exception("Format exception is required when multiple fields are involved.")
    else:
        value = get_single_value(instance, field)
        values = [value]
    
    if format_expression is not None:
        return format_expression.format(*values)
    
    return value


def get_values(instance, *fields):
    return get_value(instance, fields, None, True)


def get_model(*args, **kwargs):
    """
    Returns a model from the given name consistantly
    in old form or new form.
    
    :param args:
    :param kwargs:
    :return:
    """
    try:
        from django.db.models.loading import get_model as _get_model
    except ImportError:
        from django.apps import apps
        _get_model = apps.get_model
    
    return _get_model(*args, **kwargs)
