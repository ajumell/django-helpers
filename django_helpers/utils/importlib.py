# coding=utf-8

try:
    # Django versions < 1.9
    from django.utils.importlib import import_module
except ImportError:
    # Django versions >= 1.9
    from django.utils.module_loading import import_module
