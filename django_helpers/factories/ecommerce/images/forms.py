# coding=utf-8
from django import forms

from django_helpers.apps.form_renderer import FormRenderer


def get_image_form(product_model):
    from models import get_image_model
    image_model = get_image_model(product_model)

    class AddImageForm(forms.ModelForm):
        class Meta:
            model = image_model
            fields = (
                'image',
                'alt_text',
                'is_default',
            )

    return AddImageForm


class AddImageFormRenderer(FormRenderer):
    form_id = 'image-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='image')

        row = self.add_row()
        row.add_col(field='alt_text')

        row = self.add_row()
        row.add_col(field='is_default')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')


class EditImageFormRenderer(FormRenderer):
    form_id = 'edit-image-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='image')

        row = self.add_row()
        row.add_col(field='alt_text')

        row = self.add_row()
        row.add_col(field='is_default')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')
