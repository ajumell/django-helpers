# coding=utf-8
from django.db import models

from django_helpers.apps.forms.fields import ThumbnailImageField


#
# Getters
#
def get_image_model(product_model):
    return getattr(product_model, 'ImageModel', None)


#
# Setters
#
def set_image_model(product_model, image_model):
    setattr(product_model, 'ImageModel', image_model)


def images(product_model):
    class ProductImage(models.Model):
        __module__ = product_model.__module__

        product = models.ForeignKey(product_model)

        image = ThumbnailImageField(upload_to='product-images')
        alt_text = models.CharField(max_length=255)
        is_default = models.BooleanField(default=False)

        def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
            product = self.product

            if not product.default_image:
                self.is_default = True

            if self.is_default:
                product.default_image = self.image
                product.default_image_alt = self.alt_text
                product.save()

            models.Model.save(self, force_insert, force_update, using, update_fields)

    default_image = ThumbnailImageField(upload_to='product-images', null=True, blank=True, editable=False)
    default_image_alt = models.CharField(max_length=255, null=True, blank=True, editable=False)

    product_model.add_to_class("default_image", default_image)
    product_model.add_to_class("default_image_alt", default_image_alt)
    set_image_model(product_model, ProductImage)

    return product_model
