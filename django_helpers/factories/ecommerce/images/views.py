# coding=utf-8

from django.utils import six

from django_helpers.core.views import DeleteAction
from django_helpers.core.views import FormAction
from django_helpers.core.views import ListAction
from django_helpers.core.views import View
from django_helpers.core.views import ViewMetaClass
from django_helpers.core.views import ViewDecorators
from forms import AddImageFormRenderer
from forms import EditImageFormRenderer


class MetaClass(ViewMetaClass):
    def __new__(mcs, name, bases, attrs):
        from forms import get_image_form
        from listable import get_image_listing
        from models import get_image_model
        
        product_model = attrs.get('product_model')
        
        obj = type.__new__(mcs, name, bases, attrs)
        
        if product_model is not None:
            image_model = get_image_model(product_model)
            setattr(obj, 'image_model', image_model)
            
            form_class = get_image_form(product_model)
            setattr(obj, 'image_from', form_class)
            
            listing_cls = get_image_listing(obj)
            setattr(obj, 'image_listing', listing_cls)
        
        return obj


class ImageBaseView(six.with_metaclass(MetaClass, View)):
    url_name_prefix = "product-images"
    
    url_pattern_prefix = "product-images"
    
    product_model = None
    
    image_form = None
    
    image_listings = None
    
    add_image_form_render = AddImageFormRenderer
    
    edit_image_form_render = EditImageFormRenderer
    
    #
    # Buttons
    #
    @classmethod
    def edit_image_button(cls, user):
        return cls.get_delete_icon_button(user, action='edit_image', fields=['product_id', 'id'])
    
    @classmethod
    def delete_image_button(cls, user):
        action = cls.get_delete_icon_button(user, action='delete_image', fields=['product_id', 'id'])
        return action
    
    #
    # Getters
    #
    @classmethod
    def get_image_form(cls):
        from forms import get_image_form
        
        form = getattr(cls, 'image_from', None)
        if form is not None:
            return form
        
        form_class = get_image_form(cls.product_model)
        setattr(cls, 'image_from', form_class)
        
        return form_class
    
    @classmethod
    def get_image_listings(cls):
        from listable import get_image_listing
        
        form = getattr(cls, 'image_listing', None)
        if form is not None:
            return form
        
        listing_cls = get_image_listing(cls)
        setattr(cls, 'image_listing', listing_cls)
        
        return listing_cls
    
    #
    # Image Actions
    #
    @ViewDecorators.url('add-image', '(?P<product_id>\d+)/images/add/')
    @ViewDecorators.title('Add Image')
    @ViewDecorators.instance('product', pk='product_id', model='self.product_model')
    def add_image(self, product):
        action = FormAction(self)
        
        action.form_class = self.get_image_form()
        action.renderer = self.add_image_form_render
        
        action.redirect_url = self.url_to_action('list_image', args=(
            product.id,
        ))
        action.success_message = 'Image added successfully.'
        
        listings = self.get_image_listings()
        action.refresh_listing(listings.listing_id, product_id=product.id)

        action.add_extra("product", product)
        
        return action.render()
    
    @ViewDecorators.url('edit-image', '(?P<product_id>\d+)/images/(?P<pk>\d+)/edit/')
    @ViewDecorators.instance(id='pk', model='self.image_model')
    @ViewDecorators.instance('product', pk='product_id', model='self.product_model')
    def edit_image(self, instance, product):
        action = FormAction(self)
        
        action.form_class = self.get_image_form()
        action.renderer = self.edit_image_form_render
        
        action.redirect_url = self.url_to_action('list_image', args=(
            product.id,
        ))
        action.instance = instance
        action.success_message = 'Image updated successfully.'
        
        listings = self.get_image_listings()
        action.refresh_listing(listings.listing_id, product_id=product.id)
        
        return action.render()
    
    @ViewDecorators.url('list-image', '(?P<product_id>\d+)/images/list/')
    @ViewDecorators.title('List Images')
    @ViewDecorators.instance('product', pk='product_id', model='self.product_model')
    def list_image(self, product, request):
        listings = self.get_image_listings()
        listing = listings(request)
        listing.set_params(product_id=product.id)

        action = ListAction(self)
        action.table = listing
        action.template = 'e-commerce/images/image.html'

        add_url = self.url_to_action('add_image', args=(
            product.id,
        ))

        return action.render(add_url=add_url)
    
    @ViewDecorators.url('delete-image', '(?P<product_id>\d+)/images/(?P<pk>\d+)/delete/')
    @ViewDecorators.instance(id='pk', model='self.image_model')
    @ViewDecorators.instance('product', pk='product_id', model='self.product_model')
    def delete_image(self, instance, product):
        action = DeleteAction(self, instance=instance)

        to_action = self.url_to_action('list_image', args=(
            product.id,
        ))

        action.redirect_link = to_action
        action.cancel_link = to_action
        action.success_message = 'Image deleted successfully.'
        action.title = 'Delete Image'
        
        listings = self.get_image_listings()
        action.refresh_listing(listings.listing_id, product_id=product.id)
        
        return action.render()
