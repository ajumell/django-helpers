# coding=utf-8
from django_helpers.apps.listings import SimpleListing
from django_helpers.core.listable.fields import TextField
from django_helpers.core.listable.fields import ImageURLField
from django_helpers.core.listable.fields import YesNoField


def get_image_listing(view_cls):
    product_model = view_cls.product_model

    meta = getattr(product_model, "_meta")

    app_label = meta.app_label

    class ImageListing(SimpleListing):
        register_class = True

        listing_id = "{}-image-listing".format(app_label)

        view = view_cls

        url_search_parameters = ['product_id']

        def __init__(self, request):
            SimpleListing.__init__(self, request)

        def item_template(self, row):
            return "e-commerce/images/listings/image.html"

        def get_fields(self):
            user = self.request.user
            view = self.view

            fields = []

            image_url = ImageURLField("image", "image")
            fields.append(image_url)

            alt_text = TextField("alt_text", "alt_text")
            alt_text.make_editable()
            fields.append(alt_text)

            alt_text_text = TextField("alt_text", "alt_text_text")
            fields.append(alt_text_text)

            is_default = YesNoField("is_default", "is_default")
            fields.append(is_default)

            delete_button = view.delete_image_button(user)

            if delete_button is not None:
                fields.append(delete_button)

            return fields

        def get_query(self, request, kwargs=None):
            from models import get_image_model

            product_model = self.view.product_model
            image_model = get_image_model(product_model)

            query = image_model.objects.all()
            return query

    return ImageListing
