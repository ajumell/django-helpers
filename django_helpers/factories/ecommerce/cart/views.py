# coding=utf-8
from django.contrib import messages
from django.utils import six

from django_helpers.core.views import Action
from django_helpers.core.views import DeleteAction
from django_helpers.core.views import FormAction
from django_helpers.core.views import ListAction
from django_helpers.core.views import JSONResponse
from django_helpers.core.views import View
from django_helpers.core.views import ViewMetaClass
from django_helpers.core.views import ViewDecorators
from forms import CartDiscountFormRenderer


class CartBaseView(View):
    cart_model = None

    discount_model = None

    cart_template = None

    empty_cart_template = None

    url_name_prefix = ''

    url_pattern_prefix = ''

    def get(self, request):
        """

        :param request:
        :return: Cart
        """
        model = self.cart_model
        cart = model.get(request)

        return cart

    @ViewDecorators.url('add-to-cart', 'cart/add/')
    def add_to_cart(self, request):
        model = self.cart_model
        cart = model.get(request)

        post = request.POST.get

        product = post("product")
        price = post("price")
        count = post("count", "1")
        add = post("add", "1")

        cart.add(product, price, count, add == "1")

        return self.redirect_to_action('cart_view')

    @ViewDecorators.url('remove-cart-item', 'cart/(?P<pk>\d+)/remove/')
    def remove_item(self, request, pk):
        model = self.cart_model
        cart = model.get(request)

        cart.remove(pk)

        return self.redirect_to_action('cart_view')

    @ViewDecorators.url('cart-page', 'cart/view/')
    def cart_view(self, request):
        action = Action(self)
        action.title = "Cart"

        cart = self.get(request)
        items = cart.cartitem_set.all()
        items = items.select_related()

        items_count = len(items)
        if items_count > 0:
            action.template = self.cart_template
        else:
            action.template = self.empty_cart_template

        cart_total = cart.cart_total()
        total = cart.total()
        discount = float(cart.discount_amount)

        return action.render(
                cart=cart,
                promo_code=cart.promo_code,
                items=items,
                total=total,
                cart_total=cart_total,
                discount=discount
        )

    @ViewDecorators.url('add-promo-code', 'cart/promo/add/')
    @ViewDecorators.csrf_exempt
    def add_promo_code(self, request):
        post = request.POST.get
        code = post("code")

        response = JSONResponse(request)
        if not code:
            return response

        discount_model = self.discount_model
        try:

            discount_model.objects.get(code=code)

            model = self.cart_model
            cart = model.get(request)

            cart.promo_code = code
            cart.recalculate_amount()

            action = self.url_to_action('cart_view')
            response.redirect(action)

        except discount_model.DoesNotExist:
            messages.error(request, "Invalid Promo Code.")

        return response

    @ViewDecorators.url('remove-promo-code', 'cart/promo/remove/')
    def remove_promo_code(self, request):
        model = self.cart_model
        cart = model.get(request)

        cart.promo_code = ""
        cart.recalculate_amount()

        return self.redirect_to_action('cart_view')


class MetaClass(ViewMetaClass):
    def __new__(mcs, name, bases, attrs):
        from forms import get_cart_discount_form

        product_model = attrs.get('product_model')

        obj = type.__new__(mcs, name, bases, attrs)

        if product_model is not None:
            form_class = get_cart_discount_form(product_model)
            setattr(obj, 'form', form_class)

        return obj


class TaxClassBaseView(six.with_metaclass(MetaClass, View)):
    url_name_prefix = "tax-class"

    url_pattern_prefix = "tax-class"

    product_model = None

    tax_class_form = None

    tax_class_listings = None

    tax_rule_form = None

    tax_rule_listings = None

    #
    # Buttons
    #
    @classmethod
    def edit_tax_class_button(cls, user):
        return cls.get_edit_icon_button(user, action='edit_tax_class')

    @classmethod
    def delete_tax_class_button(cls, user):
        action = cls.get_delete_icon_button(user, action='delete_tax_class')
        return action

    @classmethod
    def edit_tax_rule_button(cls, user):
        button = cls.get_edit_icon_button(user, action='edit_tax_rule', fields=['tax_class_id', 'id'])
        button.set_modal(title="Edit Rule")
        return button

    @classmethod
    def delete_tax_rule_button(cls, user):
        action = cls.get_delete_icon_button(user, action='delete_tax_rule', fields=['tax_class_id', 'id'])
        return action

    @classmethod
    def add_rule_link(cls):
        return cls.get_url_name('add_tax_rule')

    #
    # Getters
    #

    @classmethod
    def add_nav_link(cls):
        return cls.url_to_action('list_tax_class')

    @classmethod
    def get_tax_class_form(cls):
        from forms import get_tax_class_form

        form = getattr(cls, 'tax_class_from', None)
        if form is not None:
            return form

        form_class = get_tax_class_form(cls.product_model)
        setattr(cls, 'tax_class_from', form_class)

        return form_class

    @classmethod
    def get_tax_class_listings(cls):
        from listable import get_tax_listing

        form = getattr(cls, 'tax_class_listing', None)
        if form is not None:
            return form

        listing_cls = get_tax_listing(cls)
        setattr(cls, 'tax_class_listing', listing_cls)

        return listing_cls

    @classmethod
    def get_tax_rule_form(cls):
        from forms import get_tax_rule_form

        form = getattr(cls, 'tax_rule_form', None)
        if form is not None:
            return form

        form_class = get_tax_rule_form(cls.product_model)
        setattr(cls, 'tax_rule_form', form_class)

        return form_class

    #
    # Tax Class Actions
    #
    @ViewDecorators.url('add-tax-class', 'add/')
    @ViewDecorators.title('Add Tax Class')
    def add_tax_class(self, request):
        action = FormAction(self)

        action.form_class = self.get_tax_class_form()
        action.renderer = self.add_tax_class_form_render

        action.redirect_url = self.url_to_action('list_tax_class')
        action.success_message = 'Tax Class added successfully.'

        listings = self.get_tax_class_listings()
        action.refresh_listing(listings.listing_id)

        return action.render()

    @ViewDecorators.url('edit-tax-class', '(?P<pk>\d+)/edit/')
    @ViewDecorators.instance(id='pk', model='self.tax_model')
    def edit_tax_class(self, instance):
        action = FormAction(self)

        action.form_class = self.get_tax_class_form()
        action.renderer = self.edit_tax_class_form_render

        action.redirect_url = self.url_to_action('list_tax_class')
        action.instance = instance
        action.success_message = 'Tax Class updated successfully.'

        listings = self.get_tax_class_listings()
        action.refresh_listing(listings.listing_id)

        return action.render()

    @ViewDecorators.url('list-tax-class', 'list/')
    @ViewDecorators.title('List Tax Classes')
    def list_tax_class(self, request):
        action = ListAction(self)
        action.table_class = self.get_tax_class_listings()
        action.template = 'e-commerce/pricable/tax-class.html'

        add_url = self.url_to_action('add_tax_class')

        return action.render(add_url=add_url)

    @ViewDecorators.url('delete-tax-class', '(?P<pk>\d+)/delete/')
    @ViewDecorators.instance(id='pk', model='self.tax_model')
    def delete_tax_class(self, instance):
        action = DeleteAction(self, instance=instance)

        action.redirect_link = self.url_to_action('list_tax_class')
        action.cancel_link = self.url_to_action('list_tax_class')
        action.success_message = 'TaxClass deleted successfully.'
        action.title = 'Delete Tax Class'

        listings = self.get_tax_class_listings()
        action.refresh_listing(listings.listing_id)

        return action.render()
