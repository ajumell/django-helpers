# coding=utf-8
from django import forms

from django_helpers.apps.form_renderer import FormRenderer


def get_cart_discount_form(discount_model):
    class AddCartDiscountForm(forms.ModelForm):
        class Meta:
            model = discount_model
            exclude = (
                'is_active',
            )

    return AddCartDiscountForm


class CartDiscountFormRenderer(FormRenderer):
    form_id = 'cart-discount-form'

    def setup(self):
        pass
