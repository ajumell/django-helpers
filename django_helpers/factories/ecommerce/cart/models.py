# coding=utf-8

from django.conf import settings
from django.db import models
from django.db import transaction

from django_helpers.factories.ecommerce.pricable.utils import round_price
from django_helpers.factories.utils import ModelFactory
from django_helpers.humaize import format_price

user_model = settings.AUTH_USER_MODEL

ACTIONS = (
    ('product_percentage_discount', 'product_percentage_discount'),
    ('product_fixed_discount', 'product_fixed_discount'),
)


def calculate_discount(discount, price):
    value = discount.value

    if discount.action == 'product_percentage_discount':
        c = price * float(value) / 100
        return c

    elif discount.action == 'product_fixed_discount':
        c = price * float(value) / 100
        return c


def calculate_total_discount(discount, items):
    discount_amount = 0.0
    for item in items:
        price = float(item.unit_price)
        amount = calculate_discount(discount, price)
        amount = amount * item.count

        discount_amount += amount

    return discount_amount


def find_max_discount(discounts, items):
    max_discount = None
    selected_discount = None

    for discount in discounts:
        amount = calculate_total_discount(discount, items)

        if discount is None or max_discount < amount:
            selected_discount = discount
            max_discount = amount

    return selected_discount


class ItemAlreadyExists(Exception):
    pass


class ItemDoesNotExist(Exception):
    pass


class Cart(ModelFactory):
    created_at = models.DateTimeField(auto_created=True, auto_now_add=True)
    user = models.ForeignKey(user_model, null=True)
    total_amount = models.DecimalField(default=0, decimal_places=2, max_digits=10, editable=False)
    discount_amount = models.DecimalField(default=0, decimal_places=2, max_digits=10, editable=False)
    discount_text = models.CharField(max_length=1024, default='')
    promo_code = models.CharField(max_length=255)
    has_checked_out = models.BooleanField(default=False, editable=False)

    def checkout(self):
        self.has_checked_out = True
        self.save()

    def get_total_amount(self):
        return format_price(self.total_amount)

    @classmethod
    def get(cls, request, create=True):
        """
        Returns the cart for the current session.

        :type request: django.http.HttpRequest
        :type create: bool
        :return: Cart
        """
        user = request.user
        session = request.session
        logged_in = user.is_authenticated()

        obj = None
        if logged_in:
            obj = cls.get_user_cart(user)

        if obj is None:
            cart_id = session.get(cls.session_key)
            try:
                if cart_id:
                    obj = cls.objects.get(id=cart_id, has_checked_out=False)
                    if logged_in:
                        obj.user = user
                        obj.save()
            except cls.DoesNotExist:
                pass

        if not obj and create is False:
            return None

        if obj is None:
            obj = cls()
            obj.set_user(request)
            obj.save()

        return obj

    @classmethod
    @transaction.atomic()
    def get_user_cart(cls, user):
        """

        :param user: The current user object
        :return:
        """
        obj = None
        try:
            obj = cls.objects.get(user=user, has_checked_out=False)
        except cls.DoesNotExist:
            pass
        except cls.MultipleObjectsReturned:
            carts = cls.objects.filter(user=user, has_checked_out=False)
            carts = list(carts)
            obj = carts.pop(0)

            while len(carts):
                cart_obj = carts.pop(0)
                items = cart_obj.items()
                for item in items:
                    obj.add(item.product_id, item.unit_price, item.count)
                    item.delete()
                cart_obj.delete()

        return obj

    def set_user(self, request):
        """
        :type request: django.http.HttpRequest
        """
        user = request.user
        session = request.session
        logged_in = user.is_authenticated()

        if logged_in:
            self.user_id = user.id

        self.save()
        session[self.session_key] = self.id

    def add(self, product, unit_price, count=1, add=False):
        count = int(count)
        try:
            item = self.ItemsModel.objects.get(cart=self, product_id=product)
        except self.ItemsModel.DoesNotExist:
            item = self.ItemsModel()
            item.cart = self
            item.product_id = product
            item.unit_price = unit_price
            item.count = count
            item.save()
        else:  # ItemAlreadyExists
            item.unit_price = unit_price
            if add is True:
                item.count += count
            else:
                item.count = count
            item.save()

        self.recalculate_amount()

    def remove(self, product):
        try:
            item = self.ItemsModel.objects.get(cart=self, product_id=product)
            item.delete()
        except self.ItemsModel.DoesNotExist:
            pass

        self.recalculate_amount()

    def update(self, product, count, unit_price=None):
        try:
            item = self.ItemsModel.objects.get(cart=self, product=product)
        except self.ItemsModel.DoesNotExist:
            raise ItemDoesNotExist
        else:  # ItemAlreadyExists
            if count == 0:
                item.delete()
            else:
                item.unit_price = unit_price
                item.count = int(count)
                item.save()

        self.recalculate_amount()

    def has_items(self):
        return self.cartitem_set.count() > 0

    def count(self):
        result = 0
        for item in self.items():
            result += 1 * item.count
        return result

    def items(self):
        if not hasattr(self, '_items'):
            _items = self.cartitem_set.all()
            setattr(self, "_items", _items)

        return getattr(self, "_items")

    def recalculate_amount(self):
        items = self.items()

        applicable_discounts = self.find_discounts()
        discount = find_max_discount(applicable_discounts, items)

        discount_amount = 0
        for item in items:
            discount_amount += item.compute_price(discount)

        self.discount_amount = discount_amount
        if discount is not None:
            self.discount_text = discount.name
        else:
            self.discount_text = ""

        self.save()

    def find_discounts(self):
        applicable_discounts = []

        discounts = self.DiscountModel.get(self.promo_code)
        count = self.count()
        total = self.total()
        for discount in discounts:
            from_amount = discount.from_sub_total
            to_amount = discount.to_sub_total

            from_count = discount.from_total_items
            to_count = discount.to_total_items

            can_amount = False
            can_count = False

            if from_amount > 0:
                if to_amount > 0:
                    if from_amount <= total <= to_amount:
                        can_amount = True
                else:
                    if from_amount <= total:
                        can_amount = True

            if from_count > 0:
                if to_count > 0:
                    if from_count <= count <= to_count:
                        can_count = True
                else:
                    if from_count <= count:
                        can_count = True

            if can_amount or can_count or (from_amount == 0 and from_count == 0):
                applicable_discounts.append(discount)

        return applicable_discounts

    def summary(self):
        result = 0
        for item in self.items():
            result += item.total_amount
        return result

    def subtotal(self):
        result = 0.0
        for item in self.items():
            result += float(item.total_amount)
        return result - self.tax()

    def total(self):
        result = 0.0
        for item in self.items():
            result += float(item.total_amount)
        return result

    def cart_total(self):
        result = 0.0
        for item in self.items():
            result += float(item.cart_total_amount())
        return result

    def tax(self):
        result = 0.0
        for item in self.items():
            t = float(item.tax_amount) * item.count
            result += t
        return result

    @transaction.atomic()
    def clear(self):
        for item in self.items():
            item.delete()


class CartItem(ModelFactory):
    unit_price = models.DecimalField(default=0, decimal_places=2, max_digits=10)

    tax_amount = models.DecimalField(default=0, decimal_places=2, max_digits=10)
    discount_amount = models.DecimalField(default=0, decimal_places=2, max_digits=10)

    computed_price = models.DecimalField(default=0, decimal_places=2, max_digits=10)

    count = models.IntegerField(default=1)
    total_amount = models.DecimalField(default=0, decimal_places=2, max_digits=10, editable=False)

    def reset_price(self):
        self.unit_price = self.product.unit_price
        self.save()

    #
    # Price Calculations
    #
    def compute_price(self, discount=None):
        product = self.product
        discount_amount = 0.0
        price = float(self.unit_price)
        has_tax = getattr(product, 'HAS_TAX', False)

        if discount is not None:
            discount_amount = calculate_discount(discount, price)
            price -= discount_amount

        if not has_tax:
            self.tax_amount = 0
        else:
            tax_class = product.tax_class
            self.tax_amount = tax_class.calculate_tax(price)
            actual_tax = tax_class.calculate_tax(price + discount_amount)

            tax_diff = actual_tax - self.tax_amount
            discount_amount += tax_diff

            discount_amount = round_price(discount_amount)

        count = int(self.count)

        self.computed_price = round_price(price, self.tax_amount)
        self.total_amount = self.computed_price * count
        self.discount_amount = discount_amount

        self.save()

        return discount_amount * count

    def get_unit_price(self):
        return format_price(self.unit_price)

    def get_computed_price(self):
        return format_price(self.computed_price)

    def get_total_amount(self):
        return format_price(self.total_amount)

    def get_discount_amount(self):
        return format_price(self.discount_amount)

    #
    # Tax
    #
    def get_tax_amount(self):
        return format_price(float(self.tax_amount) * float(self.count))

    def get_gross_value(self):
        return format_price(float(self.unit_price) * float(self.count))

    def iter_tax(self):
        product = self.product
        if not getattr(product, 'HAS_TAX', False):
            return

        tax_class = product.tax_class
        return tax_class.iterate_tax(self.unit_price, self.count)

    #
    # Cart Display
    #
    def cart_price(self):
        return self.computed_price + self.discount_amount

    def cart_total_amount(self):
        return (self.computed_price + self.discount_amount) * self.count


class CartDiscount(ModelFactory):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=15, default='', blank=True)

    is_active = models.BooleanField(default=True)

    #
    # Action
    #
    action = models.CharField(max_length=255, choices=ACTIONS)
    value = models.FloatField(default=0)
    maximum_amount = models.DecimalField(default=0, decimal_places=2, max_digits=10)

    #
    # Conditions
    #
    from_total_items = models.IntegerField(default=0)
    to_total_items = models.IntegerField(default=0)

    from_sub_total = models.DecimalField(default=0, decimal_places=2, max_digits=10)
    to_sub_total = models.DecimalField(default=0, decimal_places=2, max_digits=10)

    def __unicode__(self):
        return self.name

    @classmethod
    def get(cls, coupon=None):
        from django.db.models import Q

        query = Q(code='')

        if coupon:
            query = query | Q(code=coupon)

        query = cls.objects.filter(query)
        query = query.filter(is_active=True)

        return query


def cart(product_model):
    meta = getattr(product_model, '_meta')

    product_model_name = meta.object_name

    session_key = "{}_CART".format(product_model_name.upper())

    cart_model = Cart.create({
        '__module__': product_model.__module__,
        'session_key': session_key
    })

    cart_item_model = CartItem.create({
        'cart': models.ForeignKey(cart_model),
        '__module__': product_model.__module__,
        'product': models.ForeignKey(product_model)
    })

    cart_discount_model = CartDiscount.create({
        '__module__': product_model.__module__,
    })

    cart_model.extend(
            ItemsModel=cart_item_model,
            DiscountModel=cart_discount_model
    )

    return cart_model, cart_item_model, cart_discount_model
