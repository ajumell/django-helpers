# coding=utf-8

from django.utils import six

from django_helpers.core.views import DeleteAction
from django_helpers.core.views import FormAction
from django_helpers.core.views import ListAction
from django_helpers.core.views import View
from django_helpers.core.views import ViewDecorators
from django_helpers.core.views import ViewMetaClass
from forms import AddTaxClassFormRenderer
from forms import AddTaxRuleFormRenderer
from forms import EditTaxClassFormRenderer
from forms import EditTaxRuleFormRenderer


class MetaClass(ViewMetaClass):
    def __new__(mcs, name, bases, attrs):
        from forms import get_tax_class_form, get_tax_rule_form
        from listable import get_tax_listing
        from models import get_tax_class_model
        from models import get_tax_rule_model

        product_model = attrs.get('product_model')

        obj = type.__new__(mcs, name, bases, attrs)

        if product_model is not None:
            tax_model = get_tax_class_model(product_model)
            setattr(obj, 'tax_model', tax_model)

            rule_model = get_tax_rule_model(product_model)
            setattr(obj, 'rule_model', rule_model)

            form_class = get_tax_class_form(product_model)
            setattr(obj, 'tax_class_from', form_class)

            form_class = get_tax_rule_form(product_model)
            setattr(obj, 'tax_rule_form', form_class)

            listing_cls = get_tax_listing(obj)
            setattr(obj, 'tax_class_listing', listing_cls)

        return obj


class TaxClassBaseView(six.with_metaclass(MetaClass, View)):
    url_name_prefix = "tax-class"

    url_pattern_prefix = "tax-class"

    product_model = None

    tax_class_form = None

    tax_class_listings = None

    tax_rule_form = None

    tax_rule_listings = None

    add_tax_class_form_render = AddTaxClassFormRenderer

    edit_tax_class_form_render = EditTaxClassFormRenderer

    add_tax_rule_form_render = AddTaxRuleFormRenderer

    edit_tax_rule_form_render = EditTaxRuleFormRenderer

    #
    # Buttons
    #
    @classmethod
    def edit_tax_class_button(cls, user):
        return cls.get_delete_icon_button(user, action='edit_tax_class')

    @classmethod
    def delete_tax_class_button(cls, user):
        action = cls.get_delete_icon_button(user, action='delete_tax_class')
        return action

    @classmethod
    def edit_tax_rule_button(cls, user):
        button = cls.get_edit_icon_button(user, action='edit_tax_rule', fields=['tax_class_id', 'id'])
        button.set_modal(title="Edit Rule")
        return button

    @classmethod
    def delete_tax_rule_button(cls, user):
        action = cls.get_delete_icon_button(user, action='delete_tax_rule', fields=['tax_class_id', 'id'])
        return action

    @classmethod
    def add_rule_link(cls):
        return cls.get_url_name('add_tax_rule')

    #
    # Getters
    #

    @classmethod
    def add_nav_link(cls):
        return cls.url_to_action('list_tax_class')

    @classmethod
    def get_tax_class_form(cls):
        from forms import get_tax_class_form

        form = getattr(cls, 'tax_class_from', None)
        if form is not None:
            return form

        form_class = get_tax_class_form(cls.product_model)
        setattr(cls, 'tax_class_from', form_class)

        return form_class

    @classmethod
    def get_tax_class_listings(cls):
        from listable import get_tax_listing

        form = getattr(cls, 'tax_class_listing', None)
        if form is not None:
            return form

        listing_cls = get_tax_listing(cls)
        setattr(cls, 'tax_class_listing', listing_cls)

        return listing_cls

    @classmethod
    def get_tax_rule_form(cls):
        from forms import get_tax_rule_form

        form = getattr(cls, 'tax_rule_form', None)
        if form is not None:
            return form

        form_class = get_tax_rule_form(cls.product_model)
        setattr(cls, 'tax_rule_form', form_class)

        return form_class

    #
    # Tax Class Actions
    #
    @ViewDecorators.url('add-tax-class', 'add/')
    @ViewDecorators.title('Add Tax Class')
    def add_tax_class(self, request):
        action = FormAction(self)

        action.form_class = self.get_tax_class_form()
        action.renderer = self.add_tax_class_form_render

        action.redirect_url = self.url_to_action('list_tax_class')
        action.success_message = 'Tax Class added successfully.'

        listings = self.get_tax_class_listings()
        action.refresh_listing(listings.listing_id)

        return action.render()

    @ViewDecorators.url('edit-tax-class', '(?P<pk>\d+)/edit/')
    @ViewDecorators.instance(id='pk', model='self.tax_model')
    def edit_tax_class(self, instance):
        action = FormAction(self)

        action.form_class = self.get_tax_class_form()
        action.renderer = self.edit_tax_class_form_render

        action.redirect_url = self.url_to_action('list_tax_class')
        action.instance = instance
        action.success_message = 'Tax Class updated successfully.'

        listings = self.get_tax_class_listings()
        action.refresh_listing(listings.listing_id)

        return action.render()

    @ViewDecorators.url('list-tax-class', 'list/')
    @ViewDecorators.title('List Tax Classes')
    def list_tax_class(self, request):
        action = ListAction(self)
        action.table_class = self.get_tax_class_listings()
        action.template = 'e-commerce/pricable/tax-class.html'

        add_url = self.url_to_action('add_tax_class')

        return action.render(add_url=add_url)

    @ViewDecorators.url('delete-tax-class', '(?P<pk>\d+)/delete/')
    @ViewDecorators.instance(id='pk', model='self.tax_model')
    def delete_tax_class(self, instance):
        action = DeleteAction(self, instance=instance)

        action.redirect_link = self.url_to_action('list_tax_class')
        action.cancel_link = self.url_to_action('list_tax_class')
        action.success_message = 'TaxClass deleted successfully.'
        action.title = 'Delete Tax Class'

        listings = self.get_tax_class_listings()
        action.refresh_listing(listings.listing_id)

        return action.render()

    #
    # Tax Rule Actions
    #
    @ViewDecorators.url('add-tax-rule', '(?P<class_id>\d+)/rules/add/')
    @ViewDecorators.title('Add Tax Rule')
    @ViewDecorators.instance('tax_class', pk='class_id', model='self.tax_model')
    def add_tax_rule(self, request, tax_class):
        action = FormAction(self)

        action.form_class = self.get_tax_rule_form()
        action.renderer = self.add_tax_rule_form_render
        action.redirect_url = self.url_to_action('list_tax_class')
        action.success_message = 'Tax Rule added successfully.'

        action.add_extra("tax_class", tax_class)

        return action.render()

    @ViewDecorators.url('edit-tax-rule', '(?P<class_id>\d+)/rules/(?P<pk>\d+)/edit/')
    @ViewDecorators.instance(id='pk', model="self.rule_model")
    def edit_tax_rule(self, request, instance):
        action = FormAction(self)

        action.form_class = self.get_tax_rule_form()
        action.renderer = self.edit_tax_rule_form_render
        action.instance = instance
        action.success_message = 'Tax Rule updated successfully.'

        return action.render()

    @ViewDecorators.url('delete-tax-rule', '(?P<class_id>\d+)/rules/(?P<pk>\d+)/delete/')
    @ViewDecorators.instance(id='pk', model="self.rule_model")
    def delete_tax_rule(self, instance):
        action = DeleteAction(self, instance=instance)

        action.success_message = 'TaxRule deleted successfully.'
        action.title = 'Delete Tax Rule'

        return action.render()
