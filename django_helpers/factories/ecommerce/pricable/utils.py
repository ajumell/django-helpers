# coding=utf-8


def round_price(*values):
    amount = 0.0

    for value in values:
        amount += float(value)

    if amount == 0.0:
        return 0.0

    new_price = int(amount * 100)
    decimal = int(new_price % 100)

    delta = decimal % 5
    delta = 5 - delta
    decimal += delta
    decimal = float(decimal) / 100

    new_price = int(amount)
    new_price = float(new_price) + decimal

    return new_price
