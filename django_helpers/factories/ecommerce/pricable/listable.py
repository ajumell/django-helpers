# coding=utf-8
from django_helpers.apps.listings import SimpleListing
from django_helpers.core.listable.fields import TextField
from django_helpers.core.listable.fields import BootstrapSuccessButton
from django_helpers.core.listable.fields import Widget


def get_tax_rule_listing(view_cls):
    product_model = view_cls.product_model

    meta = getattr(product_model, "_meta")

    app_label = meta.app_label

    class TaxRuleListing(SimpleListing):
        register_class = True

        listing_id = "{}-tax-rule-listing".format(app_label)

        view = view_cls

        url_search_parameters = ['tax_class_id']

        def __init__(self, request):
            SimpleListing.__init__(self, request)

        def item_template(self, row):
            return "e-commerce/pricable/listings/tax-rule.html"

        def get_fields(self):
            user = self.request.user
            view = self.view

            fields = []

            name_column = TextField("name", "Name", True, True)
            fields.append(name_column)

            percentage_column = TextField("percentage", "Percentage", False, True)
            fields.append(percentage_column)

            apply_on_column = TextField("apply_on", "Apply On", True, True)
            fields.append(apply_on_column)

            edit_button = view.edit_tax_rule_button(user)
            delete_button = view.delete_tax_rule_button(user)

            if edit_button is not None:
                fields.append(edit_button)

            if delete_button is not None:
                fields.append(delete_button)

            return fields

        def get_query(self, request, kwargs=None):
            from models import get_tax_rule_model

            product_model = self.view.product_model
            tax_class = get_tax_rule_model(product_model)

            query = tax_class.objects.all()
            return query

    return TaxRuleListing


def get_tax_listing(view_cls):
    product_model = view_cls.product_model

    meta = getattr(product_model, "_meta")

    app_label = meta.app_label

    rule_listing = get_tax_rule_listing(view_cls)

    class TaxClassListing(SimpleListing):
        register_class = True

        listing_id = "{}-tax-class-listing".format(app_label)

        view = view_cls

        rule_listings = rule_listing

        def __init__(self, request):
            SimpleListing.__init__(self, request)

        def item_template(self, row):
            return "e-commerce/pricable/listings/tax-class.html"

        def get_fields(self):
            user = self.request.user
            view = self.view

            fields = []

            name_field = TextField("name", "Name", True, True)
            fields.append(name_field)
            name_field.make_editable()

            edit_button = view.edit_tax_class_button(user)
            delete_button = view.delete_tax_class_button(user)

            if edit_button is not None:
                fields.append(edit_button)

            if delete_button is not None:
                fields.append(delete_button)

            add_button = BootstrapSuccessButton("Add Rule", view.add_rule_link(), title='add_rule')
            add_button.add_class_name('btn-block')
            add_button.set_modal(title='Add Rule')
            fields.append(add_button)

            rules = Widget(self.rule_listings, {
                "tax_class_id": "id"
            }, "rules")
            fields.append(rules)

            return fields

        def get_query(self, request, kwargs=None):
            from models import get_tax_class_model

            product_model = self.view.product_model
            tax_class = get_tax_class_model(product_model)

            query = tax_class.objects.all()
            return query

    return TaxClassListing
