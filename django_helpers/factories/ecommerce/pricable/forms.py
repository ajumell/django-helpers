# coding=utf-8
from django import forms

from django_helpers.apps.form_renderer import FormRenderer


def get_tax_class_form(product_model):
    from models import get_tax_class_model
    tax_model = get_tax_class_model(product_model)

    class AddTaxClassForm(forms.ModelForm):
        class Meta:
            model = tax_model
            fields = (
                'name',
            )

    return AddTaxClassForm


def get_tax_rule_form(product_model):
    from models import get_tax_rule_model
    rule_model = get_tax_rule_model(product_model)

    class AddTaxRuleForm(forms.ModelForm):
        class Meta:
            model = rule_model

            fields = (
                'name',
                'percentage',
                'apply_on',
                'order',
            )

    return AddTaxRuleForm


class AddTaxClassFormRenderer(FormRenderer):
    form_id = 'tax-class-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='name')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')


class EditTaxClassFormRenderer(FormRenderer):
    form_id = 'tax-class-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='name')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')


class AddTaxRuleFormRenderer(FormRenderer):
    form_id = 'add-tax-rule-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='name')

        row = self.add_row()
        row.add_col(field='percentage')

        row = self.add_row()
        row.add_col(field='apply_on')

        row = self.add_row()
        row.add_col(field='order')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')


class EditTaxRuleFormRenderer(FormRenderer):
    form_id = 'edit-tax-rule-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='name')

        row = self.add_row()
        row.add_col(field='percentage')

        row = self.add_row()
        row.add_col(field='apply_on')

        row = self.add_row()
        row.add_col(field='order')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')
