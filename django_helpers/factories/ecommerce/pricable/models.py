# coding=utf-8
from django.core.exceptions import ValidationError
from django.db import models

from django_helpers.factories.utils import ModelFactory
from django_helpers.humaize import format_price

# noinspection PyUnresolvedReferences
import signals

__all__ = [
    'pricable',
    'get_tax_class_model',
    'get_tax_rule_model',
]

#
# Choices
#
APPLY_ON_OPTIONS = [
    'Base Price',
    'Calculated Price'
]

APPLY_ON_CHOICES = [(x, x) for x in APPLY_ON_OPTIONS]


#
# Calculations
#
def percentage(price, percent):
    value = price * float(percent) / 100

    return value


#
# Getters
#
def get_tax_class_model(product_model):
    return getattr(product_model, 'TaxClassModel', None)


def get_tax_rule_model(product_model):
    return getattr(product_model, 'TaxRuleModel', None)


#
# Setters
#
def set_tax_class_model(product_model, tax_class_model):
    setattr(product_model, 'TaxClassModel', tax_class_model)


def set_tax_rule_model(product_model, tax_rule_model):
    setattr(product_model, 'TaxRuleModel', tax_rule_model)


#
# Decorator
#
def pricable(tax=True, discounts=True):
    def decorator(product_model):
        unit_price = models.DecimalField(max_digits=15, decimal_places=2)
        product_model.add_to_class('unit_price', unit_price)

        computed_price = models.DecimalField(max_digits=15, decimal_places=2)
        product_model.add_to_class('computed_price', computed_price)

        def get_unit_price(self):
            return format_price(self.unit_price)

        product_model.add_to_class('get_unit_price', get_unit_price)

        add_tax_models(product_model, tax)

        if discounts is True:
            pass

        product_model.IS_PRODUCT_MODEL = True

        return product_model

    return decorator


#
# Models
#

# noinspection PyMethodMayBeStatic
class TaxClass(ModelFactory):
    name = models.CharField(max_length=255)

    def calculate_tax(self, price):
        price = float(price)
        new_price = self.calculate_price(price)

        return new_price - price

    def iterate_tax(self, price, count=1):
        modes = self.get_modes()
        price = float(price)
        count = float(count)

        rules = modes.get('Base Price', [])
        extra = 0
        for rule in rules:
            value = percentage(price, rule.percentage)
            extra += value
            yield rule.name, rule.percentage, value * count

        rules = modes.get('Calculated Price', [])
        price += extra
        for rule in rules:
            value = percentage(price, rule.percentage)
            yield rule.name, rule.percentage, value * count

    def calculate_price(self, price):
        modes = self.get_modes()
        price = float(price)

        price = self.calculate_base_price_rules(modes, price)
        price = self.calculate_calculated_price_rules(modes, price)

        return price

    def calculate_calculated_price_rules(self, modes, price):
        rules = modes.get('Calculated Price', None)
        if not rules:
            return price

        for rule in rules:
            value = percentage(price, rule.percentage)
            price += value

        return price

    def calculate_base_price_rules(self, modes, price):
        rules = modes.get('Base Price', None)
        if not rules:
            return price

        extra = 0.0
        for rule in rules:
            value = percentage(price, rule.percentage)
            extra += value

        return extra + price

    def get_modes(self):
        rules = self.taxrule_set.all()
        modes = {}

        for rule in rules:
            mode = rule.apply_on

            if mode not in modes:
                modes[mode] = []

            modes[mode].append(rule)

        return modes

    def __unicode__(self):
        return self.name


# noinspection PyCallByClass
class TaxRule(ModelFactory):
    name = models.CharField(max_length=255)
    percentage = models.FloatField()
    apply_on = models.CharField(max_length=255, choices=APPLY_ON_CHOICES)

    order = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.apply_on not in APPLY_ON_OPTIONS:
            raise ValidationError("Value of apply on fields is not valid.")

        models.Model.save(self, force_insert, force_update, using, update_fields)

    class Meta:
        unique_together = (
            ('tax_class', 'name'),
        )

        ordering = (
            'order',
        )


def add_tax_models(product_model, tax):
    module = product_model.__module__

    if tax is True:
        # noinspection PyMethodMayBeStatic

        tax_class = TaxClass.create({'__module__': module})
        product_model.TaxClassModel = tax_class

        product_model.TaxRuleModel = TaxRule.create({
            '__module__': module,
            'tax_class': models.ForeignKey(tax_class)
        })

        product_model.HAS_TAX = True

        tax_class = models.ForeignKey(tax_class)
        product_model.add_to_class('tax_class', tax_class)
