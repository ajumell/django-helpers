# coding=utf-8

from django.db.models.signals import pre_save
from django.dispatch import receiver
from utils import round_price


def compute_price(instance):
    unit_price = instance.unit_price
    tax_class = instance.tax_class
    new_price = tax_class.calculate_price(unit_price)

    return round_price(new_price)


@receiver(pre_save)
def my_callback(sender, instance, *args, **kwargs):
    is_product_model = getattr(sender, 'IS_PRODUCT_MODEL', False)

    if not is_product_model:
        return

    instance.computed_price = compute_price(instance)
