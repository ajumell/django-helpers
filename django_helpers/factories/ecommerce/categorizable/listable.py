# coding=utf-8
from django_helpers.apps.listings import SimpleListing
from django_helpers.core.listable.fields import TextField


def get_category_listing(view_cls):
    product_model = view_cls.product_model

    meta = getattr(product_model, "_meta")

    app_label = meta.app_label

    class CategoryListing(SimpleListing):
        register_class = True

        listing_id = "{}-category-listing".format(app_label)

        view = view_cls

        def __init__(self, request):
            SimpleListing.__init__(self, request)

        def item_template(self, row):
            return "e-commerce/categorizable/listings/category.html"

        def get_fields(self):
            user = self.request.user
            view = self.view

            fields = []

            name_field = TextField("name", "Name", True, True)
            fields.append(name_field)
            name_field.make_editable()

            delete_button = view.delete_category_button(user)

            if delete_button is not None:
                fields.append(delete_button)

            return fields

        def get_query(self, request, kwargs=None):
            from models import get_category_model

            product_model = self.view.product_model
            category_model = get_category_model(product_model)

            query = category_model.objects.all()
            return query

    return CategoryListing
