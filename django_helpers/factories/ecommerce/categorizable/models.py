# coding=utf-8
# coding=utf-8
from django.db import models

__all__ = [
    'categorizable',
    'get_category_model',
]


#
# Getters
#
def get_category_model(product_model):
    return getattr(product_model, 'CategoryModel', None)


#
# Setters
#
def set_category_model(product_model, category_model):
    setattr(product_model, 'CategoryModel', category_model)


#
# Decorator
#
def categorizable(product_model):
    module = product_model.__module__

    # noinspection PyMethodMayBeStatic
    class ProductCategory(models.Model):
        __module__ = module

        name = models.CharField(max_length=255)

        def __unicode__(self):
            return self.name

    product_model.CategoryModel = ProductCategory
    category = models.ForeignKey(ProductCategory)
    product_model.add_to_class('category', category)

    return product_model
