# coding=utf-8
from django import forms

from django_helpers.apps.form_renderer import FormRenderer


def get_category_form(product_model):
    from models import get_category_model
    category_model = get_category_model(product_model)

    class AddCategoryForm(forms.ModelForm):
        class Meta:
            model = category_model
            fields = (
                'name',
            )

    return AddCategoryForm


class AddCategoryFormRenderer(FormRenderer):
    form_id = 'category-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='name')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')


class EditCategoryFormRenderer(FormRenderer):
    form_id = 'edit-category-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='name')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')
