# coding=utf-8

from django.utils import six

from django_helpers.core.views import DeleteAction
from django_helpers.core.views import FormAction
from django_helpers.core.views import ListAction
from django_helpers.core.views import View
from django_helpers.core.views import ViewMetaClass
from django_helpers.core.views import ViewDecorators
from forms import AddCategoryFormRenderer
from forms import EditCategoryFormRenderer


class MetaClass(ViewMetaClass):
    def __new__(mcs, name, bases, attrs):
        from forms import get_category_form
        from listable import get_category_listing
        from models import get_category_model
        
        product_model = attrs.get('product_model')
        
        obj = type.__new__(mcs, name, bases, attrs)
        
        if product_model is not None:
            category_model = get_category_model(product_model)
            setattr(obj, 'category_model', category_model)
            
            form_class = get_category_form(product_model)
            setattr(obj, 'category_from', form_class)
            
            listing_cls = get_category_listing(obj)
            setattr(obj, 'category_listing', listing_cls)
        
        return obj


class CategoryBaseView(six.with_metaclass(MetaClass, View)):
    url_name_prefix = "product-categories"
    
    url_pattern_prefix = "product-categories"
    
    product_model = None
    
    category_form = None
    
    category_listings = None
    
    add_category_form_render = AddCategoryFormRenderer
    
    edit_category_form_render = EditCategoryFormRenderer
    
    #
    # Buttons
    #
    @classmethod
    def edit_category_button(cls, user):
        return cls.get_delete_icon_button(user, action='edit_category')
    
    @classmethod
    def delete_category_button(cls, user):
        action = cls.get_delete_icon_button(user, action='delete_category')
        return action
    
    #
    # Getters
    #
    @classmethod
    def get_category_form(cls):
        from forms import get_category_form
        
        form = getattr(cls, 'category_from', None)
        if form is not None:
            return form
        
        form_class = get_category_form(cls.product_model)
        setattr(cls, 'category_from', form_class)
        
        return form_class
    
    @classmethod
    def get_category_listings(cls):
        from listable import get_category_listing
        
        form = getattr(cls, 'category_listing', None)
        if form is not None:
            return form
        
        listing_cls = get_category_listing(cls)
        setattr(cls, 'category_listing', listing_cls)
        
        return listing_cls
    
    #
    # Category Actions
    #
    @ViewDecorators.url('add-category', 'add/')
    @ViewDecorators.title('Add Category')
    def add_category(self):
        action = FormAction(self)
        
        action.form_class = self.get_category_form()
        action.renderer = self.add_category_form_render
        
        action.redirect_url = self.url_to_action('list_category')
        action.success_message = 'Category added successfully.'
        
        listings = self.get_category_listings()
        action.refresh_listing(listings.listing_id)
        
        return action.render()
    
    @ViewDecorators.url('edit-category', '(?P<pk>\d+)/edit/')
    @ViewDecorators.instance(id='pk', model='self.category_model')
    def edit_category(self, instance):
        action = FormAction(self)
        
        action.form_class = self.get_category_form()
        action.renderer = self.edit_category_form_render
        
        action.redirect_url = self.url_to_action('list_category')
        action.instance = instance
        action.success_message = 'Category updated successfully.'
        
        listings = self.get_category_listings()
        action.refresh_listing(listings.listing_id)
        
        return action.render()
    
    @ViewDecorators.url('list-category', 'list/')
    @ViewDecorators.title('List Categories')
    def list_category(self):
        action = ListAction(self)
        action.table_class = self.get_category_listings()
        action.template = 'e-commerce/categorizable/category.html'

        add_url = self.url_to_action('add_category')

        return action.render(add_url=add_url)
    
    @ViewDecorators.url('delete-category', '(?P<pk>\d+)/delete/')
    @ViewDecorators.instance(id='pk', model='self.category_model')
    def delete_category(self, instance):
        action = DeleteAction(self, instance=instance)
        
        action.redirect_link = self.url_to_action('list_category')
        action.cancel_link = self.url_to_action('list_category')
        action.success_message = 'Category deleted successfully.'
        action.title = 'Delete Category'
        
        listings = self.get_category_listings()
        action.refresh_listing(listings.listing_id)
        
        return action.render()
