# coding=utf-8

from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()

    unit_price = models.DecimalField()

    class Meta:
        abstract = True
