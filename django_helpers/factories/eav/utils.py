# coding=utf-8
from json import dumps
from django.db import transaction


@transaction.atomic()
def recreate_cache(model):
    """

    1. Update field cache
    2. Update the value cache

    :param model: The Entity Model in which caches has to be updated.
    """
    from eav import get_entity_type
    from eav import get_attrs_model
    from eav import get_values_model

    entity_type_model = get_entity_type(model)
    attribute_model = get_attrs_model(model)
    values_model = get_values_model(attribute_model)

    #
    # Update field cache
    #

    entity_types = entity_type_model.objects.all()
    for entity_type in entity_types:
        query = attribute_model.objects.filter(entity_type=entity_type)
        names = query.value_list('name')
        names = dumps(names)

        query = model.objects.filter(entity_type=entity_type)
        query.update(attribute_names_cache=names)

    #
    # Update the value cache
    #
    values = values_model.objects.filter()
    # TODO: Complete
