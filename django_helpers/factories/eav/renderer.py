# coding=utf-8
from django_helpers.apps.form_renderer import FormRenderer


class AddProductAttributeFormRenderer(FormRenderer):
    form_id = 'product-attribute-form'

    def setup(self):
        row = self.add_row()
        row.add_col(field='entity_type')

        row = self.add_row()
        row.add_col(field='label')

        row = self.add_row()
        row.add_col(field='name')

        row = self.add_row()
        row.add_col(field='type')

        row = self.add_row()
        row.add_col(field='required')

        row = self.add_row()
        row.add_col(field='required_msg')

        row = self.add_row()
        row.add_col(field='invalid_msg')

        row = self.add_row()
        row.add_col(field='min_length')

        row = self.add_row()
        row.add_col(field='min_length_msg')

        row = self.add_row()
        row.add_col(field='max_length')

        row = self.add_row()
        row.add_col(field='max_length_msg')

        row = self.add_row()
        row.add_col(field='min_value')

        row = self.add_row()
        row.add_col(field='min_value_msg')

        row = self.add_row()
        row.add_col(field='max_value')

        row = self.add_row()
        row.add_col(field='max_value_msg')

        row = self.add_row()
        row.add_col(field='is_searchable')

        row = self.add_row()
        row.add_col(field='is_sortable')

        self.hide_if_unchecked('required', 'required_msg')

        row = self.add_row()
        col = row.add_col()
        col.add_submit_button('Save')
