# coding=utf-8
from django.db.models import signals
from django.dispatch import receiver


@receiver(signals.post_save)
def save_attr_to_entity(sender, instance, *args, **kwargs):
    from eav import get_entity_model

    if not hasattr(instance, '__is_attribute_model__'):
        return

    entity_type_id = instance.entity_type_id
    entity_model = get_entity_model(sender)

    attr = getattr(entity_model, "_entity_type_field", None)
    kwargs = {
        attr + "_id": entity_type_id
    }

    entity_query = entity_model.objects.filter(**kwargs)

    if entity_query.count() == 0:
        return

    # TODO: Find method to prevent this on a single model
    # query = sender.objects.filter(entity_type_id=entity_type_id)
    # names = query.value_list('name')
    # names = dumps(names)

    # entity_query.update(attribute_names_cache=names)
    entity_query.update(attribute_names_cache='')


@receiver(signals.post_init)
def load_values_to_instance(sender, instance, *args, **kwargs):
    if not hasattr(instance, '__is_entity_model'):
        return

    instance.load_attributes()


@receiver(signals.pre_save)
def save_attrs_pre(sender, instance, *args, **kwargs):
    if not hasattr(instance, '__is_entity_model'):
        return

    instance.save_attributes()


@receiver(signals.post_save)
def save_attrs_post(sender, instance, *args, **kwargs):
    if not hasattr(instance, '__is_entity_model'):
        return

    if getattr(instance, '__save_attrs__', False):
        instance.save_attributes()
