# coding=utf-8
import copy
from collections import OrderedDict

from django import forms
from django.core.exceptions import FieldError, ValidationError
from django.db import transaction
from django.forms.forms import DeclarativeFieldsMetaclass
from django.forms.models import ModelFormOptions, ALL_FIELDS
from django.utils import six

field_mapping = {
    "int": forms.IntegerField,
    "char": forms.CharField,
    "date": forms.DateField,
    "time": forms.TimeField,
    "text": forms.CharField,
}


def get_formfield(self, form_class=None, choices_form_class=None, **kwargs):
    """
    Returns a django.forms.Field instance for this database Field.
    """
    defaults = {
        'required': self.required,
        'label': self.label,
        'help_text': self.help_text
    }

    default = self.default()
    if default:
        defaults['initial'] = default

    # if self.choices:
    #     # Fields with choices get special treatment.
    #     include_blank = self.blank or not (self.has_default() or 'initial' in kwargs)
    #
    #     defaults['choices'] = self.get_choices(include_blank=include_blank)
    #     defaults['coerce'] = self.to_python
    #
    #     if self.null:
    #         defaults['empty_value'] = None
    #
    #     if choices_form_class is not None:
    #         form_class = choices_form_class
    #     else:
    #         form_class = forms.TypedChoiceField

    for k in list(kwargs):
        allowed_items = ('coerce', 'empty_value', 'choices', 'required', 'widget', 'label', 'initial', 'help_text', 'error_messages', 'show_hidden_initial')
        if k not in allowed_items:
            del kwargs[k]

    defaults.update(kwargs)
    field_type = self.type

    if form_class is None:
        form_class = get_form_field(field_type)

    return form_class(**defaults)


def get_form_field(field_type):
    if field_type in field_mapping:
        return field_mapping[field_type]

    raise Exception("Invalid Form Type")


class EAVFormMetaclass(DeclarativeFieldsMetaclass):
    def __new__(mcs, name, bases, attrs):
        new_class = super(EAVFormMetaclass, mcs).__new__(mcs, name, bases, attrs)

        if bases == (BaseEAVForm,):
            return new_class

        opts = new_class._meta = ModelFormOptions(getattr(new_class, 'Meta', None))

        # We check if a string was passed to `fields` or `exclude`,
        # which is likely to be a mistake where the user typed ('foo') instead
        # of ('foo',)
        for opt in ['fields', 'exclude', 'localized_fields']:
            value = getattr(opts, opt)
            if isinstance(value, six.string_types) and value != ALL_FIELDS:
                msg = ("%(model)s.Meta.%(opt)s cannot be a string. "
                       "Did you mean to type: ('%(value)s',)?" % {
                           'model': new_class.__name__,
                           'opt': opt,
                           'value': value,
                       })
                raise TypeError(msg)

        new_class.base_fields = None

        return new_class


class BaseEAVForm(forms.BaseForm):
    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """

        # :type instance: django.db.models.Model
        instance = kwargs.pop('instance', None)
        initial = kwargs.pop('initial', None)

        if instance is None:
            raise ValidationError("EAVForm cannot be created with instance")

        fields = self.get_fields(instance)
        object_data = self.get_initial_data(fields, instance)

        if initial is not None:
            object_data.update(initial)

        kwargs['initial'] = object_data

        forms.BaseForm.__init__(self, *args, **kwargs)
        self.instance = instance
        self.fields = fields

    @classmethod
    def get_initial_data(cls, fields, instance):
        initial = {}
        attrs = instance.attrs
        for field in fields:
            value = attrs.get(field)
            if value is not None:
                initial[field] = value

        return initial

    @classmethod
    def get_fields(cls, instance):
        opts = getattr(cls, '_meta')
        declared_fields = cls.declared_fields

        exclude = opts.exclude
        fields = opts.fields
        widgets = opts.widgets
        localized_fields = opts.localized_fields
        labels = opts.labels
        help_texts = opts.help_texts
        error_messages = opts.error_messages

        field_list = []
        ignored = []

        attributes = instance.get_attributes()

        for field_name, f in attributes.items():
            if field_name in declared_fields:
                continue

            if fields is not None and field_name not in fields:
                continue

            if exclude and field_name in exclude:
                continue

            kwargs = {}
            if widgets and field_name in widgets:
                kwargs['widget'] = widgets[field_name]

            if localized_fields == ALL_FIELDS or (localized_fields and field_name in localized_fields):
                kwargs['localize'] = True

            if labels and field_name in labels:
                kwargs['label'] = labels[field_name]

            if help_texts and field_name in help_texts:
                kwargs['help_text'] = help_texts[field_name]

            if error_messages and field_name in error_messages:
                kwargs['error_messages'] = error_messages[field_name]

            formfield = get_formfield(f, **kwargs)

            if formfield:
                field_list.append((field_name, formfield))
            else:
                ignored.append(field_name)

        field_dict = OrderedDict(field_list)

        if fields:
            field_dict = OrderedDict(
                    [(f, field_dict.get(f)) for f in fields
                        if ((not exclude) or (exclude and f not in exclude)) and (f not in ignored)]
            )

            none_model_fields = [k for k, v in six.iteritems(field_dict) if not v]
            missing_fields = (set(none_model_fields) - set(declared_fields.keys()))
            if missing_fields:
                message = 'Unknown field(s) (%s) specified for %s'
                message = message % (', '.join(missing_fields), instance.__class__.__name__)
                raise FieldError(message)

        field_dict.update(copy.deepcopy(declared_fields))
        return field_dict

    @transaction.atomic()
    def save(self, commit=True):
        data = self.cleaned_data
        instance = self.instance
        for key, value in data.items():
            instance.set_attr(key, value)

        return self.instance


class EAVForm(six.with_metaclass(EAVFormMetaclass, BaseEAVForm)):
    pass
