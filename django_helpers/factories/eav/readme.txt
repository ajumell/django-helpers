
EAV Has 4 models

1. Entity Type
   -----------
   This represents the category or type or class of an entity. The
   type will define the attributes of an entity.


2. Attribute
   ---------
   The attribute defines a custom attribute of an entity. This will
   be related to entity type. Each entity type will have a set of
   attributes.


3. Attribute Choice
   ----------------
   Some attributes will be defined as a choice field. Then that attribute
   can define some choices. The choices and its values will be defined in
   this model.


4. Values
   ------
   The value model is where the value of each entity for custom attributes
   is saved.

