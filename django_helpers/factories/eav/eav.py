# coding=utf-8
from json import dumps

from django.db import models
from django.db import transaction
from django.utils import six

from django_helpers.factories.utils import create_model, ModelFactory, extend_model

# noinspection PyUnresolvedReferences
import signals

fields = (
    ('char', 'Character'),
    ('bool', 'Boolean'),
    ('int', 'Integer'),
    ('date', 'Date'),
    ('time', 'Time'),
    ('text', 'Text'),
)

ATTRIBUTE_VALUE_CACHE = 'attribute_value_cache'

ATTRIBUTE_NAMES_CACHE = 'attribute_names_cache'


class EntityModel(ModelFactory):
    def get_attributes(self, force=False):
        # Exit early if primary key is not set
        # If force is set then it should go further.
        if not self.pk and not force:
            return

        key = ATTRIBUTE_NAMES_CACHE
        obj_key = '{}_obj'.format(ATTRIBUTE_NAMES_CACHE)
        if not getattr(self, key, ''):
            attributes_model = get_attrs_model(self)

            # noinspection PyProtectedMember
            entity_type = getattr(self, self._entity_type_field, None)

            # Exit if entity type is not set
            if entity_type is None:
                return

            cache = entity_type.get_attributes_str()
            setattr(self, key, cache)

        if not getattr(self, obj_key, ''):
            from json import loads

            obj = loads(getattr(self, key, ''))
            setattr(self, obj_key, obj)

        return getattr(self, obj_key, None)

    def load_attributes(self):
        # Exit early if primary key is not set
        if not self.pk:
            return

        key = ATTRIBUTE_VALUE_CACHE
        obj_key = '{}_obj'.format(ATTRIBUTE_VALUE_CACHE)

        if not getattr(self, key, ''):
            attributes = self.get_attributes()

            values_model = get_values_model(self)
            query = values_model.objects.filter(
                    entity=self
            )

            data = {}
            for record in query:
                data[record.name] = record.value()

            setattr(self, key, dumps(data))

        if not getattr(self, obj_key, ''):
            from json import loads

            json_str = getattr(self, key, '')
            obj = loads(json_str)

            for k, v in obj.items():
                setattr(self, k, v)

            setattr(self, obj_key, obj)

        return getattr(self, obj_key, None)

    @transaction.atomic()
    def save_attributes(self, *names):

        attrs = self.get_attributes(True)

        if not attrs:
            return

        if not names:
            names = attrs.keys()

        data = {}
        for name in names:
            val = getattr(self, name, None)
            data[name] = val

        setattr(self, ATTRIBUTE_VALUE_CACHE, dumps(data))

        # If data is not saved yet then dont
        # Proceed further.
        if not self.pk:
            setattr(self, '__save_attrs__', True)
            return

        cls = get_values_model(self)
        for name in names:
            val = getattr(self, name, None)
            attr = attrs[name]

            try:
                instance = cls.objects.get(
                        entity=self,
                        attribute_id=attr['id']
                )
            except cls.DoesNotExist:
                instance = cls(
                        entity=self,
                        attribute_id=attr['id']
                )

            instance.type = attr['type']
            instance.name = attr['name']
            instance.set_value(val)

            instance.save()

    def __setattr__(self, key, value):
        models.Model.__setattr__(self, key, value)

    def set_attr(self, name, value):
        values = self.get_values()
        values_objs = getattr(self, '_eav_values_obj_cache', None)
        attrs = getattr(self, '_eav_attrs', None)

        is_new = name not in values_objs
        if is_new:
            attr = attrs[name]
            cls = get_values_model(self)
            instance = cls()

            instance.entity = self
            instance.attribute = attr
            instance.type = attr.type
            instance.name = attr.name
        else:
            instance = values_objs[name]

        prev_value = None
        if not is_new:
            prev_value = values[name]

        instance.set_value(value)
        if is_new or prev_value != value:
            instance.save()

        values[name] = value


class EntityTypeModel(ModelFactory):
    name = models.CharField(max_length=255)
    description = models.TextField()

    def __unicode__(self):
        return self.name

    def get_attributes(self):
        attribute_model_name = self.attribute_model_name
        attribute_model_name = attribute_model_name.lower()
        queryset_name = attribute_model_name + "_set"

        query = getattr(self, queryset_name, None)

        if query is None:
            raise Exception()

        query = query.all()

        return query

    def get_attributes_str(self):
        from json import dumps

        query = self.get_attributes()
        result = {}
        for record in query:
            result[record.name] = {
                'id': record.id,

                'name': record.name,
                'type': record.type,
            }

        return dumps(result)


class AttributeModel(ModelFactory):
    __is_attribute_model__ = True

    type = models.CharField(max_length=255, choices=fields)

    name = models.CharField(max_length=255)
    label = models.CharField(max_length=255)
    help_text = models.CharField(max_length=255, default='', blank=True)

    #
    # Validations
    #
    required = models.BooleanField(default=True)
    required_msg = models.CharField(default='This field is required', max_length=255)

    invalid_msg = models.CharField(default='Please enter correct value', max_length=255)

    min_length = models.IntegerField(default=0)
    min_length_msg = models.CharField(default='Please enter correct value', max_length=255)

    max_length = models.IntegerField(default=0)
    max_length_msg = models.CharField(default='Please enter correct value', max_length=255)

    min_value = models.IntegerField(default=0)
    min_value_msg = models.CharField(default='Please enter correct value', max_length=255)

    max_value = models.IntegerField(default=0)
    max_value_msg = models.CharField(default='Please enter correct value', max_length=255)

    #
    # Misc
    #
    is_searchable = models.BooleanField(default=False)
    is_sortable = models.BooleanField(default=False)

    def __unicode__(self):
        return "{} ({})".format(self.name, self.type)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.pk:
            values_model = get_values_model(self)
            query = values_model.objects.filter(attribute=self)
            query.update(name=self.name, type=self.type)

        models.Model.save(self, force_insert, force_update, using, update_fields)

    def default(self):
        return None


class AttributeChoiceModel(ModelFactory):
    label = models.CharField(max_length=255)
    value = models.CharField(max_length=255)

    def __unicode__(self):
        return "{} => {}".format(self.label, self.value)


class ValueModel(ModelFactory):
    int_value = models.IntegerField(blank=True, null=True)
    char_value = models.CharField(max_length=255, blank=True)
    date_value = models.DateField(null=True)
    time_value = models.TimeField(null=True)
    bool_value = models.BooleanField(default=False)
    text_value = models.TextField(null=True)

    # Cache the data type and name in the
    # value model to reduce queries.
    type = models.CharField(max_length=255)
    name = models.CharField(max_length=255)

    def value(self):
        return getattr(self, "{}_value".format(self.type), None)

    def set_value(self, val):
        return setattr(self, "{}_value".format(self.type), val)

    def load_value(self, entity):
        setattr(entity, self.name, self.value())


# noinspection PyUnusedLocal
class EAVFactory(object):
    def __init__(self, model):
        meta = getattr(model, '_meta')

        self.model = model
        self.model_name = meta.object_name
        self.module = model.__module__

        self.entity_type = self.create_entity_type_model()
        self.attribute_model = self.create_attribute_model()
        self.attribute_choice_model = self.create_attribute_choice_model()
        self.value_model = self.create_value_model()

        model._entity_type_model = self.entity_type
        model._attrs_model = self.attribute_model
        model._choices_model = self.attribute_choice_model
        model._values_model = self.value_model

        self.attribute_model._values_model = self.value_model
        self.attribute_model._entity_model = self.model

        self.add_extra_attributes()

    def create_entity_type_model(self):
        attrs = {
            '__module__': self.module,
            'attribute_model_name': "{}Attribute".format(self.model_name),
        }

        name = "{}Type".format(self.model_name)
        model = EntityTypeModel.create(attrs, name)

        return model

    def create_attribute_model(self):
        attrs = {
            '__module__': self.module,
            'entity_type': models.ForeignKey(self.entity_type)
        }

        name = "{}Attribute".format(self.model_name)
        model = AttributeModel.create(attrs, name)

        return model

    def create_attribute_choice_model(self):
        attrs = {
            '__module__': self.module,
            'attribute': models.ForeignKey(self.attribute_model),
        }
        name = "{}AttributeChoice".format(self.model_name)
        model = AttributeChoiceModel.create(attrs, name)

        return model

    def create_value_model(self):
        attrs = {
            '__module__': self.module,

            'entity': models.ForeignKey(self.model),
            'attribute': models.ForeignKey(self.attribute_model),
        }

        name = "{}Value".format(self.model_name)
        model = ValueModel.create(attrs, name)

        return model

    def add_extra_attributes(self):
        model = self.model

        field_name = "{}_type".format(self.model_name.lower())

        attrs = {
            field_name: models.ForeignKey(self.entity_type),
            ATTRIBUTE_NAMES_CACHE: models.TextField(default=''),
            ATTRIBUTE_VALUE_CACHE: models.TextField(default=''),

            '__is_entity_model': True,
            '_entity_type_field': field_name
        }

        attrs.update(EntityModel.attrs)

        extend_model(model, attrs)


#
# Getters
#

def get_cls(obj):
    if isinstance(obj, models.Model):
        obj = obj.__class__

    return obj


def get_cls_attr(obj, name, default=None):
    obj = get_cls(obj)

    return getattr(obj, name, default)


def get_entity_type_model(obj):
    return get_cls_attr(obj, '_entity_type_model')


def get_attrs_model(obj):
    return get_cls_attr(obj, '_attrs_model')


def get_entity_model(obj):
    return get_cls_attr(obj, '_entity_model')


def get_values_model(obj):
    return get_cls_attr(obj, '_values_model')


def get_entity_type(obj):
    field_name = get_cls_attr(obj, '_entity_type_field')
    if field_name is None:
        return None

    return getattr(obj, field_name, None)


#
# Factory
#
def eav(model):
    EAVFactory(model)
    return model
