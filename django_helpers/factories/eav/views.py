# coding=utf-8
from django_helpers.core.views import FormAction


class EAVFormAction(FormAction):
    def save_form(self, form):
        kwargs = {}
        args = []
        if self.need_request_on_save:
            kwargs['request'] = self.request

        # Append False to prevent saving.
        if self.is_model_form:
            args.append(False)

        obj = form.save(*args, **kwargs)

        if hasattr(self.save, '__call__'):
            self.save(form, obj)

        return obj
