# coding=utf-8

from django.db import models
from django.utils import six


def create_model(name, attrs, meta=None):
    """
    :rtype: models.Model
    """
    if meta is not None:
        meta_class = type('Meta', dict=meta)
        attrs['Meta'] = meta_class

    return type(name, (models.Model,), attrs)


def extend_model(model, attrs):
    for k, v in attrs.items():
        model.add_to_class(k, v)


@classmethod
def _extend(cls, **kwargs):
    for k, v in kwargs.items():
        cls.add_to_class(k, v)


class ModelCreator(object):
    def __init__(self, attrs, name=None):
        self.attrs = attrs
        self.name = name

    def create(self, attrs=None, name=None, meta=None):
        if attrs is None:
            attrs = {}

        attrs.update(self.attrs)

        if name is None:
            name = self.name

        if name is None:
            raise Exception("Name is required")

        model = create_model(name, attrs, meta)
        model.add_to_class('extend', _extend)

        return model


class _ModelFactory(type):
    def __new__(mcs, name, bases, attrs):
        key = '__module__'

        if key in attrs:
            attrs.pop(key)

        return ModelCreator(attrs, name)


ModelFactory = six.with_metaclass(_ModelFactory, models.Model)
