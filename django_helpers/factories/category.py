# coding=utf-8
from django.db import models
from utils import create_model


def create_category_model(prefix, module):
    attrs = {
        '__module__': module,

        'name': models.CharField(max_length=255),
        'description': models.TextField()
    }

    name = "{}Category".format(prefix)
    return create_model(name, attrs)


def update(model):
    meta = getattr(model, '_meta')
    prefix = meta.label
    module = model.__module__

    category = create_category_model(prefix, module)

    field_name = "category".format(prefix.lower())
    model.add_to_class(field_name, models.ForeignKey(category))

    return model
