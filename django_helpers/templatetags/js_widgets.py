# coding=utf-8

from django import template

from django_helpers.js_plugins.core import ModalLinkPlugin
from django_helpers.js_plugins.core import AjaxLinkPlugin
from django_helpers.js_plugins.core import HotKeyPlugin
from django_helpers.js_plugins.core import ConfirmPlugin

register = template.Library()


@register.simple_tag()
def modal(*args, **kwargs):
    plugin = ModalLinkPlugin()
    plugin.is_ajax = True
    if len(args) > 0:
        plugin.title = args[0]

    return plugin.render()


@register.simple_tag()
def confirm(question, title=None, yes_text="Yes", no_text="No", yes_color="success", no_color=""):
    plugin = ConfirmPlugin()
    plugin.need_confirm = True

    plugin.question = question
    plugin.title = title

    plugin.yes_text = yes_text
    plugin.yes_color = yes_color

    plugin.no_text = no_text
    plugin.no_color = no_color

    return plugin.render()


@register.simple_tag()
def hotkey(*args, **kwargs):
    if len(args) > 0:
        plugin = HotKeyPlugin(args[0])
        return plugin.render()

    return ""


@register.simple_tag()
def ajax(*args, **kwargs):
    plugin = AjaxLinkPlugin()
    params = ["question", "yes_text", "no_text", "yes_color", "no_color", "loading"]

    for param in params:
        if param in kwargs:
            value = kwargs.get(param)
            print param, value
            setattr(plugin, param, value)

    if kwargs.get("question"):
        plugin.need_confirm = True

    return plugin.render()
