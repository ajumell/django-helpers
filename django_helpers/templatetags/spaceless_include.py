# coding=utf-8
from django import template
from django.template.loader_tags import do_include, IncludeNode

from helpers import remove_breaks

register = template.Library()


class SpacelessIncludeNode(IncludeNode):
    def __init__(self, template_name, *args, **kwargs):
        IncludeNode.__init__(self, *args, **kwargs)
        self.template = template_name

    def render(self, context):
        if not self.template:
            return ''

        op = self.render(context)
        return remove_breaks(op)


@register.tag('spaceless_include')
def spaceless_include(parser, token):
    node = do_include(parser, token)
    args_dict = {
        "extra_context": node.extra_context,
        "isolated_context": node.isolated_context,
        "template": node.template
    }
    return SpacelessIncludeNode(**args_dict)
