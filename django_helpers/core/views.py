# coding=utf-8

from inspect import getargspec
from types import StringTypes

from django.contrib import messages
from django.contrib.messages import get_messages
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse, NoReverseMatch
from django.db import transaction
from django.http import HttpResponseForbidden, HttpResponse
from django.shortcuts import redirect, render
from django.utils import six
from django.utils.decorators import classonlymethod
from django.views.decorators.csrf import csrf_exempt as real_csrf_exempt

from django_helpers.apps.form_renderer import FormRenderer
from django_helpers.utils.models import get_model
from django_helpers.widgets.ajax.widget import widget_id


class Action(object):
    ajax_only = False
    title = None
    page_heading = None
    
    template = None
    ajax_template = None
    theme_template = None
    
    def __init__(self, view=None, request=None):
        if request is None and view is not None:
            request = view.get_request()
        
        self.request = request
        self.view = view
        self.buttons = {}
    
    def add_button(self, slot, btn):
        if slot not in self.buttons:
            self.buttons[slot] = []
        
        self.buttons[slot].append(btn)
    
    def render_json(self, data=None):
        self.view.render_json(data)
    
    def render(self, **context):
        if self.is_ajax():
            renderer = JSONResponse(self.request)
            
            # renderer.refresh_table()
            # renderer.refresh_listing()
            # renderer.close_modal()
            
            self.add_refresh_items(renderer)
            
            return renderer
        
        return self.render_template(context)
    
    def render_template(self, context, template=None):
        self.update_context(context)
        
        if template is None:
            template = self.get_template()
        
        return self.view.render(template, context)
    
    def redirect(self, redirect_url):
        return self.view.redirect(redirect_url)
    
    def get_template(self):
        is_ajax = self.is_ajax()
        
        if is_ajax and self.ajax_template is not None:
            return self.ajax_template
        
        if self.template is not None:
            return self.template
        
        if self.theme_template:
            theme = self.view.theme
            template = getattr(theme, self.theme_template, None)
            if template is not None:
                self.template = template
                
                return template
        
        raise ValueError('Template Not Found')
    
    def is_ajax(self):
        request = self.request
        return request.is_ajax()
    
    def is_modal(self):
        request = self.request
        is_m = request.META.get('HTTP_DJ_MODAL', False)
        if is_m:
            is_m = True
        return is_m
    
    def update_context(self, context):
        heading = self.page_heading
        title = self.title
        if 'title' not in context and title is not None:
            context['title'] = title
        
        if 'page_heading' not in context and heading is not None:
            context['page_heading'] = heading
        
        context['view_buttons'] = self.buttons
    
    def get_title(self, *instances):
        return self.title
    
    # noinspection PyUnusedLocal
    def get_page_heading(self, *instances):
        return self.page_heading
    
    #
    # AJAX Extras Pubsub
    #
    
    #
    # Listable Class
    #
    
    # noinspection PyAttributeOutsideInit
    def get_refresh_listable_class_pubsub(self):
        if not hasattr(self, '_refresh_listable_class_pubsub'):
            self._refresh_listable_class_pubsub = {}
        return self._refresh_listable_class_pubsub
    
    def refresh_listable_class_pubsub(self, name, **params):
        listable_class = self.get_refresh_listable_class_pubsub()
        listable_class[name] = params
    
    def ajax_add_refresh_listable_class_pubsub(self, renderer):
        for name, params in self.get_refresh_listable_class_pubsub().items():
            selector = '[data-listable-class={}]'.format(name)
            selectors = [selector]
            for key, value in params.items():
                selector = '[data-listable-{}={}]'.format(key.replace("_", '-'), value)
                selectors.append(selector)
            
            renderer.refresh_listable_class("".join(selectors))
            
            #
    
    # Listings
    #
    
    # noinspection PyAttributeOutsideInit
    def get_refresh_listings_pubsub(self):
        if not hasattr(self, '_refresh_listings_pubsub'):
            self._refresh_listings_pubsub = {}
        return self._refresh_listings_pubsub
    
    def refresh_listing_pubsub(self, name, **params):
        listings = self.get_refresh_listings_pubsub()
        listings[name] = params
    
    def ajax_add_refresh_listing_pubsub(self, renderer):
        for name, params in self.get_refresh_listings_pubsub().items():
            renderer.refresh_listing(widget_id(params, name))
    
    #
    # Tables
    #
    
    # noinspection PyAttributeOutsideInit
    def get_refresh_tables_pubsub(self):
        if not hasattr(self, '_refresh_tables_pubsub'):
            self._refresh_tables_pubsub = {}
        return self._refresh_tables_pubsub
    
    def refresh_table_pubsub(self, name, **params):
        tables = self.get_refresh_tables_pubsub()
        tables[name] = params
    
    def ajax_add_refresh_table_pubsub(self, renderer):
        for name, params in self.get_refresh_tables_pubsub().items():
            renderer.refresh_table(widget_id(params, name))
    
    #
    # Update Widgets
    #
    
    # noinspection PyAttributeOutsideInit
    def get_update_widgets_pubsub(self):
        if not hasattr(self, '_update_widgets_pubsub'):
            self._update_widgets_pubsub = []
        return self._update_widgets_pubsub
    
    def update_widget_pubsub(self, widget):
        tables = self.get_update_widgets_pubsub()
        tables.append(widget)
    
    def ajax_add_update_widget_pubsub(self, renderer):
        for widget in self.get_update_widgets_pubsub():
            html_id = widget.widget_id()
            html = widget.html()
            
            renderer.update_widget(html_id, html)
    
    #
    # AJAX Extras
    #
    
    #
    # Listable Class
    #
    
    # noinspection PyAttributeOutsideInit
    def get_refresh_listable_class(self):
        if not hasattr(self, '_refresh_listable_class'):
            self._refresh_listable_class = {}
        return self._refresh_listable_class
    
    def refresh_listable_class(self, name, **params):
        listable_class = self.get_refresh_listable_class()
        listable_class[name] = params
    
    def ajax_add_refresh_listable_class(self, renderer):
        for name, params in self.get_refresh_listable_class().items():
            selector = '[data-listable-class={}]'.format(name)
            selectors = [selector]
            for key, value in params.items():
                selector = '[data-listable-{}={}]'.format(key.replace("_", '-'), value)
                selectors.append(selector)
            
            renderer.refresh_listable_class("".join(selectors))
    
    #
    # Listings
    #
    
    # noinspection PyAttributeOutsideInit
    def get_refresh_listings(self):
        if not hasattr(self, '_refresh_listings'):
            self._refresh_listings = {}
        return self._refresh_listings
    
    def refresh_listing(self, name, **params):
        listings = self.get_refresh_listings()
        listings[name] = params
    
    def ajax_add_refresh_listing(self, renderer):
        for name, params in self.get_refresh_listings().items():
            renderer.refresh_listing(widget_id(params, name))
    
    #
    # Tables
    #
    
    # noinspection PyAttributeOutsideInit
    def get_refresh_tables(self):
        if not hasattr(self, '_refresh_tables'):
            self._refresh_tables = {}
        return self._refresh_tables
    
    def refresh_table(self, name, **params):
        tables = self.get_refresh_tables()
        tables[name] = params
    
    def ajax_add_refresh_table(self, renderer):
        for name, params in self.get_refresh_tables().items():
            renderer.refresh_table(widget_id(params, name))
    
    #
    # Update Widgets
    #
    
    # noinspection PyAttributeOutsideInit
    def get_update_widgets(self):
        if not hasattr(self, '_update_widgets'):
            self._update_widgets = []
        return self._update_widgets
    
    def update_widget(self, widget):
        tables = self.get_update_widgets()
        tables.append(widget)
    
    def ajax_add_update_widget(self, renderer):
        for widget in self.get_update_widgets():
            html_id = widget.widget_id()
            html = widget.html()
            
            renderer.update_widget(html_id, html)
    
    #
    # Update Renderer
    #
    def add_refresh_items(self, renderer):
        self.ajax_add_refresh_listing(renderer)
        self.ajax_add_refresh_listable_class(renderer)
        self.ajax_add_refresh_table(renderer)
        self.ajax_add_update_widget(renderer)
        
        self.publish_refresh_items()
    
    def publish_refresh_items(self):
        pubsub = self.view.pubsub
        if pubsub:
            pubsub_render = JSONResponse(self.request)
            
            self.ajax_add_refresh_listing_pubsub(pubsub_render)
            self.ajax_add_refresh_listable_class_pubsub(pubsub_render)
            self.ajax_add_refresh_table_pubsub(pubsub_render)
            self.ajax_add_update_widget_pubsub(pubsub_render)
            
            pubsub.publish_django_helpers(pubsub_render)


class PreviewAction(Action):
    instance = None
    theme_template = 'FORM_TEMPLATE'
    
    def render(self, **context):
        instance = self.instance
        if self.is_ajax():
            return HttpResponse(instance.render())
        
        context['form'] = instance
        return Action.render(self, **context)


# noinspection PyMethodMayBeStatic
class FormAction(Action):
    current_user_field = None
    extra_data = None
    form_class = None
    instance = None
    redirect_on_ajax = False
    redirect_url = None
    initial = None
    
    error_template = None
    
    success_message = None
    success_template = None
    renderer = None
    
    save = None
    
    need_request_on_save = False
    need_request_on_init = False
    
    disable_renderer = False
    add_form_data_to_context = False
    form_data_prefix = 'form'
    
    is_model_form = True
    
    def __init__(self, view=None, request=None, form=None):
        Action.__init__(self, view, request)
        self.form = form
        self.is_renderer = None
        self.init_args = {}
        
        self.hidden_fields = []
        self.visible_fields = []
        
        self.has_saved = False
        self.has_saved_variable = None
    
    #
    # Extras
    #
    def add_extra(self, name, value):
        if self.extra_data is None:
            self.extra_data = {}
        
        self.extra_data[name] = value
    
    def add_init_args(self, name, value):
        if self.init_args is None:
            self.init_args = {}
        
        self.init_args[name] = value
    
    #
    # Getters
    #
    def get_template(self):
        template = self.template
        
        if template is not None:
            return self.template
        
        theme = self.view.theme
        template = theme.FORM_TEMPLATE
        self.template = template
        
        return template
    
    def get_form(self):
        if self.form:
            return self.form
        
        form_class = self.form_class
        request = self.request
        instance = self.instance
        
        # If the passed instance is a form render instance
        if hasattr(form_class, 'form'):
            self.is_renderer = True
            return form_class(request=request, instance=instance)
        
        argspec = getargspec(form_class.__init__)
        arg_names = argspec.args
        kwargs = {}
        
        kwargs.update(self.init_args)
        
        if 'request' in arg_names:
            kwargs['request'] = request
        
        if instance is not None:
            kwargs['instance'] = instance
        
        initial = self.initial
        if initial is not None:
            kwargs['initial'] = initial
        
        kwargs['data'] = request.POST or None
        kwargs['files'] = request.FILES or None
        
        form = form_class(**kwargs)
        self.form = form
        
        self._hide_fields()
        self._only_fields()
        
        return form
    
    def _hide_fields(self):
        fields = self.hidden_fields
        if not fields:
            return
        
        if self.visible_fields:
            return
        
        form = self.form
        for name in fields:
            del form.fields[name]
    
    def _only_fields(self):
        fields = self.visible_fields
        if not fields:
            return
        
        form = self.form
        for name in form.fields:
            if name not in fields:
                del form.fields[name]
    
    def hide_fields(self, *fields):
        self.hidden_fields.extend(fields)
    
    def only_fields(self, *fields):
        self.visible_fields.extend(fields)
    
    def get_form_id(self, form):
        from django.utils.text import slugify
        
        title = form.__class__.__name__
        form_id = slugify(unicode(title))
        return form_id
    
    #
    # Rendering
    #
    def render(self, **context):
        form = self.get_form()
        
        if form.is_valid():
            obj = self.save_form(form)
            self.has_saved = True
            self.add_success_message(obj)
            return self.render_success(obj, **context)
        
        return self.render_form(form, **context)
    
    def add_success_message(self, obj):
        success_message = self.success_message
        if success_message is not None:
            self.view.success_message(success_message, obj)
    
    def render_success(self, obj=None, **context):
        redirect_url = self.redirect_url
        if hasattr(redirect_url, '__call__'):
            redirect_url = redirect_url(obj)
            self.redirect_url = redirect_url
        
        has_saved_variable = self.has_saved_variable
        if has_saved_variable is not None:
            context[has_saved_variable] = True
        
        if self.request.is_ajax():
            return self.render_success_ajax(obj, **context)
        
        template = self.success_template
        if template is not None:
            return self.render_template(context, template)
        
        return self.redirect(redirect_url)
    
    def render_form(self, form=None, **context):
        request = self.request
        if form is None:
            form = self.get_form()
        
        if not self.disable_renderer:
            form = self.get_renderer(form, request)
        
        ajax = self.is_ajax() or self.ajax_only
        if ajax:
            if request.method == 'POST':
                form_instance = form.instance
                return self.render_ajax_errors(form_instance)
            else:
                return HttpResponse(form.render())
        
        if self.add_form_data_to_context:
            cleaned_data = form.cleaned_data
            for key, value in cleaned_data.items():
                context[self.form_data_prefix + "_" + key] = value
        
        context['form'] = form
        return Action.render(self, **context)
    
    def get_renderer(self, form, request):
        if not hasattr(form, 'form'):
            form_id = self.get_form_id(form)
            if self.renderer:
                form = self.renderer(form, request)
            else:
                form = FormRenderer(form, request, form_id)
        
        form.form_action = request.get_full_path()
        
        return form
    
    #
    # AJAX
    #
    def render_ajax_errors(self, form, msg="Please correct errors.", extra_dict=None):
        return self.render_ajax_json(form, msg, True, extra_dict)
    
    def render_success_ajax(self, form, msg="Form submitted successfully.", extra_dict=None):
        return self.render_ajax_json(form, msg, False, extra_dict)
    
    def render_ajax_json(self, form, msg, err, extra_dict=None):
        d = {
            "err": err,
            "success": not err,
            "message": msg
        }
        
        if extra_dict is not None:
            d.update(extra_dict)
        
        renderer = JSONResponse(self.request, d)
        
        if self.redirect_on_ajax:
            renderer.redirect(self.redirect_url)
        
        if err:
            renderer.add("err_type", 'FORM_ERR')
            renderer.add("errors", form.errors)
        else:
            renderer.refresh_table()
            renderer.refresh_listing()
            renderer.close_modal()
            
            self.add_refresh_items(renderer)
        
        return renderer
    
    #
    # Saving
    #
    def save_form(self, form):
        kwargs = {}
        args = []
        if self.need_request_on_save:
            kwargs['request'] = self.request
        
        # Append False to prevent saving.
        if self.is_model_form:
            args.append(False)
        
        obj = form.save(*args, **kwargs)
        
        if hasattr(self.save, '__call__'):
            self.save(form, obj)
        
        if self.is_model_form and obj is not None:
            self.set_current_user(obj)
            self.save_extras(obj)
            self.save_m2m(form, obj)
        
        return obj
    
    def save_m2m(self, form, obj):
        if obj is None:
            return
        
        try:
            with transaction.atomic():
                obj.validate_unique()
                obj.save()
                
                if hasattr(form, 'save_m2m'):
                    form.save_m2m()
                
                if hasattr(form, 'save_array_fields'):
                    form.save_array_fields()
        
        except ValidationError, ex:
            error_dict = getattr(ex, "error_dict", None)
            if error_dict is not None:
                # noinspection PyUnresolvedReferences
                for k, m in error_dict.items():
                    form.add_error(k, m)
            else:
                form.add_error("__all__", ex.message)
            
            raise
    
    def save_extras(self, obj):
        kwargs = self.extra_data
        if kwargs:
            for key, value in kwargs.items():
                setattr(obj, key, value)
    
    def set_current_user(self, obj):
        current_user_field = self.current_user_field
        if current_user_field:
            request = self.request
            user = request.user
            if type(current_user_field) in (list, tuple):
                for field in current_user_field:
                    setattr(obj, field, user)
            else:
                setattr(obj, current_user_field, user)


class ListAction(Action):
    list_class = None
    listable = None
    table_class = None
    
    def __init__(self, view=None, request=None, table=None, params=None):
        Action.__init__(self, view, request)
        if params is None:
            params = {}
        
        self.params = params
        self.table = table
    
    def set_params(self, **params):
        self.params.update(params)
    
    def get_listable(self):
        """
        Returns the data table instance for the current action.
        """
        if self.table is not None:
            return self.table
        
        request = self.request
        table_class = self.table_class
        table = table_class(request)
        if self.params:
            table.set_params(self.params)
        
        self.table = table
        return table
    
    def get_template(self):
        template = self.template
        
        if template is not None:
            return self.template
        
        theme = self.view.theme
        template = theme.DATA_TABLE_TEMPLATE
        self.template = template
        
        return template
    
    def render(self, **context):
        if self.is_ajax():
            return self.render_ajax(**context)
        table = self.get_listable()
        if table not in context:
            context['table'] = table
        
        return Action.render(self, **context)
    
    # noinspection PyUnusedLocal
    def render_ajax(self, **context):
        table = self.get_listable()
        
        return HttpResponse(table.render())


class InstanceAction(Action):
    success_message = None
    redirect_link = None
    cancel_link = None
    default_title = None
    redirect_on_ajax = False
    
    def __init__(self, view=None, request=None, instance=None):
        Action.__init__(self, view, request)
        self.instance = instance
    
    def render(self, **context):
        request = self.request
        instance = self.instance
        cancel_link = self.cancel_link
        
        if request.method == 'POST':
            self.action()
            return self.success_response(instance)
        
        elif request.is_ajax():
            self.action()
            return self.success_ajax_response(instance)
        
        title = self.title
        page_heading = self.page_heading
        
        if not title:
            title = self.get_title(instance)
        
        if not page_heading:
            page_heading = title
        
        return Action.render(self, **{
            'title': title,
            'page_heading': page_heading,
            'obj': instance,
            'return_url': cancel_link
        })
    
    def success_ajax_response(self, obj):
        response = JSONResponse(self.request)
        response.refresh_table()
        
        self.add_refresh_items(response)
        
        return response
    
    # noinspection PyUnusedLocal
    def success_response(self, instance):
        redirect_link = self.redirect_link
        op = self.redirect(redirect_link)
        
        return op
    
    def action(self):
        raise NotImplementedError()
    
    def get_title(self, *instances):
        instance = instances[0]
        name = str(instance)
        
        return self.view.get_title(instance, default=self.default_title % name)


class DeleteAction(InstanceAction):
    theme_template = 'DELETE_TEMPLATE'
    default_title = 'Delete %s'
    
    def success_ajax_response(self, obj):
        r = InstanceAction.success_ajax_response(self, obj)
        r.add('remove-item')
        
        if self.redirect_on_ajax:
            r.redirect(self.redirect_link)
        
        return r
    
    def action(self):
        instance = self.instance
        self.view.delete_instance(instance, self.success_message)


class EnableAction(InstanceAction):
    theme_template = 'DELETE_TEMPLATE'
    default_title = 'Enable %s'
    
    def action(self):
        instance = self.instance
        self.view.enable_instance(instance, self.success_message)


class DisableAction(InstanceAction):
    theme_template = 'DELETE_TEMPLATE'
    default_title = 'Disable %s'
    
    def action(self):
        instance = self.instance
        self.view.disable_instance(instance, self.success_message)


class AuditAction(InstanceAction):
    default_template = 'AUDIT_TEMPLATE'
    default_title = 'Audit %s'
    
    def action(self):
        instance = self.instance
        self.view.delete_instance(instance, self.success_message)


class PhotoCropAction(Action):
    instance = None
    
    photo_field = None
    
    cropped_field = None
    
    cancel_url = None
    
    success_url = None
    
    cropper_class = None
    
    min_height = None
    
    min_width = None
    
    aspectRatio = None
    
    theme_template = 'PHOTO_CROPPER_TEMPLATE'
    
    def __init__(self, view=None, request=None):
        Action.__init__(self, view, request)
        self._photo = None
        self._cropper = None
    
    def get_photo(self):
        if self._photo:
            return self._photo
        
        instance = self.instance
        photo_field = self.photo_field
        
        photo = getattr(instance, photo_field)
        self._photo = photo
        
        return photo
    
    def get_cropper(self):
        from django_helpers.apps.jcrop import Cropper
        if self._cropper:
            return self._cropper
        
        cropper_class = self.cropper_class
        
        if cropper_class is None:
            cropper_class = Cropper
        
        photo = self.get_photo()
        cropper = cropper_class(photo.url, photo.width, photo.height, self.request)
        cropper.cancel_url = self.cancel_url
        cropper.action_url = self.request.get_full_path()
        
        if self.aspectRatio is not None:
            cropper.aspectRatio = self.aspectRatio
        
        if self.min_height is not None:
            cropper.min_height = self.min_height
        
        if self.min_width is not None:
            cropper.min_width = self.min_width
        
        self._cropper = cropper
        
        return cropper
    
    def save(self):
        cropper = self.get_cropper()
        photo = self.get_photo()
        photo.open()
        cropped = cropper.crop_image(photo)
        cropped_field = self.cropped_field
        instance = self.instance
        setattr(instance, cropped_field, cropped)
        instance.save()
    
    def render(self, **context):
        cropper = self.get_cropper()
        if cropper.is_valid():
            self.save()
            return self.render_success()
        
        context['cropper'] = cropper
        return self.render_cropper(context)
    
    def success_redirect(self):
        return self.redirect(self.success_url)
    
    def render_cropper(self, context):
        if self.is_ajax():
            return self.render_cropper_ajax(context)
        return Action.render(self, **context)
    
    # noinspection PyUnusedLocal
    def render_cropper_ajax(self, context):
        return HttpResponse(self.get_cropper().render())
    
    # noinspection PyUnusedLocal
    def render_success(self, **context):
        if self.is_ajax():
            return self.render_success_json()
        return self.success_redirect()
    
    def render_success_json(self):
        d = {
            "err": False,
            "success": True,
        }
        
        renderer = JSONResponse(self.request, d)
        renderer.refresh_table()
        renderer.close_modal()
        return renderer


#
# View Helpers
#
class ViewHelperBase(object):
    def __init__(self, cls=None):
        self.view = cls
    
    def get_instance(self):
        if self.view is not None:
            return self.view()


class ListableButtonHelpers(ViewHelperBase):
    def link(self, user, action_name, text=None, title='', **kwargs):
        from django_helpers.core.listable import StaticLinkField
        
        view = self.get_instance()
        if text is None:
            text = view.get_title(action=action_name)
        
        if not view.has_perm_for_action(action_name, user):
            return None
        
        url = view.get_url_params(action_name)[1]
        
        button = StaticLinkField(text, url, title=title)
        button.ajax = kwargs.get('is_ajax', False)
        
        if 'confirm' in kwargs:
            button.set_confirm(**kwargs['confirm'])
        
        if 'tipsy' in kwargs:
            button.set_tipsy_tip(**kwargs['tipsy'])
        
        fields = kwargs.get("fields", None)
        if fields is None:
            fields = ['pk']
            # # Remove any reserved parameters
            # reserved_args = ['self', 'request', 'user', 'instance', 'pk', 'id']
            # has_pk = 'id' in args or 'pk' in args or 'instance' in args
            #
            # for reserved_arg in reserved_args:
            #     if reserved_arg in new_args:
            #         new_args.remove(reserved_arg)
            #
            # fields = []
            # if has_pk:
            #     fields.append('pk')
            # else:
            #     fields.extend(new_args)
        
        button.field = fields
        # button.can_render = getattr(cls, action_name + '_validator', None)
        
        return button
    
    def icon_link(self, user, action_name, icon=None, title='', **kwargs):
        from django_helpers.core.listable import IconLinkField
        
        view = self.get_instance()
        
        if not view.has_perm_for_action(action_name, user):
            return None
        
        url = view.get_url_params(action_name)[1]
        
        fields = kwargs.get("fields", None)
        if fields is None:
            fields = ['pk']
        
        button = IconLinkField(icon, fields, url, title)
        button.ajax = kwargs.get('is_ajax', False)
        
        if 'confirm' in kwargs:
            button.set_confirm(**kwargs['confirm'])
        
        if 'tipsy' in kwargs:
            button.set_tipsy_tip(**kwargs['tipsy'])
        
        # button.can_render = getattr(cls, action_name + '_validator', None)
        
        return button
    
    def button(self, user, action_name, text=None, color='primary', title='', **kwargs):
        from django_helpers.core.listable import BootstrapButton
        
        view = self.get_instance()
        if text is None:
            text = view.get_title(action=action_name)
        
        if not view.has_perm_for_action(action_name, user):
            return None
        
        url = view.get_url_params(action_name)[1]
        icon = kwargs.get('icon', '')
        button = BootstrapButton(text, color, url, icon, title=title)
        button.ajax = kwargs.get('is_ajax', False)
        
        if 'confirm' in kwargs:
            button.set_confirm(**kwargs['confirm'])
        
        if 'tipsy' in kwargs:
            button.set_tipsy_tip(**kwargs['tipsy'])
        
        fields = kwargs.get("fields", None)
        if fields is None:
            fields = ['pk']
            # # Remove any reserved parameters
            # reserved_args = ['self', 'request', 'user', 'instance', 'pk', 'id']
            # has_pk = 'id' in args or 'pk' in args or 'instance' in args
            #
            # for reserved_arg in reserved_args:
            #     if reserved_arg in new_args:
            #         new_args.remove(reserved_arg)
            #
            # fields = []
            # if has_pk:
            #     fields.append('pk')
            # else:
            #     fields.extend(new_args)
        
        button.field = fields
        # button.can_render = getattr(cls, action_name + '_validator', None)
        
        return button
    
    def delete_button(self, user, ajax=True, action=None, **kwargs):
        if action is None:
            action = 'delete'
        
        text = kwargs.pop('text', 'Delete')
        btn = self.button(user, action, text, 'danger', icon='trash', **kwargs)
        
        if btn is None:
            return btn
        
        name = str(getattr(self.view, 'confirm_name', None))
        
        if name != 'None':
            name += " this "
        else:
            name = ""
        
        if ajax:
            confirm_text = kwargs.get('confirm_text', None)
            if confirm_text is None:
                confirm_text = 'Are you sure about deleting%s ?' % name
            
            btn.set_confirm(confirm_text)
            
            loading_text = kwargs.get('loading_text', 'Deleting...')
            btn.set_ajax(loading_text)
        
        return btn
    
    def delete_icon_button(self, user, ajax=True, action=None, **kwargs):
        if action is None:
            action = 'delete'
        
        icon = kwargs.pop('icon', 'trash')
        btn = self.icon_link(user, action, icon, 'Delete', **kwargs)
        
        if btn is None:
            return btn
        
        name = str(getattr(self.view, 'confirm_name', None))
        
        if name != 'None':
            name += " this "
        else:
            name = ""
        
        if ajax:
            confirm_text = kwargs.get('confirm_text', None)
            if confirm_text is None:
                confirm_text = 'Are you sure about deleting%s ?' % name
            btn.set_confirm(confirm_text)
            
            loading_text = kwargs.get('loading_text', 'Deleting...')
            btn.set_ajax(loading_text)
        
        return btn
    
    def audit_button(self, user, ajax=True, action=None, **kwargs):
        if action is None:
            action = 'audit'
        
        view = self.view
        
        text = kwargs.pop('text', 'Audit')
        btn = self.button(user, action, text, 'success', icon='check', **kwargs)
        name = str(getattr(view, 'confirm_name', None))
        if name is not None:
            name = " this " + name
        else:
            name = ""
        
        if ajax:
            confirm_text = kwargs.get('confirm_text', None)
            if confirm_text is None:
                confirm_text = 'Are you sure about auditing%s ?' % name
            
            btn.set_confirm(confirm_text)
            
            loading_text = kwargs.get('loading_text', 'Auditing...')
            btn.set_ajax(loading_text)
        return btn
    
    def edit_button(self, user, confirm=False, action=None, **kwargs):
        if action is None:
            action = 'edit'
        
        view = self.view
        text = kwargs.pop('text', 'Edit')
        btn = self.button(user, action, text, 'default', icon='pencil', **kwargs)
        
        if confirm:
            name = str(getattr(view, 'confirm_name', None))
            if name is not None:
                name = " this " + name
            else:
                name = ""
            confirm_text = kwargs.get('confirm_text', None)
            if confirm_text is None:
                confirm_text = 'Are you sure about editing%s ?' % name
            btn.set_confirm(confirm_text)
        return btn
    
    def edit_icon_button(self, user, confirm=False, action=None, **kwargs):
        if action is None:
            action = 'edit'
        
        icon = kwargs.pop('icon', 'pencil')
        view = self.view
        
        btn = self.icon_link(user, action, icon, 'Edit', **kwargs)
        
        if confirm:
            name = str(getattr(view, 'confirm_name', None))
            if name is not None:
                name = " this " + name
            else:
                name = ""
            confirm_text = kwargs.get('confirm_text', None)
            
            if confirm_text is None:
                confirm_text = 'Are you sure about editing%s ?' % name
            
            btn.set_confirm(confirm_text)
        
        return btn


class WidgetButtonHelpers(ViewHelperBase):
    def link(self, user, action_name, text=None, args=None, icon=None, **kwargs):
        from django_helpers.widgets import LinkButton
        from django_helpers.widgets import Icon
        
        view = self.get_instance()
        
        if text is None:
            text = view.get_title(action=action_name)
        
        if not view.has_perm_for_action(action_name, user):
            return None
        
        url = view.url_to_action(action_name, args)
        
        button = LinkButton(text, url)
        button.ajax = kwargs.get('is_ajax', False)
        
        if 'confirm' in kwargs:
            button.set_confirm(**kwargs['confirm'])
        
        if 'modal' in kwargs:
            button.set_modal(title=text)
        
        if icon is not None:
            button.add(Icon(icon))
        
        return button


class ViewDecorators(object):
    #
    # Decorators: Permissions
    #
    
    @staticmethod
    def staff_only(fn):
        fn._staff_only = True
        return fn
    
    @staticmethod
    def csrf_exempt(fn):
        fn._csrf_exempt = True
        return fn
    
    @staticmethod
    def user_only(msg):
        if hasattr(msg, '__call__'):
            msg._user_only = True
            return msg
        
        def decorator(fn):
            fn._user_only = True
            fn._user_only_msg = msg
            
            return fn
        
        return decorator
    
    @staticmethod
    def admin_only(msg):
        if hasattr(msg, '__call__'):
            msg._admin_only = True
            return msg
        
        def decorator(fn):
            fn._admin_only = True
            fn._admin_only_msg = msg
            
            return fn
        
        return decorator
    
    @staticmethod
    def permission(*permissions):
        def decorator(fn):
            existing_permissions = getattr(fn, 'permissions', None)
            custom_permissions = getattr(fn, 'custom_permissions', None)
            
            if existing_permissions is None:
                existing_permissions = []
                fn.permissions = existing_permissions
            
            if custom_permissions is None:
                custom_permissions = []
                fn.custom_permissions = custom_permissions
            
            for permission in permissions:
                if hasattr(permission, '__call__'):
                    custom_permissions.append(permission)
                else:
                    existing_permissions.append(permission)
            
            return fn
        
        return decorator
    
    @staticmethod
    def params_permission(*permissions):
        def decorator(fn):
            params_permissions = getattr(fn, 'params_permissions', None)
            
            if params_permissions is None:
                params_permissions = []
                fn.params_permissions = params_permissions
            
            for permission in permissions:
                params_permissions.append(permission)
            
            return fn
        
        return decorator
    
    @staticmethod
    def instance_permission(name, *permissions):
        def decorator(fn):
            instance_permissions = getattr(fn, 'instance_permissions', None)
            if instance_permissions is None:
                instance_permissions = {}
                fn.instance_permissions = instance_permissions
            
            exiting_permissions = instance_permissions[name]
            for permission in permissions:
                exiting_permissions.append(permission)
            
            return fn
        
        return decorator
    
    @staticmethod
    def instances_permission(*permissions):
        def decorator(fn):
            instances_permissions = getattr(fn, 'instances_permissions', None)
            if instances_permissions is None:
                instances_permissions = []
                fn.instances_permissions = instances_permissions
            
            for permission in permissions:
                instances_permissions.append(permission)
            
            return fn
        
        return decorator
    
    #
    # Decorators
    #
    
    @staticmethod
    def instance(instance_name="instance", **kwargs):
        def decorator(fn):
            model = None
            if 'model' in kwargs:
                model = kwargs.pop('model')
            
            key = 'instances'
            instances = getattr(fn, key, None)
            if instances is None:
                instances = {}
                setattr(fn, key, instances)
            
            instances[instance_name] = {
                'args': kwargs,
                'model': model
            }
            fn.needs_instance = True
            
            return fn
        
        return decorator
    
    @staticmethod
    def url(name, pattern):
        def decorator(fn):
            fn.url_name = name
            fn.url_pattern = pattern
            
            return fn
        
        return decorator
    
    @staticmethod
    def title(text):
        def decorator(fn):
            fn.title = text
            
            return fn
        
        return decorator
    
    #
    # Decorators: Response
    #
    @staticmethod
    def as_template(template, ajax_template=None):
        if hasattr(template, '__call__') and ajax_template is None:
            template.as_template = True
            return template
        
        def decorator(fn):
            fn.as_template = True
            fn.template = template
            fn.ajax_template = ajax_template
            
            return fn
        
        return decorator
    
    @staticmethod
    def as_form(fn):
        fn.as_form = True
        return fn
    
    @staticmethod
    def as_response(fn):
        fn.as_response = True
        return fn
    
    @staticmethod
    def as_svg(fn):
        fn.as_svg = True
        return fn
    
    @staticmethod
    def as_listing(fn):
        fn.as_listing = True
        return fn


#
# View
#
class ViewMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        new_cls = type.__new__(mcs, name, bases, attrs)
        
        for name, fn in attrs.items():
            if hasattr(fn, 'url_name') and hasattr(fn, 'url_pattern'):
                ViewMetaClass.setup_url(fn, name, new_cls)
        
        new_cls.listable = ListableButtonHelpers(new_cls)
        new_cls.widgets = WidgetButtonHelpers(new_cls)
        
        return new_cls
    
    @staticmethod
    def setup_url(fn, name, new_cls):
        def url(*args, **kwargs):
            cls = url.cls
            nam = url.name
            
            return cls.url_to_action(nam, args, kwargs)
        
        url.name = name
        url.cls = new_cls
        fn.url = url


class BaseView(six.with_metaclass(ViewMetaClass, object)):
    url_name_prefix = None
    url_pattern_prefix = None
    
    #
    # Main View Function
    #
    
    @classonlymethod
    def wrap_view(cls, view):
        return view
    
    def pre_init(self):
        pass
    
    def post_init(self):
        pass
    
    def pre_permission_cls(self):
        pass
    
    def post_permission_cls(self):
        pass
    
    def pre_permission_action(self):
        pass
    
    def post_permission_action(self):
        pass
    
    def pre_execution(self):
        pass
    
    @classonlymethod
    def as_view(cls):
        """
        Main entry point for a request-response process.
        """
        
        def view(request, action, *args, **kwargs):
            need_request, need_user, need_instance = cls.get_action_args(action)
            
            self = cls()
            
            self.pre_init()
            
            self.request = request
            self.action = action
            user = request.user
            
            self.post_init()
            
            self.pre_permission_cls()
            
            if not self.has_perm_for_cls(user):
                return self.handle_no_permission()
            
            self.args = args
            self.kwargs = kwargs
            
            self.post_permission_cls()
            
            fn = getattr(self, action)
            
            if fn is None:
                raise Exception('Action not found : %s' % action)
            self.pre_permission_action()
            if not self.has_perm_for_action(fn, user, **kwargs):
                return self.handle_no_permission()
            self.post_permission_action()
            fn_kwargs = {}
            
            if need_instance:
                instances = self.get_instances(fn, kwargs)
                if not self.has_instances_perm(fn, user, **instances):
                    return self.handle_no_permission()
                
                for instance_name, instance in instances.items():
                    fn_kwargs[instance_name] = instance
                    
                    if not self.has_instance_perm(fn, user, instance_name, instance):
                        return self.handle_no_permission()
            
            if need_request is True:
                fn_kwargs['request'] = request
            
            if need_user is True:
                fn_kwargs['user'] = user
            
            args = getattr(fn, 'args')
            for arg in args:
                if arg not in fn_kwargs and arg in kwargs:
                    fn_kwargs[arg] = kwargs[arg]
            
            try:
                self.pre_execution()
                action_response = fn(**fn_kwargs)
                
                return self.send_response(fn, action_response)
            except Exception, ex:
                print ex
                print action
                print args
                print kwargs
                print fn_kwargs
                raise
        
        return cls.wrap_view(view)
    
    def send_response(self, fn, action_response):
        if isinstance(action_response, HttpResponse):
            return action_response
        
        if getattr(fn, 'as_response', False):
            return HttpResponse(action_response)
        
        if getattr(fn, 'as_svg', False):
            return HttpResponse(action_response, content_type="image/svg+xml")
        
        return action_response
    
    @classonlymethod
    def get_action_args(cls, action_name):
        from inspect import getargspec
        
        action = getattr(cls, action_name)
        if hasattr(action, 'needs_request'):
            need_user = getattr(action, 'needs_user')
            need_request = getattr(action, 'needs_request')
            need_instance = getattr(action, 'need_instance')
            
            return need_request, need_user, need_instance
        
        inspection = getargspec(action)
        args = list(inspection.args)
        
        needs_request = False
        if 'request' in args:
            needs_request = True
        
        needs_user = False
        if 'user' in args:
            needs_user = True
        
        need_instance = hasattr(action, 'instances')
        
        try:
            action.needs_request = needs_request
            action.needs_user = needs_user
            action.need_instance = need_instance
            action.args = args
        
        except AttributeError:
            action = action.__func__
            
            action.needs_request = needs_request
            action.needs_user = needs_user
            action.need_instance = need_instance
            action.args = args
        
        return needs_request, needs_user, need_instance
    
    #
    # Instance Functions
    #
    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def get_model(self, action):
        return None
    
    def get_instances(self, action, kwargs):
        instances = getattr(action, 'instances', None)
        view_model = self.get_model(action)
        request = self.get_request()
        user = request.user
        
        result = {}
        for name, instance in instances.items():
            args = instance.get('args')
            model = instance.get('model')
            
            if model is None:
                if view_model is None:
                    raise Exception("Model is missing.")
                model = view_model
            
            if type(model) in StringTypes:
                if model.startswith("self."):
                    attr = model[5:]
                    model = getattr(self, attr)
                else:
                    model = get_model(model)
            
            manager = getattr(model, '_default_manager')
            lookup = {}
            
            for key, value in args.items():
                if value.startswith('__user__'):
                    user_key = value[len('__user__'):]
                    if user_key == "":
                        lookup[key] = user
                    else:
                        lookup[key] = getattr(user, user_key)
                else:
                    lookup[key] = kwargs[value]
            
            value = manager.get(**lookup)
            result[name] = value
        
        return result
    
    #
    # Permissions
    #
    # noinspection PyUnusedLocal
    def has_perm_for_cls(self, user):
        perm = getattr(self, 'permissions', None)
        custom_permissions = getattr(self, 'custom_permissions', None)
        user_only = getattr(self, '_user_only', False)
        admin_only = getattr(self, '_admin_only', False)
        
        authenticated = user.is_authenticated()
        
        if admin_only and not user.is_superuser:
            self.no_perm_admin_only()
            return False
        
        if user_only and not authenticated:
            self.no_perm_user_only()
            return False
        
        if perm:
            if not user.has_perms(perm):
                return False
        
        if custom_permissions:
            for fn in custom_permissions:
                if not fn(user):
                    return False
        
        return True
    
    def has_perm_for_action(self, action, user, **kwargs):
        perm = self.get_permission(action)
        
        user_only = getattr(action, '_user_only', False)
        admin_only = getattr(action, '_admin_only', False)
        
        custom_permissions = getattr(action, 'custom_permissions', [])
        params_permissions = getattr(action, 'params_permissions', [])
        
        authenticated = user.is_authenticated()
        
        if admin_only and not user.is_superuser:
            self.no_perm_admin_only(action)
            return False
        
        if user_only and not authenticated:
            self.no_perm_user_only(action)
            return False
        
        if perm:
            if not user.has_perms(perm):
                return False
        
        if custom_permissions:
            for fn in custom_permissions:
                if not fn(user):
                    return False
        
        if params_permissions:
            for fn in params_permissions:
                if not fn(user, **kwargs):
                    return False
        
        return True
    
    def no_perm_admin_only(self, action=None):
        pass
    
    def no_perm_user_only(self, action=None):
        pass
    
    def has_instance_perm(self, action, user, instance_name, instance):
        action_permissions = getattr(action, 'instance_permissions', {})
        cls_permissions = getattr(self, 'instance_permissions', {})
        
        action_permissions = action_permissions.get(instance_name, [])
        cls_permissions = cls_permissions.get(instance_name, [])
        
        permissions = action_permissions + cls_permissions
        
        for fn in permissions:
            if hasattr(fn, '__call__'):
                if fn(user, instance) is False:
                    return False
            else:
                fn = getattr(instance, fn, None)
                if fn is not None and fn(user) is False:
                    return False
        
        return True
    
    # noinspection PyMethodMayBeStatic
    def has_instances_perm(self, action, user, **instances):
        permissions = getattr(action, 'instances_permissions', [])
        
        for fn in permissions:
            if fn(user, **instances) is False:
                return False
        
        return True
    
    # noinspection PyMethodMayBeStatic,PyBroadException
    def handle_no_permission(self):
        return self.no_permission_error()
    
    # noinspection PyMethodMayBeStatic
    def no_permission_error(self):
        return HttpResponseForbidden()
    
    #
    # URL Generation
    #
    
    @classmethod
    def urls(cls):
        from django.conf.urls import url
        
        url_patterns = []
        props = dir(cls)
        for prop in props:
            action = getattr(cls, prop)
            if hasattr(action, 'url_name'):
                result = cls.get_url_params(prop)
                if result is not None:
                    pattern, url_name = result
                    fn = cls.as_view()
                    need_csrf_exempt = getattr(action, '_csrf_exempt', False)
                    if need_csrf_exempt is True:
                        fn = real_csrf_exempt(fn)
                    url_patterns.append(url(pattern, fn, {
                        'action': prop
                    }, url_name))
        
        return url_patterns
    
    @classmethod
    def get_url_name(cls, action_name):
        return cls.get_url_params(action_name)[1]
    
    # noinspection PyAugmentAssignment
    @classmethod
    def get_url_params(cls, action_name):
        action = getattr(cls, action_name)
        pattern = getattr(action, "url_pattern")
        url_name = getattr(action, "url_name")
        
        pattern_prefix = cls.url_pattern_prefix
        name_prefix = cls.url_name_prefix
        
        if name_prefix:
            url_name = name_prefix + "-" + url_name
        
        if pattern.endswith("$"):
            pattern = pattern[:-1]
        
        if pattern_prefix:
            if pattern_prefix.endswith("/"):
                pattern_prefix = pattern_prefix[:-1]
            
            pattern = "{0}/{1}".format(pattern_prefix, pattern)
        
        if not pattern.startswith("^"):
            pattern = "^" + pattern
        
        if not pattern.endswith("$"):
            pattern = pattern + "$"
        
        return pattern, url_name
    
    @classmethod
    def url_to_action(cls, action_name, args=None, kwargs=None):
        url_name = cls.get_url_name(action_name)
        return reverse(url_name, args=args, kwargs=kwargs)
    
    def get_absolute_url_to_action(self, action_name, args=None, kwargs=None):
        url = self.url_to_action(action_name, args, kwargs)
        return self.get_request().build_absolute_uri(url)
    
    #
    # Getters
    #
    def get_action(self, name=None):
        if name is None:
            name = getattr(self, 'action')
        
        return getattr(self, name)
    
    def getattr(self, prop, default=None, action=None):
        if action is None:
            action = self.get_action()
        
        if not hasattr(action, '__call__'):
            action = self.get_action(action)
        
        result = getattr(action, prop, default)
        return result
    
    def get_permission(self, action=None):
        return self.getattr('permissions', action=action)
    
    def get_request(self):
        return getattr(self, 'request', None)
    
    def get_user(self):
        request = self.get_request()
        user = request.user
        return user
    
    #
    # Render Functions
    #
    def render_json(self, data=None):
        renderer = JSONResponse(self.get_request(), data)
        return renderer
    
    # noinspection PyMethodMayBeStatic
    def response(self, string):
        return HttpResponse(string)


class View(BaseView):
    pubsub = None
    
    listable = ListableButtonHelpers()
    widgets = WidgetButtonHelpers()
    
    theme = None
    
    login_url_name = 'auth-login'
    
    #
    # Main View Function
    #
    
    def send_response(self, fn, action_response):
        request = self.get_request()
        
        if getattr(fn, 'as_template', False):
            template = getattr(fn, 'template', None)
            if template is None:
                return self.render(action_response)
            
            if request.is_ajax():
                template = getattr(fn, 'ajax_template', template)
            return self.render(template, action_response)
        
        if getattr(fn, 'as_form', False):
            return self.form_view(**action_response)
        
        if getattr(fn, 'as_listing', False):
            return self.listing_view(**action_response)
        
        if getattr(fn, 'as_response', False):
            return HttpResponse(action_response)
        
        if getattr(fn, 'as_svg', False):
            return HttpResponse(action_response, content_type="image/svg+xml")
        
        return action_response
    
    def no_perm_admin_only(self, action=None):
        if action is None:
            action = self
        
        admin_only_msg = getattr(action, 'admin_only_msg', None)
        self.info_message(admin_only_msg)
    
    def no_perm_user_only(self, action=None):
        if action is None:
            action = self
        
        user_only_msg = getattr(action, 'user_only_msg', None)
        self.info_message(user_only_msg)
    
    # noinspection PyMethodMayBeStatic,PyBroadException
    def handle_no_permission(self):
        user = self.get_user()
        if user.is_authenticated():
            return self.no_permission_error()
        
        try:
            return self.redirect_to_login()
        except Exception, ex:
            return self.no_permission_error()
    
    # noinspection PyMethodMayBeStatic
    def redirect_to_login(self):
        return redirect(self.login_url_name)
    
    @classmethod
    def redirect_to_action(cls, action_name, args=None, kwargs=None):
        return redirect(cls.url_to_action(action_name, args, kwargs))
    
    @classmethod
    def get_nav(cls, item, *actions):
        self = cls()
        is_single = len(actions) == 1
        for action in actions:
            if type(action) is tuple:
                action, title = action
            else:
                title = None
            
            sub_item = cls.add_nav_item(item, action, title, self)
            
            if is_single is True:
                return sub_item
    
    @classmethod
    def add_nav_item(cls, item, action, title=None, self=None):
        if self is None:
            self = cls()
        
        if title is None:
            title = self.get_title(action)
        
        fn = getattr(self, action)
        
        admin_only = getattr(cls, '_admin_only', None)
        guest_only = getattr(cls, '_guest_only', None)
        user_only = getattr(cls, '_user_only', None)
        
        admin_only = getattr(fn, '_admin_only', admin_only) or admin_only
        guest_only = getattr(fn, '_guest_only', guest_only) or guest_only
        user_only = getattr(fn, '_user_only', user_only) or user_only
        
        url_name = cls.get_url_name(action)
        sub_item = item.add_item(title, url_name)
        
        permission = self.get_permission(action)
        if permission:
            sub_item.add_permissions(*permission)
        
        if admin_only is not None or user_only is not None or guest_only is not None:
            if admin_only is True:
                user_only = False
                guest_only = False
            
            sub_item.allow_users = user_only
            sub_item.allow_admin = admin_only or user_only
            sub_item.allow_guest = guest_only and not (admin_only or user_only)
        
        return sub_item
    
    def get_title(self, action=None, default=None):
        return self.getattr('title', default, action)
    
    #
    # Message Helpers
    #
    def add_message(self, message_type, message, obj=None):
        request = self.get_request()
        
        if message is None:
            return
        
        if obj is not None:
            message = message.format(obj)
        
        fn = getattr(messages, message_type, None)
        
        if fn is None or not hasattr(fn, '__call__'):
            raise Exception('Given message type is not valid')
        
        fn(request, message)
    
    def success_message(self, message, obj=None):
        self.add_message('success', message, obj)
    
    def info_message(self, message, obj=None):
        self.add_message('info', message, obj)
    
    def warning_message(self, message, obj=None):
        self.add_message('warning', message, obj)
    
    def error_message(self, message, obj=None):
        self.add_message('error', message, obj)
    
    # noinspection PyMethodMayBeStatic
    def redirect(self, url_name=None, *args, **kwargs):
        """
        Redirects to the given url
        """
        if url_name is None:
            request = self.get_request()
            path = request.get_full_path()
            return redirect(path)
        
        if url_name.startswith("/"):
            return redirect(url_name)
        
        try:
            return redirect(reverse(url_name, args=args, kwargs=kwargs))
        except NoReverseMatch:
            return redirect(url_name)
    
    def render(self, template, context=None):
        request = self.get_request()
        
        if context is None:
            context = {}
        
        title = context.get('title')
        page_heading = context.get('page_heading')
        if title is None:
            title = self.get_title()
        
        if title:
            context['title'] = title
        
        if not page_heading:
            page_heading = title
        
        if page_heading:
            context['page_heading'] = page_heading
        
        if self.theme is None:
            return render(request, template, context)
        
        return self.theme.render(request, template, context)
    
    #
    # Form Functions
    #
    def form_view(self, form_class, redirect_url, instance=None, success_message=None, current_user_field=None, title=None, page_heading=None, **kwargs):
        action = FormAction(self)
        
        action.form_class = form_class
        action.redirect_url = redirect_url
        action.instance = instance
        action.success_message = success_message
        action.current_user_field = current_user_field
        action.title = title
        action.page_heading = page_heading
        
        return action.render(**kwargs)
    
    def render_form(self, form, template=None, renderer=None, **context):
        action = FormAction(self)
        
        action.form = form
        action.template = template
        action.renderer = renderer
        
        return action.render_form(form, **context)
    
    #
    # Modification Function
    #
    def delete_view(self, instance, cancel_link, redirect_link, success_message=None, title=None, page_heading=None):
        action = DeleteAction(self, instance=instance)
        action.redirect_link = redirect_link
        action.cancel_link = cancel_link
        action.success_message = success_message
        action.title = title
        action.page_heading = page_heading
        
        return action.render()
    
    def enable_view(self, instance, cancel_link, redirect_link, success_message=None, title=None, page_heading=None, enabled=True):
        action = EnableAction(self, instance=instance)
        action.redirect_link = redirect_link
        action.cancel_link = cancel_link
        action.success_message = success_message
        action.title = title
        action.page_heading = page_heading
        
        return action.render()
    
    def disable_view(self, instance, cancel_link, redirect_link, success_message=None, title=None, page_heading=None):
        action = DisableAction(self, instance=instance)
        action.redirect_link = redirect_link
        action.cancel_link = cancel_link
        action.success_message = success_message
        action.title = title
        action.page_heading = page_heading
        
        return action.render()
    
    #
    # Instance Operations Helpers
    #
    def delete_instance(self, instance, success_message):
        instance.delete()
        self.success_message(success_message, instance)
    
    def enable_instance(self, instance, success_message, enabled=True):
        instance.enabled = enabled
        instance.disabled = not enabled
        instance.save()
        
        self.success_message(success_message, instance)
    
    def disable_instance(self, instance, success_message, enabled=False):
        self.enable_instance(instance, success_message, enabled)
    
    #
    # Listing View
    #
    def listing_view(self, table=None, table_class=None, **kwargs):
        action = ListAction(self)
        action.table_class = table_class
        action.table = table
        
        return action.render(**kwargs)
    
    def render_table(self, table, template=None, **context):
        action = ListAction(self)
        action.template = template
        action.table = table
        
        return action.render(**context)
        
        #
        # Listable Button Helpers
        #


#
# JSON
#
class JSONResponse(HttpResponse):
    def __init__(self, request, data=None):
        mimetype = 'application/json'
        HttpResponse.__init__(self, content_type=mimetype)
        
        self.request = request
        self.data = {}
        
        if type(data) is dict:
            self.data.update(data)
        
        self.json_string = None
    
    def add_messages(self):
        request = self.request
        data = self.data
        
        if not request:
            return
        
        messages = get_messages(request)
        messages_arr = []
        
        for message in messages:
            messages_arr.append({
                "message": message.message,
                "tags": message.tags
            })
        
        data["dj-messages"] = messages_arr
    
    def console_log(self, str):
        key = 'console-log'
        
        arr = self.get_array(key)
        arr.append(str)
    
    def redirect(self, url, args=None, kwargs=None):
        from django.core.urlresolvers import reverse
        from django.core.urlresolvers import NoReverseMatch
        
        if args is not None or kwargs is not None:
            try:
                url = reverse(url, args, kwargs)
            except NoReverseMatch:
                pass
        
        self.data["dj-redirect"] = url
    
    def refresh_table(self, name=None):
        if name is None:
            self.add('dj-refresh-table')
            return
        key = "dj-refresh-tables"
        
        arr = self.get_array(key)
        if name not in arr:
            arr.append(name)
    
    def update_widget(self, name, html):
        key = "dj-update-widgets"
        
        arr = self.get_dict(key)
        if name not in arr:
            arr[name] = html
    
    def refresh_listing(self, name=None):
        if name is None:
            self.add('dj-refresh-listing')
            return
        key = "dj-refresh-listings"
        
        arr = self.get_array(key)
        if name not in arr:
            arr.append(name)
    
    def refresh_listable_class(self, name=None):
        if name is None:
            self.add('dj-refresh-listables')
            return
        key = "dj-refresh-listables"
        
        arr = self.get_array(key)
        if name not in arr:
            arr.append(name)
    
    def open_modal(self, title, url):
        self.add('dj-open-modal', {
            'title': title,
            'url': url
        })
    
    def close_modal(self):
        self.add('dj-close-modal')
    
    def get_array(self, key):
        data = self.data
        
        if key not in data:
            data[key] = []
        
        arr = data[key]
        return arr
    
    def get_dict(self, key):
        data = self.data
        
        if key not in data:
            data[key] = {}
        
        arr = data[key]
        return arr
    
    def add(self, key, value=True):
        """
        :param key: str
        :type value: dict, bool, str, int
        """
        reserved = ('dj-redirect', 'dj-refresh-tables', 'dj-messages')
        
        if key in reserved:
            raise ValidationError('Reserved key provided.')
        
        self.data[key] = value
    
    #
    #   Rendering Functions
    #
    def render(self):
        self.add_messages()
        
        return self
    
    def to_str(self, indent=None):
        from json import dumps
        if self.json_string is not None:
            return self.json_string
        
        s = dumps(self.data, indent=indent)
        self.json_string = s
        
        return s
    
    @property
    def content(self):
        return self.make_bytes(self.to_str())
    
    @content.setter
    def content(self, value):
        pass
    
    #
    # Over rides
    #
    def write(self, content):
        raise Exception("Write is not supported in JSON Response")
    
    def __iter__(self):
        return iter([self.content])
