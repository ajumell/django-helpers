# coding=utf-8
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


class QueryBackend(ModelBackend):
    """
    Authenticates against settings.AUTH_USER_MODEL.
    """

    def authenticate(self, **kwargs):
        UserModel = get_user_model()

        password = kwargs.pop('password', None)

        try:
            user = UserModel._default_manager.get(**kwargs)
            if password is None:
                return user

            if user.check_password(password):
                return user

        except UserModel.DoesNotExist:
            pass
