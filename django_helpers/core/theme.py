# coding=utf-8
import os

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponse
from django.template import loader, RequestContext, Template, Context
from django.utils import six
from django_helpers.apps.data_tables import DataTable
from django_helpers.apps.form_renderer import FormRenderer
from django_helpers.apps.listings import Listing
from django_helpers.apps.menu import Menu
from django_helpers.apps.static_manager import StaticRequirement
from django_helpers.apps.static_manager import register_css
from django_helpers.apps.static_manager import register_js

__all__ = [
    'BaseTheme',
    'get_theme',
]

_REGISTERED_THEMES = {}

_LOADED_THEMES = {}


def has_registered(name):
    return name in _REGISTERED_THEMES


def register_name(class_name, name):
    if not name:
        return
    
    if name in _REGISTERED_THEMES:
        old_name = _REGISTERED_THEMES[name]
        
        msg = "You tried to register a theme with id " + name + " for " + class_name + ".\n"
        msg += "But " + name + " is already registered for " + old_name + ".\n"
        msg += "Try giving a new ID for " + class_name
        
        raise ImproperlyConfigured(msg)
    
    _REGISTERED_THEMES[name] = class_name


def get_theme(theme_id=None):
    """
    :param theme_id: id of the theme.
    :return: django_helpers.core.theme.Theme
    """
    if theme_id is None:
        keys = _REGISTERED_THEMES.keys()
        if len(keys):
            theme_id = keys[0]
    
    if theme_id not in _REGISTERED_THEMES:
        raise Exception("Theme not found.")
    
    if theme_id not in _LOADED_THEMES:
        cls = _REGISTERED_THEMES[theme_id]
        obj = cls()
        _LOADED_THEMES[theme_id] = obj
    else:
        obj = _LOADED_THEMES[theme_id]
    
    return obj


class ThemeMetaClass(type):
    def __new__(mcs, name, bases, attrs):
        obj = type.__new__(mcs, name, bases, attrs)
        theme_id = attrs.get('theme_id', None)
        register_name(obj, theme_id)
        return obj


class BaseTheme(six.with_metaclass(ThemeMetaClass, object)):
    """
    A Theme is a class which provide the lay out in a website.

    The theme should add the required Modal Adapter
    The theme should add the required Pagination Adapter
    The theme should add the required Form Validation Rendering Adapter
    The theme should add the required Messages Adapter
    The theme should add the required Confirm Adapter
    """
    
    MODAL_BASE_TEMPLATE = 'theme/modal-base.html'
    BASE_TEMPLATE = 'theme/base.html'
    DATA_TABLE_TEMPLATE = 'theme/data-table.html'
    FORM_TEMPLATE = 'theme/form-page.html'
    DETAILS_TEMPLATE = 'theme/details-page.html'
    FORM_HALF_TEMPLATE = 'theme/form-page-half.html'
    DELETE_CONFIRM_TEMPLATE = 'theme/delete-confirm.html'
    SUCCESS_CONFIRM_TEMPLATE = 'theme/success-confirm.html'
    ERROR_TEMPLATE = 'theme/error.html'
    PHOTO_CROPPER_TEMPLATE = 'bootstrap/photo-cropper.html'
    LOGIN_TEMPLATE = FORM_TEMPLATE
    
    DataTable = DataTable
    FormRenderer = FormRenderer
    Listing = Listing
    Menu = Menu
    
    title_suffix = None
    
    theme_id = None
    
    extra_css_files = []
    extra_js_files = []
    
    def __init__(self):
        self.has_created_files = False
        
        self.addon_js_requirements = []
        self.addon_css_requirements = []
        
        self.validate_options()
    
    def get_static_requirements(self):
        requirements = StaticRequirement()
        
        requirements.add_css_files(*self.addon_css_requirements)
        requirements.add_js_files(*self.addon_js_requirements)
        
        files = self.extra_css_files
        names = self.register_files(files)
        requirements.add_css_files(*names)
        
        files = self.extra_js_files
        names = self.register_files(files)
        requirements.add_js_files(*names)
        
        return requirements
    
    @staticmethod
    def register_files(files):
        names = []
        for f in files:
            deps = None
            if type(f) in (list, tuple):
                deps = f[1:]
                f = f[0]
                
                print deps
            
            name = f
            for c in ["/", "(", ")", " ", "_", "."]:
                name = name.replace(c, "-")
            names.append(name)
            
            if f.endswith(".css"):
                register_css(name, f)
            
            if f.endswith(".js"):
                register_js(name, f, deps)
        
        return names
    
    def context_processor(self, request):
        pass
    
    def base_context_processor(self, request):
        requirements = self.get_static_requirements()
        
        context = {
            'static_app': self.theme_id,
            'request': request,
            'BASE_TEMPLATE': self.get_base_template(request)
        }
        
        if requirements is not None:
            context['static_requirements'] = requirements
        
        return context
    
    def get_base_template(self, request):
        if request.is_ajax():
            return self.MODAL_BASE_TEMPLATE
        
        return self.BASE_TEMPLATE
    
    def validate_options(self):
        if not self.theme_id:
            raise Exception("Theme ID is missing.")
    
    def create_files(self):
        pass
    
    def process_data_table(self, table):
        pass
    
    def process_form_renderer(self, renderer):
        pass
    
    def pre_process_context(self, data):
        for k, v in data.items():
            if isinstance(v, DataTable):
                self.process_data_table(v)
            
            if isinstance(v, FormRenderer):
                self.process_form_renderer(v)
    
    def render(self, request, template, data=None):
        if settings.DEBUG is True and self.has_created_files is False:
            self.create_files()
            self.create_addon_files()
            self.has_created_files = True
        
        if data:
            self.pre_process_context(data)
        
        theme_data = self.context_processor(request)
        
        if type(theme_data) is not dict:
            theme_data = {}
        
        if not data:
            data = {}
        
        for key, val in theme_data.items():
            if key not in data:
                data[key] = val
        
        request_context = {
            'user': request.user,
            'request': request,
        }
        
        for key, val in request_context.items():
            if key not in data:
                data[key] = val
        
        base_context = self.base_context_processor(request)
        if type(base_context) is dict:
            for key, val in base_context.items():
                if key not in data:
                    data[key] = val
        
        httpresponse_kwargs = {
            'content_type': data.pop('content_type', None),
            'status': data.pop('status', None),
        }
        
        context_instance = RequestContext(request, data)
        data['context_instance'] = context_instance
        self.get_menu(context_instance, request)
        html = loader.render_to_string(template, context_instance=context_instance)
        return HttpResponse(html, **httpresponse_kwargs)
    
    #
    # Helpers
    #
    def show_error(self, request, message, title='Error occured'):
        return self.render(request, self.ERROR_TEMPLATE, {
            'page_heading': title,
            'title': title,
            'message': message
        })
    
    def get_static_folder(self, name=None):
        static_folders = settings.STATICFILES_DIRS
        
        if len(static_folders) == 0:
            raise ValueError("The STATICFILES_DIRS is not defined.")
        
        folder = static_folders[0]
        
        if not name:
            return folder
        
        path = os.path.join(folder, name)
        
        if not os.path.exists(path):
            os.mkdir(path)
        
        return path
    
    #
    # Menu
    #
    def get_menu(self, context, request):
        menu_id = self.theme_id + "-menu"
        menu = self.Menu(context, menu_id, request)
        self.setup_menu(menu, context, request)
        
        return menu
    
    def setup_menu(self, menu, context, request):
        pass
    
    #
    # Addons
    #
    def get_addons(self):
        return []
    
    def create_addon_files(self):
        from django_helpers.apps.static_manager import register_css
        from django_helpers.apps.static_manager import register_js
        
        addons = self.get_addons()
        if not addons:
            return
        
        for addon in addons:
            path = self.get_static_folder('addon-js')
            
            js_files = addon.iter_js_files()
            if js_files is not None:
                for obj in js_files:
                    name, contents, deps = obj
                    
                    fp = open(os.path.join(path, name), 'w')
                    fp.write(contents)
                    fp.close()
                    
                    static_path = 'addon-js/' + name
                    register_js(name, static_path, deps)
                    
                    self.addon_js_requirements.append(name)
            
            path = self.get_static_folder('addon-css')
            css_files = addon.iter_css_files()
            if css_files is not None:
                for name, contents in css_files:
                    fp = open(os.path.join(path, name), 'w')
                    fp.write(contents)
                    fp.close()
                    
                    static_path = 'addon-css/' + name
                    register_css(name, static_path)
                    
                    self.addon_css_requirements.append(name)


class BaseThemeAddon(object):
    @classmethod
    def iter_js_files(cls):
        raise NotImplementedError()
    
    @classmethod
    def iter_css_files(cls):
        raise NotImplementedError()
    
    @classmethod
    def render_template(cls, file_path, context):
        fp = open(file_path)
        contents = fp.read()
        fp.close()
        
        tmpl = Template(contents)
        return tmpl.render(Context(context))
