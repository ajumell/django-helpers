# coding=utf-8
from collections import OrderedDict

from rest_framework.authentication import SessionAuthentication
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from django.http import HttpResponse
from django.http.response import HttpResponseBase
from django.utils.decorators import classonlymethod
from views import BaseView
from views import JSONResponse


def get_queryset(model):
    manager = getattr(model, "_default_manager")
    return manager.all()


class SessionAuth(SessionAuthentication):
    def enforce_csrf(self, request):
        pass


class Pager(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10000
    
    def get_paginated_response(self, data):
        response = Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))
        
        last_updated = self.get_last_updated()
        if last_updated is not None:
            response['last_updated'] = last_updated
        
        return response
    
    def get_last_updated(self):
        viewset = getattr(self, 'viewset', None)
        if viewset is not None:
            last_updated = getattr(viewset, 'last_updated', None)
        else:
            last_updated = None
        return last_updated


class APIView(BaseView):
    api_request = None
    
    @classonlymethod
    def wrap_view(cls, view):
        from django.views.decorators.csrf import csrf_exempt
        
        view = csrf_exempt(view)
        return view
    
    def post_permission_cls(self):
        self.api_request = Request(self.get_request())
    
    #
    # Pagination
    #
    pagination_class = Pager
    
    def paginate_queryset(self, queryset):
        """
        Return a single page of results, or `None` if pagination is disabled.
        """
        paginator = self.paginator
        if paginator is None:
            return None
        
        request = self.api_request
        return paginator.paginate_queryset(queryset, request, view=self)
    
    def get_paginated_response(self, data):
        """
        Return a paginated style `Response` object for the given output data.
        """
        assert self.paginator is not None
        return self.paginator.get_paginated_response(data)
    
    # noinspection PyAttributeOutsideInit
    @property
    def paginator(self):
        """
        The paginator instance associated with the view, or `None`.
        """
        if not hasattr(self, '_paginator'):
            if self.pagination_class is None:
                self._paginator = None
            else:
                pager = self.pagination_class()
                pager.viewset = self
                self._paginator = pager
        
        return self._paginator
    
    #
    # Shortcuts
    #
    def list(self, queryset, serializer_class):
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)
        
        serializer = serializer_class(queryset, many=True)
        return Response(serializer.data)
    
    def single(self, instance, serializer_class):
        serializer = serializer_class(instance)
        return Response(serializer.data)
    
    def form_errors(self, form):
        d = {
            "err": True,
        }
        
        request = self.get_request()
        renderer = JSONResponse(request, d)
        
        renderer.add("errors", form.errors)
        
        return renderer
    
    #
    # DRF Functions
    #
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES
    
    format_kwarg = "format"
    
    def perform_content_negotiation(self, request, force=False):
        """
        Determine which renderer and media type to use render the response.
        """
        renderers = self.get_renderers()
        negotiator = self.get_content_negotiator()
        
        try:
            frmt = self.get_request().GET.get(self.format_kwarg)
            return negotiator.select_renderer(request, renderers, frmt)
        except Exception:
            if force:
                renderer = renderers[0]
                return renderer, renderer.media_type
            raise
    
    def send_response(self, fn, response):
        if type(response) is dict:
            request = self.get_request()
            return JSONResponse(request, response)
        
        if isinstance(response, Response):
            request = self.api_request
            
            if not getattr(request, 'accepted_renderer', None):
                neg = self.perform_content_negotiation(request, force=True)
                request.accepted_renderer, request.accepted_media_type = neg
            
            response.accepted_renderer = request.accepted_renderer
            response.accepted_media_type = request.accepted_media_type
            response.renderer_context = self.get_renderer_context()
        
        return response
    
    def get_renderer_context(self):
        """
        Returns a dict that is passed through to Renderer.render(),
        as the `renderer_context` keyword argument.
        """
        # Note: Additionally 'response' will also be added to the context,
        #       by the Response object.
        return {
            'view': self,
            'args': getattr(self, 'args', ()),
            'kwargs': getattr(self, 'kwargs', {}),
            'request': getattr(self, 'api_request', None)
        }
    
    def get_content_negotiator(self):
        from rest_framework.negotiation import DefaultContentNegotiation
        return DefaultContentNegotiation()
    
    def get_renderers(self):
        """
        Instantiates and returns the list of renderers that this view can use.
        """
        return [renderer() for renderer in self.renderer_classes]


class APIViewSet(ModelViewSet):
    serializer = None
    filter_kwargs = None
    _paginator = None
    
    pagination_class = Pager
    
    authentication_classes = [SessionAuth]
    
    @property
    def paginator(self):
        """
        The paginator instance associated with the view, or `None`.
        """
        if not hasattr(self, '_paginator'):
            if self.pagination_class is None:
                self._paginator = None
            else:
                pager = self.pagination_class()
                pager.viewset = self
                self._paginator = pager
        
        return self._paginator
    
    def get_object(self):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        filter_kwargs = getattr(self, 'filter_kwargs', None)
        if filter_kwargs is None:
            # Perform the lookup filtering.
            lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
            
            assert lookup_url_kwarg in self.kwargs, (
                'Expected view %s to be called with a URL keyword argument '
                'named "%s". Fix your URL conf, or set the `.lookup_field` '
                'attribute on the view correctly.' %
                (self.__class__.__name__, lookup_url_kwarg)
            )
            
            filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        
        queryset = self.filter_queryset(self.get_queryset())
        obj = get_object_or_404(queryset, **filter_kwargs)
        
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        
        return obj
    
    def get_queryset(self):
        cls = getattr(self, "queryset", None)
        if cls is not None:
            return cls
        
        model = getattr(self, "model", None)
        if model is not None:
            return get_queryset(model)
        
        return ModelViewSet.get_queryset(self)
    
    def get_serializer_class(self):
        cls = getattr(self, "serializer", None)
        if cls is not None:
            return cls
        
        cls = getattr(self, self.action + "_serializer_class", None)
        
        if cls is not None:
            return cls
        
        cls = GenericViewSet.get_serializer_class(self)
        return cls
    
    def render_success(self):
        return HttpResponse('{"success": true}')
    
    def render(self, data):
        return Response(data)
