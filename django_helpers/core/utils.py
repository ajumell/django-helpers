# coding=utf-8


class QuerySetModifier(object):
    def __init__(self, instance=None, mode="form"):
        self.mode = mode
        self.instance = instance
        self.modifiers = {}
        self.excludes = {}
        self.should_hide_fields = []

    def add_modifier(self, *names, **params):
        for name in names:
            self.modifiers[name] = params

    def add_excludes(self, *names, **params):
        for name in names:
            self.excludes[name] = params

    def add_hide_if_empty(self, *names):
        for name in names:
            self.should_hide_fields.append(name)

    def get_queryset(self, name, instance=None, field=None):
        if field is None:
            field = self.get_field(instance, name)

        key = 'queryset'
        return getattr(field, key, None)

    def set_queryset(self, name, queryset, instance=None, field=None):
        if field is None:
            field = self.get_field(instance, name)

        key = 'queryset'
        setattr(field, key, queryset)

    def get_field(self, instance, name):
        if self.mode in ('form', 'xeditable'):
            fields = instance.fields
            field = fields[name]

            return field

        if self.mode in ('listable',):
            field = instance.get_field_with_name(name)
            editable = field.editable

            return editable

    def hide_field(self, instance, name):
        if self.mode in ('form', 'xeditable'):
            fields = instance.fields
            del fields[name]

    def apply_name(self, name, instance=None, mode=None):
        self.apply_modifier(name, instance, mode)
        self.apply_exclude(name, instance, mode)

    # noinspection PyArgumentEqualDefault
    def apply_field(self, name, field, mode=None):
        self.apply_modifier(name, None, mode, field)
        self.apply_exclude(name, None, mode, field)

    def apply_queryset(self, queryset, name):
        queryset = self.apply_queryset_filter(queryset, name=name)
        queryset = self.apply_queryset_exclude(queryset, name=name)

        return queryset

    def apply(self, instance=None, mode=None):
        prev_mode = self.mode
        if mode is not None:
            self.mode = mode

        if instance is None:
            instance = self.instance

        modifiers = self.modifiers
        for name in modifiers:
            self.apply_modifier(name, instance)

        excludes = self.excludes
        for name in excludes:
            self.apply_exclude(name, instance)

        if self.mode in ('form', 'xeditable'):
            should_hide_fields = self.should_hide_fields
            for name in should_hide_fields:
                self.apply_hide_if_empty(name, instance)

        if mode is not None:
            self.mode = prev_mode

    def apply_exclude(self, name, instance=None, mode=None, field=None):
        prev_mode = self.mode
        if mode is not None:
            self.mode = mode

        if instance is None:
            instance = self.instance

        params = self.excludes[name]
        queryset = self.get_queryset(name, instance, field)
        if queryset is not None:
            queryset = self.apply_queryset_exclude(queryset, params)
            self.set_queryset(name, queryset, instance, field)

        if mode is not None:
            self.mode = prev_mode

    def apply_modifier(self, name, instance=None, mode=None, field=None):
        prev_mode = self.mode
        if mode is not None:
            self.mode = mode

        if instance is None:
            instance = self.instance

        params = self.modifiers[name]
        queryset = self.get_queryset(name, instance, field)
        if queryset is not None:
            queryset = self.apply_queryset_filter(queryset, params)
            self.set_queryset(name, queryset, instance, field)

        if mode is not None:
            self.mode = prev_mode

    def apply_queryset_exclude(self, queryset, params=None, name=None):
        if params is None:
            excludes = self.excludes
            if name not in excludes:
                return queryset

            params = excludes[name]

        model = getattr(queryset, 'model')
        default_queryset = getattr(model, '_default_manager')
        existing = default_queryset.filter(**params)
        queryset = queryset.exclude(pk__in=existing)

        return queryset

    def apply_queryset_filter(self, queryset, params=None, name=None):
        if params is None:
            modifiers = self.modifiers

            if name not in modifiers:
                return queryset

            params = modifiers[name]

        queryset = queryset.filter(**params)

        return queryset

    def apply_hide_if_empty(self, name, instance=None, mode=None, field=None):
        prev_mode = self.mode
        if mode is not None:
            self.mode = mode

        if instance is None:
            instance = self.instance

        queryset = self.get_queryset(name, instance, field)

        if queryset is not None:
            count = queryset.count()
            if count == 0:
                self.hide_field(instance, name)

        if mode is not None:
            self.mode = prev_mode


class ModelChangeDetector(object):
    def __init__(self, instance, *fields):
        self.instance = instance
        self.fields = fields
        self.cache = cache = {}

        cls = type(instance)
        if getattr(cls, '_deferred', False):
            self.valid = False
            return

        self.valid = True
        for field in fields:
            cache[field] = getattr(instance, field, None)

    def changed_list(self):
        if not self.valid:
            return []

        instance = self.instance
        fields = self.fields
        cache = self.cache
        result = []

        for field in fields:
            value = getattr(instance, field)
            if cache[field] != value:
                result.append(field)

        return result

    def revert(self, *fields):
        if not self.valid:
            return
        if len(fields) == 0:
            fields = self.fields

        instance = self.instance
        cache = self.cache

        for field in fields:
            value = cache[field]
            setattr(instance, field, value)

    def original_value(self, field):
        if not self.valid:
            return None

        return self.cache.get(field)
