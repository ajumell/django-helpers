# coding=utf-8


class JavascriptWidget(object):
    def setup_static_files(self):
        raise NotImplementedError()

    def js_files(self):
        raise NotImplementedError()

    def css_files(self):
        raise NotImplementedError()

    def render_js(self):
        raise NotImplementedError()

    def render(self):
        raise NotImplementedError()
