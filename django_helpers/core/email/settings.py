# coding=utf-8
from django.conf import settings

__author__ = 'ajumell'

SEND_GRID_KEY = getattr(settings, "SEND_GRID_KEY", "")

MAIL_GUN_KEY = getattr(settings, "MAIL_GUN_KEY", "")

MAIL_GUN_DOMAIN = getattr(settings, "MAIL_GUN_DOMAIN", "")

MANDRILL_KEY = getattr(settings, "MANDRILL_KEY", "")

MANDRILL_DOMAIN = getattr(settings, "MANDRILL_DOMAIN", "")
