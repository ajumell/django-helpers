# coding=utf-8
from os.path import basename

from django.core.files.uploadedfile import InMemoryUploadedFile

from django_helpers.helpers.views import render_to_string
from django.utils.html import strip_tags
from mimetypes import guess_type


class Emailer(object):
    def __init__(self, subject='', body='', **kwargs):
        self.body = body
        self.subject = subject
        self.attachments = []
        self.recipients = []
        self.from_address = ''
        self.template = None
        self.template_dict = {}
        self.request = None
        self.html = None

        if 'template' in kwargs:
            template = kwargs.get('template')
            template_dict = kwargs.get('template_data')

            self.set_template(template, template_dict)

    #
    # Getters
    #
    def get_body(self):
        if self.html:
            return self.html

        if self.body:
            html = self.body
            self.html = html

            return html

        if self.template:
            request = self.request
            template = self.template
            template_dict = self.template_dict

            html = render_to_string(template, template_dict, request)
            self.html = html

            return html

        return ''

    def get_html(self):
        return self.get_body()

    def get_text(self):
        return strip_tags(self.get_body())

    def get_attachments(self):
        for attachment in self.attachments:
            yield self.sanitize_attachment(attachment)

    #
    # Setters
    #
    def attach(self, attachment):
        if attachment is not None:
            self.attachments.append(attachment)

    def add_recipient(self, email, name=None):
        recipient = self.sanitize_email(email, name)
        self.recipients.append(recipient)

    def set_from_address(self, email, name=None):
        address = self.sanitize_email(email, name)
        self.from_address = address

    def sanitize_email(self, email, name):
        if name:
            address = '{} <{}>'.format(name, email)
        else:
            address = email
        return address

    def set_template(self, template, data=None):
        self.template = template
        self.template_dict = data

    def add_template_data(self, name, value):
        self.template_dict[name] = value

    #
    # Process
    #
    def send_text(self):
        raise NotImplementedError()

    def send_html(self):
        raise NotImplementedError()

    def send(self):
        self.send_html()

    #
    # Utils
    #
    def sanitize_attachment(self, attachment):
        is_file = isinstance(attachment, InMemoryUploadedFile)

        file_pointer = attachment.file if is_file else open(attachment)
        # contents = file_pointer.read()
        # file_pointer.close()

        if is_file:
            file_name = basename(attachment.name)
            file_type = attachment.content_type
        else:
            file_name = basename(attachment)
            file_type = self.guess_mime(file_name)

        return file_pointer, file_name, file_type

    def guess_mime(self, file_name):
        file_type = None
        file_types = guess_type(file_name)
        for file_type in file_types:
            if file_type is not None:
                file_type = file_type
                break
        return file_type
