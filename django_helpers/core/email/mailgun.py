# coding=utf-8
from __future__ import unicode_literals

import requests

from django_helpers.core.email.settings import MAIL_GUN_DOMAIN, MAIL_GUN_KEY
from django_helpers.core.email.emailer import Emailer

BASE_URL = 'https://api.mailgun.net/v2'


class Email(Emailer):
    def __init__(self, subject='', body='', **kwargs):
        Emailer.__init__(self, subject, body, **kwargs)

        self.auth = ('api', self.get_api_key(kwargs))
        self.base_url = '{0}/{1}'.format(BASE_URL, self.get_domain(kwargs))

    def post(self, path, data, files=None, include_domain=True):
        url = self.base_url if include_domain else BASE_URL
        return requests.post(url + path, auth=self.auth, data=data, files=files)

    def post_message(self, html=True):
        data = self.get_data(html)
        files = self.get_files()

        print files

        return self.post('/messages', data, files=files)

    def get_domain(self, kwargs):
        if 'domain' in kwargs:
            return kwargs['domain']

        return MAIL_GUN_DOMAIN

    def get_api_key(self, kwargs):
        if 'api_key' in kwargs:
            return kwargs['api_key']

        return MAIL_GUN_KEY

    def get_files(self):
        files = []

        # if inlines:
        #     for filename in inlines:
        #         files.append(('inline', open(filename)))

        for content, filename, content_type in self.get_attachments():
            files.append(('attachment', (filename, content, content_type)))

        return files

    def get_data(self, html=False):
        data = {
            'from': self.from_address,
            'to': self.recipients,
            # 'cc': cc or [],
            # 'bcc': bcc or [],
            'subject': self.subject,
            'text': self.get_text(),
        }

        if html:
            data['html'] = self.get_html()

        # if reply_to:
        #     data['h:Reply-To'] = reply_to

        # if headers:
        #     for k, v in headers.items():
        #         data["h:%s" % k] = v

        # if campaign_id:
        #     data['o:campaign'] = campaign_id

        return data

    def send_html(self):
        return self.post_message(True)

    def send_text(self):
        self.post_message(False)
