# coding=utf-8

from django.core.mail.message import EmailMultiAlternatives

from django_helpers.core.email.emailer import Emailer

__author__ = 'ajumell'


class Email(Emailer):
    def get_mail(self):
        """
        :return: django.core.mail.message.EmailMultiAlternatives
        """
        body = self.get_text()
        subject = self.subject
        recipients = self.recipients
        from_email = self.from_address

        mail = EmailMultiAlternatives(subject, body, from_email, recipients)
        for attachment in self.attachments:
            contents, file_name, file_type = self.sanitize_attachment(attachment)
            mail.attach(file_name, contents, file_type)

        return mail

    def send_html(self):
        html = self.get_html()
        mail = self.get_mail()
        mail.attach_alternative(html, "text/html")
        mail.send()

    def send_text(self):
        mail = self.get_mail()
        mail.send()
