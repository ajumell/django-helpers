# coding=utf-8
def get_mailer(provider, subject='', body='', **kwargs):
    if provider == 'smtp':
        from smtp import Email
    elif provider == 'mailgun':
        from mailgun import Email
    else:
        raise ValueError("Invalid Provider")

    return Email(subject, body, **kwargs)
