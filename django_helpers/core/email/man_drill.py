# coding=utf-8
from __future__ import unicode_literals

import mandrill

from django_helpers.core.email.settings import MANDRILL_KEY
from django_helpers.core.email.emailer import Emailer

BASE_URL = 'https://api.mailgun.net/v2'


class Email(Emailer):
    def __init__(self, subject='', body='', **kwargs):
        Emailer.__init__(self, subject, body, **kwargs)

    def post(self, html):
        try:
            mandrill_client = mandrill.Mandrill(MANDRILL_KEY)
            message = {
                'attachments': [
                    {
                        'content': 'ZXhhbXBsZSBmaWxl',
                        'name': 'myfile.txt',
                        'type': 'text/plain'
                    }
                ],

                'auto_html': None,
                'auto_text': None,

                'bcc_address': 'message.bcc_address@example.com',
                'from_email': 'message.from_email@example.com',
                'from_name': 'Example Name',
                'global_merge_vars': [
                    {
                        'content': 'merge1 content',
                        'name': 'merge1'
                    }
                ],

                'headers': {
                    'Reply-To': 'message.reply@example.com'
                },

                'html': html,
                'images': [
                    {'content': 'ZXhhbXBsZSBmaWxl',
                        'name': 'IMAGECID',
                        'type': 'image/png'
                    }
                ],

                'important': False,
                'inline_css': None,
                'merge': True,
                'merge_language': 'mailchimp',

                'preserve_recipients': None,
                'return_path_domain': None,
                'signing_domain': None,
                'subaccount': 'customer-123',
                'subject': 'example subject',
                'tags': ['password-resets'],
                'text': 'Example text content',
                'to': [{'email': 'recipient.email@example.com',
                    'name': 'Recipient Name',
                    'type': 'to'}],
                'track_clicks': None,
                'track_opens': None,
                'tracking_domain': None,
                'url_strip_qs': None,
                'view_content_link': None}
            result = mandrill_client.messages.send(message=message, async=False, ip_pool='Main Pool', send_at='example send_at')
            '''
            [{'_id': 'abc123abc123abc123abc123abc123',
              'email': 'recipient.email@example.com',
              'reject_reason': 'hard-bounce',
              'status': 'sent'}]
            '''

        except mandrill.Error, e:
            # Mandrill errors are thrown as exceptions
            print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
            # A mandrill error occurred: <class 'mandrill.UnknownSubaccountError'> - No subaccount exists with the id 'customer-123'
            raise

    def send_html(self):
        return self.post_message(True)

    def send_text(self):
        self.post_message(False)
