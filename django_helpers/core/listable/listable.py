# coding=utf-8
from inspect import getargspec
from json import dumps
from math import ceil

from django.db.models import Q, Sum, Avg, Min, Max, Count
from django.http import HttpResponse

from django_helpers.apps.static_manager.utils import get_js_files, get_css_files
from django_helpers.apps.xeditable.editable import BaseModelXEditable
from django_helpers.widgets.ajax.widget import AJAXWidget

_annotates = {
    'sum': Sum,
    'avg': Avg,
    'min': Min,
    'max': Max,
    'count': Count,
}


def validate_annotate(name):
    return name in _annotates


# noinspection PyAbstractClass
class BaseModelListable(AJAXWidget):
    listable_class = None
    
    display_length = 10
    fields = None
    url_search_parameters = []
    
    def __init__(self, request):
        AJAXWidget.__init__(self, request)
        
        self._data = None
        self.refresh_columns(request)
        self.url_kwargs = None
        self.fields = self.fields[:]
        
        self.filters = None
        
        field_names = []
        i = 0
        for field in self.fields:
            field.controller = self
            field.index = i
            field_names.append(field.field)
            i += 1
        
        self._column_names = field_names
    
    def setup_filter(self):
        if self.filters is not None:
            return
        
        self.filters = {}
        filters = self.get_filters(self.url_kwargs)
        
        for fltr in filters:
            self.filters[fltr.name] = fltr
    
    #
    # Filter
    #
    def get_filters(self, kwargs):
        return []
    
    def filter(self, query, data_dict=None):
        self.setup_filter()
        for name, fltr in self.filters.items():
            query = fltr.query(query, data_dict)
        return query
    
    def preprocess_filter(self, filter):
        k = '__rendered_preprocessors'
        
        if getattr(filter, k, False) is True:
            return
        
        from filters.preprocessors import add_form_control_class
        add_form_control_class(filter)
        setattr(filter, k, True)
    
    #
    # Static Files
    #
    # noinspection PyAttributeOutsideInit
    def setup_static_files(self):
        js_files = []
        css_files = []
        
        for field in self.fields:
            js_files += get_js_files(field)
            css_files += get_css_files(field)
        
        self.js_files = js_files
        self.css_files = css_files
        
        self.extra_css_files(css_files)
        self.extra_js_files(js_files)
        
        self.filter_css_files(css_files)
        self.filter_js_files(js_files)
    
    def filter_js_files(self, js):
        self.setup_filter()
        for name, field in self.filters.items():
            widget = field.widget
            js += get_js_files(widget)
        return js
    
    def filter_css_files(self, css):
        self.setup_filter()
        for name, field in self.filters.items():
            widget = field.widget
            css += get_css_files(widget)
        return css
    
    def has_tipsy(self):
        if not hasattr(self, '_has_tipsy'):
            has_tipsy = False
            i = 0
            for field in self.fields:
                i += 1
                txt = getattr(field, 'tipsy_tip', False)
                if txt:
                    has_tipsy = True
                    d = {
                        'index': i,
                        "txt": txt,
                    }
                    position = getattr(field, 'tipsy_position', None)
                    if position is not None:
                        d['position'] = position
                    
                    is_html = getattr(field, 'tipsy_is_html', None)
                    if is_html is not None:
                        d['is_html'] = is_html
            self._has_tipsy = has_tipsy
        
        return self._has_tipsy
    
    def has_drop_zone(self):
        if not hasattr(self, '_has_drop_zone'):
            for field in self.fields:
                if getattr(field, 'drop_zone'):
                    self._has_drop_zone = True
                    break
        
        # noinspection PyUnresolvedReferences
        return self._has_drop_zone
    
    def is_editable(self):
        if not hasattr(self, '_is_editable'):
            is_editable = False
            for field in self.fields:
                xeditable = field.xeditable
                if xeditable:
                    xeditable.set_controller(self)
                    is_editable = True
            
            self._is_editable = is_editable
        
        return self._is_editable
    
    def extra_css_files(self, css_files):
        pass
    
    def extra_js_files(self, css_files):
        pass
    
    #
    # Query
    #
    # noinspection PyUnusedLocal
    def get_query(self, request, kwargs=None):
        if hasattr(self, 'query'):
            return self.query
    
    def process_extra_params(self, query, kwargs):
        extra_params = self.url_search_parameters
        if extra_params is not None:
            filter_dict = {}
            for param_name in extra_params:
                param_value = kwargs.get(param_name, '')
                if param_value != '':
                    filter_dict[param_name] = param_value
            query = query.filter(**filter_dict)
        return query
    
    def annotate(self, query):
        fields = self.fields
        for field in fields:
            model_field = field.field
            annotate = field.annotate
            
            if annotate is not None:
                i = model_field.rfind('__')
                model_field = model_field[:i]
                fn = _annotates[annotate]
                query = query.annotate(fn(model_field))
        return query
    
    def _get_query(self, request, kwargs=None):
        query = self.get_query(request, kwargs)
        query = self.process_extra_params(query, kwargs)
        query = self.annotate(query)
        return query
    
    #
    # Filtering and Sorting
    #
    def get_search_term(self, data_dict, search_term):
        raise NotImplementedError()
    
    def search(self, query, data_dict=None, search_term=None):
        columns = self.fields
        
        search_term = self.get_search_term(data_dict, search_term)
        
        if search_term is None:
            return query
        
        search = None
        for field in columns:
            model_field = field.get_search_arg()
            
            if field.searchable and search_term != "":
                kwargs = {model_field: search_term}
                if search is None:
                    search = Q(**kwargs)
                else:
                    search = search | Q(**kwargs)
        
        if search is not None:
            query = query.filter(search)
        
        return query
    
    def page_data(self, query, data_dict=None, start=None, max_items=None):
        raise NotImplementedError()
    
    def do_form_filter(self, query, data_dict):
        raise NotImplementedError()
    
    def sort(self, query, data_dict):
        raise NotImplementedError()
    
    # noinspection PyShadowingBuiltins
    def initial_pagination(self, request, count, page=None):
        qs = self.id() + '-page'
        
        if page is None:
            try:
                page = request.GET.get(qs, 1)
                page = int(page)
            except ValueError:
                page = 1
        
        per_page = self.display_length
        hits = max(1, count)
        num_pages = int(ceil(hits / float(per_page)))
        prev_pages = []
        next_pages = []
        
        next_count = 4
        prev = page - 1
        
        while len(prev_pages) < 2 and prev > 0:
            if prev == 1:
                prev_pages.append((prev, None))
                break
            prev_pages.append((prev, prev))
            next_count -= 1
            prev -= 1
        prev_pages.reverse()
        
        next = page + 1
        while len(next_pages) < next_count and next <= num_pages:
            next_pages.append((next, next))
            next += 1
        
        paginator = {
            'from': per_page * (page - 1) + 1,
            'to': count if page == num_pages else page * per_page,
            'has_next': page < num_pages,
            'has_prev': page > 1,
            
            'current': page,
            
            'next_page': page + 1,
            'next_pages': next_pages,
            
            'prev_pages': prev_pages,
            'prev_page': None if page - 1 == 1 else page - 1,
            
            'qs': qs
        }
        
        return paginator
    
    #
    # Optimisation
    #
    def only(self, query):
        fields = self.fields
        model_fields = []
        need_related = False
        aggregate_fields = self.get_aggregate_fields()
        related_fields = []
        
        for field in fields:
            field_need_related = field.need_related()
            need_related = need_related or field_need_related
            column_model_fields = field.get_fields()
            annotate = field.annotate
            
            for model_field in column_model_fields:
                if annotate is None and model_field not in aggregate_fields:
                    model_fields.append(model_field)
                    if field_need_related:
                        related_fields.append(model_field)
        
        related_fields = tuple(related_fields)
        
        if need_related:
            query = query.select_related(*related_fields)
        
        extra_fields = self.get_extra_fields()
        if type(extra_fields) in (list, tuple):
            model_fields.extend(extra_fields)
        
        model_fields = tuple(model_fields)
        query = query.only(*model_fields)
        return query
    
    def get_extra_fields(self):
        extra_fields = getattr(self, 'extra_fields', None)
        return extra_fields
    
    def get_aggregate_fields(self):
        if not hasattr(self, "aggregate_fields"):
            aggregate_fields = []
            setattr(self, "aggregate_fields", aggregate_fields)
        
        return getattr(self, "aggregate_fields")
    
    #
    # Fields
    #
    def field_at_index(self, index):
        return self.fields[index]
    
    def field_with_name(self, name):
        index = self._column_names.index(name)
        return self.field_at_index(index)
    
    def refresh_columns(self, request):
        get_columns = getattr(self, 'get_columns', None)
        
        if get_columns is None:
            get_columns = getattr(self, 'get_fields', None)
        
        if hasattr(get_columns, '__call__'):
            argspec = getargspec(get_columns)
            arg_names = argspec.args
            kwargs = {}
            if 'request' in arg_names:
                if request is None:
                    raise Exception('Request is needed to get columns.')
                kwargs['request'] = request
            
            if 'user' in arg_names:
                kwargs['user'] = request.user
            
            columns = get_columns(**kwargs)
            
            if isinstance(columns, list):
                self.fields = columns
        
        if len(self.fields) is 0:
            raise Exception('There are no columns.')
    
    #
    # Item ID
    #
    def get_item_pk(self):
        raise NotImplementedError()
    
    def can_set_params(self):
        if self._data is not None:
            raise Exception("Parameters cannot be set after render data is generated.")
    
    #
    # AJAX Rendering
    #
    def generate_ajax_json(self, query, request, data_dict, total_length, current_length):
        raise NotImplementedError()
    
    def get_data(self, request):
        kwargs = self.get_params()
        
        data_dict = request.GET
        query = self._get_query(request, kwargs)
        total_length = query.count()
        query = self.search(query, data_dict)
        query = self.filter(query, data_dict)
        query = self.do_form_filter(query, data_dict)
        query = self.sort(query, data_dict)
        query = self.only(query)
        current_length = query.count()
        query = self.page_data(query, data_dict)
        result = self.generate_ajax_json(query, request, data_dict, total_length, current_length)
        return HttpResponse(dumps(result))
    
    #
    # Rendering
    #
    def __getattr__(self, item):
        if item.startswith("filter_"):
            name = item[7:]
            self.setup_filter()
            fltr = self.filters.get(name)
            if fltr:
                self.preprocess_filter(fltr)
                return fltr.render(self.widget_id())
        
        raise AttributeError()


# noinspection PyAbstractClass
class BaseModelEditableListable(BaseModelListable, BaseModelXEditable):
    editable_mode = 'bootstrap3'
    
    def setup_static_files(self):
        super(BaseModelEditableListable, self).setup_static_files()
        js_files = self.js_files
        css_files = self.css_files
        
        for field in self.fields:
            xeditable = field.xeditable
            if xeditable is not None:
                js_files += get_js_files(xeditable)
                css_files += get_css_files(xeditable)
        
        if self.is_editable():
            if not self.get_item_pk():
                raise Exception("Row ID is required when using editables.")
            
            if 'django-xeditable-js' not in js_files:
                js_files.append('django-xeditable-js')
            
            js_files.append("xeditable-%s-base" % self.editable_mode)
    
    def refresh_columns(self, request):
        super(BaseModelEditableListable, self).refresh_columns(request)
        
        for field in self.fields:
            xeditable = getattr(field, 'xeditable', None)
            if xeditable:
                xeditable.set_controller(self)
    
    #
    # Editable Feature
    #
    
    # noinspection PyAttributeOutsideInit
    def get_editable_with_name(self, name):
        field = self.field_with_name(name)
        if not field:
            return None
        
        editable = field.xeditable
        return editable
    
    def get_model(self):
        model = getattr(self, 'model', None)
        
        if model is None:
            model = self.get_query(self.request).model
            setattr(self, 'model', model)
        
        return model
    
    # noinspection PyAttributeOutsideInit
    def get_instance(self):
        instance = getattr(self, "_instance", None)
        if instance is None:
            pk = self.get_pk()
            query = self.get_query(self.request)
            instance = query.get(**{
                self.get_item_pk(): pk
            })
            self._instance = instance
        return instance
    
    def get_pk(self):
        if hasattr(self, 'pk'):
            return self.pk
        return None
    
    def render_value_for_field(self, name, value):
        field = self.field_with_name(name)
        return field.get_text(self.request, self.get_instance())
    
    def get_widget_id(self, field, pk):
        if pk is None:
            pk = self.get_pk()
        
        return "editable-{}-{}-{}".format(self.id(), field, pk)
