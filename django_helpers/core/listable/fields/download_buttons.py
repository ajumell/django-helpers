# coding=utf-8
__author__ = 'ajumell'

from bootstrap_buttons import BootstrapButton
from links import StaticLinkField
from django_helpers.utils.models import get_value


# noinspection PyBroadException
def get_download_link(instance, field):
    if type(field) in (list, tuple):
        raise Exception("Download button cannot have multiple fields.")
    else:
        value = get_value(instance, field)
        try:
            link = value.url
        except:
            link = "#"
    return link


class DownloadLink(StaticLinkField):
    def __init__(self, text, title_width="20px", title='', field='id', title_class=None, search_lookup='contains'):
        StaticLinkField.__init__(self, text, 'download', title_width, title, field, title_class, search_lookup)

    def get_widget(self, request, instance):
        widget = StaticLinkField.get_widget(self, request, instance)
        widget.attr('target', "_blank")
        return widget

    def get_link(self, instance, field=None):
        field = field or self.field
        return get_download_link(instance, field)


class BootstrapDownloadButton(BootstrapButton):
    def __init__(self, text, color, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapButton.__init__(self, text, color, 'download', icon, size, title_class, title_width, title, field)

    def get_widget(self, request, instance):
        widget = BootstrapButton.get_widget(self, request, instance)
        widget.attr('target', "_blank")
        return widget

    def get_link(self, instance, field=None):
        field = field or self.field
        return get_download_link(instance, field)


class BootstrapInfoDownloadButton(BootstrapDownloadButton):
    def __init__(self, text, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapDownloadButton.__init__(self, text, 'info', icon, size, title_class, title_width, title, field)


class BootstrapSuccessDownloadButton(BootstrapDownloadButton):
    def __init__(self, text, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapDownloadButton.__init__(self, text, 'success', icon, size, title_class, title_width, title, field)


class BootstrapErrorDownloadButton(BootstrapDownloadButton):
    def __init__(self, text, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapDownloadButton.__init__(self, text, 'danger', icon, size, title_class, title_width, title, field)


class BootstrapWarningDownloadButton(BootstrapDownloadButton):
    def __init__(self, text, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapDownloadButton.__init__(self, text, 'warning', icon, size, title_class, title_width, title, field)


class BootstrapPrimaryDownloadButton(BootstrapDownloadButton):
    def __init__(self, text, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapDownloadButton.__init__(self, text, 'primary', icon, size, title_class, title_width, title, field)
