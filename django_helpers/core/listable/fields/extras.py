# coding=utf-8
from django_helpers.widgets import Link, Input
from field import TextField

__all__ = (
    'EmailField',
    'CheckboxField'
)


class EmailField(TextField):
    can_be_editable = False

    def __init__(self, field, title, searchable, sortable, title_class=None, title_width=None,
                 annotate=None, search_lookup='contains', format_expression=None):
        TextField.__init__(self, field, title, searchable, sortable, title_class, title_width, annotate, search_lookup,
                           format_expression)

    def get_widget(self, request, instance):
        value = self.get_text(request, instance)
        obj = Link(value, 'mailto:' + value)
        return obj


class CheckboxField(TextField):
    can_be_editable = False

    def __init__(self, title_class=None, name=None, extra_classes=None, title_width="25px", title='', field='id'):
        TextField.__init__(self, field, title, False, False, title_class, title_width)
        # self.title = mark_safe('<input type="checkbox">')
        self.name = name
        self.extra_classes = extra_classes

    def get_widget(self, request, instance):
        obj = Input(self.get_text(request, instance))
        obj.type = 'checkbox'
        obj.add_class(self.extra_classes)
        obj.attr('name', self.name)

        return obj
