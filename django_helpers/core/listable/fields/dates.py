# coding=utf-8
from django.contrib.humanize.templatetags.humanize import naturaltime, naturalday
from django.utils.dateparse import parse_datetime, parse_date
from django.utils.timesince import timesince

from django_helpers.apps.xeditable import DatePickerEditable
from django_helpers.humaize import *
from field import TextField
from django_helpers.utils.models import get_value

__all__ = (
    'DateField',
    'NaturalTimeField',
    'NaturalDayField',
    'TimeSinceField',
    'AgeField'
)


class DateField(TextField):
    EditableField = DatePickerEditable

    def __init__(self, field, title, searchable=False, sortable=False, date_format="%d, %b %Y", title_class=None, title_width=None,
                 annotate=None, search_lookup='contains', format_expression=None):
        TextField.__init__(self, field, title, searchable, sortable, title_class, title_width, annotate, search_lookup,
                           format_expression)
        self.date_format = date_format

    def get_text(self, request, instance):
        value = get_value(instance, self.field)
        if value is None:
            return ""

        if self.annotate is not None:
            str_value = value
            value = parse_date(str_value)
            if value is None:
                value = parse_datetime(str_value)
        # value = date(value, self.date_format)
        if self.date_format is not None:
            value = value.strftime(self.date_format)

        if self.format_expression is not None:
            value = self.format_expression % value

        return value


class NaturalTimeField(TextField):
    EditableField = DatePickerEditable

    def text_transform_function(self, text):
        return naturaltime(text)


class NaturalDayField(TextField):
    EditableField = DatePickerEditable

    def text_transform_function(self, text):
        return naturalday(text)


class TimeSinceField(TextField):
    EditableField = DatePickerEditable

    def get_text(self, request, instance):
        value = TextField.get_text(self, request, instance)
        # TODO: Convert to function
        return str(timesince(value)) + " ago"


class AgeField(DateField):
    EditableField = DatePickerEditable

    def get_text(self, request, instance):
        value = TextField.get_text(self, request, instance)
        # TODO: Convert to function
        return str(age(value)) + " years old"
