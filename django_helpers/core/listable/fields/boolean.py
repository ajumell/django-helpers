# coding=utf-8
from django_helpers.utils.models import get_value
from field import TextField


class YesNoField(TextField):
    can_be_editable = False

    def __init__(self, field, title='', title_class=None, title_width="25px"):
        TextField.__init__(self, field, title, False, False, title_class, title_width)

    def get_text(self, request, instance):
        obj = get_value(instance, self.field)
        text = "Yes" if obj else "No"

        return text
