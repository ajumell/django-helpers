# coding=utf-8
from django_helpers.apps.static_manager.utils import get_js_files, get_css_files
from django_helpers.widgets import bootstrap_widgets
from field import TextField
from links import StaticLinkField

__all__ = (
    'BootstrapButton',
    'BootstrapButtonSet',
    'BootstrapInfoButton',
    'BootstrapSuccessButton',
    'BootstrapErrorButton',
    'BootstrapWarningButton',
    'BootstrapPrimaryButton',
    'BootstrapDropdownButton',
    'BootstrapVerticalButtonSet',
)


class BootstrapButton(StaticLinkField):
    icon_prefix = 'icon'

    def __init__(self, text, color, link, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        StaticLinkField.__init__(self, text, link, title_width, title, field, title_class)
        self.text = text
        self.color = color
        self.link = link

        if hasattr(self, 'icon'):
            if icon != '' and icon is not None:
                self.icon = icon
        else:
            self.icon = icon

        if hasattr(self, 'size'):
            if size != '' and size is not None:
                self.size = size
        else:
            self.size = size

    def get_widget(self, request, instance):
        text = self.get_text(request, instance)
        url = self.get_link(instance)
        btn = bootstrap_widgets.LinkButton(text, url)

        btn.color = self.color
        btn.icon = self.icon
        btn.size = self.size

        return btn


class BootstrapButtonSet(TextField):
    icon_prefix = 'icon'
    can_be_editable = False

    def __init__(self, buttons=None, field='id', title='', title_class=None, title_width="auto"):
        TextField.__init__(self, field, title, False, False, title_class, title_width)
        if buttons is None:
            buttons = []

        self.buttons = buttons

    def add_button(self, btn):
        if not btn:
            return

        self.buttons.append(btn)

    def has_buttons(self):
        return len(self.buttons) > 0

    def remove_button(self, index):
        return self.buttons.pop(index)

    def get_widget(self, request, instance):
        btn = bootstrap_widgets.BootstrapButtonSet()
        for button in self.buttons:
            code = button.render(request, instance)
            btn.add(code)

        return btn

    def js_files(self):
        files = []
        for btn in self.buttons:
            files += get_js_files(btn)

        return files

    def css_files(self):
        files = []
        for btn in self.buttons:
            files += get_css_files(btn)

        return files


class BootstrapVerticalButtonSet(BootstrapButtonSet):
    def get_widget(self, request, instance):
        btn = bootstrap_widgets.BootstrapVerticalButtonSet()
        for button in self.buttons:
            code = button.render(request, instance)
            btn.add(code)

        return btn



class BootstrapDropdownDivider(object):
    # noinspection PyUnusedLocal
    def render(self, *args, **kwargs):
        return '<li class="divider"></li>'


class BootstrapDropdownButton(TextField):
    can_be_editable = False

    def __init__(self, text, color='default', icon='', size=None, buttons=None, field='id', title='', title_class=None, title_width="20px", ):
        TextField.__init__(self, field, title, False, False, title_class, title_width)
        self.size = size
        self.icon = icon
        self.color = color
        self.text = text

        if buttons is None:
            buttons = []

        self.buttons = buttons

    def add_button(self, btn):
        self.buttons.append(btn)

    def add_divider(self):
        self.buttons.append(BootstrapDropdownDivider())

    def has_buttons(self):
        return len(self.buttons) > 0

    def remove_button(self, index):
        return self.buttons.pop(index)

    def get_widget(self, request, instance):
        btn = bootstrap_widgets.DropDownLinkButton(self.text, self.color)
        for button in self.buttons:
            code = button.render(request, instance)
            btn.add(code)

        return btn

    def js_files(self):
        files = []
        for btn in self.buttons:
            files += get_js_files(btn)

        return files

    def css_files(self):
        files = []
        for btn in self.buttons:
            files += get_css_files(btn)

        return files


class BootstrapInfoButton(BootstrapButton):
    def __init__(self, text, link, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapButton.__init__(self, text, 'info', link, icon, size, title_class, title_width, title, field)


class BootstrapSuccessButton(BootstrapButton):
    def __init__(self, text, link, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapButton.__init__(self, text, 'success', link, icon, size, title_class, title_width, title, field)


class BootstrapErrorButton(BootstrapButton):
    def __init__(self, text, link, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapButton.__init__(self, text, 'danger', link, icon, size, title_class, title_width, title, field)


class BootstrapWarningButton(BootstrapButton):
    def __init__(self, text, link, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapButton.__init__(self, text, 'warning', link, icon, size, title_class, title_width, title, field)


class BootstrapPrimaryButton(BootstrapButton):
    def __init__(self, text, link, icon='', size=None, title_class=None, title_width="85px", title='', field='id'):
        BootstrapButton.__init__(self, text, 'primary', link, icon, size, title_class, title_width, title, field)
