# coding=utf-8
from bootstrap_buttons import BootstrapButton
from django_helpers.utils.models import get_value

__all__ = (
    'BootstrapToggleButton',
)


class BootstrapToggleButton(BootstrapButton):
    def __init__(self, field, bool_field, *args, **kwargs):
        kwargs['field'] = field
        BootstrapButton.__init__(self, "", "", "", *args, **kwargs)
        self.bool_field = bool_field

        self.true_link = ""
        self.true_color = ""
        self.true_text = ""

        self.false_link = ""
        self.false_color = ""
        self.false_text = ""

        self.extra_fields.append(bool_field)

    def set_true_case(self, text, link, color):
        self.true_link = link
        self.true_text = text
        self.true_color = color

    def set_false_case(self, text, link, color):
        self.false_link = link
        self.false_text = text
        self.false_color = color

    def get_widget(self, request, instance):
        condition = get_value(instance, self.bool_field)
        if condition:
            self.color = self.true_color
            self.text = self.true_text
            self.link = self.true_link
        else:
            self.text = self.false_text
            self.color = self.false_color
            self.link = self.false_link

        return BootstrapButton.get_widget(self, request, instance)
