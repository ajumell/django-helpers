# coding=utf-8
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse

from django_helpers.js_plugins.core import AjaxLinkPlugin, ModalLinkPlugin, ConfirmPlugin, DropZonePlugin
from django_helpers.utils.models import get_value
from django_helpers.widgets import Link, IconLink, Icon
from field import TextField

__all__ = (
    'StaticLinkField',
    'LinkField',
    'IconLinkField',
)


class StaticLinkField(TextField):
    can_be_editable = False

    def __init__(self, text, link, title_width="20px", title='', field='id', title_class=None, search_lookup='contains'):
        TextField.__init__(self, field, title, False, False, title_class, title_width, search_lookup=search_lookup)
        self.text = text
        self.link = link

    # noinspection PyBroadException
    def get_link(self, instance, field=None):
        field = field or self.field
        if type(field) in (list, tuple):
            args = []
            for f in field:
                value = get_value(instance, f)
                args.append(value)
            try:
                link = reverse(self.link, args=tuple(args))
            except:
                link = self.link
        else:
            value = get_value(instance, field)
            try:
                link = reverse(self.link, args=(
                    value,
                ))
            except:
                link = self.link
        return link

    def get_text(self, request, instance):
        return self.text

    def get_widget(self, request, instance):
        url = self.get_link(instance)
        widget = Link(self.text, url)

        return widget

    #
    # Javascript Plugins
    #

    # noinspection PyAttributeOutsideInit
    def set_ajax(self, processing_text=None):
        current = getattr(self, 'ajax_processing_text', 'Loading...')
        if processing_text is None:
            processing_text = current

        ajax_obj = AjaxLinkPlugin()
        ajax_obj.loading = processing_text

        self.js_plugins.add(ajax_obj)

    def set_modal(self, loading_text='Loading...', title=None):
        modal = ModalLinkPlugin()
        modal.title = title
        modal.loading = loading_text

        self.js_plugins.add(modal)

    # noinspection PyAttributeOutsideInit
    def set_confirm(self, question, title=None, yes_text="Yes", no_text="No", yes_color="success", no_color=""):
        confirm = ConfirmPlugin()
        confirm.need_confirm = True

        confirm.question = question
        confirm.title = title

        confirm.yes_text = yes_text
        confirm.yes_color = yes_color

        confirm.no_text = no_text
        confirm.no_color = no_color

        self.js_plugins.add(confirm)

    def set_dropzone(self, dropzone):
        if dropzone.hidden_fields:
            raise ValidationError('Dropzone with hidden fields cannot be used in Listables.')

        if dropzone.url_search_parameters:
            self.extra_fields.extend(dropzone.url_search_parameters)

        plugin = DropZonePlugin(dropzone)
        self.js_plugins.add(plugin)


class LinkField(StaticLinkField):
    def __init__(self, field, link, title, searchable, sortable, link_field=None, title_class=None, title_width=None, search_lookup='contains',
                 format_expression=None):
        StaticLinkField.__init__(self, "", link, title_width, title, field, title_class, search_lookup)
        self.link = link
        self.link_field = link_field
        self.format_expression = format_expression
        self.searchable = searchable
        self.sortable = sortable

        if type(link_field) in (list, tuple):
            self.extra_fields.extend(link_field)
        else:
            self.extra_fields.append(link_field)

    def get_text(self, request, instance):
        value = get_value(instance, self.field, self.format_expression)
        return value

    def get_widget(self, request, instance):
        text = self.get_text(request, instance)
        link = self.get_link(instance, self.link_field or self.field)

        obj = Link(text, link)
        return obj


class IconLinkField(StaticLinkField):
    def __init__(self, icon, field, link, title, title_class=None, title_width=None):
        StaticLinkField.__init__(self, "", link, title_width, title, field, title_class)

        self.icon = icon
        self.link = link

    def get_widget(self, request, instance):
        text = self.get_text(request, instance)
        link = self.get_link(instance, self.field)

        obj = IconLink(text, link)
        obj.add(Icon(self.icon))
        return obj
