# coding=utf-8
from django.contrib.humanize.templatetags.humanize import ordinal, intcomma
from django.utils.html import linebreaks

from field import TextField
from django_helpers.apps.xeditable import TextAreaEditable
from django_helpers.humaize.file_size import formatted_file_size

__all__ = (
    'OrdinalIntegerField',
    'LineBreaksField',
    'ComaSeparatedIntegerField',
    'FileSizeField',
)


class OrdinalIntegerField(TextField):
    def text_transform_function(self, text):
        return ordinal(text)


class FileSizeField(TextField):
    def text_transform_function(self, text):
        return formatted_file_size(text)


class LineBreaksField(TextField):
    EditableField = TextAreaEditable

    def text_transform_function(self, text):
        return linebreaks(text)


class ComaSeparatedIntegerField(TextField):
    def text_transform_function(self, text):
        return intcomma(text)
