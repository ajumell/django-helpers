# coding=utf-8
from types import StringTypes

from django_helpers.apps.xeditable import TextEditable, BaseChoiceField
from django_helpers.apps.xeditable.fields import ModelSelectField
from django_helpers.js_plugins.core import JavascriptPlugins, TipsyPlugin
from django_helpers.utils.models import get_value

__author__ = 'ajumell'

_lookups = (
    "exact",
    "iexact",
    "contains",
    "icontains",
    "in",
    "gt",
    "gte",
    "lt",
    "lte",
    "startswith",
    "istartswith",
    "endswith",
    "iendswith",
    "range",
    "year",
    "month",
    "day",
    "week_day",
    "isnull",
    "search",
    "regex",
    "iregex"
)


def can_render_request(name, instance, request):
    fn = getattr(instance, name, None)
    return fn(request=request)


# noinspection PyUnusedLocal
def can_render_no_request(name, instance, request):
    fn = getattr(instance, name, None)
    return fn()


class TextField(object):
    EditableField = TextEditable
    SelectField = BaseChoiceField
    Widget = None
    text_transform_function = None
    can_be_editable = True
    filter_type = None
    is_string = True
    xeditable_template = 'data-tables/editable.js'
    
    def __init__(self, field, title, searchable=False, sortable=False, title_class=None,
                 title_width=None, annotate=None, search_lookup='contains', format_expression=None):
        
        self.controller = None
        self.field = field
        self.title = title
        self.searchable = searchable
        self.sortable = sortable
        self.title_class = title_class
        self.title_width = title_width
        self.format_expression = format_expression
        self.xeditable = None
        self.html_id = None
        
        self.filter = None
        self.extra_fields = []
        self.js_plugins = JavascriptPlugins()
        self.class_names = []
        
        if searchable and search_lookup not in _lookups:
            raise
        
        self.search_lookup = search_lookup
        
        if annotate is not None:
            from django_helpers.core.listable import validate_annotate
            
            if not validate_annotate(annotate):
                raise
            
            self.field = field + "__" + annotate
        
        self.annotate = annotate
    
    #
    # Render
    #
    def get_text(self, request, instance):
        text = get_value(instance, self.field, self.format_expression)
        if text is None:
            text = ""
        
        transform_function = self.text_transform_function
        if transform_function:
            text = transform_function(text)
        
        return text
    
    def get_widget(self, request, instance):
        xeditable = self.xeditable
        if self.can_be_editable and xeditable is not None:
            text = self.get_text(request, instance)
            
            xeditable.set_text(text)
            xeditable.render_prepare(True)
            
            return xeditable
        
        if self.Widget is None:
            return None
        
        text = self.get_text(request, instance)
        return self.Widget(text)
    
    # noinspection PyBroadException
    def check_can_render(self, instance, request):
        can_render_fn = getattr(self, 'can_render_fn', None)
        
        if can_render_fn is True:
            return True
        
        try:
            name, can_render_fn = can_render_fn
            return can_render_fn(name, instance, request)
        except:
            pass
        
        if can_render_fn is not None:
            # noinspection PyCallingNonCallable
            return can_render_fn(instance, request)
        
        can_render_fn = getattr(self, 'can_render', None)
        if can_render_fn and hasattr(can_render_fn, "__call__"):
            setattr(self, 'can_render_fn', can_render_fn)
            return can_render_fn(request, instance)
        
        elif type(can_render_fn) in StringTypes:
            from inspect import getargspec
            
            try:
                fn = getattr(instance, can_render_fn, None)
                fn_name = can_render_fn
                inspection = getargspec(fn)
                args = list(inspection.args)
                if 'request' in args:
                    can_render_fn = can_render_request
                else:
                    can_render_fn = can_render_no_request
                
                can_render = can_render_fn(fn_name, instance, request)
                
                setattr(self, "can_render_fn", (fn_name, can_render_fn))
            except:
                setattr(self, 'can_render_fn', True)
                can_render = True
        else:
            can_render = True
        
        return can_render
    
    def render(self, request, instance):
        can_render = self.check_can_render(instance, request)
        
        if not can_render:
            return ""
        
        widget = self.get_widget(request, instance)
        
        if hasattr(widget, 'attrs'):
            self.js_plugins.apply(widget.attrs, instance)
        elif self.js_plugins.has_plugin():
            raise Exception("A widget is required when Javascript Plugins are used.")
        
        if widget:
            return self.render_widget(widget)
        else:
            return self.get_text(request, instance)
    
    def render_widget(self, widget):
        for class_name in self.class_names:
            widget.add_class(class_name)
        
        return widget.render()
    
    #
    # Listable Getters
    #
    def get_fields(self):
        field = self.field
        result = []
        if type(field) in (tuple, list):
            result.extend(field)
        else:
            result.append(field)
        
        result.extend(self.extra_fields)
        
        return tuple(result)
    
    def get_field_title(self):
        title = self.title
        title = title.lower()
        title = title.replace(" ", "_")
        title = title.replace("-", "_")
        return title
    
    def set_searchable(self, is_searchable, lookup=None):
        self.searchable = is_searchable
        if lookup is not None:
            self.search_lookup = lookup
    
    def get_sort_field(self):
        fields = self.get_fields()
        
        if type(fields) in (list, tuple):
            return fields[0]
        
        return fields
    
    def get_search_arg(self):
        field = self.get_fields()[0]
        return field + "__" + self.search_lookup
    
    def need_related(self):
        if self.annotate is not None:
            return False
        
        fields = self.field
        if type(fields) in (list, tuple):
            for field in fields:
                if field.find("__") > 0:
                    return True
        elif fields.find("__") > 0:
            return True
        return False
    
    #
    # Editable Fields
    #
    def set_editable(self, xeditable, tag='span'):
        """
        @type xeditable django_helpers.apps.xeditable.fields.BaseEditable
        """
        cls = self.__class__
        if not cls.can_be_editable:
            raise Exception("%s cannot be made editable" % cls.__name__)
        
        field = self.field
        if type(field) in ('list', 'tuple'):
            raise Exception('Inline Editing is not supported for columns with multiple fields.')
        
        if self.annotate is not None:
            raise Exception('Inline Editing is not supported for columns with annotations.')
        
        self.xeditable = xeditable
        xeditable.tag = tag
        
        xeditable.popup_placement = 'top'
        xeditable.field_name = field
        xeditable.js_template = self.xeditable_template
    
    def make_editable(self, tag='span', select=False):
        if self.field.find("__") > 0:
            editable = ModelSelectField()
        else:
            if select is True:
                editable = self.SelectField()
                print 'called...'
            else:
                editable = self.EditableField()
        self.set_editable(editable, tag)
        return editable
    
    def render_filter(self):
        if not self.searchable:
            return ""
        
        field = self.get_search_arg()
        
        if self.filter is None:
            if self.filter_type is None:
                table = self.controller
                model_field = table.get_model_field(field)
                self.filter = model_field.formfield().widget
            else:
                self.filter = self.filter_type()
        
        return self.filter.render(field, None)
    
    #
    # Repr
    #
    def __unicode__(self):
        return self.field
    
    def __repr__(self):
        return self.field
    
    def __str__(self):
        return self.field
    
    #
    # Javascript Plugins
    #
    
    # noinspection PyAttributeOutsideInit
    def set_tipsy_tip(self, text, is_html=True, position=None):
        tip = getattr(self, '_tipsy', None)
        if tip is not None:
            tip.data('tipsy-title', text)
            tip.data('is-html', is_html)
            tip.data('position', position)
        else:
            tip = TipsyPlugin(text, is_html, position)
            self.js_plugins.add(tip)
            setattr(self, '_tipsy', tip)
    
    #
    # Static Files
    #
    def js_files(self):
        return self.js_plugins.js_files()
    
    def css_files(self):
        return self.js_plugins.css_files()
    
    def add_class_name(self, name):
        self.class_names.append(name)
