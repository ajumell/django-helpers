# coding=utf-8

from field import TextField

__author__ = 'ajumell'


class UserField(TextField):
    def __init__(self, field, title, searchable=False, sortable=False, title_class=None, title_width=None):
        _fields = ("username", "first_name", "last_name")
        fields = ["{}__{}".format(field, _field) for _field in _fields]
        TextField.__init__(self, fields, title, searchable, sortable, title_class, title_width)

    def get_text(self, request, instance):
        from django_helpers.utils.models import get_value

        values = get_value(instance, self.field, self.format_expression, True)
        username, first_name, last_name = values
        if username is None:
            return ""

        name = "{} {}".format(first_name, last_name)
        name = name.strip()
        if name != "":
            return name
        return username
