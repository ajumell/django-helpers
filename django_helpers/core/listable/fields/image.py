# coding=utf-8
from django_helpers.widgets import Image
from field import TextField
from django_helpers.utils.models import get_value

__all__ = (
    'ImageField',
    'ImageURLField',
    'BooleanImageField',
)


class ImageURLField(TextField):
    def __init__(self, field, title='', convert_url=True, title_class=None, extra_classes=None, title_width="25px", image_width="", url_attr=None):
        TextField.__init__(self, field, title, False, False, title_class, title_width)
        self.convert_url = convert_url
        self.extra_classes = extra_classes
        self.image_width = image_width
        self.url_attr = url_attr

    # noinspection PyBroadException
    def get_url(self, obj):
        if self.url_attr is None:
            try:
                return obj.thumbnail
            except:
                try:
                    return obj.url
                except:
                    return ""

        return getattr(obj, self.url_attr)

    def get_text(self, request, instance):
        obj = get_value(instance, self.field)
        url = self.get_url(obj)

        return url


class ImageField(ImageURLField):
    can_be_editable = False

    def get_widget(self, request, instance):
        url = ImageURLField.get_text(self, request, instance)
        if not url:
            return ""

        img = Image(url)
        img.add_class(self.extra_classes)
        img.attr('width', self.image_width)

        return img


class BooleanImageField(TextField):
    can_be_editable = False

    def __init__(self, field, yes_image, no_image, title='', title_class=None, extra_classes=None, title_width="25px"):
        TextField.__init__(self, field, title, False, False, title_class, title_width)
        self.extra_classes = extra_classes
        self.yes_image = yes_image
        self.no_image = no_image

    def get_widget(self, request, instance):
        obj = get_value(instance, self.field)
        url = self.yes_image if obj else self.no_image

        img = Image(url)
        img.add_class(self.extra_classes)

        return img
