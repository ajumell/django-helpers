# coding=utf-8
from django_helpers.utils.models import get_single_value
from field import TextField
from inspect import isclass


class Widget(TextField):
    can_be_editable = False

    def __init__(self, widget, mapping, title='', title_class=None, search_lookup='contains'):
        if type(mapping) is not dict:
            raise Exception("Mapping should be a dic.")

        field = mapping.values()
        TextField.__init__(self, field, title, title_class=title_class, search_lookup=search_lookup)

        self.widget = widget
        self.mapping = mapping

    def get_text(self, request, instance):
        return ""

    def get_params(self, instance):
        mapping = self.mapping
        result = {}

        for key, field in mapping.items():
            result[key] = get_single_value(instance, field)

        return result

    def get_widget(self, request, instance):
        widget = self.widget

        if isclass(widget):
            widget = widget(request=request)

        if hasattr(widget, 'set_params'):
            params = self.get_params(instance)
            widget.set_params(**params)

        return widget


class FunctionField(TextField):
    def __init__(self, field, function_name, title, disable_widget=False, searchable=False, sortable=False, title_class=None, search_lookup='contains'):
        TextField.__init__(self, field, title, searchable, sortable, title_class=title_class, search_lookup=search_lookup)
        self.disable_widget = disable_widget
        self.function_name = function_name

    def get_widget(self, request, instance):
        if self.disable_widget:
            return None

        TextField.get_widget(self, request, instance)

    def get_text(self, request, instance):
        fn = getattr(instance, self.function_name, None)
        if fn is None:
            return None

        return fn()


class ListingFunctionField(FunctionField):
    def __init__(self, function_name, title, field=None, disable_widget=False, searchable=False, sortable=False, title_class=None, search_lookup='contains'):
        TextField.__init__(self, field, title, searchable, sortable, title_class=title_class, search_lookup=search_lookup)
        self.disable_widget = disable_widget
        self.function_name = function_name

    def get_text(self, request, instance):
        controller = self.controller
        fn = getattr(controller, self.function_name, None)
        if fn is None:
            return None

        return fn()


class StaticDataField(TextField):
    def __init__(self, data, title, field=None, disable_widget=False, searchable=False, sortable=False, title_class=None, search_lookup='contains'):
        TextField.__init__(self, field, title, searchable, sortable, title_class=title_class, search_lookup=search_lookup)

        self.disable_widget = disable_widget
        self.data = data

    def get_widget(self, request, instance):
        if self.disable_widget:
            return None

        TextField.get_widget(self, request, instance)

    def get_text(self, request, instance):
        return self.data


class JSONField(TextField):
    Widget = None
    is_string = False

    def get_text(self, request, instance):
        from json import loads

        txt = TextField.get_text(self, request, instance)
        try:
            return loads(txt)
        except:
            return ""
