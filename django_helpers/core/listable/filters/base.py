# coding=utf-8
from django.forms import TextInput


def get_name(name):
    name = name.lower()
    name = name.replace(" ", "_")
    name = name.replace("-", "_")
    name = name.replace("'", "_")
    name = name.replace('"', "_")
    
    return name


class BaseFilter(object):
    label = None
    placeholder = None
    query_lookup = ""
    
    widget_cls = TextInput
    
    def __init__(self, label, query_lookup, widget=None, placeholder=None):
        self.label = label
        self.query_lookup = query_lookup
        self.placeholder = placeholder
        self.name = get_name(label)
        
        if widget is not None:
            self.widget = widget
        else:
            self.widget = self.widget_cls()
    
    def render(self, listing_id):
        attrs = {
            "data-" + listing_id + "-filter": "true"
        }
        
        if self.placeholder:
            attrs['placeholder'] = self.placeholder
        
        return self.widget.render(self.name, None, attrs)
    
    def css_files(self):
        from django_helpers.apps.static_manager import utils
        return utils.get_css_files(self.widget)
    
    def js_files(self):
        from django_helpers.apps.static_manager import utils
        return utils.get_js_files(self.widget)
    
    def to_python(self, value):
        return value
    
    def query(self, query, data_dict):
        value = data_dict.get(self.name)
        value = self.to_python(value)
        
        if not value:
            return query
        
        kwargs = {self.query_lookup: value}
        query = query.filter(**kwargs)
        
        return query
