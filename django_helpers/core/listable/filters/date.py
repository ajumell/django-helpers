# coding=utf-8
from django_helpers.apps.forms.widgets.bootstrap import DatePickerWidget
from .base import BaseFilter


class DateFilter(BaseFilter):
    widget_cls = DatePickerWidget
    format = '%Y-%m-%d'
    
    def to_python(self, value):
        if not value:
            return None
        
        from datetime import datetime
        try:
            return datetime.strptime(value, self.format)
        except ValueError:
            return
