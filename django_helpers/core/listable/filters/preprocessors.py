# coding=utf-8

from django import forms
from django_helpers.apps.forms.widgets import bootstrap
from django_helpers.apps.forms.widgets import jui

FORM_CONTROL_WIDGETS = (
    
    jui.DateInput,
    jui.SpinnerInput,
    jui.MaskedInput,
    
    bootstrap.DatePickerWidget,
    bootstrap.DateRangeInputWidget,
    bootstrap.TimePickerWidget,
    bootstrap.ClockFaceWidget,
    bootstrap.ColorPickerWidget,
    bootstrap.SelectWidget,
    bootstrap.MultiSelectWidget,
    
    forms.TextInput,
    forms.FileInput,
    forms.Textarea,
    forms.SelectMultiple,
    forms.Select,
    forms.PasswordInput
)

FORM_CONTROL_WIDGETS_EXCEPTIONS = [
    forms.RadioSelect,
    forms.CheckboxInput
]


def add_form_control_class(filter):
    widget = filter.widget
    
    if not isinstance(widget, FORM_CONTROL_WIDGETS):
        return
    
    if isinstance(widget, tuple(FORM_CONTROL_WIDGETS_EXCEPTIONS)):
        return
    
    if getattr(widget, 'no_form_control', False):
        return
    
    class_name = widget.attrs.get('class', '')
    new_class_name = ' '.join((class_name, 'form-control'))
    
    widget.attrs['class'] = new_class_name
